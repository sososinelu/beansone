package uk.co.beanssystems.setup;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.RatesConnector;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.help.Help;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class Setup extends SherlockFragmentActivity   {
	EditText nameEditText, numberEditText, companyEditText, emailEditText, calloutEditText, standardEditText; 
	Spinner calloutHrsSpinner;
	Cursor settCur;
	SettingsConnector settCon;
	RatesConnector ratesCon;
	public static SetupViewPagerAdapter vAdapter;
	ViewPager myPager;
	Fragment setup1, setup2, setup3, setup4, setup5;
	double globalVAT = 20.0;
	String name; 
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);    		
		setup1 = new Setup1();
		setup2 = new Setup2();
		setup3 = new Setup3();
		setup4 = new Setup4();
		setup5 = new Setup5();
		vAdapter = new SetupViewPagerAdapter(getSupportFragmentManager());
	    myPager = (ViewPager) findViewById(R.id.viewPagerSetup);
	    myPager.setAdapter(vAdapter);	    
	    myPager.setCurrentItem(0); 	  
	    settCon= new SettingsConnector(this);
	    ratesCon = new RatesConnector(this);    
	    vAdapter.addFragment(setup1);
	    vAdapter.addFragment(setup2);
	    vAdapter.addFragment(setup3);
	    myPager.setOffscreenPageLimit(5);		
	}	
	
	public boolean saveMyDetails () {	
		nameEditText = (EditText) findViewById(R.id.nameSetupEditText);
	    numberEditText = (EditText) findViewById(R.id.numberSetupEditText);
		companyEditText = (EditText) findViewById(R.id.companySetupEditText);
		emailEditText = (EditText) findViewById(R.id.emailSetupEditText);
		calloutEditText = (EditText) findViewById(R.id.calloutChargeSetup4EditText);
        standardEditText = (EditText) findViewById(R.id.standardChargeSetup4EditText);
        calloutHrsSpinner = (Spinner) findViewById(R.id.calloutTimeSetup4Spinner);
		
		if (nameEditText.getText().length()==0 || numberEditText.getText().length()==0 || companyEditText.getText().length()==0
		  		|| emailEditText.getText().length()==0 || standardEditText.getText().length()==0){
			validation(nameEditText);
			validation(numberEditText);
			validation(companyEditText);
			validation(emailEditText);
			validation(standardEditText);
			Toast.makeText(getApplicationContext(), "Setup fields not completed!", Toast.LENGTH_SHORT).show();
			return false;
		}else{
			System.out.println("NAME >>>> "+nameEditText.getText() .toString());
			System.out.println("NUMBER >>>> "+numberEditText.getText().toString());
			System.out.println("COMPANY >>>> "+companyEditText.getText().toString());
			System.out.println("EMAIL >>>> "+emailEditText.getText().toString());
			System.out.println("CALLOUT >>>> "+calloutEditText.getText().toString());
			System.out.println("STANDARD >>>> "+standardEditText.getText().toString());
			try{
				settCon.insertUserDetails(nameEditText.getText().toString(), numberEditText.getText().toString(), companyEditText.getText().toString(), null, null, emailEditText.getText().toString());
				insertRate ("Standard", standardEditText, null);
				if(calloutEditText.getText().length()!=0){
					insertRate ("Callout", calloutEditText, calloutHrsSpinner);
				}
				return true;
			} catch (Exception e) { 
				e.printStackTrace();
	        }
			
			return false;
		}
	}
	
	public void insertRate (String rateName, EditText rateValue, Spinner timeSpin) {
		double rateVal = Double.valueOf(stripDecimal(rateValue.getText().toString()));
		double rateVAT = 0.0;
		try{
			ratesCon.insertRate(rateName, rateVal, rateVAT, "", timeSpin.getSelectedItem().toString());
			System.out.println(" <<< RATE ADDED >>> ");
		} catch (Exception e) { 
			//e.printStackTrace();
			try{
				ratesCon.insertRate(rateName, rateVal, rateVAT, "", null);
				System.out.println(" <<< RATE ADDED - NO TIME >>> ");
			} catch (Exception ex) { 
				//ex.printStackTrace();
	        }
        }
		
	}
	
	public void validation (final EditText text){
		if(text.getText().length()==0){
			text.setError("Please Complete Field");
		} else {
			text.setError(null,null);
		}
		
		text.addTextChangedListener(new TextWatcher() {	 
		    public void afterTextChanged(Editable s) {
		    	if(text.getText().length()==0){
		    		text.setError("Please Complete Field");
		    	}else{
		    		text.setError(null,null);
		    	}
		    }
		 
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }
		 
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    }
		});
	}	
	
	public void onClick(View v){
		switch(v.getId()){
		case R.id.doneSetupButton:
			if (saveMyDetails()){
				Intent inDone = new Intent(this, MainActivity.class);
				inDone.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(inDone);
				finish();
			}
			break;
		case R.id.helpSetupButton:   		   
			if (saveMyDetails()){
				Intent inHelp = new Intent(this, Help.class);
				inHelp.putExtra("source", "Setup");
				startActivity(inHelp);
				finish();
			}
			break;
		case R.id.includeHourLabel:
			InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        	im.hideSoftInputFromWindow(((Spinner) findViewById(R.id.calloutTimeSetup4Spinner)).getWindowToken(), 0);
			((Spinner) findViewById(R.id.calloutTimeSetup4Spinner)).performClick();
			break;
		case R.id.includeHourLabel2:
			InputMethodManager i = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        	i.hideSoftInputFromWindow(((Spinner) findViewById(R.id.calloutTimeSetup4Spinner)).getWindowToken(), 0);
			((Spinner) findViewById(R.id.calloutTimeSetup4Spinner)).performClick();
			break;
		default:break;
		}
	}
	
	public String stripDecimal(String str){
		String[] amount;
		try{
			amount = str.split("\\.");
			amount[1] = amount[1].substring(0, 2);
		} catch (Exception e){
			System.out.println("Entered Integer");
			return str;
		}
		return amount[0]+"."+amount[1];
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	        finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
}
