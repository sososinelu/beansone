package uk.co.beanssystems.setup;

import java.util.ArrayList;
import uk.co.beanssystems.beansone.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.actionbarsherlock.app.SherlockFragment;

public class Setup4 extends SherlockFragment {
	private  EditText calloutEditText, standardEditText;
	private TextView slideTextView;
	Spinner calloutHrsSpinner;
	private boolean standardBool = false;
	Fragment setup5;
	ArrayList<String> calloutTimeSpinnerArray = new ArrayList<String>();
	Context context;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {   	
        View view = inflater.inflate(R.layout.activity_setup_4, container, false);	       
        context = view.getContext();
        calloutEditText = (EditText)view.findViewById(R.id.calloutChargeSetup4EditText);
        standardEditText = (EditText)view.findViewById(R.id.standardChargeSetup4EditText);
        calloutHrsSpinner = (Spinner)view.findViewById(R.id.calloutTimeSetup4Spinner);
        slideTextView = (TextView)view.findViewById(R.id.slideSetup4);
        setup5 = new Setup5();
        setCalloutTimeSpinner();
        
        standardEditText.addTextChangedListener(new TextWatcher() {	 
		    public void afterTextChanged(Editable s) {
		    	if(standardEditText.getText().length()==0 || standardEditText.getText().equals("0")){
		    		standardEditText.setError("Please Enter Valid Rate");
		    		standardBool = false;
		    		slideTextView.setVisibility(View.GONE);
		    	}else{
		    		standardEditText.setError(null,null);
		    		standardBool = true;	 
		    		if (standardBool == true && Setup.vAdapter.getFragments().size() == 4) {
		    			Setup.vAdapter.addFragment(setup5);
		    			standardEditText.requestFocus();
		    			slideTextView.setVisibility(View.VISIBLE);
		    		}
		    	}
		    }
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }            		 
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    }
		});
        
        calloutEditText.addTextChangedListener(new TextWatcher() {	 
		    public void afterTextChanged(Editable s) {
		    	if(calloutEditText.getText().equals("0")){
		    		calloutEditText.setError("Please Enter Valid Rate");
		    		slideTextView.setVisibility(View.GONE);
		    	}else{
		    		standardEditText.setError(null,null);
		    	}
		    }
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }            		 
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    }
		});  
        return view;
    }
    
    public void setCalloutTimeSpinner () {
    	calloutTimeSpinnerArray.clear();
		for(int i=0; i <= 20; i++){
			calloutTimeSpinnerArray.add(String.valueOf(i));
			//calloutTimeSpinnerArray.add(String.valueOf(Double.valueOf(i)+0.5));
		}
		ArrayAdapter<String> spinnerArraAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, calloutTimeSpinnerArray);
		spinnerArraAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		calloutHrsSpinner.setAdapter(spinnerArraAdapter);	
		calloutHrsSpinner.setSelection(1);
	}

}
