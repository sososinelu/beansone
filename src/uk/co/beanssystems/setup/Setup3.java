package uk.co.beanssystems.setup;

import uk.co.beanssystems.beansone.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class Setup3 extends SherlockFragment {
	private EditText nameEditText, numberEditText, companyEditText, emailEditText;
	private TextView slideTextView;
	private boolean nameBool = false, phoneBool = false, companyBool = false, emailBool = false;
	Fragment setup4;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {	    	
		View view = inflater.inflate(R.layout.activity_setup_3, container, false);
	    nameEditText = (EditText)view.findViewById(R.id.nameSetupEditText);
	    numberEditText = (EditText)view.findViewById(R.id.numberSetupEditText);
	    companyEditText = (EditText)view.findViewById(R.id.companySetupEditText);
        emailEditText = (EditText)view.findViewById(R.id.emailSetupEditText);
	    slideTextView = (TextView)view.findViewById(R.id.slideSetup3);
	    setup4 = new Setup4();
	    
		nameEditText.addTextChangedListener(new TextWatcher() {	 
		    public void afterTextChanged(Editable s) {
		    	if(nameEditText.getText().length()==0){
		    		nameEditText.setError("Please Complete Field");
		    		nameBool = false;
		    		slideTextView.setVisibility(View.GONE);
		    	}else{
		    		nameEditText.setError(null,null);
		    		nameBool = true;	 
		    		enableFrag4();
		    	}
		    }
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }            		 
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    }
		});

		numberEditText.addTextChangedListener(new TextWatcher() {	 
		    public void afterTextChanged(Editable s) {
		    	if(numberEditText.getText().length()==0){
		    		numberEditText.setError("Please Complete Field");
		    		phoneBool = false;
		    		slideTextView.setVisibility(View.GONE);
		    	}else{
		    		numberEditText.setError(null,null);
		    		phoneBool = true;
		    		enableFrag4();
		    	}
		    }	            		 
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }	            		 
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    }
		});
		
		companyEditText.addTextChangedListener(new TextWatcher() {	 
		    public void afterTextChanged(Editable s) {
		    	if(companyEditText.getText().length()==0){
		    		companyEditText.setError("Please Complete Field");
		    		companyBool = false;
		    		slideTextView.setVisibility(View.GONE);
		    	}else{
		    		companyEditText.setError(null,null);
		    		companyBool = true;	 
		    		enableFrag4();
		    	}
		    }
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }            		 
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    }
		}); 
		
		emailEditText.addTextChangedListener(new TextWatcher() {	 
		    public void afterTextChanged(Editable s) {
		    	if(emailEditText.getText().length()==0){
		    		emailEditText.setError("Please Complete Field");
		    		emailBool = false;
		    		slideTextView.setVisibility(View.GONE);
		    	}else{
		    		String e = emailEditText.getText().toString();
		    		if (!e.contains("@") || !e.contains(".")){
		    			emailEditText.setError("Invalid Email Address");
		    			emailBool = false;
		    		}else{
		    			emailEditText.setError(null,null);
		    			emailBool = true;
		    			enableFrag4();
		    		}
		    	}
		    }
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }            		 
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    }
		});
		
	    return view;
	}
	
	public void enableFrag4 () {
		if (nameBool == true && phoneBool == true && companyBool == true && emailBool == true 
				&& Setup.vAdapter.getFragments().size() == 3) {
			Setup.vAdapter.addFragment(setup4);
			emailEditText.requestFocus();
			slideTextView.setVisibility(View.VISIBLE);
		}
	}
}

