package uk.co.beanssystems.setup;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

public class SetupViewPagerAdapter extends FragmentStatePagerAdapter {
    
    private List<Fragment> fragments;

    public SetupViewPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fragments = new ArrayList<Fragment>();
    }

    @Override
    public Fragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public synchronized List<Fragment> getFragments() {
        return fragments;
    }


    public synchronized void addFragment(Fragment fragment) {
        fragments.add(fragment);
        notifyDataSetChanged();
    }
    
	@Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }

}
