package uk.co.beanssystems.expenses;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.popups.ExpenseTypePopup;
import uk.co.beanssystems.popups.FullscreenImage;
import uk.co.beanssystems.settings.Settings;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class NewExpense extends SherlockActivity {
 	Spinner expenseTypeSpinner;
	EditText dateEditText, costEditText, VATEditText, notesEditText, categoryEditText;
	TextView  titleTextView, takeTextView, viewTextView, removeTextView;
	String userName="NO", bussName="NO", source="", imageInSD ;
	ImageView thumbImageView;
	View takeView;
	RelativeLayout bottomExpenseLayout;
	private Uri tempUri = null, storedUri = null, deleteUri = null;
	private int expenseCat, expenseID, cancelTab=0, picExist=0;
	double globalVAT = 20.0;
	DecimalFormat df = new DecimalFormat("0.##");
	Cursor settCur; 
	SettingsConnector settCon;
	Bitmap bitmap;
	Context context;
	SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		LayoutInflater inflater = getLayoutInflater();
		View main = inflater.inflate(R.layout.activity_new_expense, null);
		setContentView(main);
		context = this;
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.coinstack);
	    bar.setDisplayHomeAsUpEnabled(true);
	    
	    settCon = new SettingsConnector(this);
	    titleTextView = (TextView) findViewById(R.id.expenseTitleLabelNewExpenseTextView);
	    categoryEditText = (EditText) findViewById(R.id.expenseCategoryNewExpenseEditText);
		dateEditText = (EditText)findViewById(R.id.expenseDateNewExpenseEditText);
		costEditText = (EditText)findViewById(R.id.expenseCostNewExpenseEditText);
		VATEditText = (EditText)findViewById(R.id.expenseVatNewExpenseEditText);
		notesEditText = (EditText)findViewById(R.id.expenseNotesNewExpenseEditText);
		thumbImageView = (ImageView)findViewById(R.id.cameraIconNewExpenseImageView);
		takeTextView = (TextView) findViewById(R.id.takeLabelNewExpenseTextView);
		viewTextView = (TextView) findViewById(R.id.viewLabelNewExpenseTextView);
		removeTextView = (TextView) findViewById(R.id.removeLabelNewExpenseTextView);
		bottomExpenseLayout = (RelativeLayout)findViewById(R.id.bottomExpenseLayout);
		takeView = (View)findViewById(R.id.takeView01);
		
		Intent i = getIntent();
		try{
			source = getCallingActivity().getShortClassName();
			System.out.println("0> "+source);
			if(source.equals("uk.co.beanssystems.expenses.ViewExpense")){
				source = getCallingActivity().getShortClassName().replace("uk.co.beanssystems.expenses.", "");
				System.out.println("1> "+source);
			}else{
				source = getCallingActivity().getShortClassName().replace("uk.co.beanssystems.beansone.", "");
				System.out.println("2> "+source);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		
		if (source.compareTo("ViewExpense")==0){
			expenseID = i.getIntExtra("expenseID", 0);
			expenseCat = i.getIntExtra("expenseCat", 0);
            //set the default according to value
			categoryEditText.setText(i.getStringExtra("expenseType"));
			dateEditText.setText(i.getStringExtra("date"));
			costEditText.setText(i.getStringExtra("total"));
			VATEditText.setText(i.getStringExtra("vat"));
			notesEditText.setText(i.getStringExtra("description"));
			String path = i.getStringExtra("imagePath");
			System.out.println("Path: "+ path);
			if (path.length()>0){
				storedUri = Uri.parse(i.getStringExtra("imagePath"));
				picExist=1;
				loadThumb();
			} else {
				storedUri = null;
				picExist=0;
			}
		}
		
		// Set todays date in Start Date edit text
		Calendar today = Calendar.getInstance();
		dateEditText.setText(outDate.format(today.getTime()));
		
		if (i.getExtras()!=null){
			expenseCat = i.getIntExtra("expenseCat", 0);
			cancelTab = i.getIntExtra("cancelTab", 0);
		}
		
		costEditText.addTextChangedListener(new TextWatcher() {	 
		    public void afterTextChanged(Editable s) {
		    	if(costEditText.getText().length()==0){
		    		VATEditText.setText("");
		    	}else{		    		
		    		try{
		    			String value = costEditText.getText().toString();
		    			if(value.contains(".")){ 
		    				String[] parts = value.split("\\.");
			    			if(parts[0].equals("")){
			    				value = "0"+value;
				    		}
			    		}
			    		DecimalFormat formatter = new DecimalFormat("0.00");
			    		VATEditText.setText(String.valueOf(formatter.format( (globalVAT / (globalVAT + 100.0)) * Double.valueOf(value))));
		    		}catch (Exception e) { 
		            	e.printStackTrace();
		            }	
		    		
		    	}
		    }
		 
		    public void beforeTextChanged(CharSequence s, int start, 
		      int count, int after) {
		    }
		 
		    public void onTextChanged(CharSequence s, int start, 
		      int before, int count) {
		    }
		});
		// Get the user details from the database
		getUserDetails();
		
		switch (expenseCat){
			case 1:
				//Client expense
				titleTextView.setVisibility(View.GONE);
				VATEditText.setVisibility(View.GONE);
				bar.setTitle("New Client Expense");
				break;
			case 2:
				//Business Expense
				titleTextView.setVisibility(View.VISIBLE);
				VATEditText.setVisibility(View.VISIBLE);
				bar.setTitle("New Business Expense");
				System.out.println(bussName + " <<<<<<<<<< Buss");
				titleTextView.setText(bussName);
				break;
			case 3:
				//Personal Expense
				TextView middle = (TextView) findViewById(R.id.middle);
				middle.setVisibility(View.GONE);
				titleTextView.setVisibility(View.VISIBLE);
				VATEditText.setVisibility(View.GONE);
				bar.setTitle("New Personal Expense");
				System.out.println(userName  + " <<<<<<<<<< User");
				titleTextView.setText(userName);
				break;
			default:
				System.out.println("Code Number Error");
				break;
			}
	}
	
	public void getUserDetails () {
		try{	
			settCon.open();
			settCur = settCon.getUserDetails();		
			settCur.moveToFirst();
			int userNameCol = settCur.getColumnIndex(SettingsConnector.USERNAME);
			int companyCol = settCur.getColumnIndex(SettingsConnector.USERCOMPANY);
			bussName = settCur.getString(companyCol);
			userName = settCur.getString(userNameCol);
			settCon.close();
		} catch (Exception e) { 
        	e.printStackTrace();
        }		
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		if (bitmap!=null){
			bitmap.recycle();
		}
	}
	
	public void loadThumb(){
		imageInSD = storedUri.getPath();
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = 4;
		bitmap = BitmapFactory.decodeFile(imageInSD, opts);
		thumbImageView.setImageBitmap(bitmap);
		bottomExpenseLayout.getLayoutParams().height = 170;
		viewTextView.setVisibility(View.VISIBLE);
		removeTextView.setVisibility(View.VISIBLE);
		takeTextView.setVisibility(View.VISIBLE);
		takeView.setVisibility(View.VISIBLE);
	}
	
	// Get data from DatePickerDialog & display it
	final OnDateSetListener odsl = new OnDateSetListener() {
		public void onDateSet(DatePicker arg0, int year, int month, int dayOfMonth) {
			month++;
			String dayStr = String.valueOf(dayOfMonth);
			String monthStr = String.valueOf(month);
			if (dayStr.length()==1){
				dayStr = "0"+ dayStr;
			}
			if (monthStr.length()==1){
				monthStr = "0"+ monthStr;
			}
			dateEditText.setText(dayStr + "/" + monthStr + "/" + year);
		}
	};
		
	public void onClick(View v){
		switch (v.getId()){
		case R.id.expenseDateNewExpenseEditText:
			Calendar cal = Calendar.getInstance();
			DatePickerDialog datePickDiag = new DatePickerDialog(this, odsl, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			datePickDiag.show();
			break;
		case R.id.cameraIconNewExpenseImageView:
			if (removeTextView.getVisibility()==View.GONE){
				if (!Environment.MEDIA_REMOVED.equals(Environment.getExternalStorageState())){
					Intent inCam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					tempUri = this.getOutputMediaFileUri();
					inCam.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
				    startActivityForResult(inCam, 5);
				} else {
					Toast.makeText(this, "No storage available for picture", Toast.LENGTH_SHORT).show();	
				} 
			} else {
				Intent inView = new Intent(NewExpense.this, FullscreenImage.class);
				inView.putExtra("from", 2);
				inView.putExtra("path", imageInSD);
	        	startActivity(inView);
			}
		    break;
		case R.id.removeLabelNewExpenseTextView:
			if (deleteUri==null && "ViewExpense".equals(source) && picExist==1){
				deleteUri = storedUri;
				storedUri=null;
			} else {
				try{
					File file = new File(storedUri.getPath());
					file.delete();
				} catch (Exception e){
					System.out.println("Could not delete file: "+storedUri.getPath());
				}
			}
			thumbImageView.setImageResource(R.drawable.camera);
			bottomExpenseLayout.getLayoutParams().height = 120;
			viewTextView.setVisibility(View.GONE);
			removeTextView.setVisibility(View.GONE);
			takeTextView.setVisibility(View.GONE);
			takeView.setVisibility(View.GONE);
		    break;
		case R.id.viewLabelNewExpenseTextView:
			Intent inView = new Intent(NewExpense.this, FullscreenImage.class);
			inView.putExtra("from", 2);
			inView.putExtra("path", imageInSD);
        	startActivity(inView);
		    break;
		case R.id.takeLabelNewExpenseTextView:
			if (!Environment.MEDIA_REMOVED.equals(Environment.getExternalStorageState())){
				Intent inTake = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				tempUri = this.getOutputMediaFileUri();
				inTake.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
			    startActivityForResult(inTake, 5);
			} else {
				Toast.makeText(this, "No storage available for picture", Toast.LENGTH_SHORT).show();	
			}
		    break;
		case R.id.expenseCategoryNewExpenseEditText:
			Intent inCatPop = new Intent(this, ExpenseTypePopup.class);
			startActivityForResult(inCatPop, 2);
		default:
			break;
		}
	}
	private Uri getOutputMediaFileUri(){
	      return Uri.fromFile(getOutputMediaFile());
	}
	
	private static File getOutputMediaFile(){
	    File mediaStorageDir = new File(
	    		Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "BeansOne" + File.separator + "Expenses");
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            System.out.println("Beans, failed to create directory");
	            return null;
	        }
	    }
	    
	    String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
	    File mediaFile;
	    mediaFile = new File(mediaStorageDir.getPath() + File.separator +"EXP_"+ timeStamp + ".jpg");
	    return mediaFile;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode){
		case 2:
			if (resultCode==RESULT_OK){
				categoryEditText.setText(data.getStringExtra("expenseType"));
			}
			break;
		case 5:
			//Camera
			if (resultCode==RESULT_OK){
				if (deleteUri==null && "ViewExpense".equals(source) && picExist==1){
					deleteUri = storedUri;
					storedUri = tempUri;
					loadThumb();
				} else {
					if (storedUri!=null){
						try{
							File file = new File(storedUri.getPath());
							file.delete();
						} catch (Exception e){
							System.out.println("Could not delete file: "+storedUri.getPath());
						}
						sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
						storedUri = tempUri;
						loadThumb();
					} else {
						storedUri = tempUri;
						loadThumb();
					}
				}
			} else if (resultCode == RESULT_CANCELED) {
				tempUri = null;
	        }
			break;
		default:break;
		}
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	   MenuInflater inflater = getSupportMenuInflater();
	   inflater.inflate(R.menu.menu_options, menu);
	   return true;
    }  
	 
	 public void addExpense(){
		 ExpensesConnector expCon = new ExpensesConnector(this);
		 String value = costEditText.getText().toString();
		 if(value.contains(".")){ 
			 String[] parts = value.split("\\.");
			 if(parts[0].equals("")){
				 value = "0"+value;
			 }
 		 }
		 
		 String costDouble = df.format(Double.valueOf((value)));
		 String VATDouble = df.format(Double.valueOf((VATEditText.getText().toString())));
		 String imagePath = "";
		 if (storedUri!=null){
			 imagePath = storedUri.getPath();
		 }
		 Date date = new Date();
		 try {
			date = outDate.parse(dateEditText.getText().toString());
		} catch (ParseException e1) {
			e1.printStackTrace();
		}	
		 
		 try{
			 if (source.compareTo("ViewExpense")==0){
				 expCon.updateExpense(expenseID, 0, expenseCat, categoryEditText.getText().toString(), inDate.format(date),
						 Double.valueOf(costDouble), Double.valueOf(VATDouble), 0.0, notesEditText.getText().toString(), imagePath);
				 if (deleteUri!=null){
					 File file = new File(deleteUri.getPath());
					 file.delete();
				 }
				 
			 } else {
			 expCon.insertExpense(0, expenseCat, categoryEditText.getText().toString(), inDate.format(date),
				 Double.valueOf(costDouble), Double.valueOf(VATDouble), 0.0, notesEditText.getText().toString(), imagePath);
			 }
		 } catch (Exception e){
			e.printStackTrace();
		 }
		 sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
	 }
	 
	  public void validation (final EditText text){
			if(text.getText().length()==0){
				text.setError("Please Complete Field");
			} else {
				text.setError(null,null);
			}
			text.addTextChangedListener(new TextWatcher() {	 
			    public void afterTextChanged(Editable s) {
			    	if(text.getText().length()==0){
			    		text.setError("Please Complete Field");
			    	}else{
			    		text.setError(null,null);
			    	}
			    }
			 
			    public void beforeTextChanged(CharSequence s, int start, 
			    	int count, int after) {
			    }
			 
			    public void onTextChanged(CharSequence s, int start, 
			    	int before, int count) {
			    }
			});
		}		
	 
	 @Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	      switch (item.getItemId()) {
	      case R.id.add_option:
	    	  item.setEnabled(false);
	    	  switch(expenseCat){
		    	  case 2: 
		    		  if (dateEditText.getText().length()==0 || costEditText.getText().length()==0 || VATEditText.getText().length()==0
		    				  || notesEditText.getText().length()==0){
		    			  item.setEnabled(true);
		    			  validation(dateEditText);
		    			  validation(costEditText);
		    			  validation(VATEditText);
		    			  validation(notesEditText);
		    			  return false;
		    		  } else {
		    			  addExpense();
		    			  if (source.compareTo("ViewExpense")==0){
		    				  Intent i = new Intent(this, ViewExpense.class);
		    				  setResult(RESULT_OK, i);
		    				  finish();
		    			  } else {
			    	    	  Intent i = new Intent(this, ExpenseList.class);
		    				  i.putExtra("tabIndex", 0);
		    				  startActivity(i);
			    	    	  finish();
		    			  }
		    	          return true;
		    		  }
		    	  case 3: 
		    		  if (dateEditText.getText().length()==0 || costEditText.getText().length()==0 || notesEditText.getText().length()==0){
		    			  item.setEnabled(true);
		    			  validation(dateEditText);
		    			  validation(costEditText);
		    			  validation(notesEditText);
		    			  return false;
		    		  } else {
		    			  addExpense();
		    			  if (source.compareTo("ViewExpense")==0){
		    				  Intent i = new Intent(this, ViewExpense.class);
		    				  setResult(RESULT_OK, i);
		    				  finish();
		    			  } else {
			    	    	  Intent i = new Intent(this, ExpenseList.class);
		    				  i.putExtra("tabIndex", 1);
		    				  startActivity(i);
			    	    	  finish();
		    			  }
		    	          return true;
		    		  }
	    	  }

	      case android.R.id.home:
	 		 final Builder builder = new AlertDialog.Builder(this);			
			 TextView title = new TextView(this);
			 title.setText("Expense details not saved!");
			 title.setPadding(10, 10, 10, 10);
			 title.setGravity(Gravity.CENTER);
			 title.setTextSize(20);
			 builder.setCustomTitle(title);
			 TextView msg = new TextView(this);
			 msg.setText("Are you sure you want to go back?");
			 msg.setPadding(10, 10, 10, 10);
			 msg.setGravity(Gravity.CENTER);
			 msg.setTextSize(18);
			 builder.setView(msg);
			 builder.setCancelable(true);
			 builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				 public void onClick(DialogInterface dialog, int which) {
			    	  	if (source.compareTo("ViewExpense")==0){
			    	  		if (picExist==1 && deleteUri!=null){
			    	  			try {
				    	  			File file = new File(storedUri.getPath());
					  	    		file.delete();
			    	  			} catch (Exception e) {
			    	  				System.out.println("No Picture to Delete");
			    	  			}
			    	  		} else if (picExist==0 && storedUri!=null) {
			    	  			File file = new File(storedUri.getPath());
				  	    		file.delete();
			    	  		}
			    			sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
			    	  		Intent intent = new Intent(context, ViewExpense.class);            
			    	  		intent.putExtra("expenseID", expenseID);
					        startActivity(intent);
					        finish();
			    	  	} else if (source.equals(".MainActivity")){
			    	  		finish();
			    	  	} else {
			    			sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
			    	  		Intent intent = new Intent(context, ExpenseList.class);            
					        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							intent.putExtra("tabIndex", cancelTab);
					        startActivity(intent);
					        finish();
			    	  	}
				 }
			 });
			 builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				 public void onClick(DialogInterface dialog, int which) {
				 }
			 });
			 AlertDialog dialog = builder.create();
			 if (costEditText.getText().length()!=0 || VATEditText.getText().length()!=0 || categoryEditText.getText().length()!=0 
					 || notesEditText.getText().length()!=0 || picExist==1){
				 dialog.show();
			 }else{
				 finish();
			 }
	    	 return true;
	      case R.id.settings_option:
		    	Intent in = new Intent(this, Settings.class);
		    	startActivity(in);
		        return true;  
		        
		    case R.id.about_option:
		    	return true;
		    	
		    case R.id.help_option:	
		    	Intent ih = new Intent(this, Help.class);
		    	startActivity(ih);
		    	return true;
		    	
		    case R.id.home_option:
		    	Intent iHome = new Intent(this, MainActivity.class);
		    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    	startActivity(iHome);
		    	finish();
		    	return true;
	      default:
	    	  System.out.println(expenseCat+ " Error");
	          return super.onOptionsItemSelected(item);
	      }
	  }
	 
	 public void alertDialogDataModified () {

	 }
}
