package uk.co.beanssystems.expenses;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.popups.FullscreenImage;
import uk.co.beanssystems.settings.Settings;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewExpense extends SherlockActivity {
	private TextView title1, title2, title3, expenseType, description, date, vat, vatLabel, total;
	private ImageView thumb;
	private Cursor expCur, jobCur, cliCur;
	private int expID, clientID, jobID, category;
	private String photoName;
	private Bitmap bitmap;
	NumberFormat nf;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		setContentView(R.layout.activity_view_expense);
		nf = NumberFormat.getCurrencyInstance(Locale.UK);		
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.coinstack);
	    bar.setDisplayHomeAsUpEnabled(true);    
		Intent i = getIntent();
		expID = i.getIntExtra("expenseID", 0);
		title1 = (TextView)findViewById(R.id.title1ViewExpenseTextView);
		title2 = (TextView)findViewById(R.id.title2ViewExpenseTextView);
		title3 = (TextView)findViewById(R.id.title3ViewExpenseTextView);
		expenseType = (TextView)findViewById(R.id.expenseTypeViewExpenseTextView);
		description = (TextView)findViewById(R.id.descriptionViewExpenseTextView);
		date = (TextView)findViewById(R.id.dateViewExpenseTextView);
		vat = (TextView)findViewById(R.id.vatValueViewExpenseTextView);
		total = (TextView)findViewById(R.id.totalCostValueViewExpenseTextView);
		thumb = (ImageView)findViewById(R.id.cameraIconViewExpenseImageView);	
	}
	
	@Override
	public void onStart(){
		super.onStart();
		ExpensesConnector expCon = new ExpensesConnector(this);
		expCon.open();
		expCur = expCon.getExpense(expID);
		expCur.moveToFirst();
		//column indexes
		int jobIDCol = expCur.getColumnIndex(ExpensesConnector.JOBID);
		int clientIDCol = expCur.getColumnIndex(ExpensesConnector.CLIENTID);
		int expCategoryCol = expCur.getColumnIndex(ExpensesConnector.EXPCATEGORY);
		int expTypeCol = expCur.getColumnIndex(ExpensesConnector.EXPTYPE);
		int descriptionCol = expCur.getColumnIndex(ExpensesConnector.DESCRIPTION);
		int dateCol = expCur.getColumnIndex(ExpensesConnector.DATE);
		int costCol = expCur.getColumnIndex(ExpensesConnector.COST);
		int vatCol = expCur.getColumnIndex(ExpensesConnector.VAT);
		int photoCol = expCur.getColumnIndex(ExpensesConnector.PHOTO);
		photoName = expCur.getString(photoCol);	
		final ActionBar bar = getSupportActionBar();
		//column values
		clientID = expCur.getInt(clientIDCol);
		jobID = expCur.getInt(jobIDCol);
		category = expCur.getInt(expCategoryCol);
		switch (category){
		case 1: // Job Expense		
			bar.setTitle("View Job Expense");
			//get Job Name
			JobsConnector jobCon = new JobsConnector(this);
			jobCon.open();
			jobCur = jobCon.getJob(expCur.getInt(jobIDCol));
			int jobNameCol = jobCur.getColumnIndex(JobsConnector.JOBNAME);
			jobCur.moveToFirst();
			title3.setText(jobCur.getString(jobNameCol));
			//Get Client name and company
			ClientsConnector cliCon = new ClientsConnector(this);
			cliCon.open();
			cliCur = cliCon.getClient(clientID);
			int clientNameCol = jobCur.getColumnIndex(JobsConnector.CLIENTNAME);
			int companyNameCol = cliCur.getColumnIndex(ClientsConnector.COMPANY);
			cliCur.moveToFirst();
			title1.setText(jobCur.getString(clientNameCol));
			title2.setText(cliCur.getString(companyNameCol));
			vat.setVisibility(View.GONE);
			cliCon.close();
			jobCon.close();
			break;
		case 2: // Business Expense
			bar.setTitle("View Business Expense");
			SettingsConnector setCon = new SettingsConnector(this);
			setCon.open();
			try{
				Cursor cur = setCon.getUserDetails();
				cur.moveToFirst();
				int userName = cur.getColumnIndex(SettingsConnector.USERNAME);
				int userCompany = cur.getColumnIndex(SettingsConnector.USERCOMPANY);
				title1.setText(cur.getString(userCompany));
				title2.setText(cur.getString(userName));
				title3.setText("");
				vat.setVisibility(View.VISIBLE);
				vat.setText("VAT: "+nf.format(expCur.getDouble(vatCol)));
			} catch (Exception e){
				System.out.println("No User Record found");
			}
			setCon.close();
			break;
		case 3: // Personal Expense
			bar.setTitle("View Personal Expense");
			SettingsConnector setCon2 = new SettingsConnector(this);
			setCon2.open();
			try{
				Cursor cur2 = setCon2.getUserDetails();
				cur2.moveToFirst();
				int userName2 = cur2.getColumnIndex(SettingsConnector.USERNAME);
				title1.setText("");
				title2.setText(cur2.getString(userName2));
				title3.setText("");
				vat.setVisibility(View.GONE);				
			} catch (Exception e){
				System.out.println("No User Record found");
			}
			setCon2.close();
			break;
		default:
			break;
		}
		
		Calendar startCal = Calendar.getInstance();	
		try {
			startCal = convertStringToDate(expCur.getString(dateCol));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		expenseType.setText(expCur.getString(expTypeCol));
		description.setText(expCur.getString(descriptionCol));
		date.setText( outDate.format(startCal.getTime()));
		double costVal = expCur.getDouble(costCol);
		total.setText(nf.format((costVal)));
		expCur.close();
		expCon.close();
		if (photoName.length()>0){
			loadThumb();
		} else {
			thumb.setImageResource(R.drawable.camera);
		}
	}
	
	public void loadThumb(){
		System.out.println(photoName);
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = 4;
		bitmap = BitmapFactory.decodeFile(photoName, opts);
		thumb.setImageBitmap(bitmap);
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		if (bitmap!=null){
			bitmap.recycle();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_options_view, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.edit_option:
			Intent i = new Intent(this, NewExpense.class);
			i.putExtra("expenseCat", category);
			i.putExtra("expenseID", expID);
			i.putExtra("clientID", clientID);
			i.putExtra("jobID", jobID);
			i.putExtra("jobName", title3.getText().toString());
			i.putExtra("clientName", title1.getText().toString());
			i.putExtra("expenseType", expenseType.getText().toString());
			i.putExtra("description", description.getText().toString());
			i.putExtra("date", date.getText().toString());
			i.putExtra("vat", vat.getText().toString().replace("VAT: �", ""));
			i.putExtra("total", total.getText().toString().replace("�", ""));
			i.putExtra("imagePath", photoName);
			startActivityForResult(i, 1);
			return true;
		case android.R.id.home:
	    	Intent intent = new Intent(this, ExpenseList.class); 
	    	intent.putExtra("tabIndex", (category-2));
	        startActivity(intent);
	        finish();
	    	return true;
		case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  
	    case R.id.about_option:
	    	return true;
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
		
	public void onClick(View v){
		switch (v.getId()){
		case R.id.cameraIconViewExpenseImageView:	
			if (!photoName.equals("")){
				Intent i = new Intent(ViewExpense.this, FullscreenImage.class);
				i.putExtra("from", 2);
	        	i.putExtra("path", photoName);
	        	startActivity(i);
			}
        	break;
		default:
			break;
		}
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance(Locale.UK);
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    //cal.roll(Calendar.MONTH, 1);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
}
