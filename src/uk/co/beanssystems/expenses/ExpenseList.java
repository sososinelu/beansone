package uk.co.beanssystems.expenses;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.beansone.TabManager;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.settings.Settings;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;


public class ExpenseList extends SherlockFragmentActivity {
	String[] actions = new String[] {"Job","Business","Personal"};
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_expense_list);
	           
	    final ActionBar bar = getSupportActionBar();
        bar.setIcon(R.drawable.coinstack);
	    bar.setDisplayHomeAsUpEnabled(true);
	    bar.setDisplayShowTitleEnabled(true);
	    bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	    
	    //bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.user_mini)).setText("Job").setTabListener(new TabManager<ExpenseListClient>(this, "job", ExpenseListClient.class)));
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.busi_mini)).setText("Business").setTabListener(new TabManager<ExpenseListBusiness>(this, "business", ExpenseListBusiness.class)));
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.personal_mini)).setText("Personal").setTabListener(new TabManager<ExpenseListPersonal>(this, "personal", ExpenseListPersonal.class)));
	    
	}
	
	@Override
	public void onStart(){
		super.onStart();
		Intent i = null;
		try {
			i = getIntent();
			final ActionBar bar = getSupportActionBar();
			bar.setSelectedNavigationItem(i.getIntExtra("tabIndex", 0));
		} catch (Exception e){
			System.out.println("No Intent to get");
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    menu.removeItem(R.id.menuSort);
	    return true;
  	}
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	    	Intent intent = new Intent(this, MainActivity.class);            
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	        startActivity(intent);
	        finish();
	    	return true;
	    	
	    case R.id.search_option:
	    	RelativeLayout item1 = (RelativeLayout)findViewById(R.id.expListLayout);
	    	if(item1.getVisibility() == View.GONE){
	    		item1.setVisibility(View.VISIBLE);
	    	}else{
	    		item1 .setVisibility(View.GONE);
	    		EditText textFragment = (EditText)findViewById(R.id.expListSearch);
	    	    textFragment.setText("");
	    	}  	
	    	return true;
	    case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  
	    case R.id.about_option:
	    	return true;
	    case R.id.help_option:
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
}
