package uk.co.beanssystems.expenses;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import uk.co.beanssystems.beansone.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpensesAdapter extends ArrayAdapter<ExpenseListItem> {
	Context context;
	int layoutResourceId;
	ArrayList<ExpenseListItem> items = null;
	
	public ExpensesAdapter (Context context, int layoutResourceId, ArrayList<ExpenseListItem> items){
		super(context, layoutResourceId, items);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.items = items;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
        ExpenseListItemHolder holder = null;
        
        if(row == null){
	        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	        row = inflater.inflate(layoutResourceId, parent, false);
	        holder = new ExpenseListItemHolder();
	        holder.icon = (ImageView)row.findViewById(R.id.iconExpensesListImageView);
	        holder.expenseType = (TextView)row.findViewById(R.id.expenseTypeLabelExpensesListTextView);
	        holder.expenseDesc = (TextView)row.findViewById(R.id.expenseDescriptionLabelExpensesListTextView);
	        holder.name = (TextView)row.findViewById(R.id.nameLabelExpensesListTextView);
	        holder.date = (TextView)row.findViewById(R.id.dateLabelExpensesListTextView);
	        holder.cost = (TextView)row.findViewById(R.id.costLabelExpensesListTextView);
	        holder.camera = (ImageView)row.findViewById(R.id.cameraIconExpensesListImageView);
	        row.setTag(holder);
	    }
        else{
        	holder = (ExpenseListItemHolder)row.getTag();
	    }
        
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.UK);

        ExpenseListItem expense = items.get(position);
        holder.expenseType.setText(expense.expenseType);
        holder.expenseDesc.setText(expense.description);
        holder.icon.setImageResource(expense.icon);
        holder.name.setText(expense.name);
        holder.date.setText(expense.date);
        
        double vat = 0;
        double cost = 0;
        //double total=0;
        
        if (expense.vat.compareTo("0.0")!=0){
        	vat = Double.parseDouble(expense.vat);
        }
        
    	cost = Double.parseDouble(expense.cost);
        holder.cost.setText(nf.format(cost));
        if (expense.imagePath.length()>0){
        	holder.camera.setVisibility(View.VISIBLE);
        } else {
        	holder.camera.setVisibility(View.INVISIBLE);
        }
		return row;
	}
	
	static class ExpenseListItemHolder
	   {
	       ImageView icon;
	       TextView expenseType;
	       TextView expenseDesc;
	       TextView name;
	       TextView date;
	       TextView cost;
	       ImageView camera;
	   }
}
