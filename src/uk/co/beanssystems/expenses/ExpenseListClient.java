package uk.co.beanssystems.expenses;

import java.util.ArrayList;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragment;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.clients.NewClient;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.ExpensesConnector;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ExpenseListClient extends SherlockFragment {
	private static final String EXPENSEID = "expenseID";
	private static final String CLIENTID = "clientID";
	private static final String CLIENTFORENAME = "forename";
	private static final String CLIENTSURNAME = "surname";
	private static final String CLIENTCOMPANYNAME = "company";
	//private static final String EXPCATEGORY = "expCategory";
	private static final String EXPTYPE = "expType";
	private static final String DESCRIPTION = "description";
	private static final String DATE = "date";
	private static final String VAT = "vat";
	private static final String COST = "cost";
	private static final String PHOTO = "photo";
	private Button newExpense;
	private ListView expensesListView;
	private EditText searchEditText;
	private ImageView clearSearch;
	ArrayList<ExpenseListItem> expensesArrayList = null;
	ArrayList<ExpenseListItem> expensesFixed = null;
	private ArrayList<String> expensesSearchArray;
	ExpensesAdapter expensesAdapter;
	private Cursor expCur;
	private Cursor cliCur;
	private Context context;
	
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		context = getActivity();
		loadExpenses();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		searchEditText.setText("");
	}
		
	public void loadExpenses(){
		expensesArrayList = new ArrayList<ExpenseListItem>();
		expensesFixed = new ArrayList<ExpenseListItem>();
		expensesSearchArray = new ArrayList<String>();
        
		ExpensesConnector expCon = new ExpensesConnector(context);
		ClientsConnector cliCon = new ClientsConnector(context);
		
		expCon.open();
		cliCon.open();
		expCur = expCon.getExpensesByCategory(1);
		System.out.println(expCur.getCount());
		try{
			int idCol = expCur.getColumnIndex(EXPENSEID);
			int expTypeCol = expCur.getColumnIndex(EXPTYPE);
			int descriptionCol = expCur.getColumnIndex(DESCRIPTION);
			int dateCol = expCur.getColumnIndex(DATE);
			int vatCol = expCur.getColumnIndex(VAT);
			int costCol = expCur.getColumnIndex(COST);
			int photoCol = expCur.getColumnIndex(PHOTO);
			int clientIDCol = expCur.getColumnIndex(CLIENTID);
			//
			int clientForenameCol;
			int clientSurnameCol;
			int clientCompanyNameCol;
			//
			int id;
			int clientID;
			int icon;
			String expType;
			String description;
			String name;//
			String date;
			String vat;
			String cost;
			String imagePath;
			expCur.moveToFirst();
			do {
				id = expCur.getInt(idCol);
				expType = expCur.getString(expTypeCol);
				description = expCur.getString(descriptionCol);
				date = expCur.getString(dateCol);
				vat = expCur.getString(vatCol);
				cost = expCur.getString(costCol);
				clientID = expCur.getInt(clientIDCol);
				imagePath = expCur.getString(photoCol);
				
				cliCur = cliCon.getClient(clientID);
				cliCur.moveToFirst();
				clientForenameCol = cliCur.getColumnIndex(CLIENTFORENAME);
				clientSurnameCol = cliCur.getColumnIndex(CLIENTSURNAME);
				clientCompanyNameCol = cliCur.getColumnIndex(CLIENTCOMPANYNAME);
				String comp = cliCur.getString(clientCompanyNameCol);
				
				
				if (comp.compareTo("Private")==0){
					name = cliCur.getString(clientForenameCol) + " " + cliCur.getString(clientSurnameCol);
					icon = R.drawable.user;
				} else {
					name = cliCur.getString(clientCompanyNameCol);
					icon = R.drawable.work_user;
				}
				String string = name +" "+ expType +" "+ date + " " + comp;
				ExpenseListItem exp = new ExpenseListItem(id, icon, expType, description, name, date, vat, cost, imagePath);
				expensesSearchArray.add(string.toLowerCase());
				expensesArrayList.add(exp);
				expensesFixed.add(exp);
			}while(expCur.moveToNext());
		} catch (Exception e){
			e.printStackTrace();
		}
		expCon.close();
		cliCon.close();
	}	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle instance){
		View fragView = inflater.inflate(R.layout.activity_expense_list_client, container, false);
		return fragView;
	}
	
	@Override
	public void onActivityCreated(Bundle instance){
		super.onActivityCreated(instance);	
		expensesListView = (ListView)getActivity().findViewById(R.id.expenses_list_client);
		searchEditText = (EditText)getActivity().findViewById(R.id.expListSearch);		
		clearSearch = (ImageView)getActivity().findViewById(R.id.clearSearchExp);
		newExpense = (Button)getActivity().findViewById(R.id.newExpensesListButton);
		clearSearch.setOnClickListener(clickListener);
		newExpense.setOnClickListener(clickListener);
		newExpense.setText("Add New Job Expense");
	}
	
	OnClickListener clickListener = new OnClickListener() {
		public void onClick(View view){
			switch (view.getId()){
			case R.id.clearSearchExp:
				searchEditText.setText("");
				break;
			case R.id.newExpensesListButton:			
				try {
					Intent i = new Intent(context, NewExpense.class);
					i.putExtra("cancelTab", 0);
					i.putExtra("expenseCat", 1);
					System.out.println("Client Expense");
			    	startActivity(i);
				} catch (Exception e){
				}
				break;

			default:
				break;
			}
		}
	};
	
	@Override
	public void onStart(){
		super.onStart();
		loadExpenses();
		expensesAdapter = new ExpensesAdapter(context, R.layout.activity_expenses_list_row, expensesArrayList);
		expensesAdapter.notifyDataSetChanged();
		expensesListView.setEmptyView(getActivity().findViewById(R.id.empty_expenses_list_view));
		expensesListView.setAdapter(expensesAdapter);
		expensesListView.setOnItemClickListener(new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ExpenseListItem exp = (ExpenseListItem) expensesListView.getItemAtPosition(position);
				Intent i = new Intent(context, ViewExpense.class);
				i.putExtra("expenseID", exp.id);
				startActivity(i);				
			}
    	});
		
		searchEditText.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {	
			}

			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				expensesArrayList.clear();
				String search = searchEditText.getText().toString().toLowerCase();
				
				if(searchEditText.getText().toString().length() == 0){
					clearSearch.setVisibility(View.GONE);
				}else{
					clearSearch.setVisibility(View.VISIBLE);
				}
				
				int searchListLength = expensesSearchArray.size();
				for (int i = 0; i < searchListLength; i++) {
					if (expensesSearchArray.get(i).contains(search)) {	
						add (i);
					}
				} 
			
				ExpensesAdapter expAdapterT = new ExpensesAdapter(context, R.layout.activity_expenses_list_row, expensesArrayList);
				expensesListView.setAdapter(expAdapterT);
			}
		});
	}
	
	public void add (int i){
		ExpenseListItem e = (ExpenseListItem) expensesFixed.get(i); 
		expensesArrayList.add(e);
	}
	
}
