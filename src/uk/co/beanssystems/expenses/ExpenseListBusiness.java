package uk.co.beanssystems.expenses;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragment;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.database.SettingsConnector;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ExpenseListBusiness extends SherlockFragment {
	private String bussName="NO";
	private Button newExpense;
	private ListView expensesListView;
	private EditText searchEditText;
	private ImageView clearSearch;
	private ArrayList<ExpenseListItem> expensesArrayList = null, expensesFixed = null;
	private ArrayList<String> expensesSearchArray;
	ExpensesAdapter expensesAdapter;
	private Cursor expCur;
	private Context context;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		context = getActivity();
		getUserDetails();
		System.out.println("BUSS NAME >>>>>>>>>>> "+bussName);
		loadExpenses();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		searchEditText.setText("");
	}
	
	public void getUserDetails () {
		try{
			SettingsConnector settCon = new SettingsConnector(context);
			settCon.open();
			Cursor settCur = settCon.getUserDetails();		
			settCur.moveToFirst();
			int companyCol = settCur.getColumnIndex(SettingsConnector.USERCOMPANY);
			bussName = settCur.getString(companyCol);
			settCon.close();
		} catch (Exception e) { 
        	e.printStackTrace();
        }		
	}
	
	public void loadExpenses(){
		expensesArrayList = new ArrayList<ExpenseListItem>();
		expensesFixed = new ArrayList<ExpenseListItem>();
		expensesSearchArray = new ArrayList<String>();
		ExpensesConnector expCon = new ExpensesConnector(context);
		expCon.open();
		try{
			expCur = expCon.getExpensesByCategory(2);
			int idCol = expCur.getColumnIndex(ExpensesConnector.EXPENSEID);
			int expTypeCol = expCur.getColumnIndex(ExpensesConnector.EXPTYPE);
			int descriptionCol = expCur.getColumnIndex(ExpensesConnector.DESCRIPTION);
			int dateCol = expCur.getColumnIndex(ExpensesConnector.DATE);
			int vatCol = expCur.getColumnIndex(ExpensesConnector.VAT);
			int costCol = expCur.getColumnIndex(ExpensesConnector.COST);
			int photoCol = expCur.getColumnIndex(ExpensesConnector.PHOTO);
			String expType;
			String date;
			Calendar startCal = Calendar.getInstance();			
			expCur.moveToFirst();
			do {
				expType = expCur.getString(expTypeCol);
				date = expCur.getString(dateCol);
				String string =  expType +" "+ date;			
				try {
					startCal = convertStringToDate(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}				
				ExpenseListItem exp = new ExpenseListItem(expCur.getInt(idCol), R.drawable.work_user, expType, expCur.getString(descriptionCol), 
						bussName, outDate.format(startCal.getTime()), expCur.getString(vatCol),  expCur.getString(costCol), expCur.getString(photoCol));
				expensesSearchArray.add(string.toLowerCase());
				expensesArrayList.add(exp);
				expensesFixed.add(exp);
			}while(expCur.moveToNext());
		} catch (Exception e){
			System.out.println("No Business Expenses");
		}
		expCon.close();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle instance){
		View fragView = inflater.inflate(R.layout.activity_expense_list_client, container, false);
		return fragView;
	}

	@Override
	public void onActivityCreated(Bundle instance){
		super.onActivityCreated(instance);
		expensesListView = (ListView)getActivity().findViewById(R.id.expenses_list_client);
		searchEditText = (EditText)getActivity().findViewById(R.id.expListSearch);
		clearSearch = (ImageView)getActivity().findViewById(R.id.clearSearchExp);
		newExpense = (Button)getActivity().findViewById(R.id.newExpensesListButton);
		clearSearch.setOnClickListener(clickListener);
		newExpense.setOnClickListener(clickListener);
		newExpense.setText("Add New Business Expense");
	}
	
	OnClickListener clickListener = new OnClickListener() {
		public void onClick(View view){
			switch (view.getId()){
			case R.id.clearSearchExp:
				searchEditText.setText("");
				break;
			case R.id.newExpensesListButton:			
				try {
					Intent i = new Intent(context, NewExpense.class);
					i.putExtra("cancelTab", 0);
					i.putExtra("expenseCat", 2);
					System.out.println("Business Expense");
			    	startActivity(i);
				} catch (Exception e){
				}
				break;

			default:
				break;
			}
		}
	};
	
	@Override
	public void onStart(){
		super.onStart();
		loadExpenses();
		expensesAdapter = new ExpensesAdapter(context, R.layout.activity_expenses_list_row, expensesArrayList);
		expensesAdapter.notifyDataSetChanged();
		expensesListView.setEmptyView(getActivity().findViewById(R.id.empty_expenses_list_view));
		expensesListView.setAdapter(expensesAdapter);
		expensesListView.setOnItemClickListener(new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ExpenseListItem exp = (ExpenseListItem) expensesListView.getItemAtPosition(position);
				Intent i = new Intent(context, ViewExpense.class);
				i.putExtra("expenseID", exp.id);
				startActivity(i);
			}
    	});
		
		searchEditText.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {	
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				expensesArrayList.clear();
				String search = searchEditText.getText().toString().toLowerCase();
				
				if(searchEditText.getText().toString().length() == 0){
					clearSearch.setVisibility(View.GONE);
				}else{
					clearSearch.setVisibility(View.VISIBLE);
				}

				int searchListLength = expensesSearchArray.size();
				for (int i = 0; i < searchListLength; i++) {
					if (expensesSearchArray.get(i).contains(search)) {	
						add (i);
					}
				} 
				
				ExpensesAdapter expAdapterT = new ExpensesAdapter(context, R.layout.activity_expenses_list_row, expensesArrayList);
				expensesListView.setAdapter(expAdapterT);
			}
		});
	}
	
	public void add (int i){
		ExpenseListItem e = (ExpenseListItem) expensesFixed.get(i); 
		expensesArrayList.add(e);
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.newExpensesListButton:			
			Intent i = new Intent(context, NewExpense.class);
			final ActionBar bar = ((SherlockActivity) context).getSupportActionBar();
			i.putExtra("cancelTab", bar.getSelectedNavigationIndex());
			i.putExtra("expenseCat", 2);
			System.out.println("Business Expense");
	    	startActivity(i);
          	break;
		default:
			break;
		}
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance(Locale.UK);
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    //cal.roll(Calendar.MONTH, 1);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}

}
