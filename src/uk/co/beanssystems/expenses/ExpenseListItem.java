package uk.co.beanssystems.expenses;

import java.io.Serializable;

public class ExpenseListItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7782681447175312622L;
	public int id;
	public int icon;
	public String expenseType;
	public String description;
	public String name;
	public String date;
	public String vat;
	public String cost;
	public String imagePath;
	
	public ExpenseListItem(){
		super();
	}
	
	public ExpenseListItem(int id, int icon, String expenseType, String description, String name, String date, String vat, String cost, String imagePath){
		super();
		this.id = id;
		this.icon = icon;
		this.expenseType = expenseType;
		this.description = description;
		this.name = name;
		this.date = date;
		this.vat = vat;
		this.cost = cost;
		this.imagePath = imagePath;
	}
}
