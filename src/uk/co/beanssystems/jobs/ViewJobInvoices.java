package uk.co.beanssystems.jobs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.actionbarsherlock.app.SherlockFragment;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.database.InvoicesConnector;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.invoices.InvoicesAdapter;
import uk.co.beanssystems.invoices.InvoicesListItem;
import uk.co.beanssystems.invoices.NewInvoice;
import uk.co.beanssystems.invoices.ViewInvoice;
import uk.co.beanssystems.popups.AddExpensePopup;
import uk.co.beanssystems.popups.AddItemPopup;
import uk.co.beanssystems.popups.AddTaskPopup;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ViewJobInvoices extends SherlockFragment {
	private ListView invoicesListView;
	ArrayList<InvoicesListItem> invoicesArrayList = null;
	InvoicesAdapter invoicesAdapter;
	Button newInvoice, newInvoice2;
	private int jobID;
	private Cursor invoiceCur, jobCur;
	private String clientName, jobName;
	private int clientId;
	private Context context;
	SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		context = getActivity();
		jobID = getArguments().getInt("id");
	}
	
	public void loadInvoices(){
		invoicesArrayList = new ArrayList<InvoicesListItem>();
        InvoicesConnector invoicesCon = new InvoicesConnector(context);
        JobsConnector jobsCon = new JobsConnector(context);  
		jobsCon.open();
		jobCur = jobsCon.getJob(jobID);
		jobCur.moveToFirst();
		int clientIdCol = jobCur.getColumnIndex(JobsConnector.CLIENTID);
		int jobNameCol = jobCur.getColumnIndex(JobsConnector.JOBNAME);
		int clientNameCol = jobCur.getColumnIndex(JobsConnector.CLIENTNAME);
		clientId = jobCur.getInt(clientIdCol);
		jobName = jobCur.getString(jobNameCol);
		clientName = jobCur.getString(clientNameCol);
        jobsCon.close();
        invoicesCon.open();
        try{
            invoiceCur = invoicesCon.getInvoicesByJob(jobID);
        	for (int i=0; i<4; i++){
        		invoiceCur.moveToFirst();
        		int idCol = invoiceCur.getColumnIndex(InvoicesConnector.INVOICEID);
    	        int dateDueCol = invoiceCur.getColumnIndex(InvoicesConnector.DATEDUE);
    	        int outstandingCol = invoiceCur.getColumnIndex(InvoicesConnector.OUTSTANDING);
    	        int totalDueCol = invoiceCur.getColumnIndex(InvoicesConnector.TOTALDUE);
    			int statusCol = invoiceCur.getColumnIndex(InvoicesConnector.STATUS);	
        		do{
        			int id = invoiceCur.getInt(idCol);
                    String dateDue = invoiceCur.getString(dateDueCol);
                	double outstanding = invoiceCur.getDouble(outstandingCol);
                	double totalDue = invoiceCur.getDouble(totalDueCol);
                    String status = invoiceCur.getString(statusCol);
                    String paidStatus;
                    if (outstanding == 0){
                    	paidStatus = "Paid";
                    	outstanding = totalDue;
                    }else{
                    	paidStatus = "Outstanding";
                    }
                    Calendar cal = Calendar.getInstance();
                    try {
                    	cal = convertStringToDate(dateDue);
                    } catch (Exception e){
                    	
                    }
                    dateDue = outDate.format(cal.getTime());
                  	if (status.compareTo("overdue")==0 && i==0){
                    	InvoicesListItem invoice = new InvoicesListItem(id, R.drawable.user, clientId, clientName, jobID, jobName, dateDue, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                  	} else if (status.compareTo("draft")==0 && i==1){
                  		InvoicesListItem invoice = new InvoicesListItem(id, R.drawable.user, clientId, clientName, jobID, jobName, dateDue, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                  	} else if (status.compareTo("sent")==0 && i==2){
                  		InvoicesListItem invoice = new InvoicesListItem(id, R.drawable.user, clientId, clientName, jobID, jobName, dateDue, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                  	} else if (status.equals("paid") && i==3){
                  		InvoicesListItem invoice = new InvoicesListItem(id, R.drawable.user, clientId, clientName, jobID, jobName, dateDue, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                    	System.out.println("PAID ADDED");
                  	}
                } while(invoiceCur.moveToNext());
        	}
        } catch (Exception e) {
        	System.out.println("No Invoices in database for jobID: "+jobID);
        }
	 invoicesCon.close();
	 if (showButton()==true){
		 newInvoice.setVisibility(View.VISIBLE);
		 newInvoice2.setVisibility(View.GONE);
	 } else {
		 newInvoice.setVisibility(View.GONE);
		 newInvoice2.setVisibility(View.VISIBLE);
	 }
	}
	
	public boolean showButton(){
		TasksConnector tCon = new TasksConnector(context);
		ItemsConnector iCon = new ItemsConnector(context);
		ExpensesConnector eCon = new ExpensesConnector(context);
		if (tCon.countByJob(jobID)){
			return true;
		} else if (iCon.countByJob(jobID)){
			return true;
		} else if (eCon.countByJob(jobID)){
			return true;
		}
		return false;
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance(Locale.UK);
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View fragView = inflater.inflate(R.layout.activity_view_job_invoices, container, false);

       return fragView;
    }
	
	@Override
	public void onActivityCreated(Bundle instance){
		super.onActivityCreated(instance);
		invoicesListView = (ListView)getActivity().findViewById(R.id.view_job_invoices_list);
		newInvoice = (Button)getActivity().findViewById(R.id.newInvoicesListButton);
		newInvoice2 = (Button)getActivity().findViewById(R.id.newInvoicesListButton2);
		newInvoice.setOnClickListener(clickListener);
	}
	
	OnClickListener clickListener = new OnClickListener() {
	    public void onClick(final View v) {
	        switch(v.getId()) {
	        case R.id.newInvoicesListButton:
	        	Intent i = new Intent(context, NewInvoice.class);
				i.putExtra("inNo", 2);
				i.putExtra("passJobID", jobID);
				i.putExtra("passJobName", jobName);
				i.putExtra("passClientName", clientName);
				i.putExtra("passClientID", clientId);
				startActivity(i);
	           default:
		          System.out.println("Unknown");
	        	   break;
	        }
	    }
	};
	
	@Override
	public void onStart(){
		super.onStart();
		loadInvoices();
		invoicesAdapter = new InvoicesAdapter(context, R.layout.activity_invoices_list_row, invoicesArrayList);
		invoicesAdapter.notifyDataSetChanged();
		invoicesListView.setEmptyView(getActivity().findViewById(R.id.empty_jobs_invoices_list_view));
    	invoicesListView.setAdapter(invoicesAdapter);
    	invoicesListView.setOnItemClickListener(new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				InvoicesListItem ili = (InvoicesListItem) invoicesListView.getItemAtPosition(position);
				Intent i = new Intent(context, ViewInvoice.class);
				i.putExtra("passInvoiceID", ili.id);
				i.putExtra("passClientID", ili.clientId);
				i.putExtra("passJobID", ili.jobId);
				i.putExtra("passJobName", ili.jobName);
				i.putExtra("passDateDue", ili.dateDue);			
				i.putExtra("passPaidStatus", ili.paidStatus);
				startActivity(i);			
			}
    	});
	}
	
}