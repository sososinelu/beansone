package uk.co.beanssystems.jobs;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import uk.co.beanssystems.beansone.PDFMaker;
import uk.co.beanssystems.beansone.QuotePDF;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.QitemsConnector;
import uk.co.beanssystems.database.QtasksConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.quotes.NewQuote;
import uk.co.beanssystems.quotes.PrintDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;

public class ViewJobQuote extends SherlockFragment {
	Context context;
	private Cursor tasksCur, itemsCur;
	double globalVAT = 20, totalTasks, totatlVatTasks, totalItems, totalVatItems, fixedAmount = 0;
	private ArrayList<Task> tasks = new ArrayList<Task>();
	private ArrayList<Item> items = new ArrayList<Item>();
	private int jobID, clientID, quoteID;
	private String jobName, emailSend, clientName, company, mobile, email, file, address, postcode, vatType, status;
	private int noQuote;
	private double subTotal, baseTotal, totalCost, totalVAT, fixedCost;
	private boolean emailStarted=false, isFixed = false;
	private TextView totalSumTextView, statusTextView, companyTextView, jobTextView, dateIssuedTextView, newQuoteTextView, emailQLink, printLink, 
		pdfLink, phoneLink, smsLink, emailCLink, mapLink, fixedLabel, vatLabel, subtotalLabel, noIWTextView;
	private Button btn, clientButton, sendButton;
	QuotesConnector qCon;
	View main, tasksView, itemsView, expView;
	LayoutInflater inflater;
	LinearLayout linear, sendLayout, vatLabelLayout, subTotalLabelLayout;
	NumberFormat nf;
	RelativeLayout quickLayout;	
	Drawable arrowDw, arrowUp;
	TranslateAnimation animationInSend, animationOutSend, animationInQuick, animationOutQuick;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		context = getActivity();
		jobID = getArguments().getInt("id");
		inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		nf = NumberFormat.getCurrencyInstance(Locale.UK);
	    
	    animationInSend = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
			    Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
	    animationOutSend = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f );
	     animationInQuick = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
			    Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
		animationOutQuick = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -1.0f );
		animationInSend.setDuration(200);
		animationOutSend.setDuration(200);
		animationInQuick.setDuration(200);
		animationOutQuick.setDuration(200);
		arrowDw = getActivity().getApplicationContext().getResources().getDrawable(R.drawable.arrow_down);
	    arrowUp = getActivity().getApplicationContext().getResources().getDrawable(R.drawable.arrow_up);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		QuotesConnector qCon = new QuotesConnector(context);
		qCon.open();
		Cursor qCur = qCon.getQuoteByJob(jobID);
		if (qCur.getCount()==0){
			qCon.close();
			View fragView = inflater.inflate(R.layout.activity_view_client_quotes_header, container, false);
			noQuote=1;
	        return fragView;
		}else {
			qCon.close();
			View fragView = inflater.inflate(R.layout.activity_view_quote, container, false);
	        noQuote=0;
			return fragView;
		}
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		if (noQuote==0){
			totalSumTextView = (TextView) getActivity().findViewById(R.id.totalDueViewQuoteTextView);
		    statusTextView = (TextView) getActivity().findViewById(R.id.statusViewQuoteTextView);
		    statusTextView.setVisibility(View.GONE);
		    clientButton = (Button) getActivity().findViewById(R.id.clientViewQuoteButton);
		    sendButton = (Button) getActivity().findViewById(R.id.sendJobViewQuoteButton); 
		    companyTextView = (TextView) getActivity().findViewById(R.id.companyViewQuoteTextView);
		    jobTextView = (TextView) getActivity().findViewById(R.id.jobViewQuoteTextView);
		    dateIssuedTextView = (TextView) getActivity().findViewById(R.id.issuedDateViewQuoteTextView);
		    quickLayout = (RelativeLayout) getActivity().findViewById(R.id.quickContactPopupViewQuote);
		    sendLayout = (LinearLayout) getActivity().findViewById(R.id.sendPopupViewQuote);
		    fixedLabel = (TextView) getActivity().findViewById(R.id.fixedPriceLabelViewQuote);	    
		    emailQLink = (TextView) getActivity().findViewById(R.id.emailLabelViewQuoteTextView);
		    printLink = (TextView) getActivity().findViewById(R.id.printLabelViewQuoteTextView);
		    pdfLink = (TextView) getActivity().findViewById(R.id.pdfLabelViewQuoteTextView);	    
		    phoneLink = (TextView) getActivity().findViewById(R.id.mobileQCViewQuoteTextView);
		    smsLink = (TextView) getActivity().findViewById(R.id.smsQCViewQuoteTextView);
		    emailCLink = (TextView) getActivity().findViewById(R.id.emailQCViewQuoteTextView);
		    mapLink = (TextView) getActivity().findViewById(R.id.mapQCViewQuoteTextView);
		    vatLabel = (TextView) getActivity().findViewById(R.id.vatValueLabelViewQuote);
		    vatLabelLayout = (LinearLayout) getActivity().findViewById(R.id.vatLayoutViewQuote);
		    subtotalLabel = (TextView) getActivity().findViewById(R.id.subtotalValueLabelViewQuote);
		    subTotalLabelLayout = (LinearLayout) getActivity().findViewById(R.id.subtotalLayoutViewQuote);
		    fixedLabel = (TextView) getActivity().findViewById(R.id.fixedPriceLabelViewQuote);
		    noIWTextView = (TextView) getActivity().findViewById(R.id.noIWViewQuoteTextView);
		   
		    emailQLink.setOnClickListener(clickListener);
		    printLink.setOnClickListener(clickListener);
		    pdfLink.setOnClickListener(clickListener);
		    phoneLink.setOnClickListener(clickListener);
		    smsLink.setOnClickListener(clickListener);
		    emailCLink.setOnClickListener(clickListener);
		    mapLink.setOnClickListener(clickListener);
		    clientButton.setOnClickListener(clickListener);
		    sendButton.setOnClickListener(clickListener);
		} else {
			newQuoteTextView = (TextView) getActivity().findViewById(R.id.headerViewClientQuotes);
			newQuoteTextView.setOnClickListener(clickListener);
		}
	}
	
	OnClickListener clickListener = new OnClickListener() {
	    public void onClick(final View v) {
	        switch(v.getId()) {
	           case R.id.clientViewQuoteButton:
	        	   animationInQuick.setDuration(200);
	   		    	animationOutQuick.setDuration(200);		    
	   		    if(quickLayout.getVisibility() == View.GONE){
	   		    	clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
	   		    	quickLayout.setVisibility(View.VISIBLE);
	   		    	quickLayout.startAnimation(animationInQuick);
	   	    	}else{
	   	    		clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
	   	    		quickLayout.startAnimation(animationOutQuick);
	   	    		quickLayout.setVisibility(View.GONE);
	   	    	} 
	        		break;
	           case R.id.sendJobViewQuoteButton:
	        	   animationInSend.setDuration(200);
		   		    animationOutSend.setDuration(200);	    
		   		    if(sendLayout.getVisibility() == View.GONE){
		   		    	sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
		   		    	sendLayout.setVisibility(View.VISIBLE);
		   		    	sendLayout.startAnimation(animationInSend);
		   	    	}else{
		   	    		sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
		   	    		sendLayout.startAnimation(animationOutSend);
		   	    		sendLayout .setVisibility(View.GONE);
		   	    	} 
	        	   break;
	           case R.id.headerViewClientQuotes:
	        	   Intent inQuote = new Intent(context, NewQuote.class);
	        	   inQuote.putExtra("source", "ViewJobQuote");
	        	   inQuote.putExtra("jobID", jobID);
	        	   inQuote.putExtra("clientID", clientID);
	        	   inQuote.putExtra("jobName", jobName);
	        	   inQuote.putExtra("clientName", clientName);
	        	   inQuote.putExtra("tasks", tasks);
	        	   inQuote.putExtra("items", items);
	        	   startActivity(inQuote);
	        	   break;
	        	   case R.id.mobileQCViewQuoteTextView:
	       			try {
	       				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
	       				quickLayout.startAnimation(animationOutQuick);
	       				quickLayout.setVisibility(View.GONE);
	       				startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("tel", mobile, null)));
	       			} catch (Exception e){
	       			}
	       			break;
	       			
	       		case R.id.smsQCViewQuoteTextView:
	       			try {
	       				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
	       				quickLayout.startAnimation(animationOutQuick);
	       				quickLayout.setVisibility(View.GONE);
	       				startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", mobile, null)));
	       			} catch (Exception e){
	       			}
	       			break;
	       					
	       		case R.id.emailQCViewQuoteTextView:
	       			try {
	       				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
	       				quickLayout.startAnimation(animationOutQuick);
	       				quickLayout.setVisibility(View.GONE);
	       				Intent intent = new Intent(Intent.ACTION_SEND);
	       	            intent.setType("plain/text");
	       	            intent.putExtra(Intent.EXTRA_EMAIL,new String[] {email});
	       	            startActivity(Intent.createChooser(intent, "Choose Email Client"));
	       			} catch (Exception e){
	       			}
	       			break;
	       			
	       		case R.id.mapQCViewQuoteTextView:
	       			try {
	       				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
	       				quickLayout.startAnimation(animationOutQuick);
	       				quickLayout.setVisibility(View.GONE);
	       				String address1 = address + postcode; // Get address
	       				address1 = address1.replace(" ", "+");
	       				Intent geoIntent = new Intent (android.content.Intent.ACTION_VIEW, Uri.parse ("geo:0,0?q=" + address1)); // Prepare intent
	       				startActivity(geoIntent);	// Initiate lookup
	       			} catch (Exception e){
	       				Toast.makeText(getActivity(), "No Application Available to Display Maps!!", Toast.LENGTH_SHORT).show();
	       				
	       			}
	       			break;
	       			
	       		case R.id.emailLabelViewQuoteTextView:
	       			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
	       			sendLayout.startAnimation(animationOutSend);
	       			sendLayout.setVisibility(View.GONE);
	       			ClientsConnector cliCon = new ClientsConnector(getActivity());
	       			cliCon.open();
	       			Cursor cur = cliCon.getClient(clientID);
	       			cur.moveToFirst();
	       			int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
	       			String email = cur.getString(emailCol).replace(" ", "");
	       			cliCon.close();
	       			if (!email.equals("") && email.contains("@")){
	       				String client = clientName.length()<8 ? clientName : clientName.substring(0, 7);
	    				String job = jobName.length()<8 ? jobName : jobName.substring(0, 7);
	       				emailSend = email;
	       				String filename = quoteID + "_quo_"+client+"_"+job+".pdf";
	       				QuotePDF obj = new QuotePDF(clientID, jobName, dateIssuedTextView.getText().toString(), subTotal, totalVAT, totalCost);
	    				PDFMaker pdf = new PDFMaker(context, 0, filename, "Thank you for your business", obj);
	       	    		if (pdf.makeFile()){
	       	    			file = pdf.build();
		       	    		emailPDF();
		       				getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
	       				} else {
	       					Toast.makeText(context, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();
	       				}
	       				} else {
	       				Toast.makeText(getActivity(), "You do not have a valid email address for this client. Please check client email details and resend", Toast.LENGTH_LONG).show();
	       			}
	       			break;
	       			
	       		case R.id.printLabelViewQuoteTextView:
	       			Toast.makeText(getActivity(), "Coming Soon", Toast.LENGTH_SHORT).show();
//	       			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
//	       			sendLayout.startAnimation(animationOutSend);
//	       			sendLayout.setVisibility(View.GONE);
//	       			makePDF();
//	           		printPDF();
	       			break;
	       			
	       		case R.id.pdfLabelViewQuoteTextView:
	       			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
	       			sendLayout.startAnimation(animationOutSend);
	       			sendLayout.setVisibility(View.GONE);
	       			String client = clientName.length()<8 ? clientName : clientName.substring(0, 7);
    				String job = jobName.length()<8 ? jobName : jobName.substring(0, 7);
       				String filename = quoteID + "_quo_"+client+"_"+job+".pdf";
       				QuotePDF obj = new QuotePDF(clientID, jobName, dateIssuedTextView.getText().toString(), subTotal, totalVAT, totalCost);
    				PDFMaker pdf = new PDFMaker(context, 0, filename, "Thank you for your business", obj);
	        		if (pdf.makeFile()){
	        			file = pdf.build();
	       				startPDF();
	       			} else {
	       				Toast.makeText(context, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();
	       			}
	       			break;
	       		  
	           default:
		          System.out.println("Unknown");
	        	   break;
	        }
	    }
	};
	
	@Override
	public void onStart(){
		super.onStart();
		System.out.println("noQuote val: "+noQuote);
			try{
				if (noQuote==0){
					loadQuote();
				} else if (noQuote==1) {
					loadJob();
				}
			} catch (Exception e){
				System.out.println("No Quote Available");
				e.printStackTrace();
			}
		}
	
	public void loadJob(){
			//Get Job Details
			JobsConnector jobCon = new JobsConnector(context);
			jobCon.open();
			try{				
				Cursor cur = jobCon.getJob(jobID);
				cur.moveToFirst();	
				int jobNameCol = cur.getColumnIndex(JobsConnector.JOBNAME);
				int clientIDCol = cur.getColumnIndex(JobsConnector.CLIENTID);
				int clientNameCol = cur.getColumnIndex(JobsConnector.CLIENTNAME);
				int estimatedHoursCol = cur.getColumnIndex(JobsConnector.ESTIMATEDHOURS);
				jobName = cur.getString(jobNameCol);
				clientID = cur.getInt(clientIDCol);
				clientName = cur.getString(clientNameCol);
			} catch (Exception e){
				e.printStackTrace();
			}
			jobCon.close();
			tasks = new ArrayList<Task>();
			//Load Works
			TasksConnector tCon = new TasksConnector(context);
			try {
				Cursor cur = tCon.getTasksByJob(jobID);
				cur.moveToFirst();
				int taskIdCol = cur.getColumnIndex(TasksConnector.TASKID);
				int taskNameCol = cur.getColumnIndex(TasksConnector.TASKNAME);
				int durationCol = cur.getColumnIndexOrThrow(TasksConnector.ACTUALDURATION);
				int chargeValueCol = cur.getColumnIndex(TasksConnector.CHARGEVALUE);
				do{					
					Task tsk = new Task(cur.getInt(taskIdCol), cur.getString(taskNameCol), cur.getDouble(durationCol), cur.getDouble(chargeValueCol));
					tasks.add(tsk);
					if(tsk.taskName.equals("Callout")){
						totalTasks += tsk.value;
					}else{
						totalTasks += tsk.duration * tsk.value;
					} 
				} while (cur.moveToNext());
			} catch (Exception e){
				System.out.println("No Tasks");
			}
			tCon.close();
			items = new ArrayList<Item>();
			//Load Items
			ItemsConnector iCon = new ItemsConnector(context);
			try {
				Cursor cur = iCon.getItemsByJob(jobID);
				cur.moveToFirst();
				int itemIdCol = cur.getColumnIndex(ItemsConnector.ITEMID);
				int itemNameCol = cur.getColumnIndex(ItemsConnector.ITEMNAME);
				int unitCol = cur.getColumnIndex(ItemsConnector.UNITCOST);
				int quantityCol = cur.getColumnIndex(ItemsConnector.QUANTITY);
				do{
					Item it = new Item(cur.getInt(itemIdCol), cur.getString(itemNameCol), cur.getDouble(unitCol), cur.getInt(quantityCol),
							cur.getDouble(unitCol) * cur.getInt(quantityCol));
					items.add(it);
					totalItems += it.cost * it.quantity;
				} while (cur.moveToNext());
			} catch (Exception e){
				System.out.println("No Items");
			}
			iCon.close();
	}
	
	public void loadQuote(){
		linear = (LinearLayout)getActivity().findViewById(R.id.parentLayoutViewQuote);
		linear.removeAllViews();
		tasksView = inflater.inflate(R.layout.activity_view_quote_holder, null);
		itemsView = inflater.inflate(R.layout.activity_view_quote_holder, null);
		qCon = new QuotesConnector(context);
		qCon.open();
		Cursor qCur = qCon.getQuoteByJob(jobID);
		qCur.moveToFirst();
		int quoteIDCol = qCur.getColumnIndex(QuotesConnector.QUOTEID);
		int jobNameCol = qCur.getColumnIndex(QuotesConnector.JOBNAME);
		int dateIssuedCol = qCur.getColumnIndex(QuotesConnector.DATEISSUED);
		int totalCostCol = qCur.getColumnIndex(QuotesConnector.TOTALCOST);
		int clientIDCol = qCur.getColumnIndex(QuotesConnector.CLIENTID);
		int fixedCostCol = qCur.getColumnIndex(QuotesConnector.FIXEDCOST);
		int subtotalCol = qCur.getColumnIndex(QuotesConnector.SUBTOTAL);
		int basetotalCol = qCur.getColumnIndex(QuotesConnector.BASETOTAL);
		int vatTypeCol = qCur.getColumnIndex(QuotesConnector.VATTYPE);
		int totalVATCol = qCur.getColumnIndex(QuotesConnector.TOTALVAT);
		int statusCol = qCur.getColumnIndex(QuotesConnector.STATUS);
		
		subTotal = qCur.getDouble(subtotalCol);
		totalCost = qCur.getDouble(qCur.getColumnIndex(QuotesConnector.TOTALCOST));
		quoteID = qCur.getInt(quoteIDCol);
		jobTextView.setText(qCur.getString(jobNameCol));
		jobName = qCur.getString(jobNameCol);
		vatType = qCur.getString(vatTypeCol);
		subTotal = qCur.getDouble(subtotalCol);
		baseTotal = qCur.getDouble(basetotalCol);
		totalVAT = qCur.getDouble(totalVATCol);
		status = qCur.getString(statusCol);
		Calendar cal = Calendar.getInstance();
		
		try {
			cal = convertStringToDate(qCur.getString(dateIssuedCol));
		} catch (Exception e){
			//
		}
		dateIssuedTextView.setText(outDate.format(cal.getTime()));
		clientID = qCur.getInt(clientIDCol);
		fixedAmount = qCur.getDouble(fixedCostCol);
		
		if (fixedAmount!=0){
			isFixed = true;
			fixedLabel.setVisibility(View.VISIBLE);
			fixedLabel.setText("Fixed Price");
			totalSumTextView.setText(nf.format(fixedAmount));
		} else {
			isFixed = false;
			fixedLabel.setVisibility(View.GONE);
			fixedLabel.setText("");
			totalSumTextView.setText(nf.format(qCur.getDouble(totalCostCol)));
		}
		if(vatType.equals("none")){
			vatLabelLayout.setVisibility(View.GONE);
			subTotalLabelLayout.setVisibility(View.GONE);
		}else{
			vatLabelLayout.setVisibility(View.VISIBLE);
			subTotalLabelLayout.setVisibility(View.VISIBLE);
			vatLabel.setText(String.valueOf(nf.format(totalVAT)));
			subtotalLabel.setText(String.valueOf(nf.format(subTotal)));
		}
		qCon.close();
		tasks = new ArrayList<Task>();
		items = new ArrayList<Item>();
		System.out.println("id: "+quoteID);
		getClient(clientID);
		getItems(quoteID);
		getTasks(quoteID);
		file = clientName.replaceAll(" ", "_")+ "_"+jobName.replaceAll(" ", "_") +"_quote.pdf";
		System.out.println("Filename: "+file);
		btn = (Button)getActivity().findViewById(R.id.makeJobViewQuoteButton);
		btn.setVisibility(View.GONE);
		if(tasks.size() == 0 && items.size() == 0){
			linear.addView(noIWTextView);
		}
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance();
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    cal.roll(Calendar.MONTH, -1);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
	
	public void getClient(int id){		
		if (id != 0){
			ClientsConnector cliCon = new ClientsConnector(context);
			cliCon.open();
			try{
				Cursor cur = cliCon.getClient(id);
				cur.moveToFirst();
				int forenameCol = cur.getColumnIndex(ClientsConnector.FORENAME);
				int surnameCol = cur.getColumnIndex(ClientsConnector.SURNAME);
				int companyNameCol = cur.getColumnIndex(ClientsConnector.COMPANY);
				int addressCol = cur.getColumnIndex(ClientsConnector.ADDRESS);
				int postcodeCol = cur.getColumnIndex(ClientsConnector.POSTCODE);
				int mobileCol = cur.getColumnIndex(ClientsConnector.MOBILE);
				int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
				clientName = cur.getString(forenameCol) + " " + cur.getString(surnameCol);
				company = cur.getString(companyNameCol);
				mobile = cur.getString(mobileCol);
				address = cur.getString(addressCol);
				postcode = cur.getString(postcodeCol);
				email = cur.getString(emailCol);
				clientButton.setText(clientName);
				companyTextView.setText(company);  
			} catch (Exception e) { 
	        	System.out.println("++++++No Clients+++++ "+e.getLocalizedMessage().toString());
	        }
			cliCon.close();
		}
	}
	public void getTasks(int id) {
		totalTasks = 0;
		QtasksConnector qtasksCon = new QtasksConnector(context);
		qtasksCon.open();
		try{
			tasksCur = qtasksCon.getTasksByQuote(id);	
			if(tasksCur.getCount() != 0){
				((TextView) tasksView.findViewById(R.id.typeViewQuoteHolderTextView)).setText("Work");
		        linear.addView(tasksView);
				tasksCur.moveToFirst();
				// Get the Task columns
				int taskIdCol = tasksCur.getColumnIndex(QtasksConnector.QTASKID); 
				int taskNameCol = tasksCur.getColumnIndex(QtasksConnector.TASKNAME);
				int estimatedDurationCol = tasksCur.getColumnIndex(QtasksConnector.ESTIMATEDDURATION);
				int chargeValueCol = tasksCur.getColumnIndex(QtasksConnector.CHARGEVALUE);	
				do {
					Task task = new Task(tasksCur.getInt(taskIdCol), tasksCur.getString(taskNameCol), tasksCur.getDouble(estimatedDurationCol), tasksCur.getDouble(chargeValueCol));
					RelativeLayout inflatedView = (RelativeLayout) View.inflate(context, R.layout.activity_view_quote_child, null);
					((TextView) inflatedView.findViewById(R.id.typeNameViewQuoteTextView)).setText(task.taskName);
					((TextView) inflatedView.findViewById(R.id.quantityViewQuoteTextView)).setText(" "+String.valueOf(task.duration)+" Hrs");
					((TextView) inflatedView.findViewById(R.id.unitViewQuoteTextView)).setText("Rate = ");
					((TextView) inflatedView.findViewById(R.id.unitCostViewQuoteTextView)).setText(nf.format(task.value));
			     	((LinearLayout) tasksView.findViewById(R.id.taskItemExpLayoutChild)).addView(inflatedView);
			     	if(task.taskName.equals("Callout")){
						totalTasks += task.value;
					}else{
						totalTasks += task.duration * task.value;
					} 
					tasks.add(task);
				}while(tasksCur.moveToNext());	
				((TextView) tasksView.findViewById(R.id.totalViewQuoteTextView)).setText(String.valueOf(nf.format(totalTasks)));
			}
		} catch (Exception e) { 
	    	System.out.println("++++++No QTasks+++++ "+e.getLocalizedMessage().toString());
	    }
		qtasksCon.close();
	}
	
	public void getItems(int id) {
		totalItems = 0;
		QitemsConnector qitemsCon = new QitemsConnector(context);
		qitemsCon.open();
		try{
			itemsCur = qitemsCon.getItemsByQuote(id);
			if(itemsCur.getCount() != 0){
				((TextView) itemsView.findViewById(R.id.typeViewQuoteHolderTextView)).setText("Items");		        
		        linear.addView(itemsView);
				itemsCur.moveToFirst();
				// Get the Item columns
				int itemIdCol = itemsCur.getColumnIndex(QitemsConnector.QITEMID);
				int itemNameCol = itemsCur.getColumnIndex(QitemsConnector.ITEMNAME);
				int unitCostCol = itemsCur.getColumnIndex(QitemsConnector.UNITCOST);
				int quantityCol = itemsCur.getColumnIndex(QitemsConnector.QUANTITY);
				int totalCostCol = itemsCur.getColumnIndex(QitemsConnector.TOTALCOST);
				
				do {
					Item item = new Item(itemsCur.getInt(itemIdCol), itemsCur.getString(itemNameCol), itemsCur.getDouble(unitCostCol), 
							itemsCur.getInt(quantityCol), itemsCur.getDouble(totalCostCol));
					RelativeLayout inflatedView = (RelativeLayout) View.inflate(context, R.layout.activity_view_quote_child, null);
					((TextView) inflatedView.findViewById(R.id.typeNameViewQuoteTextView)).setText(item.itemName);
					((TextView) inflatedView.findViewById(R.id.quantityViewQuoteTextView)).setText(" X "+String.valueOf(item.quantity));
					((TextView) inflatedView.findViewById(R.id.unitViewQuoteTextView)).setText("Unit = ");
					((TextView) inflatedView.findViewById(R.id.unitCostViewQuoteTextView)).setText(nf.format(item.cost));
			     	((LinearLayout) itemsView.findViewById(R.id.taskItemExpLayoutChild)).addView(inflatedView);
			     	double totalCost = itemsCur.getDouble(totalCostCol);
					totalItems += totalCost;
			     	items.add(item);
				}while (itemsCur.moveToNext());
				((TextView) itemsView.findViewById(R.id.totalViewQuoteTextView)).setText(nf.format(totalItems));
			}
		} catch (Exception e) { 
				e.printStackTrace();
		}
		qitemsCon.close();
	}
	
	public void startPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		
		if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } 
            catch (ActivityNotFoundException e) {
                Toast.makeText(getActivity(), "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
            }
		}
	}
	
	public void printPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		Intent printIntent = new Intent(getActivity(), PrintDialog.class);
    	printIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
    	printIntent.putExtra("title", "Quote for "+clientName);
    	startActivity(printIntent);
	}

	public void emailPDF(){
		emailStarted = true;
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		String[] mailto = {emailSend};
		i.putExtra(Intent.EXTRA_EMAIL, mailto);
		i.putExtra(Intent.EXTRA_SUBJECT, "Your Quote");
		i.putExtra(Intent.EXTRA_TEXT, "Hello "+clientName+", your quote has been attached to this email.");
		i.putExtra(Intent.EXTRA_STREAM,
			Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ java.io.File.separator + "beansone"+ java.io.File.separator + file)));
		i.setType("application/pdf");
	    startActivity(Intent.createChooser(i, "Send Email"));
	}
}
