package uk.co.beanssystems.jobs;

public class JobsListItem {
	public int id;
	public int rowType;
	public int icon;
    public String client;
    public String jobName;
    public String startDate;
    public String jobType;
    public String active;
    
	public JobsListItem(){
		super();
	}
	
	public JobsListItem(int rowType, int id, int icon, String client, String jobName, String startDate, String jobType, String active){
		super();
		this.rowType = rowType;
		this.id = id;
		this.icon = icon;
		this.client = client;
		this.jobName = jobName;
		this.startDate = startDate;
		this.jobType = jobType;
		this.active = active;
	}
}
