package uk.co.beanssystems.jobs;

import java.io.Serializable;

public class Rate implements Serializable {
	private static final long serialVersionUID = 1L;
	public int rateID;
	public String rateName, rateTime;
	public double rateValue;
	
	public Rate(){
		super();
	}
	
	public Rate (int rateID, String rateName, double rateValue, String rateTime){
		super();
		this.rateID = rateID;
		this.rateName = rateName;
		this.rateValue = rateValue;
		this.rateTime = rateTime;
	}
	
	public void setRateName(String rateName){
		this.rateName = rateName;
	}
	
	public void setRateValue(double rateValue){
		this.rateValue = rateValue;
	}
	
	public void setRateTime(String rateTime){
		this.rateTime = rateTime;
	}
}
