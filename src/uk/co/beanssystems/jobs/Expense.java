package uk.co.beanssystems.jobs;

import java.io.Serializable;

public class Expense implements Serializable{
	private static final long serialVersionUID = 8314004401003585199L;
	public int expID;
	public int jobID;
	public int invoiceID;
	public String expType;
	public String expDate;
	public double expCost;
	public String expDescription;
	public String photoPath;
	public boolean dead;
	
	public Expense(){
		super();
		this.dead = false;
	}
	
	public Expense (int expID, int jobID, int invoiceID, String expType, String expDate, double expCost, String expDescription, String photoPath){
		super();
		this.expID = expID;
		this.jobID = jobID;
		this.invoiceID = invoiceID;
		this.expType = expType;
		this.expDate = expDate;
		this.expCost = expCost;
		this.expDescription = expDescription;
		this.photoPath = photoPath;
		this.dead = false;
	}
	
	public Expense (String expType, double expCost, double expVAT){
		super();
		this.expType = expType;
		this.expCost = expCost;
		this.dead = false;
	}
	
	public Expense (int expID, String expType, double expCost){
		super();
		this.expID = expID;
		this.expType = expType;
		this.expCost = expCost;
		this.dead = false;
	}
	
	public Expense (int expID, int jobID, int invoiceID, String expType, double expCost){
		super();
		this.expID = expID;
		this.jobID = jobID;
		this.invoiceID = invoiceID;
		this.expType = expType;
		this.expCost = expCost;
		this.dead = false;
	}
	
	public void setExpType(String expType){
		this.expType = expType;
	}
	
	public void setExpDate(String expDate){
		this.expDate = expDate;
	}
	
	public void setExpCost(double expCost){
		this.expCost = expCost;
	}
	
	public void setExpDescription(String expDescription){
		this.expDescription = expDescription;
	}
	
	public void setExpPhotoPath(String photoPath){
		this.photoPath = photoPath;
	}
}
