package uk.co.beanssystems.jobs;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.beansone.TabManager;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.database.InvoicesConnector;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.QitemsConnector;
import uk.co.beanssystems.database.QtasksConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.popups.DeleteDialog;
import uk.co.beanssystems.popups.JobDonePopup;
import uk.co.beanssystems.settings.Settings;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class ViewJob extends SherlockFragmentActivity {
	private static int jobID;
	private String imagePath;
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_job);
        Intent i = getIntent();
        jobID = i.getIntExtra("jobID", 0);
        final ActionBar bar = getSupportActionBar();
        bar.setIcon(R.drawable.jobs);
	    bar.setDisplayHomeAsUpEnabled(true);
	    bar.setDisplayShowTitleEnabled(true);
	    bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	    bar.setTitle(i.getStringExtra("jobName"));
	    Bundle args = new Bundle();
	    imagePath = jobID + "_" + i.getStringExtra("jobName").replace(" ", "_");
	    args.putInt("id", jobID);
	    args.putString("imagePath", imagePath);
	    bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.tab_jobs_mini)).setTabListener(new TabManager<ViewJobInfo>(this, "info", ViewJobInfo.class, args)));
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.tab_quotes_mini)).setTabListener(new TabManager<ViewJobQuote>(this, "quotes", ViewJobQuote.class, args)));
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.tab_invoice_mini)).setTabListener(new TabManager<ViewJobInvoices>(this, "jobs", ViewJobInvoices.class, args)));
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.tab_camera_mini)).setTabListener(new TabManager<ViewJobPictures>(this, "invoices", ViewJobPictures.class, args)));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.menu_options_view, menu);
	    menu.findItem(R.id.edit_option).setTitle("Finish Job");
	    menu.findItem(R.id.delete_option).setTitle("Delete Job");
	    return true;
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode){
			case 1:
				if (resultCode==RESULT_OK){
					jobDone();
				}
				break;
			case 2:
				if (resultCode==RESULT_OK){
					JobsConnector jCon = new JobsConnector(this);
					InvoicesConnector invCon = new InvoicesConnector(this);
					if (invCon.hasInvoices(jobID)){
						jCon.updateActive(jobID, "false");
					} else {
						jCon.updateActive(jobID, "dead");
					}
					jobDelete();
					finish();
				}
				break;
			case 3:
				break;
			default:
				super.onActivityResult(requestCode, resultCode, data);
				break;
		}
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case android.R.id.home:
	        finish();
	    	return true;
		case R.id.edit_option:
			Intent i = new Intent(this, JobDonePopup.class);
			startActivityForResult(i,1);
			return true;
		case R.id.delete_option:
			Intent iDel = new Intent(this, DeleteDialog.class);
			iDel.putExtra("title", "Job");
			iDel.putExtra("msg", "Are you sure you want to delete? Any invoices can be accessed by going to View Client");
			startActivityForResult(iDel, 2);
			return true;
		case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  
	    case R.id.about_option:
	    	return true;
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	} 
	
	public void jobDelete(){
		Cursor cur = null;
		QuotesConnector qCon = new QuotesConnector(this);
		qCon.open();
		cur = qCon.getQuoteByJob(jobID);
		int quoteID=0;
		try{
			cur.moveToFirst();
			int quoteIDCol = cur.getColumnIndex(QuotesConnector.QUOTEID);
			quoteID = cur.getInt(quoteIDCol);
			qCon.updateActive(quoteID, "dead");
		} catch (Exception e){
		} finally {
			qCon.close();
		}

		QitemsConnector qItCon = new QitemsConnector(this);
		qItCon.updateAllActive(quoteID, "dead");

		QtasksConnector qTkCon = new QtasksConnector(this);
		qTkCon.updateAllActive(quoteID, "dead");
		
		TasksConnector tCon = new TasksConnector(this);
		tCon.updateActiveUninvoiced(jobID, "dead");
		
		ItemsConnector iCon = new ItemsConnector(this);
		iCon.updateActiveUninvoiced(jobID, "dead");
		
		ExpensesConnector eCon = new ExpensesConnector(this);
		eCon.updateActiveUninvoiced(jobID, "dead");
	}
	
	public void jobDone(){
		Cursor cur = null;
		//UPDATE QUOTE-------------------------------------
		QuotesConnector qCon = new QuotesConnector(this);
		qCon.open();
		cur = qCon.getQuoteByJob(jobID);
		int quoteID=0;
		try{
			cur.moveToFirst();
			int quoteIDCol = cur.getColumnIndex(QuotesConnector.QUOTEID);
			quoteID = cur.getInt(quoteIDCol);
			qCon.updateOpenActive(quoteID, "false");
			qCon.close();
		} catch (Exception e){
			qCon.close();
		}//---------------------------------END UPDATE QUOTE
		
		cur = null;
		//UPDATE QITEMS-------------------------------------
		QitemsConnector qItCon = new QitemsConnector(this);
		qItCon.open();
		cur = qItCon.getItemsByQuote(quoteID);
		try{
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(QitemsConnector.QITEMID);
			do {
				int id = cur.getInt(idCol);
				qItCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			qItCon.close();
		} catch (Exception e){
			qItCon.close();
		}//-------------------------------------END UPDATE QITEMS
		
		cur = null;
		//UPDATE QTASKS-------------------------------------
		QtasksConnector qTkCon = new QtasksConnector(this);
		qTkCon.open();
		cur = qTkCon.getTasksByQuote(quoteID);
		try{
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(QtasksConnector.QTASKID);
			do {
				int id = cur.getInt(idCol);
				qTkCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			qTkCon.close();
		} catch (Exception e){
			qTkCon.close();
		}//-------------------------------------END UPDATE QTASKS
		
		cur = null;
		//UPDATE TASKS--------------------------------------
		TasksConnector tCon = new TasksConnector(this);
		tCon.open();
		cur = tCon.getTasksByJob(jobID);
		try {
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(TasksConnector.TASKID);
			do {
				int id = cur.getInt(idCol);
				tCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			tCon.close();
		} catch (Exception e){
			tCon.close();
		}//-----------------------------------END UPDATE TASKS
		
		cur = null;
		//UPDATE ITEMS--------------------------------------
		ItemsConnector iCon = new ItemsConnector(this);
		iCon.open();
		cur = iCon.getItemsByJob(jobID);
		try {
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(ItemsConnector.ITEMID);
			do {
				int id = cur.getInt(idCol);
				iCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			iCon.close();
		} catch (Exception e){
			iCon.close();
		}//-----------------------------------END UPDATE ITEMS
		
		cur = null;
		//UPDATE EXPENSES--------------------------------------
		ExpensesConnector eCon = new ExpensesConnector(this);
		eCon.open();
		cur = eCon.getExpsByJob(jobID);
		try {
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(ExpensesConnector.EXPENSEID);
			do {
				int id = cur.getInt(idCol);
				eCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			eCon.close();
		} catch (Exception e){
			eCon.close();
		}//-----------------------------------END UPDATE EXPENSES
		
		cur = null;
		//UPDATE JOB-----------------------------------------
		JobsConnector jCon = new JobsConnector(this);
		jCon.updateActive(jobID, "finished");	//opens and closes itself
		//-------------------------------------END UPDATE JOB
		Intent i = new Intent(this, JobsList.class);
		startActivity(i);
		finish();
	}
}