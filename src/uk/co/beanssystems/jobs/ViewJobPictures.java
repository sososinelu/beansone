package uk.co.beanssystems.jobs;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.actionbarsherlock.app.SherlockFragment;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.popups.FullscreenImage;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;

public class ViewJobPictures extends SherlockFragment {
	String imagePath="";
	ImageAdapter myImageAdapter;
	ArrayList<String> paths = null;
	Button takeNew;
	GridView gridview;
	private Uri tempUri = null;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View fragView = inflater.inflate(R.layout.activity_view_job_pictures, container, false);
       return fragView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		 super.onActivityCreated(savedInstanceState);
		 	takeNew = (Button)getActivity().findViewById(R.id.takeNewPictureViewJobPictures);
		 	takeNew.setOnClickListener(clickListener);
			gridview = (GridView) getActivity().findViewById(R.id.gridview);
		    
			imagePath = getArguments().getString("imagePath");
		    gridview.setOnItemClickListener(new OnItemClickListener() {
		        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		        	Intent i = new Intent(getActivity(), FullscreenImage.class);
		        	i.putExtra("from", 1);
		        	i.putExtra("path", paths.get(position));
		        	startActivity(i);
		        }
		    });
	}
	
	OnClickListener clickListener = new OnClickListener() {
	    public void onClick(final View v) {
	        switch(v.getId()) {
	           case R.id.takeNewPictureViewJobPictures:
		   			Intent inCam3 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					tempUri = getOutputMediaFileUri();
					inCam3.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
				    startActivityForResult(inCam3, 5);
	        	   break;
	           default:
		          System.out.println("Unknown");
	        	   break;
	        }
	    }
	};
	
	public Uri getOutputMediaFileUri(){
	      return Uri.fromFile(getOutputMediaFile());
	}
	public File getOutputMediaFile(){
	    File mediaStorageDir = new File(
	    		Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "BeansOne"+ File.separator + "Jobs" + File.separator + imagePath);
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            System.out.println("Beans, failed to create directory");
	            return null;
	        }
	    }
	    
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    mediaFile = new File(mediaStorageDir.getPath() + File.separator +"IMG_"+ timeStamp + ".jpg");
	    return mediaFile;
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode){
			//Picture
			case 5:
				getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
			default: break;
		}
			
		}
	@Override
	public void onStart(){
		super.onStart();
		getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
		myImageAdapter = new ImageAdapter(getActivity());
		gridview.setAdapter(myImageAdapter);
		paths = new ArrayList<String>();
		File targetPath = new File(
	    		Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "BeansOne"+ File.separator + "Jobs" + File.separator + imagePath);
	    File targetDirector = new File(targetPath.getAbsolutePath());
        
	    try{
	        File[] files = targetDirector.listFiles();
	        for (File file : files){
	         myImageAdapter.add(file.getAbsolutePath());
	         paths.add(file.getAbsolutePath());
	        }
	    } catch (Exception e){
	    	e.printStackTrace();
	    }
	    myImageAdapter.notifyDataSetChanged();
	    gridview.setEmptyView(getActivity().findViewById(R.id.empty_jobs_pictures_grid_view));
	}
}
