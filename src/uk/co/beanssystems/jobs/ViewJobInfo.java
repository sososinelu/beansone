package uk.co.beanssystems.jobs;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.MenuItem;

import uk.co.beanssystems.adapter.TaskItemExp;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.QitemsConnector;
import uk.co.beanssystems.database.QtasksConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.popups.AddExpensePopup;
import uk.co.beanssystems.popups.AddItemPopup;
import uk.co.beanssystems.popups.AddTaskPopup;
import uk.co.beanssystems.popups.DeleteDialog;
import uk.co.beanssystems.popups.EnterFixedPricePopup;
import uk.co.beanssystems.popups.JobDonePopup;
import uk.co.beanssystems.settings.Settings;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ViewJobInfo extends SherlockFragment {
	private Context context;
	private TextView clientName, dateStarted, totalView, qPhone, qEmail, qSMS, qMap, 
		fixedAmount, workCountTextView, itemCountTextView, expCountTextView;
	ImageButton viewTaskButton, viewItemButton, viewExpButton, taskBut, itemBut, expenseBut;
	CheckBox fixedChk;
	LinearLayout popup;
	Drawable arrowUp, arrowDown, userMini;
	TranslateAnimation animationIn, animationOut;
	private Cursor cur;
	private double totalTasks=0.0, totalItems=0.0, totalExpenses=0.0, fixedCost=0;
	private int jobID = 0, clientID;	
	ArrayList<Task> tasks = new ArrayList<Task>();
	ArrayList<Item> items = new ArrayList<Item>();	
	ArrayList<Expense> expenses = new ArrayList<Expense>();
	NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.UK);
	private String imagePath="", address, postcode, email, mobile, active = "active";
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	boolean isFixed = false;
	
	public ViewJobInfo(){
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		nf = NumberFormat.getCurrencyInstance(Locale.UK);
		jobID = getArguments().getInt("id");
		imagePath = getArguments().getString("imagePath");
		context = getActivity().getApplicationContext();
		
		//Slide Up from 100% below itself
        animationIn = new TranslateAnimation(
	            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
	            Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
	        animationIn.setDuration(200);
	        
	    //Slide Out to 100% below itself
        animationOut = new TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f);
        animationOut.setDuration(200);
        
		arrowDown = getResources().getDrawable(R.drawable.arrow_down);
		arrowUp = getResources().getDrawable(R.drawable.arrow_up);
		userMini = getResources().getDrawable(R.drawable.user_mini);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View fragView = inflater.inflate(R.layout.activity_view_job_info, container, false);
		return fragView;
	}
	
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		popup = (LinearLayout) getActivity().findViewById(R.id.quickContactPopup);
		clientName = (TextView)getActivity().findViewById(R.id.clientNameViewJobTextView);
		dateStarted = (TextView)getActivity().findViewById(R.id.dateStartedValueViewJobInfo);
		totalView = (TextView)getActivity().findViewById(R.id.totalViewJobTextView);
		fixedChk = (CheckBox)getActivity().findViewById(R.id.fixedPriceChkBoxViewJobInfo);
		taskBut = (ImageButton)getActivity().findViewById(R.id.addTaskViewJobButton);
		itemBut = (ImageButton)getActivity().findViewById(R.id.addItemViewJobButton);
		expenseBut = (ImageButton)getActivity().findViewById(R.id.addExpenseViewJobButton);
		viewTaskButton = (ImageButton) getActivity().findViewById(R.id.viewTaskViewJobButton);
        viewItemButton = (ImageButton) getActivity().findViewById(R.id.viewItemViewJobButton);
        viewExpButton = (ImageButton) getActivity().findViewById(R.id.viewExpViewJobButton);
        workCountTextView = (TextView)getActivity().findViewById(R.id.workCountViewJobTextView);
        itemCountTextView = (TextView)getActivity().findViewById(R.id.itemCountViewJobTextView);
        expCountTextView = (TextView)getActivity().findViewById(R.id.expenseCountViewJobTextView);
		qPhone = (TextView)getActivity().findViewById(R.id.mobileQCLabelTextView);
		qEmail = (TextView)getActivity().findViewById(R.id.emailQCLabelTextView);
		qSMS = (TextView)getActivity().findViewById(R.id.smsQCLabelTextView);
		qMap = (TextView)getActivity().findViewById(R.id.mapQCLabelTextView);
		
		fixedChk.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton view, boolean state) {
				if (state){
					Intent inPrice = new Intent(context, EnterFixedPricePopup.class);
					startActivityForResult(inPrice, 6);
				} else {
					JobsConnector jCon = new JobsConnector(context);
					fixedCost = 0;
					jCon.updateFixed(jobID, fixedCost);
					isFixed = false;
					fixedChk.setText("Fixed Price?");
					//fixedAmount.setText("");
					setTotals();
				}
			}
		});
		fixedAmount = (TextView)getActivity().findViewById(R.id.fixedPriceAmountViewJobInfo);
		
		JobsConnector jCon = new JobsConnector(getActivity().getApplicationContext());
		jCon.open();
		cur = jCon.getJob(jobID);
		cur.moveToFirst();
		int clientIDCol = cur.getColumnIndex(JobsConnector.CLIENTID);
        int clientCol = cur.getColumnIndex(JobsConnector.CLIENTNAME);
        int estimatedHoursCol = cur.getColumnIndex(JobsConnector.ESTIMATEDHOURS);
		int startDateCol = cur.getColumnIndex(JobsConnector.STARTDATE);
		int fixedCostCol = cur.getColumnIndex(JobsConnector.FIXEDCOST);
		int activeCol = cur.getColumnIndex(JobsConnector.ACTIVE);
		
		active = cur.getString(activeCol);
		if(active.equals("true")){
			//active true
		} else {
			fixedChk.setEnabled(false);
			taskBut.setEnabled(false);
			itemBut.setEnabled(false);
			expenseBut.setEnabled(false);
			viewTaskButton.setEnabled(false); 
	        viewItemButton.setEnabled(false); 
	        viewExpButton.setEnabled(false);
	        workCountTextView.setEnabled(false); 
	        itemCountTextView.setEnabled(false); 
	        expCountTextView.setEnabled(false);
		}
		
		fixedCost = cur.getDouble(fixedCostCol);
		if (fixedCost!=0){
			isFixed = true;
			//fixedAmount.setText(nf.format((fixedCost)));
			fixedChk.setText("Fixed Price: "+nf.format(fixedCost));
		} else {
			fixedChk.setChecked(false);
		}
		clientID = cur.getInt(clientIDCol);
		clientName.setText(cur.getString(clientCol));
		Calendar me = Calendar.getInstance();
		try {
			me = convertStringToDate(cur.getString(startDateCol));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		dateStarted.setText(outDate.format(me.getTime()));	
		jCon.close();

		ClientsConnector cliCon = new ClientsConnector(getActivity());
		cliCon.open();
		Cursor cliCur = cliCon.getClient(clientID);
		cliCur.moveToFirst();
		int mobileCol = cliCur.getColumnIndex(ClientsConnector.MOBILE);
		int emailCol = cliCur.getColumnIndex(ClientsConnector.EMAIL);
		int addressCol = cliCur.getColumnIndex(ClientsConnector.ADDRESS);
		int postcodeCol = cliCur.getColumnIndex(ClientsConnector.POSTCODE);		
		mobile = cliCur.getString(mobileCol);
		email = cliCur.getString(emailCol);
		address = cliCur.getString(addressCol);
		postcode = cliCur.getString(postcodeCol);
		cliCon.close();
		
		clientName.setOnClickListener(clickListener);
		taskBut.setOnClickListener(clickListener);
		itemBut.setOnClickListener(clickListener);
		expenseBut.setOnClickListener(clickListener);
		viewTaskButton.setOnClickListener(clickListener);
		viewItemButton.setOnClickListener(clickListener);
		viewExpButton.setOnClickListener(clickListener);
		workCountTextView.setOnClickListener(clickListener);
		itemCountTextView.setOnClickListener(clickListener);
		expCountTextView.setOnClickListener(clickListener);
		qPhone.setOnClickListener(clickListener);
		qEmail.setOnClickListener(clickListener);
		qSMS.setOnClickListener(clickListener);
		qMap.setOnClickListener(clickListener);
		
	}
		
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance(Locale.UK);
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
	
	OnClickListener clickListener = new OnClickListener() {
		public void onClick(final View v) {
			switch(v.getId()) {
			case R.id.addTaskViewJobButton:
				Intent inTask = new Intent(context, AddTaskPopup.class);
				startActivityForResult(inTask, 0);
				break;
			case R.id.addItemViewJobButton:
				Intent inItem = new Intent(context, AddItemPopup.class);
				startActivityForResult(inItem, 1);
				break;
			case R.id.addExpenseViewJobButton:
				Intent inExp = new Intent(context, AddExpensePopup.class);
				startActivityForResult(inExp, 2);
				break;
			case R.id.viewTaskViewJobButton:
				System.out.println("VIEW JOB INFO - TASKS ?>>>> "+tasks.size());
				passTasksItemsExps(0);
				break;
			case R.id.viewItemViewJobButton:
				System.out.println("VIEW JOB INFO - ITEMS ?>>>> "+items.size());
				passTasksItemsExps(1);
				break;
			case R.id.viewExpViewJobButton:
				System.out.println("VIEW JOB INFO - EXPENSES ?>>>> "+expenses.size());
				passTasksItemsExps(2);
				break;   
			case R.id.workCountViewJobTextView:
				System.out.println("VIEW JOB INFO - TASKS ?>>>> "+tasks.size());
				passTasksItemsExps(0);
				break;
			case R.id.itemCountViewJobTextView:
				System.out.println("VIEW JOB INFO - ITEMS ?>>>> "+items.size());
				passTasksItemsExps(1);
				break;
			case R.id.expenseCountViewJobTextView:
				System.out.println("VIEW JOB INFO - EXPENSES ?>>>> "+expenses.size());
				passTasksItemsExps(2);
				break;  
			case R.id.clientNameViewJobTextView:	        
				if (popup.getVisibility()==View.GONE){
					clientName.setCompoundDrawablesWithIntrinsicBounds(userMini, null, arrowDown, null);
					popup.startAnimation(animationIn);
					popup.setVisibility(View.VISIBLE);
				}else {
					clientName.setCompoundDrawablesWithIntrinsicBounds(userMini, null, arrowUp, null);
					popup.startAnimation(animationOut);
					popup.setVisibility(View.GONE);
				}
				break;
			case R.id.mobileQCLabelTextView:
				try {
					clientName.setCompoundDrawablesWithIntrinsicBounds(userMini, null, arrowDown, null);
					popup.startAnimation(animationOut);
					popup.setVisibility(View.GONE);
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("tel", mobile, null)));
				} catch (Exception e){
				}
				break;
			case R.id.smsQCLabelTextView:
				try {
					clientName.setCompoundDrawablesWithIntrinsicBounds(userMini, null, arrowDown, null);
					popup.startAnimation(animationOut);
					popup.setVisibility(View.GONE);
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", mobile, null)));
				} catch (Exception e){
				}
				break;			
			case R.id.emailQCLabelTextView:
				try {
					clientName.setCompoundDrawablesWithIntrinsicBounds(userMini, null, arrowDown, null);
					popup.startAnimation(animationOut);
					popup.setVisibility(View.GONE);
					Intent intent = new Intent(Intent.ACTION_SEND);
					intent.setType("plain/text");
					intent.putExtra(Intent.EXTRA_EMAIL,new String[] {email});
//		            intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of the mail");
//		            intent.putExtra(Intent.EXTRA_TEXT, "body of the mail");
					startActivity(Intent.createChooser(intent, "Choose Email Option"));
				} catch (Exception e){
				}
				break;	
			case R.id.mapQCLabelTextView:
				try {
					clientName.setCompoundDrawablesWithIntrinsicBounds(userMini, null, arrowDown, null);
					popup.startAnimation(animationOut);
					popup.setVisibility(View.GONE);
					String address1 = address + postcode; // Get address
					address1 = address1.replace(" ", "+");
					Intent geoIntent = new Intent (android.content.Intent.ACTION_VIEW, Uri.parse ("geo:0,0?q=" + address1)); // Prepare intent
					startActivity(geoIntent);	// Initiate lookup
				} catch (Exception e){
					Toast.makeText(getActivity(), "No Application Available to Display Maps!!", Toast.LENGTH_SHORT).show();
				}
				break;
			default:
				System.out.println("Unknown");
				break;
			}
		}
	};
	
	public void passTasksItemsExps (int currentItem){
		Intent in = new Intent(context, TaskItemExp.class);
		in.putExtra("tasksArray", tasks);
		in.putExtra("itemsArray", items);
		in.putExtra("expsArray", expenses);	
		in.putExtra("tasksIndex", 0);
		in.putExtra("itemsIndex", 0);
		in.putExtra("expensesIndex", 0);
		in.putExtra("currentItem", currentItem);
		in.putExtra("loadFrags", 3);
		in.putExtra("source", 3);
		startActivityForResult(in, 3);
	}

	public Uri getOutputMediaFileUri(){
	      return Uri.fromFile(getOutputMediaFile());
	}
	
	public File getOutputMediaFile(){
	    File mediaStorageDir = new File(
	    		Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imagePath);
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            System.out.println("Beans, failed to create directory");
	            return null;
	        }
	    }
	    
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    mediaFile = new File(mediaStorageDir.getPath() + File.separator +"IMG_"+ timeStamp + ".bmp");
	    return mediaFile;
	}
	  
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode){
		case 0: // Task
			if (resultCode == -1){
				String task = data.getStringExtra("taskName");
				double duration = Double.parseDouble(data.getStringExtra("duration"));
				double value = Double.parseDouble(data.getStringExtra("value"));
				TasksConnector tCon = new TasksConnector(context);
				tCon.insertTask(jobID, 0, task, duration, "", value);
				int tkID = tCon.getNextId();
				Task tk = new Task(tkID, task, duration, value);
				tasks.add(tk);
				tCon.close();
				if(tk.taskName.equals("Callout")){
					totalTasks += tk.value;
				}else{
					totalTasks += tk.duration * tk.value;
				}  
				setTotals();
				System.out.println("TASK ADDED");
			  	}
			  	break;
		case 1: // Items
			if (resultCode == -1){
				String item = data.getStringExtra("itemName");
				double cost = Double.parseDouble(data.getStringExtra("cost"));
				int quantity = Integer.parseInt(data.getStringExtra("quantity"));	
				double total = cost*quantity;
				ItemsConnector iCon = new ItemsConnector(context);
				iCon.insertItem(jobID, 0, 0, item, cost, "", quantity, total, 0);
				int itID = iCon.getNextId();
				Item it = new Item(itID, item, cost, quantity, total);
				items.add(it);
			 	iCon.close();
			 	totalItems += total;
				setTotals();
			 	System.out.println("ITEM ADDED");
			}
			break;
		case 2: // Expenses
			if (resultCode == -1){
				String expType = data.getStringExtra("expType");
				String expDate = data.getStringExtra("expDate");
			 	double expCost = data.getDoubleExtra("expCost", 0);
			 	String expDescription = data.getStringExtra("expDescription");
			 	String photoPath = data.getStringExtra("photoPath");
			 	ExpensesConnector eCon = new ExpensesConnector(context);
			 	eCon.insertExpense(jobID, clientID, 0, 0, 1, expType, expDate, expCost, 0.0, 0, expDescription, photoPath);
			 	int expID = eCon.getNextId();
			 	Expense exp = new Expense(expID, jobID, 0, expType, expDate, expCost, expDescription, photoPath); 
			 	double total = exp.expCost;
			 	expenses.add(exp);
			 	eCon.close();
				totalExpenses += total;
				setTotals();
			 	System.out.println("EXPENSE ADDED");
			}
			break;
		case 3: // TaskItemExp
			if (resultCode == -1){
				System.out.println("On Activity Result - View Job Info");
				tasks = (ArrayList<Task>) data.getSerializableExtra("tasksArray");
				items = (ArrayList<Item>) data.getSerializableExtra("itemsArray");
				expenses = (ArrayList<Expense>) data.getSerializableExtra("expsArray");
				// Works
				if(tasks.size() == 0 ){
					totalTasks = 0;
				}else{
					totalTasks = 0;
					for(int i=0; i<tasks.size();i++){
						Task task = tasks.get(i);
				    	if(task.taskName.equals("Callout")){
							totalTasks += task.value;
						}else{
							totalTasks += task.duration * task.value;
						} 
				    }
				}
				// Items
				if(items.size() == 0){
					totalItems = 0;
				}else{
					totalItems = 0;
					for(int i=0; i<items.size();i++){
				    	Item item = items.get(i);
				    	totalItems += item.cost * item.quantity;
				    }
				}
				// Expenses
				if(expenses.size() == 0){
					totalExpenses = 0;
				}else{
					totalExpenses = 0;
					for(int i=0; i<expenses.size();i++){
				    	Expense exp = expenses.get(i);
						totalExpenses += exp.expCost;
				    }
				}
				setTotals();
			}
			break;
		case 5: //Camera returned
			context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
			break;
		//Fixed Price Entered
		case 6:
			if (resultCode == -1){
				fixedCost = data.getDoubleExtra("fixedPrice", 0);
				JobsConnector jCon = new JobsConnector(context);
				jCon.updateFixed(jobID, fixedCost);
				isFixed = true;
				fixedChk.setText("Fixed Price: "+nf.format(fixedCost));
				//fixedAmount.setText(nf.format(fixedCost));
				setTotals();
			} else {
				fixedCost = 0;
				isFixed = false;
				fixedChk.setChecked(false);
				setTotals();
			}
			break;
		default:
			System.out.println("No code matched");
			break;
		}
	}
	
	public void onStart(){
		super.onStart();
		tasks = new ArrayList<Task>();
		items = new ArrayList<Item>();
		expenses = new ArrayList<Expense>();
		totalTasks=0.0;
		totalItems=0.0;
		totalExpenses=0.0;
		TasksConnector tCon = new TasksConnector(context);
		try {
			cur = tCon.getTasksByJob(jobID);
			cur.moveToFirst();
			int taskIdCol = cur.getColumnIndex(TasksConnector.TASKID);
			int taskNameCol = cur.getColumnIndex(TasksConnector.TASKNAME);
			int durationCol = cur.getColumnIndex(TasksConnector.ACTUALDURATION);
			int chargeValueCol = cur.getColumnIndex(TasksConnector.CHARGEVALUE);
			do{
				Task tsk = new Task(cur.getInt(taskIdCol), cur.getString(taskNameCol), cur.getDouble(durationCol), cur.getDouble(chargeValueCol));
				tasks.add(tsk);
				if(tsk.taskName.equals("Callout")){
					totalTasks += tsk.value;
				}else{
					totalTasks += tsk.duration * tsk.value;
				} 
			} while (cur.moveToNext());
		} catch (Exception e){
			System.out.println("No tasks found for this job");
		}
		tCon.close();
		ItemsConnector iCon = new ItemsConnector(context);
		try {
			cur = iCon.getItemsByJob(jobID);
			cur.moveToFirst();
			int itemIdCol = cur.getColumnIndex(ItemsConnector.ITEMID);
			int itemNameCol = cur.getColumnIndex(ItemsConnector.ITEMNAME);
			int costCol = cur.getColumnIndex(ItemsConnector.TOTALCOST);
			int unitCol = cur.getColumnIndex(ItemsConnector.UNITCOST);
			int quantityCol = cur.getColumnIndex(ItemsConnector.QUANTITY);
			do{
				Item it = new Item(cur.getInt(itemIdCol), cur.getString(itemNameCol), cur.getDouble(unitCol), cur.getInt(quantityCol), cur.getDouble(costCol));
				items.add(it);
				totalItems += it.cost * it.quantity;
			} while (cur.moveToNext());
		} catch (Exception e){
			System.out.println("No items found for this job");
		}
		iCon.close();
		ExpensesConnector eCon = new ExpensesConnector(context);
		eCon.open();
		try { 
			cur = eCon.getExpsByJob(jobID);
			System.out.println(cur.getCount());
			cur.moveToFirst();
			int expIdCol = cur.getColumnIndex(ExpensesConnector.EXPENSEID);
			int expTypeCol = cur.getColumnIndex(ExpensesConnector.EXPTYPE);
			int expDateCol = cur.getColumnIndex(ExpensesConnector.DATE);
			int expCostCol = cur.getColumnIndex(ExpensesConnector.COST); 
			int expDescCol = cur.getColumnIndex(ExpensesConnector.DESCRIPTION); 
			int expPhotoCol = cur.getColumnIndex(ExpensesConnector.PHOTO); 
			do {	
				double expCost = cur.getDouble(expCostCol);
				Expense exp = new Expense(cur.getInt(expIdCol), 0, 0, cur.getString(expTypeCol), cur.getString(expDateCol), expCost, cur.getString(expDescCol), cur.getString(expPhotoCol)); 
				expenses.add(exp);
				totalExpenses += expCost;		
			} while(cur.moveToNext());
		} catch (Exception e){
			System.out.println("No Expenses found for this job");
		}
		eCon.close();
		setTotals();
	}
	
	public void setTotals (){
		if (isFixed){
			totalView.setText("Total: "+String.valueOf(nf.format(fixedCost)));
		} else {
			double sub = totalTasks + totalItems + totalExpenses;
			double overallTotal = sub;
			totalView.setText("Total: "+nf.format(overallTotal));
		}
		
		double hours=0.0;
		for (int i=0;i<tasks.size();i++){
			hours += tasks.get(i).duration;
		}
		workCountTextView.setText("Work: "+String.valueOf(hours)+" Hrs");
		int quantity=0;
		for (int i=0;i<items.size();i++){
			quantity += items.get(i).quantity;
		}
		itemCountTextView.setText("Items: "+String.valueOf(quantity));
		expCountTextView.setText("Expenses: "+String.valueOf(expenses.size()));
	}
}