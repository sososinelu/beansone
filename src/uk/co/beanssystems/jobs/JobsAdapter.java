package uk.co.beanssystems.jobs;

import java.util.ArrayList;

import uk.co.beanssystems.beansone.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class JobsAdapter extends ArrayAdapter<JobsListItem> {
	Context context;
	int layoutResourceId;
	ArrayList<JobsListItem> items = null;
	
	public JobsAdapter (Context context, int layoutResourceId, ArrayList<JobsListItem> items){
		super(context, layoutResourceId, items);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.items = items;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
        JobsListItemHolder holder = null;      
        
        if(row == null){
        	LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	        row = inflater.inflate(layoutResourceId, parent, false);
	        holder = new JobsListItemHolder();
	        holder.icon = (ImageView)row.findViewById(R.id.list_row_job_image);
	        holder.arrow = (ImageView)row.findViewById(R.id.list_row_job_arrow);
	        holder.client = (TextView)row.findViewById(R.id.list_row_job_client);
	        holder.jobName = (TextView)row.findViewById(R.id.list_row_job_name);
	        holder.startDate = (TextView)row.findViewById(R.id.list_row_job_date);
	        holder.jobType = (TextView)row.findViewById(R.id.list_row_job_type);
	        row.setTag(holder);
	    } else {
        	holder = (JobsListItemHolder)row.getTag();
       	}
        
        JobsListItem job = items.get(position);
        if (job.rowType==0){
        	row.setBackgroundColor(Color.TRANSPARENT);
            holder.jobName.setVisibility(View.VISIBLE);
            holder.startDate.setVisibility(View.VISIBLE);
            holder.jobType.setVisibility(View.VISIBLE);
            holder.icon.setVisibility(View.VISIBLE);
            holder.arrow.setVisibility(View.VISIBLE);
            holder.client.setText(job.client);
            holder.jobName.setText(job.jobName);
            holder.startDate.setText(job.startDate);
            holder.jobType.setText(job.jobType);
            holder.icon.setImageResource(job.icon);
        } else {
        	row.setBackgroundColor(Color.parseColor("#C3D69B"));
            holder.client.setText(job.client);
            holder.jobName.setVisibility(View.GONE);
            holder.startDate.setVisibility(View.GONE);
            holder.jobType.setVisibility(View.GONE);
            holder.icon.setVisibility(View.GONE);
            holder.arrow.setVisibility(View.GONE);
        }
        return row;       
	}
	
	static class JobsListItemHolder
	   {
	       ImageView icon;
	       ImageView arrow;
	       TextView client;
	       TextView jobName;
	       TextView startDate;
	       TextView jobType;
	   }
}
