package uk.co.beanssystems.jobs;

import java.io.Serializable;

public class Task implements Serializable{
	private static final long serialVersionUID = 1L;
	public int taskID;
	public int jobID;
	public int invoiceID;
	public String taskName;
	public double duration;
	public String chargeType;
	public double value;
	public boolean dead;
	
	public Task(){
		super();
		this.dead = false;
	}
	
	public Task (int taskID, int jobID, int invoiceID, String taskName, double duration, double value){
		super();
		this.taskID = taskID;
		this.invoiceID = invoiceID;
		this.jobID = jobID;
		this.taskName = taskName;
		this.duration = duration;
		this.value = value;
		this.dead = false;
	}
	
	public Task(String taskName, double duration, double value){
		super();
		this.taskName = taskName;
		this.duration = duration;
		this.value = value;
		this.dead = false;
	}
	
	public Task(int id, String taskName, double duration, double value){
		super();
		this.taskID = id;
		this.taskName = taskName;
		this.duration = duration;
		this.value = value;
		this.dead = false;
	}
	
	public void setTaskName(String taskName){
		this.taskName = taskName;
	}
	
	public void setTaskDuration(double duration){
		this.duration = duration;
	}
	
	public void setTaskValue(double value){
		this.value = value;
	}
}
