package uk.co.beanssystems.jobs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.settings.Settings;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class JobsList extends SherlockActivity {
	private ListView jobsListView;
	private EditText searchEditText;
	private ImageView clearSearch;
	ArrayList<JobsListItem> jobsArray = null;
	ArrayList<JobsListItem> jobsFixed = null;
	private ArrayList<String> jobsSearchArray;
	private Cursor cur;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs_list);        
        ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.jobs);
	    bar.setDisplayHomeAsUpEnabled(true);
    }

	public void loadJobs(){
		JobsConnector jobs = new JobsConnector(this);

		String[] headers = new String[] {"Current", "Today", "This Week", "Later"};
        jobsArray = new ArrayList<JobsListItem>();
        jobsFixed = new ArrayList<JobsListItem>();
        jobsSearchArray = new ArrayList<String>();
        searchEditText = (EditText)findViewById(R.id.jobListSearch);
		clearSearch = (ImageView)findViewById(R.id.clearSearchJobs);
        jobsListView = (ListView)findViewById(R.id.jobs_list);
        
        ArrayList<JobsListItem> jobsList = null;
        for (int i=0;i<4;i++){
        	switch (i){
	        	case 0:
	        		jobsList = jobs.getJobsCurrent(0);
	        		break;
	        	case 1:
	        		jobsList = jobs.getJobsToday(0);
	        		break;
	        	case 2:
	        		jobsList = jobs.getJobsWeek(0);
	        		break;
	        	case 3:
	        		jobsList = jobs.getJobsLater(0);
	        		break;
        		default:break;
        	}
        	
	        boolean empty = true;
			JobsListItem jobSep = new JobsListItem(1, 0, R.drawable.user, headers[i], "", "", "", "");
	    	jobsArray.add(jobSep);
			for (JobsListItem job : jobsList){
	            String string = job.client +" "+job.jobName;
	      		jobsSearchArray.add(string.toLowerCase());
	        	jobsArray.add(job);
	        	jobsFixed.add(job);
	        	empty = false;
	        }
			if (empty){
				jobsArray.remove(jobsArray.size()-1);
			}
        }
		
        jobsAdapter();
        jobsSearch();
	}
	
	
	
	public void loadUnactiveJobs(){
		JobsConnector jobs = new JobsConnector(this);
		ClientsConnector cliCon = new ClientsConnector(this);
		cliCon.open();
		Cursor cliCur = null;
        jobsArray = new ArrayList<JobsListItem>();
        jobsFixed = new ArrayList<JobsListItem>();
        jobsSearchArray = new ArrayList<String>();
        jobs.open();
        cur = jobs.getAllUnactiveJobs();
        if (cur.getCount()>0){
    		cur.moveToFirst();
    		int idCol = cur.getColumnIndex(JobsConnector.JOBID);
    		int clientIDCol = cur.getColumnIndex(JobsConnector.CLIENTID);
	        int clientCol = cur.getColumnIndex(JobsConnector.CLIENTNAME);
	        int jobNameCol = cur.getColumnIndex(JobsConnector.JOBNAME);
	        int repeatCol = cur.getColumnIndex(JobsConnector.REPEAT);
			int startDateCol = cur.getColumnIndex(JobsConnector.STARTDATE);
			int activeCol = cur.getColumnIndex(JobsConnector.ACTIVE);
			
    		do{
    			int id = cur.getInt(idCol);
                String startDate = cur.getString(startDateCol);
                String clientName = cur.getString(clientCol);
            	String jobName = cur.getString(jobNameCol);
                String repeat = cur.getString(repeatCol);
                String active = cur.getString(activeCol);
                Calendar startCal = Calendar.getInstance();
				try {
					startCal = convertStringToDate(startDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
                String string = clientName +" "+jobName;
                
                cliCur = cliCon.getSingleClientName(cur.getInt(clientIDCol));
                cliCur.moveToFirst();
                int companyCol = cliCur.getColumnIndex(ClientsConnector.COMPANY);
                String company = cliCur.getString(companyCol);
                
                int icon;
	        	if (company.compareTo("Private")==0){
	        		icon = R.drawable.user;
	        	} else {
	        		icon = R.drawable.work_user;
	        	}
                
          		jobsSearchArray.add(string.toLowerCase());
            	JobsListItem job = new JobsListItem(0, id, icon, clientName, jobName, outDate.format(startCal.getTime()), repeat, active);
            	jobsArray.add(job);
            	jobsFixed.add(job);

            } while(cur.moveToNext());
    		
    	}
        
        jobsAdapter();
        jobsSearch();
        jobs.close();
        cliCon.close();
	}       
	
	public void jobsAdapter() {
		JobsAdapter adapterToday = new JobsAdapter(this, R.layout.activity_jobs_list_row, jobsArray);
        jobsListView.setEmptyView(findViewById(R.id.empty_jobs_list_view));
        jobsListView.setAdapter(adapterToday);
        jobsListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				JobsListItem j = (JobsListItem) jobsListView.getItemAtPosition(position);
				if (j.rowType==0){
					Intent i = new Intent(getApplicationContext(), ViewJob.class);
					i.putExtra("jobID", j.id);
					i.putExtra("jobName", j.jobName);
					//i.putExtra("imagePath", j.id+"_"+j.jobName.replace(" ", "_"));
					startActivity(i);
				}
			}
        });
	}
	
	public void jobsSearch() {
		 searchEditText.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {	
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				jobsArray.clear();
				
				String search = searchEditText.getText().toString().toLowerCase();
				
				if(searchEditText.getText().toString().length() == 0){
					clearSearch.setVisibility(View.GONE);
				}else{
					clearSearch.setVisibility(View.VISIBLE);
				}
				
				boolean exist = false;		

				int searchListLength = jobsSearchArray.size();
				for (int i = 0; i < searchListLength; i++) {
					if (jobsSearchArray.get(i).contains(search)) {	
						add (i);
						exist = true;
					}
				} 

				if (exist == false) {

				}
				
				JobsAdapter adapterT = new JobsAdapter(JobsList.this, R.layout.activity_jobs_list_row, jobsArray);
		        jobsListView.setAdapter(adapterT);
			}
		});
		
	}
	
	public void add (int i){
		JobsListItem j = (JobsListItem) jobsFixed.get(i); 
		jobsArray.add(j);
	}
	
	@Override
	public void onStart(){
		super.onStart();
		loadJobs();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    MenuItem sortAll = menu.findItem(R.id.menuSortAll);
	    MenuItem sort1 = menu.findItem(R.id.menuSort1);
	    MenuItem sort2 = menu.findItem(R.id.menuSort2);
	    MenuItem sort3 = menu.findItem(R.id.menuSort3);
	    MenuItem sort4 = menu.findItem(R.id.menuSort4);
	    sortAll.setVisible(false);
	    sort1.setTitle("Active");
	    sort2.setTitle("Finished");
	    sort3.setVisible(false);
	    sort4.setVisible(false);
	    return true;
	}
	  
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	    	Intent intent = new Intent(this, MainActivity.class);            
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	        startActivity(intent);
	        finish();
	    	return true;	    	
	    case R.id.search_option:
	    	RelativeLayout item1 = (RelativeLayout)findViewById(R.id.jobListLayout);
	    	if(item1.getVisibility() == View.GONE){
	    		item1.setVisibility(View.VISIBLE);
	    	}else{
	    		item1 .setVisibility(View.GONE);
	    		searchEditText.setText("");
	    		//loadJobs();
	    	}  	
	    	return true;	    	
	    case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  	        
	    case R.id.about_option:
	    	return true;	    	
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    case R.id.menuSortAll:
//	    	JobsConnector jCon = new JobsConnector(this);
//	    	jCon.updateActive(1, "false");
	    	return true;
	    case R.id.menuSort1:
	    	loadJobs();
	    	return true;
	    case R.id.menuSort2:
	    	loadUnactiveJobs();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	public void sortList (String sort){
		jobsArray.clear();
    	for (int i = 0; i < jobsFixed.size(); i++) {
    		JobsListItem j = (JobsListItem) jobsFixed.get(i); 
			if (j.active.equals("true")) {	
				add (i);
			}else{
				add (i);
			}
    	}
    	JobsAdapter adapterT = new JobsAdapter(JobsList.this, R.layout.activity_jobs_list_row, jobsArray);
        jobsListView.setAdapter(adapterT);
	}
	
//	public void sortList (String sort){
//		jobsArray.clear();
//    	for (int i = 0; i < jobsFixed.size(); i++) {
//    		if(sort.equals("all")){
//    			add (i);
//    		}else{
//    			JobsListItem j = (JobsListItem) jobsFixed.get(i); 
//    			if (j.active.equals(sort)) {	
//    				add (i);
//    			}
//    		}
//		} 
//    	JobsAdapter adapterT = new JobsAdapter(JobsList.this, R.layout.activity_jobs_list_row, jobsArray);
//        jobsListView.setAdapter(adapterT);
//	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.clearSearchJobs:			
			searchEditText.setText("");
			break;
		case R.id.newJobsListButton:			
			Intent i = new Intent(this, NewJob.class);
	    	startActivity(i);
          	break;
		default:
			break;
		}
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance(Locale.UK);
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    //cal.roll(Calendar.MONTH, 1);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
}
