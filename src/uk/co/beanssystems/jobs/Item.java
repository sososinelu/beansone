package uk.co.beanssystems.jobs;

import java.io.Serializable;

public class Item implements Serializable{
	private static final long serialVersionUID = -3401960975890617617L;
	public int itemID;
	public int jobID;
	public int invoiceID;
	public String itemName;
	public double cost;
	public int quantity;
	public double total;
	public double totalCost;
	public double totalVAT;
	public boolean incVAT;
	public boolean dead;
	
	public Item(){
		super();
		this.dead = false;
	}
	
	public Item(int itemId, int jobID, int invoiceID, String name, int quantity, double unitCost){
		super();
		this.itemID = itemId;
		this.jobID = jobID;
		this.invoiceID = invoiceID;
		this.itemName = name;
		this.quantity = quantity;
		this.cost = unitCost;
		this.dead = false;
	}
	
	public Item(String itemName, double cost, int quantity){
		super();
		this.itemName = itemName;
		this.cost = cost;
		this.quantity = quantity;
		this.total = cost*quantity;
		this.dead = false;
	}
	
	public Item(int id, String itemName, double cost, int quantity, double total){
		super();
		this.itemID = id;
		this.itemName = itemName;
		this.cost = cost;
		this.quantity = quantity;
		this.total = total;
		this.dead = false;
	}
	
	public void setItemName(String itemName){
		this.itemName = itemName;
	}
	
	public void setItemCost(double cost){
		this.cost = cost;
	}
	
	public void setItemQuantity(int quantity){
		this.quantity = quantity;
	}
	
	public void setItemTotal(double total){
		this.total = total;
	}
}
