package uk.co.beanssystems.jobs;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.beanssystems.adapter.TaskItemExp;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.JobsProvider;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.popups.AddItemPopup;
import uk.co.beanssystems.popups.AddTaskPopup;
import uk.co.beanssystems.popups.SelectClientPopup;
import uk.co.beanssystems.settings.Settings;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class NewJob extends SherlockFragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {
	EditText clientName, jobNameEditText, startDate, description, fixedCost;
	TextView dummyFocus, subtotal, taxes, total, workCount, itemsCount;
	ListView jobNameList;
	RelativeLayout item1;
	CheckBox fixedChk;
	boolean isFixed = false, stopDrop = false;
	JobsConnector job = null;
	private CursorAdapter adapter;
	Context context;
	private ArrayList<Task> tasks = new ArrayList<Task>();
	private ArrayList<Item> items = new ArrayList<Item>();
	ArrayList<String> estHrsSpinnerArray = new ArrayList<String>();
	String jobName, hours="", fixed="";
	private int clientID, quoteID, jobNameSize = 0;
	private String client;
	private static final String ID = "rowid _id";
	private NumberFormat nf;
	double globalVAT = 20.0, totalTasks, totalTasksVAT, totalItems, totalItemsVAT, finalSubtotal, finalTotal, finalVat, fixedAmount=0;
	private Cursor tasksCur, itemsCur;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	private ComponentName sourceClass;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_job);
		nf = NumberFormat.getCurrencyInstance(Locale.UK);
		try {
			sourceClass = getCallingActivity();
		} catch (Exception e){
			sourceClass = null;
		}
        ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.jobs);
	    bar.setDisplayHomeAsUpEnabled(true);
		if (savedInstanceState != null){
			//Restore tasks
			@SuppressWarnings("unchecked")
			ArrayList<Task> tempTasks = (ArrayList<Task>) savedInstanceState.getSerializable("tasks");
			for (int i = 0;i<tempTasks.size();i++){
				Task tsk = tempTasks.get(i);
				tasks.add(tsk);
			}
			//Restore Items
			@SuppressWarnings("unchecked")
			ArrayList<Item> tempItems = (ArrayList<Item>) savedInstanceState.getSerializable("items");
			for (int i = 0;i<tempItems.size();i++){
				Item it = tempItems.get(i);
				items.add(it);
			}
		}
		clientName = (EditText) findViewById(R.id.addClientNewJobEditText);
		jobNameEditText = (EditText) findViewById(R.id.jobNameNewJobEditText);
		startDate = (EditText) findViewById(R.id.startDateNewJobEditText);
		description = (EditText) findViewById(R.id.descriptionNewJobEditText);
		dummyFocus = (TextView) findViewById(R.id.middle);
		jobNameList = (ListView) findViewById(R.id.suggestionsList);
		subtotal = (TextView) findViewById(R.id.subTotalValueNewJobTextView);
		taxes = (TextView) findViewById(R.id.taxesValueNewJobTextView);
		total = (TextView) findViewById(R.id.totalValueNewJobTextView);
        fixedChk = (CheckBox) findViewById(R.id.fixedCostChkNewJob);
        workCount = (TextView) findViewById(R.id.workCountNewJobTextView);
        itemsCount = (TextView) findViewById(R.id.itemCountNewJobTextView);
        fixedCost = (EditText) findViewById(R.id.fixedCostAmountNewJobEditText);      
        fixedChk.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton view, boolean state) {
				if(state){
					fixedChk.setText("");
					fixedCost.setVisibility(View.VISIBLE);
					isFixed = true;
					setTotals();
				}else {
					fixedChk.setText(getResources().getString(R.string.fixedCost));
					fixedCost.setVisibility(View.GONE);
					isFixed = false;
					setTotals();
				}
			}
		});
		
		fixedCost.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {	 
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}
			
			public void afterTextChanged(Editable s) {
				double amount = 0;
				try {
					amount = Double.parseDouble(fixedCost.getText().toString());
					fixedAmount = amount;
				} catch(Exception e){
					
				}
				total.setText("Total: "+String.valueOf(nf.format(amount)));
			}	
		});
		// Set todays date in Start Date edit text
		Calendar today = Calendar.getInstance();
		startDate.setText(outDate.format(today.getTime()));
		
		Intent i = getIntent();
		if (i.getIntExtra("vcjclientID", 0)!=0){
			clientID = i.getIntExtra("vcjclientID", 0);
			getClient(clientID);
		}else if(i.getIntExtra("passClientId", 0)!=0){
			quoteID = i.getIntExtra("passQuoteId", 0);
			clientID = i.getIntExtra("passClientId", 0);
			getClient(clientID);
			jobNameEditText.setText(i.getStringExtra("passJobName"));
			fixedAmount = i.getDoubleExtra("passFixedCost", 0);
			if (fixedAmount!=0){
				isFixed = true;
				fixedChk.setChecked(true);
				fixedCost.setText(String.valueOf(fixedAmount));
			}
			//finalSubtotal, finalTotal, finalVat
			totalTasks = i.getDoubleExtra("totalTasks", 0.0);
		    totalItems = i.getDoubleExtra("totalItems", 0.0);
			tasks = (ArrayList<Task>) i.getSerializableExtra("tasks");
			items = (ArrayList<Item>) i.getSerializableExtra("items");
			setTotals();
			stopDrop = true;
			Toast.makeText(this, "Quote details imported, press save to make this a Job", Toast.LENGTH_LONG).show();
		}
		
		if (!stopDrop){
			context = this;
			adapter = new SimpleCursorAdapter(this,
					android.R.layout.simple_list_item_1, 
					null, 
					new String[]{JobsConnector.JOBNAME}, 
					new int[]{android.R.id.text1,}, 
					Adapter.NO_SELECTION);
			
			jobNameList.setAdapter(adapter);
			Bundle b = new Bundle();
			b.putString("str", "");
			getSupportLoaderManager().initLoader(0, b, this);
			item1 = (RelativeLayout)findViewById(R.id.suggestionsLayout);
			jobNameEditText.addTextChangedListener(new TextWatcher() {
				public void onTextChanged(CharSequence s, int start, int before, int count) {	
				}
				
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				}
				
				public void afterTextChanged(Editable s) {
					adapter.swapCursor(null);
					Bundle bundle = new Bundle();
		    		bundle.putString("str", s.toString());
		        	getSupportLoaderManager().restartLoader(0, bundle, (LoaderCallbacks<Cursor>) context);
		        	
					jobName = jobNameEditText.getText().toString().toLowerCase(); 		
				}	
			});
			jobNameList.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					String projection[] = {ID, JobsConnector.JOBID, JobsConnector.JOBNAME, JobsConnector.ESTIMATEDHOURS, JobsConnector.DESCRIPTION};
				    Cursor cur = getContentResolver().query(Uri.withAppendedPath(JobsProvider.CONTENT_URI, String.valueOf(id)), projection, null, null, null);
				    if (cur.moveToFirst()) {
				    	jobNameEditText.setText(cur.getString(2));				
						description.setText(cur.getString(4));
						tasks = new ArrayList<Task>();
						items = new ArrayList<Item>();
						totalItems = 0;
						totalTasks = 0;
						getTasks(cur.getInt(1));
						getItems(cur.getInt(1));
						jobNameSize = jobName.length();
						item1.setVisibility(View.GONE);
					    cur.close();
				    } else {
				    	cur.close();
				    }
				}
	        });
		}
	}

	public String stripDecimal(String str){
		String[] amount = str.split("\\.");
		if(amount[1].equals("0")){
			System.out.println("Entered Integer: "+amount[0]);
			return amount[0];
		}else{
			return amount[0]+"."+amount[1];
		}
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
		String search = bundle.getString("str").replace("'", "''");
		CursorLoader curLoader = null;
		curLoader = new CursorLoader(this, 
				Uri.parse(JobsProvider.CONTENT_URI.toString()), //CONTENT URI
				new String[]{ID, JobsConnector.JOBNAME, JobsConnector.ESTIMATEDHOURS, JobsConnector.DESCRIPTION, JobsConnector.STARTDATE}, //PROJECTION
				JobsConnector.JOBNAME +" like '%"+search+"%' AND startDate BETWEEN date('now', '-120 days') AND date('now')", //SELECTION
				null, //SELECTION ARGUMENTS
				null); //SORT ORDER
		return curLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		item1 = (RelativeLayout)findViewById(R.id.suggestionsLayout);
		if (cursor.getCount()!=0 && jobNameEditText.length()>=3 && jobNameSize != jobNameEditText.length()){
			item1.setVisibility(View.VISIBLE);
		} else {
			item1.setVisibility(View.GONE);
		}
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {
		adapter.swapCursor(null);
	}
	
	public void getTasks(int id){
		TasksConnector tasksCon = new TasksConnector(this);
		tasksCon.open();
		try{
			tasksCur = tasksCon.getTasksByJob(id);
			tasksCur.moveToFirst();
			int taskNameCol = tasksCur.getColumnIndex(TasksConnector.TASKNAME);
			int actualDurationCol = tasksCur.getColumnIndex(TasksConnector.ACTUALDURATION);
			int chargeValueCol = tasksCur.getColumnIndex(TasksConnector.CHARGEVALUE);
			do{		
				Task tsk = new Task(tasksCur.getString(taskNameCol), tasksCur.getDouble(actualDurationCol), tasksCur.getDouble(chargeValueCol));
				tasks.add(tsk);
				if(tsk.taskName.equals("Callout")){
					totalTasks += tsk.value;
				}else{
					totalTasks += tsk.duration * tsk.value;
				}
				
				System.out.println("TASK ADDED");
			}while(tasksCur.moveToNext());
			setTotals();
		} catch (Exception e) { 
	    	System.out.println("++++++No Tasks+++++");
	    }
		tasksCon.close();
	}
	
	public void getItems(int id){
		ItemsConnector itemsCon = new ItemsConnector(this);
		itemsCon.open();
		try{
			itemsCur = itemsCon.getItemsByJob(id);
			itemsCur.moveToFirst();
			int itemNameCol = itemsCur.getColumnIndex(ItemsConnector.ITEMNAME);
			int unitCostCol = itemsCur.getColumnIndex(ItemsConnector.UNITCOST);
			int quantityCol = itemsCur.getColumnIndex(ItemsConnector.QUANTITY);
			do {
				Item it = new Item( itemsCur.getString(itemNameCol), itemsCur.getDouble(unitCostCol), itemsCur.getInt(quantityCol));
				items.add(it);
				totalItems += it.cost * it.quantity;
				System.out.println("ITEM ADDED");
			}while (itemsCur.moveToNext());	
			setTotals();
		} catch (Exception e) { 
        	System.out.println("++++++No Items+++++");
        }
		itemsCon.close();
	}
	
	public void setTotals (){
		if (isFixed){
			total.setText("Total: " + String.valueOf(nf.format(fixedAmount)));
		} else {
			finalTotal =  totalTasks + totalItems;
			total.setText("Total: " + String.valueOf(nf.format(finalTotal)));
		}
		double hours=0.0;
		for (int i=0;i<tasks.size();i++){
			hours += tasks.get(i).duration;
		}
		workCount.setText("Work: "+String.valueOf(hours)+" Hrs");
		int quantity=0;
		for (int i=0;i<items.size();i++){
			quantity += items.get(i).quantity;
		}
		itemsCount.setText("Items: "+String.valueOf(quantity));
	}
	
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("tasks", tasks);
        outState.putSerializable("items", items);
    }
    
	  @Override
	  public boolean onCreateOptionsMenu(Menu menu) {
		  MenuInflater inflater = getSupportMenuInflater();
		  inflater.inflate(R.menu.menu_options, menu);
		  return true;
	  }  
		public Calendar convertStringToDate(String strDate) throws ParseException{
		    Calendar cal = Calendar.getInstance(Locale.UK);
		    Date date = outDate.parse(strDate);
		    cal.setTime(date);
		    cal.set(Calendar.MINUTE, 0);
		    return cal;
		}
		
	  public void addJob(){
		  job = new JobsConnector(this);
		  String billingType="hourly";
		  
		  if (isFixed){
			  billingType="fixed";
		  } else {
			  fixedAmount = 0;
		  }
		  
		  Calendar me = Calendar.getInstance(Locale.UK);
		  try {
			  me = convertStringToDate(startDate.getText().toString());
		  } catch (ParseException e1) {
			  e1.printStackTrace();
		  }
		  String dateIn = inDate.format(me.getTime());
    	  try{
			  job.insertJob(clientID, client, jobNameEditText.getText().toString(), 0, 0, billingType, fixedAmount, dateIn, dateIn, "", description.getText().toString());	    	  	
	    	  int thisJob = job.getNextId();
			  //INSERT TASKS	    
			  TasksConnector tCon = new TasksConnector(this);
	    	  for (int i=0;i<tasks.size();i++){
	    		  Task task = tasks.get(i);
	    		  tCon.insertTask(thisJob, 0, task.taskName , task.duration,  "", task.value);
	    	  }	    	  
	    	  //INSERT ITEMS
	    	  ItemsConnector iCon = new ItemsConnector(this);
	    	  for (int i=0;i<items.size();i++){
	    		  Item item = items.get(i);
	    		  iCon.insertItem(thisJob, 0, 0, item.itemName, item.cost, "", item.quantity, item.total, 0.0);
	    	  } 	  
	    	  QuotesConnector quote = new QuotesConnector(this);
	    	  quote.updateStatus(quoteID, "converted");
	    	  quote.updateJobID(quoteID, thisJob);
    	  } catch(Exception e){
    		  
    	  }
      }
	 
	  public void validation (final EditText text){
			if(text.getText().length()==0){
				text.setError("Please Complete Field");
			} else {
				text.setError(null,null);
			}
			text.addTextChangedListener(new TextWatcher() {	 
			   public void afterTextChanged(Editable s) {
				   if(text.getText().length()==0){
					   text.setError("Please Complete Field");
				   }else{
					   text.setError(null,null);
				   }
			   }
			 
			   public void beforeTextChanged(CharSequence s, int start, 
			     int count, int after) {
			   }
			 
			   public void onTextChanged(CharSequence s, int start, 
			     int before, int count) {

			   }
			});
		}
	  
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
		 switch (item.getItemId()) {
	     case R.id.add_option:
	    	 item.setEnabled(false);
			 if (clientName.getText().length()==0 || jobNameEditText.getText().length()==0 || startDate.getText().length()==0 ){
				 item.setEnabled(true);
				 validation(clientName);
				 validation(jobNameEditText);
				 validation(startDate);
			 } else {
				 addJob();
				 if (sourceClass == null) {
					 Intent i = new Intent(this, JobsList.class);
					 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 startActivity(i);
					 finish();
		    	 }else{
		    		 Intent i = new Intent(this, sourceClass.getClass());
		    		 i.putExtra("passJobName", jobNameEditText.getText().toString());
		    	  	 i.putExtra("passClientID",clientID);
		    	  	 i.putExtra("passClientName",clientName.getText().toString());
		    	  	 i.putExtra("passJobId", job.getNextId());
		    		 setResult(RESULT_OK,i);
		    		 finish();
		    	 }
			 }
			 return true;
	     case android.R.id.home:
	    	final Builder builder = new AlertDialog.Builder(this);		
			TextView title = new TextView(this);
			title.setText("Job details not saved!");
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			title.setTextSize(20);
			builder.setCustomTitle(title);
			TextView msg = new TextView(this);
			msg.setText("Are you sure you want to go back?");
			msg.setPadding(10, 10, 10, 10);
			msg.setGravity(Gravity.CENTER);
			msg.setTextSize(18);
			builder.setView(msg);
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
			        finish();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			AlertDialog dialog = builder.create();
			if (clientName.getText().length()!=0 || jobNameEditText.getText().length()!=0 || description.getText().length()!=0
					|| fixedCost.getText().length()!=0 || tasks.size()>0 || items.size()>0){
				dialog.show();
			} else {
				finish();
			}
	     	 return true;
	     case R.id.settings_option:
	    	 Intent in = new Intent(this, Settings.class);
		     startActivity(in);
		     return true;  
		 case R.id.about_option:
			 return true;
		     case R.id.help_option:
		     Intent ih = new Intent(this, Help.class);
		     startActivity(ih);
		     return true;	 
		 case R.id.home_option:
			 Intent iHome = new Intent(this, MainActivity.class);
		     iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		     startActivity(iHome);
		     finish();
		     return true;
	     default:
	    	 return super.onOptionsItemSelected(item);
		 }
	}
	 
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 switch (requestCode){
		 //Task Added
		 case 0:
			 getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			 if (resultCode==RESULT_OK){
				 Task myTask = new Task(data.getStringExtra("taskName"), Double.parseDouble(data.getStringExtra("duration")), 
						 Double.parseDouble(data.getStringExtra("value")));
				 tasks.add(myTask);
				 System.out.println("TASK ADDED");
				 dummyFocus.requestFocus();
				 if(myTask.taskName.equals("Callout")){
					 totalTasks += myTask.value;
				 }else{
				     totalTasks += myTask.duration * myTask.value;
				 }
				 setTotals();
			 }
			 break;
		//Item Added
		 case 1:
			 getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			 if (resultCode==RESULT_OK){	
				 Item myItem = new Item(data.getStringExtra("itemName"), Double.parseDouble(data.getStringExtra("cost")), 
						 Integer.parseInt(data.getStringExtra("quantity")));
				 items.add(myItem);
				 System.out.println("ITEM ADDED");
				 dummyFocus.requestFocus();
				 totalItems = myItem.cost * myItem.quantity;
				 setTotals();
			 }
			 break;
		 case 2:
			 getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			 if (resultCode==RESULT_OK){
				 tasks = (ArrayList<Task>) data.getSerializableExtra("tasksArray");
				 items = (ArrayList<Item>) data.getSerializableExtra("itemsArray");
				 if(tasks.size() == 0 ){
					 totalTasks = 0;
				 }else{
					 totalTasks = 0;
					 for(int i=0; i<tasks.size();i++){
						 Task task = tasks.get(i);
						 if(task.taskName.equals("Callout")){
							 totalTasks += task.value;
						 }else{
						     totalTasks += task.duration * task.value;
						 }
					 }
				 }
				 if(items.size() == 0){
					 totalItems = 0;
				 } else {
					 totalItems = 0;
					 for(int i=0; i<items.size();i++){
						 Item item = items.get(i);
						 totalItems += item.cost * item.quantity;
					 }
				 }
				 setTotals();			
			 }
			 break;
		 case 3:
			 if (resultCode==RESULT_OK){
				 client = data.getStringExtra("clientName");
				 clientID = data.getIntExtra("clientID", 0);
				 clientName.setText(client);
			 }
			 break;
		 default:
			 break;
		 }
	 }
	  
	 public void onClick(View view){
		 switch (view.getId()){
		 case R.id.addTaskNewJobButton:
			 Intent inTask = new Intent(this, AddTaskPopup.class);
			 startActivityForResult(inTask, 0);
			 break;  
		 case R.id.addItemNewJobButton:
			 Intent inItem = new Intent(this, AddItemPopup.class);
			 startActivityForResult(inItem, 1);
			 break;
		 case R.id.viewTaskNewJobButton:
			 System.out.println("NEW JOB - TASKS ?>>>> "+tasks.size());
			 passTasksItems(0);
			break;
		 case R.id.workCountNewJobTextView:
			 System.out.println("NEW JOB - TASKS ?>>>> "+tasks.size());
			 passTasksItems(0);
			break;
		 case R.id.viewItemNewJobButton:
			 System.out.println("NEW JOB - ITEMS ?>>>> "+items.size());
			 passTasksItems(1);
			 break;
		 case R.id.itemCountNewJobTextView:
			 System.out.println("NEW JOB - ITEMS ?>>>> "+items.size());
			 passTasksItems(1);
			 break;
		 case R.id.addClientNewJobEditText:
			 Intent i = new Intent(this, SelectClientPopup.class);
			 startActivityForResult(i, 3);
			 break;
		 case R.id.startDateNewJobEditText:
			 Calendar cal = Calendar.getInstance();
			 DatePickerDialog datePickDiag = new DatePickerDialog( NewJob.this, odsl, cal.get(Calendar.YEAR),
					 cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			 datePickDiag.show();
			 break;
		 default:
			 break;
		 }
	 }
	 
	 public void passTasksItems (int currentItem){
			Intent in = new Intent(this, TaskItemExp.class);
			in.putExtra("tasksArray", tasks);
			in.putExtra("itemsArray", items);		
			in.putExtra("currentItem", currentItem);
			in.putExtra("loadFrags", 2);
			in.putExtra("source", 1);
			startActivityForResult(in, 2);
		}
	  
	 final OnDateSetListener odsl = new OnDateSetListener() {
		 public void onDateSet(DatePicker arg0, int year, int month, int dayOfMonth) {
			 month++;
			 TextView txt = (TextView) findViewById(R.id.startDateNewJobEditText);
			 String dayStr = String.valueOf(dayOfMonth);
			 String monthStr = String.valueOf(month);
			 if (dayStr.length()==1){
				 dayStr = "0"+ dayStr;
			 }
			 if (monthStr.length()==1){
				 monthStr = "0"+ monthStr;
			 }
			 txt.setText(dayStr + "/" + monthStr + "/" + year);
		 }
	};
	
	public void getClient (int id) {
		ClientsConnector cliCon = new ClientsConnector(this);
		cliCon.open();
		Cursor cur = cliCon.getClient(id);
		cur.moveToFirst();
		int forenameCol = cur.getColumnIndex(ClientsConnector.FORENAME);
		int surnameCol = cur.getColumnIndex(ClientsConnector.SURNAME);
		client = cur.getString(forenameCol) + " " + cur.getString(surnameCol);
		clientName.setText(client);
		cliCon.close();
	}
}