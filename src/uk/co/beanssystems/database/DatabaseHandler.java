package uk.co.beanssystems.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class DatabaseHandler extends SQLiteOpenHelper {
	
	public DatabaseHandler(Context context, String name,
		CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
        // Create Clients table
		String CREATE_CLIENTS_TABLE = "CREATE TABLE clients (clientID INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, forename TEXT, surname TEXT, company TEXT, address TEXT, postcode TEXT, mobile TEXT, phone TEXT, email TEXT, notes TEXT, active TEXT, editDate DATETIME);";
		db.execSQL(CREATE_CLIENTS_TABLE);
        
		// Create Quotes table
		String CREATE_QUOTES_TABLE = "CREATE TABLE quotes (quoteID INTEGER PRIMARY KEY AUTOINCREMENT, clientID INTEGER, jobID INTEGER, jobName TEXT, dateIssued DATE, estimatedHours REAL, notes TEXT, subTotal REAL, baseTotal REAL, vatType TEXT, totalCost REAL, totalVAT REAL, fixedCost REAL, status TEXT, active TEXT, editDate DATETIME);"; 
		db.execSQL(CREATE_QUOTES_TABLE);
		
		// Create QItems table
		String CREATE_QITEMS_TABLE = "CREATE TABLE qitems (qitemID INTEGER PRIMARY KEY AUTOINCREMENT, quoteID INTEGER, supplierID INTEGER, itemName TEXT, unitCost REAL, incVAT TEXT, quantity TEXT, totalCost REAL, totalVAT REAL, active TEXT, editDate DATETIME);"; 
		db.execSQL(CREATE_QITEMS_TABLE);
		
		// Create QTasks table
		String CREATE_QTASKS_TABLE = "CREATE TABLE qtasks (qtaskID INTEGER PRIMARY KEY AUTOINCREMENT, quoteID INTEGER, taskName TEXT, estimatedDuration REAL, incVAT TEXT, chargeValue REAL, active TEXT, editDate DATETIME);"; 
		db.execSQL(CREATE_QTASKS_TABLE);
		
		// Create Jobs table
		String CREATE_JOBS_TABLE = "CREATE TABLE jobs (jobID INTEGER PRIMARY KEY AUTOINCREMENT, clientID INTEGER, clientName TEXT, jobName TEXT, estimatedHours REAL, estimatedCost REAL, billingMethod TEXT, fixedCost REAL, startDate DATE, endDate DATE, repeatNew TEXT, description TEXT, active TEXT, editDate DATETIME);";
		db.execSQL(CREATE_JOBS_TABLE);		
		
		// Create Items table
		String CREATE_ITEMS_TABLE = "CREATE TABLE items (itemID INTEGER PRIMARY KEY AUTOINCREMENT, jobID INTEGER, supplierID INTEGER, invoiceID INTEGER, itemName TEXT, unitCost REAL, incVAT TEXT, quantity TEXT, totalCost REAL, totalVAT REAL, active TEXT, editDate DATETIME);"; 
		db.execSQL(CREATE_ITEMS_TABLE);
		
		// Create Tasks table
		String CREATE_TASKS_TABLE = "CREATE TABLE tasks (taskID INTEGER PRIMARY KEY AUTOINCREMENT, jobID INTEGER, invoiceID INTEGER,  taskName TEXT, actualDuration REAL, incVAT TEXT, chargeValue REAL, active TEXT, editDate DATETIME);"; 
		db.execSQL(CREATE_TASKS_TABLE);	
		
		// Create Expenses table
		String CREATE_EXPENSES_TABLE = "CREATE TABLE expenses (expenseID INTEGER PRIMARY KEY AUTOINCREMENT, jobID INTEGER, clientID INTEGER, invoiceID INTEGER, supplierID INTEGER, expCategory INTEGER, expType TEXT, date DATE, cost REAL, vat REAL, paid REAL, description TEXT, photo TEXT, active TEXT, editDate DATETIME);";  
		db.execSQL(CREATE_EXPENSES_TABLE);
				
		// Create Invoices table
		String CREATE_INVOICES_TABLE = "CREATE TABLE invoices (invoiceID INTEGER PRIMARY KEY AUTOINCREMENT, clientID INTEGER, jobID INTEGER, invoiceNumber INTEGER, dateIssued DATE, dateDue DATE, subTotal REAL, vat REAL, totalDue REAL, baseTotal REAL, vatType TEXT, outstanding REAL, fixedCost REAL, status TEXT, terms TEXT, notes TEXT, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_INVOICES_TABLE);
		
		// Create Settings table
		String CREATE_SETTINGS_TABLE = "CREATE TABLE settings (userID INTEGER PRIMARY KEY, userName TEXT, userCompany TEXT, logo BLOB, vatNum TEXT, vatRate TEXT, address TEXT, postcode TEXT, contactNum TEXT, email TEXT, invoiceComment TEXT, terms TEXT, backupType TEXT, backupPeriod TEXT, currency TEXT, moneyDisplay TEXT, hourlyRate TEXT, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_SETTINGS_TABLE);
		
		String CREATE_ONLINEACCOUNT_TABLE = "CREATE TABLE onlineAccount (userID INTEGER PRIMARY KEY, userName TEXT, password TEXT, secretQuestion TEXT, accCreated DATETIME, dbName TEXT, dbVersion TEXT, lastBackup DATETIME, lastRestore DATETIME, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_ONLINEACCOUNT_TABLE);
		
		// Create ExpType table
		String CREATE_EXPTYPE_TABLE = "CREATE TABLE expType (expTypeID INTEGER PRIMARY KEY AUTOINCREMENT, expType TEXT, code TEXT, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_EXPTYPE_TABLE);
		
		// Create ExpSubtype table
		String CREATE_EXPSUBTYPE_TABLE = "CREATE TABLE expSubType (expSubTypeID INTEGER PRIMARY KEY AUTOINCREMENT, expTypeID INTEGER, expSubType TEXT, code TEXT, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_EXPSUBTYPE_TABLE);
		
		// Create Transactions table
		String CREATE_TRANSACTIONS_TABLE = "CREATE TABLE transactions (transactionID INTEGER PRIMARY KEY AUTOINCREMENT, expenseID INTEGER, invoiceID INTEGER, jobID INTEGER, bankID INTEGER, date DATE, amount REAL, paidType TEXT, clearedBank DATE, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_TRANSACTIONS_TABLE);
		
		// Create Suppliers table
		String CREATE_SUPPLIERS_TABLE = "CREATE TABLE suppliers (supplierID	INTEGER PRIMARY KEY AUTOINCREMENT, company TEXT, address0 TEXT, streetNum TEXT, street TEXT, town TEXT, county TEXT, postcode TEXT, mobile TEXT, office TEXT, email TEXT, notes TEXT, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_SUPPLIERS_TABLE);
		
		// Create Banks table
		String CREATE_BANKS_TABLE = "CREATE TABLE banks (bankID INTEGER PRIMARY KEY AUTOINCREMENT,  description	TEXT, balance REAL, clearedBalance REAL, clearedDate DATE, checkNo INTEGER, payslipNo INTEGER, accountNo INTEGER, sortCode TEXT, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_BANKS_TABLE);
		
		// Create Rates table
		String CREATE_RATES_TABLE = "CREATE TABLE rates (rateID INTEGER PRIMARY KEY AUTOINCREMENT,  rateName TEXT, rateValue REAL, rateVAT REAL, incVAT TEXT, rateTime TEXT, active TEXT, editDate DATETIME)";
		db.execSQL(CREATE_RATES_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop old table if exist
		db.execSQL("DROP TABLE IF EXISTS clients, quotes, qitems, qtasks, jobs, items, tasks, expenses, invoices, settings, expType, expSubtype, transactions, suppliers, banks, rates");
		// Create tables again
		onCreate(db);
	}

}
