package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ItemsConnector {
	// Items table variables	 
	public static final String TABLE_ITEMS = "items";
	public static final String ITEMID = "itemID";
	public static final String JOBID = "jobID";
	public static final String SUPPLIERID = "supplierID";
	public static final String INVOICEID = "invoiceID";
	public static final String ITEMNAME = "itemName";
	public static final String UNITCOST = "unitCost";
	public static final String INCVAT = "incVAT";
	public static final String QUANTITY = "quantity";
	public static final String TOTALCOST = "totalCost";
	public static final String TOTALVAT = "totalVAT";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";	
	
	public static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public ItemsConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertItem(int jobID , int supplierID, int invoiceID, String itemName, double unitCost, String incVAT, int quantity, 
    		double  totalCost, double totalVAT) {  
    	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(JOBID, jobID);
		newCon.put(SUPPLIERID, supplierID);
		newCon.put(INVOICEID, invoiceID);
		newCon.put(ITEMNAME, itemName);
		newCon.put(UNITCOST, unitCost);
		newCon.put(INCVAT, incVAT);
		newCon.put(QUANTITY, quantity);
		newCon.put(TOTALCOST, totalCost);
		newCon.put(TOTALVAT, totalVAT);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_ITEMS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateItem(int id, int jobID , int supplierID, int invoiceID, String itemName, double unitCost, String incVAT, int quantity, 
    		double  totalCost, double totalVAT) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(JOBID, jobID);
		editCon.put(SUPPLIERID, supplierID);
		editCon.put(INVOICEID, invoiceID);
		editCon.put(ITEMNAME, itemName);
		editCon.put(UNITCOST, unitCost);
		editCon.put(INCVAT, incVAT);
		editCon.put(QUANTITY, quantity);
		editCon.put(TOTALCOST, totalCost);
		editCon.put(TOTALVAT, totalVAT);      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_ITEMS, editCon, ITEMID +"="+ id, null);
    	close();
    }
    
    public void updateInvoiceId(int id, int invoiceID){
    	ContentValues editCon = new ContentValues();
    	editCon.put(INVOICEID, invoiceID);
    	
    	open();
    	database.update(TABLE_ITEMS, editCon, ITEMID +"="+ id, null);
    	close();
    }
    
    public void updateIds(int id, int jobID, int invoiceID){
    	ContentValues editCon = new ContentValues();
    	editCon.put(JOBID, jobID);
    	editCon.put(INVOICEID, invoiceID);
    	open();
    	database.update(TABLE_ITEMS, editCon, ITEMID +"="+ id, null);
    	close();
    }
    
	// Get all the records from table	  
    public Cursor getAllItems() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_ITEMS, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getItem(int id) {
    	return database.query(TABLE_ITEMS, null, ITEMID +"="+ id, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getItemsByJobInvoice(int id) {
    	open();
    	return database.query(TABLE_ITEMS, null, INVOICEID +"="+ id +" AND "+INVOICEID+"<>0", null, null, null, null);
    }
    
    public boolean countByJob(int id){
    	boolean result = false;    	
    	open();
    	Cursor cur = database.query(TABLE_ITEMS, null, JOBID +"="+ id +" AND "+INVOICEID+"=0 AND "+ACTIVE+" LIKE 'true'", null, null, null, null);
    	try {
    		if (cur.getCount()>0){
    			result = true;
    		}
    	} catch (Exception e){
    	}
    	close();
    	return result;
    }
    
    // Get one record from table
    public Cursor getItemsByJob(int id) {
    	open();
    	return database.query(TABLE_ITEMS, null, JOBID +"="+ id +" AND "+INVOICEID+"=0 AND "+ACTIVE+" LIKE 'true'", null, null, null, null);
    }
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_ITEMS, editCon, ITEMID +"="+ id, null);
    	close();
    }
    
    public void updateActiveUninvoiced(int id, String active){
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_ITEMS, editCon, JOBID +"="+ id + " AND "+INVOICEID + "=0", null);
    	close();
    }
    
	 // Update the active field
    public void updateOpenActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	database.update(TABLE_ITEMS, editCon, ITEMID +"="+ id, null);
    }
    
    //get newest ID
    public int getNextId(){
    	String[] args = {"items"};
    	open();
    	Cursor cur = database.rawQuery("SELECT * FROM sqlite_sequence WHERE name=?", args);
    	cur.moveToFirst();
    	int col = cur.getColumnIndex("seq");
    	int lastRow = cur.getInt(col);
    	close();
    	return lastRow;
    }
    // Remove record from table
 	public void deleteItem(int id) {
 		open(); 
 		database.delete(TABLE_ITEMS, ITEMID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_ITEMS, null, null);
 		close();
 	}
}