package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class AccountConnector {
	public static final String 
		TABLE_ACCOUNT = "onlineAccount",
		USERID = "userID";
	private static final String
		USERNAME = "username",
		PASSWORD = "password",
		SECRETQUESTION = "secretQuestion",
		CREATED = "accCreated",
		DBNAME = "dbName",
		DBVERSION = "dbVersion",
		LASTBACKUP = "lastBackup",
		LASTRESTORE = "lastRestore",
		ACTIVE = "active",
		EDITDATE = "editDate";
		
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public AccountConnector(Context context){
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}

	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	public void delete(){
		open();
		database.delete(TABLE_ACCOUNT, null, null);
		close();
	}
	
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	public void insertAccount(
			String username, String password, String secretQuestion, String dbName, String dbVersion
			){
		
		ContentValues newCon = new ContentValues();
		newCon.put(USERID, 1);
		newCon.put(USERNAME, username);
		newCon.put(PASSWORD, password);
		newCon.put(SECRETQUESTION, secretQuestion);
		newCon.put(CREATED, getEditDate());
		newCon.put(DBNAME, dbName);
		newCon.put(DBVERSION, dbVersion);
		newCon.put(LASTBACKUP, "");
		newCon.put(LASTRESTORE, "");
		newCon.put(ACTIVE, "true");
		newCon.put(EDITDATE, getEditDate());
		
		open();
		database.insert(TABLE_ACCOUNT, null, newCon);
		close();
	}
	
	public void updateLastBackup(){
    	ContentValues editCon = new ContentValues();
    	editCon.put(LASTBACKUP, getEditDate());
    	
    	open();
    	database.update(TABLE_ACCOUNT, editCon, USERID +"="+ 1, null);
    	close();
    }
	
	public void updateLastRestore(){
    	ContentValues editCon = new ContentValues();
    	editCon.put(LASTRESTORE, getEditDate());
    	
    	open();
    	database.update(TABLE_ACCOUNT, editCon, USERID +"="+ 1, null);
    	close();
    }
	
	public void updateLastSync(String lastBackup){
 		ContentValues editCon = new ContentValues();
    	editCon.put(LASTBACKUP, lastBackup);
    	
 		open();
 		database.update(TABLE_ACCOUNT, editCon, null, null);
 		close();
 	}
	
	public void updatePassword(String pass){
 		ContentValues editCon = new ContentValues();
    	editCon.put(PASSWORD, pass);
    	
 		open();
 		database.update(TABLE_ACCOUNT, editCon, null, null);
 		close();
 	}
	
	public String getDatabase(){
    	String dbName = "";
    	open();
    	Cursor cur = database.query(TABLE_ACCOUNT, new String [] {DBNAME}, null, null, null, null, null);
    	try {
	    	cur.moveToFirst();
	 		dbName = cur.getString(0);
    	} catch (Exception e){
    		
    	}
 		close();
    	return dbName;
 	}
	
	public String getUsername(){
		String username="";
		open();
    	Cursor cur = database.query(TABLE_ACCOUNT, new String [] {USERNAME}, null, null, null, null, null);
    	try {
	    	cur.moveToFirst();
	    	username = cur.getString(0);
    	} catch (Exception e){
    		
    	}
 		close();
    	return username;
	}

    public String getLastSync(){
    	String last = "";
    	open();
    	Cursor cur = database.query(TABLE_ACCOUNT, new String [] {LASTBACKUP}, null, null, null, null, null);
 		try {
 			cur.moveToFirst();
 			last = cur.getString(0);
 		} catch (Exception e){
 			
 		}
 		close();
    	return last;
 	}
}
