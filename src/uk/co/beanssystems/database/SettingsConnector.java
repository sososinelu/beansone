package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SettingsConnector {

	// Settings table variables
	public static final String TABLE_SETTINGS = "settings", USERID = "userID", USERNAME = "userName", USERCOMPANY = "userCompany", LOGO = "logo";
	public static final String VATNUM = "vatNum", VATRATE = "vatRate", ADDRESS = "address", POSTCODE = "postcode", CONTACTNUM = "contactNum";
	public static final String EMAIL = "email", INVOICECOMMENT = "invoiceComment", TERMS = "terms", BACKUPTYPE = "backupType", BACKUPPERIOD = "backupPeriod";	
	public static final String CURRENCY = "currency", MONEYDISPLAY = "moneyDisplay", HOURLYRATE = "hourlyRate", ACTIVE = "active", EDITDATE = "editDate";
	
	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public SettingsConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertSetting(String name, String company, String logo, String vatNum, String vatRate, 
    		String address, String postcode, String contactNum,
    		String email, String invoiceComment, String terms, String backupType, String backupPeriod, String currency, 
    		String moneyDisplay, String hourlyRate) {
    	  	
    	ContentValues newCon = new ContentValues();		      
    	newCon.put(USERNAME, name);
    	newCon.put(USERCOMPANY,company);
    	newCon.put(LOGO,logo);
    	newCon.put(VATNUM,vatNum);
    	newCon.put(VATRATE,vatRate);
    	newCon.put(ADDRESS, address);
    	newCon.put(POSTCODE,postcode);
    	newCon.put(CONTACTNUM,contactNum);
    	newCon.put(EMAIL,email);
    	newCon.put(INVOICECOMMENT,invoiceComment);
    	newCon.put(TERMS,terms);
    	newCon.put(BACKUPTYPE,backupType);
    	newCon.put(BACKUPPERIOD,backupPeriod);
    	newCon.put(CURRENCY,currency);
    	newCon.put(MONEYDISPLAY,moneyDisplay);
    	newCon.put(HOURLYRATE,hourlyRate);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_SETTINGS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateSettings(int id, String name, String company, String logo, String vatNum, String vatRate, 
    		String address, String postcode, String contactNum,
    		String email, String invoiceComment, String terms, String backupType, String backupPeriod, String currency, 
    		String moneyDisplay, String hourlyRate) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(USERNAME,name);
    	editCon.put(USERCOMPANY,company);
    	editCon.put(LOGO,logo);
    	editCon.put(VATNUM,vatNum);
    	editCon.put(VATRATE,vatRate);
    	editCon.put(ADDRESS,address);
    	editCon.put(POSTCODE,postcode);
    	editCon.put(CONTACTNUM,contactNum);
    	editCon.put(EMAIL,email);
    	editCon.put(INVOICECOMMENT,invoiceComment);
    	editCon.put(TERMS,terms);
    	editCon.put(BACKUPTYPE,backupType);
    	editCon.put(BACKUPPERIOD,backupPeriod);
    	editCon.put(CURRENCY,currency);
    	editCon.put(MONEYDISPLAY,moneyDisplay);
    	editCon.put(HOURLYRATE,hourlyRate);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_SETTINGS, editCon, USERID +"="+ id, null);
    	close();
    }
    
  // Update my user details
    public void insertUserDetails(String name, String contactNum, String company, String address, String postcode, String email ) {  	
    	ContentValues newCon = new ContentValues();	
    	newCon.put(USERNAME,name);
    	newCon.put(CONTACTNUM,contactNum);
    	newCon.put(USERCOMPANY,company);;
    	newCon.put(ADDRESS,address);
    	newCon.put(POSTCODE,postcode);
    	newCon.put(EMAIL,email);
    	newCon.put(EDITDATE, getEditDate());
		
		open();
		database.insert(TABLE_SETTINGS, null, newCon);
		close();
    }
    
    // Update my user details
    public void updateUserDetails(int id, String name, String contactNum, String company, String address, String postcode, 
    		String email ) {  	
    	ContentValues editCon = new ContentValues();
    	editCon.put(USERNAME,name);
    	editCon.put(CONTACTNUM,contactNum);
    	editCon.put(USERCOMPANY,company);;
    	editCon.put(ADDRESS,address);
    	editCon.put(POSTCODE,postcode);
    	editCon.put(EMAIL,email);
		editCon.put(EDITDATE, getEditDate());
		
    	open();
    	database.update(TABLE_SETTINGS, editCon, USERID +"="+ id, null);
    	close();
    }
 	
 	public void updateFinance(String vatNum, String vatRate){
	ContentValues editCon = new ContentValues();
	editCon.put(VATNUM, vatNum);
	editCon.put(VATRATE, vatRate);

	open();
	database.update(TABLE_SETTINGS, editCon, null, null);
	close();
 	}
    
 	public void updateSync(String backupType, String backupPeriod){
 		ContentValues editCon = new ContentValues();
 		editCon.put(BACKUPTYPE,backupType);
    	editCon.put(BACKUPPERIOD,backupPeriod);
    	
 		open();
 		database.update(TABLE_SETTINGS, editCon, null, null);
 		close();
 	}
 	
	// Get all the records from table	  
    public Cursor getAllSettings() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_SETTINGS, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getSetting(int id) {
    	return database.query(TABLE_SETTINGS, null, USERID +"="+ id, null, null, null, null);
    }
    
    // Get user name & business name record from table
    public Cursor getUserDetails() {
    	return database.query(TABLE_SETTINGS, new String [] {"rowid _id", USERNAME, CONTACTNUM, USERCOMPANY, ADDRESS, POSTCODE, EMAIL}, null, null, null, null, null);
    }
    
    // Get finance details
    public Cursor getFinance(){
 		return database.query(TABLE_SETTINGS, new String [] {VATNUM, VATRATE, CURRENCY}, null, null, null, null, null);
 	}
    
    // Get sync details
    public Cursor getSync(){
 		return database.query(TABLE_SETTINGS, new String [] {"rowid _id", BACKUPTYPE, BACKUPPERIOD}, null, null, null, null, null);
 	}
	
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_SETTINGS, editCon, USERID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteSetting(int id) {
 		open(); 
 		database.delete(TABLE_SETTINGS, USERID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_SETTINGS, null, null);
 		close();
 	}
	
}