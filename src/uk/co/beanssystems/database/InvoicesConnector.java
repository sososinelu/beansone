package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class InvoicesConnector {
	
	// Invoice table variables	 
	public static final String TABLE_INVOICES = "invoices";
	public static final String INVOICEID = "invoiceID";
	public static final String CLIENTID = "clientID";
	public static final String JOBID = "jobID";
	public static final String INVOICENUMBER = "invoiceNumber";
	public static final String DATEISSUED = "dateIssued";
	public static final String DATEDUE = "dateDue";
	public static final String SUBTOTAL = "subTotal";
	public static final String VAT = "vat";
	public static final String TOTALDUE = "totalDue";
	public static final String BASETOTAL = "baseTotal";
	public static final String VATTYPE = "vatType";	
	public static final String OUTSTANDING = "outstanding";
	public static final String FIXEDCOST = "fixedCost";
	public static final String STATUS = "status";
	public static final String TERMS = "terms";
	public static final String NOTES = "notes";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";	
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public InvoicesConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertInvoice(int clientID , int jobID, int invoiceNumber, String dateIssued, String dateDue, double subTotal, 
    		double  vat, double totalDue, double baseTotal, String vatType, double outstanding, double fixedCost, String status, String terms, String notes) {  
    	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(CLIENTID, clientID);
		newCon.put(JOBID, jobID);
		newCon.put(INVOICENUMBER, invoiceNumber);
		newCon.put(DATEISSUED, dateIssued);
		newCon.put(DATEDUE, dateDue);
		newCon.put(SUBTOTAL, subTotal);
		newCon.put(VAT, vat);
		newCon.put(TOTALDUE, totalDue);
		newCon.put(BASETOTAL, baseTotal);
		newCon.put(VATTYPE, vatType);
		newCon.put(OUTSTANDING, outstanding);
		newCon.put(FIXEDCOST, fixedCost);
		newCon.put(STATUS, status);
		newCon.put(TERMS, terms);
		newCon.put(NOTES, notes);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_INVOICES, null, newCon);
		close();
    }
    
    // Update database table
    public void updateInvoice(int id, int clientID , int jobID, int invoiceNumber, String dateIssued, String dateDue, double subTotal, 
    		double  vat, double totalDue, double baseTotal, String vatType, double outstanding, double fixedCost, String status, String terms, String notes) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(CLIENTID, clientID);
		editCon.put(JOBID, jobID);
		editCon.put(INVOICENUMBER, invoiceNumber);
		editCon.put(DATEISSUED, dateIssued);
		editCon.put(DATEDUE, dateDue);
		editCon.put(SUBTOTAL, subTotal);
		editCon.put(VAT, vat);
		editCon.put(TOTALDUE, totalDue);
		editCon.put(BASETOTAL, baseTotal);
		editCon.put(VATTYPE, vatType);
		editCon.put(OUTSTANDING, outstanding);
		editCon.put(FIXEDCOST, fixedCost);
		editCon.put(STATUS, status);
		editCon.put(TERMS, terms);
		editCon.put(NOTES, notes);      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_INVOICES, editCon, INVOICEID +"="+ id, null);
    	close();
    }
    
    // Update outstanding payment
    public void updateOutstandingSum(int id,  double outstanding) {  
    	ContentValues editCon = new ContentValues();
		editCon.put(OUTSTANDING, outstanding);   
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_INVOICES, editCon, INVOICEID +"="+ id, null);
    	close();
    }
    
    // Update invoice status
    public void updateStatus(int id,  String status) {  
    	ContentValues editCon = new ContentValues();
    	editCon.put(STATUS, status);  
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_INVOICES, editCon, INVOICEID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllInvoices() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_INVOICES, null, ACTIVE +"="+"?", new String[]{"true"}, null, null, DATEISSUED);
    }
    
    public boolean hasInvoices(int jobID){
    	boolean result = false;
    	open();
    	Cursor cur = database.query(TABLE_INVOICES, null, JOBID + "="+jobID, null, null, null, null);
    	try {
    		if (cur.getCount()>0){
    			result = true;
    		}
    	} catch (Exception e){
    		result = true;
    	}
    	close();
    	return result;
    }
    
    // Get one record from table
    public Cursor getInvoice(int id) {
    	return database.query(TABLE_INVOICES, null, INVOICEID +"="+ id, null, null, null, null);
    }
	
    public Cursor getInvoicesByJob(int id){
    	return database.query(TABLE_INVOICES, null, JOBID +"="+id +" AND "+ ACTIVE +"="+"?", new String[]{"true"}, null, null, "date("+EDITDATE+") DESC");
   }
    
    public Cursor getInvoicesByClient(int id){
    	return database.query(TABLE_INVOICES, null, CLIENTID +"="+id +" AND "+ ACTIVE +"="+"?", new String[]{"true"}, null, null, null);
   }
    
    public int getNextId(){
    	String[] args = {"invoices"};
    	open();
    	Cursor cur = database.rawQuery("SELECT * FROM sqlite_sequence WHERE name=?", args);
    	cur.moveToFirst();
    	int col = cur.getColumnIndex("seq");
    	int lastRow = cur.getInt(col);
    	close();
    	return lastRow;
    }
    
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_INVOICES, editCon, INVOICEID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteInvoice(int id) {
 		open(); 
 		database.delete(TABLE_INVOICES, INVOICEID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_INVOICES, null, null);
 		close();
 	}
}
