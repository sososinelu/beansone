 package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.jobs.JobsListItem;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class JobsConnector {

	// Jobs table variables	 
	public static final String TABLE_JOBS = "jobs";
	public static final String JOBID = "jobID";
	public static final String CLIENTID = "clientID";
	public static final String CLIENTNAME = "clientName";
	public static final String JOBNAME = "jobName";
	public static final String ESTIMATEDHOURS = "estimatedHours";
	public static final String ESTIMATEDCOST = "estimatedCost";
	public static final String BILLINGMETHOD = "billingMethod";
	public static final String FIXEDCOST = "fixedCost";
	public static final String STARTDATE = "startDate";
	public static final String ENDDATE = "endDate";
	public static final String REPEAT = "repeatNew";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";
	
	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	private Context context;
	SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
	
	public JobsConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
		this.context = context;
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertJob(int clientID , String clientName, String jobName, double estimatedHours, double estimatedCost, String billingMethod, double fixedCost,
    		String startDate, String endDate, String repeat, String description) {  
    	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(CLIENTID, clientID);
		newCon.put(CLIENTNAME, clientName);
		newCon.put(JOBNAME, jobName);
		newCon.put(ESTIMATEDHOURS, estimatedHours);
		newCon.put(ESTIMATEDCOST, estimatedCost);
		newCon.put(BILLINGMETHOD, billingMethod);
		newCon.put(FIXEDCOST, fixedCost);
		newCon.put(STARTDATE, startDate);
		newCon.put(ENDDATE, endDate);
		newCon.put(REPEAT, repeat);
		newCon.put(DESCRIPTION, description);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_JOBS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateJob(int id, int clientID, String clientName, String jobName, double estimatedHours, double estimatedCost, String billingMethod, double fixedCost, 
    		String startDate, String  endDate, String repeat, String description) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(CLIENTID, clientID);
    	editCon.put(CLIENTNAME, clientName);
		editCon.put(JOBNAME, jobName);
		editCon.put(ESTIMATEDHOURS, estimatedHours);
		editCon.put(ESTIMATEDCOST, estimatedCost);
		editCon.put(BILLINGMETHOD, billingMethod);
		editCon.put(FIXEDCOST, fixedCost);
		editCon.put(STARTDATE, startDate);
		editCon.put(ENDDATE, endDate);
		editCon.put(REPEAT, repeat);
		editCon.put(DESCRIPTION, description);      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_JOBS, editCon, JOBID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllJobs() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_JOBS, null, ACTIVE +"="+"?", new String[]{"true"}, null, null, "date("+STARTDATE+") ASC");
    }
    
    // Get all the records from table	  
    public Cursor getAllUnactiveJobs() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_JOBS, null, ACTIVE +"="+"?", new String[]{"false"}, null, null, "date("+STARTDATE+") ASC");
    }   
    
    public ArrayList<JobsListItem> getJobsCurrent(int clientID){
    	ArrayList<JobsListItem> jobs = new ArrayList<JobsListItem>();
    	open();
    	Cursor cur;
    	if (clientID>0){
    		cur = database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME, CLIENTNAME, STARTDATE, ACTIVE, CLIENTID},
    				CLIENTID + "=" + clientID + " AND " +STARTDATE + " < date('now')", null, null, null, null);
    	} else {
    		cur = database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME, CLIENTNAME, STARTDATE, ACTIVE, CLIENTID},
    				STARTDATE + " < date('now')", null, null, null, null);
    	}
    	int icon;
    	try {
    		cur.moveToFirst();
    		do {
    			ClientsConnector cCon = new ClientsConnector(context);
    			if (cCon.isPrivate(database, cur.getInt(5))){
    				icon = R.drawable.user;
    			} else {
    				icon = R.drawable.work_user;
    			}
    			Date date = new Date();
    			date = inDate.parse(cur.getString(3));
    			jobs.add(new JobsListItem(0, cur.getInt(0), icon, cur.getString(2), cur.getString(1), outDate.format(date), "", cur.getString(4)));
    		} while (cur.moveToNext());
    	} catch (Exception e){
    		e.printStackTrace();    		
    	}
    	close();
    	return jobs;
    }
    
    public ArrayList<JobsListItem> getJobsToday(int clientID){
    	ArrayList<JobsListItem> jobs = new ArrayList<JobsListItem>();
    	open();
    	Cursor cur;
    	if (clientID>0){
    		cur = database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME, CLIENTNAME, STARTDATE, ACTIVE, CLIENTID}, 
    				CLIENTID + "=" + clientID + " AND " +STARTDATE + " = date('now')", null, null, null, null);
    	} else {
    		cur = database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME, CLIENTNAME, STARTDATE, ACTIVE, CLIENTID}, 
    				STARTDATE + " = date('now')", null, null, null, null);
    	}
    	int icon;
    	try {
    		cur.moveToFirst();
    		do {
    			ClientsConnector cCon = new ClientsConnector(context);
    			if (cCon.isPrivate(database, cur.getInt(5))){
    				icon = R.drawable.user;
    			} else {
    				icon = R.drawable.work_user;
    			}
    			Date date = new Date();
    			date = inDate.parse(cur.getString(3));
    			jobs.add(new JobsListItem(0, cur.getInt(0), icon, cur.getString(2), cur.getString(1),  outDate.format(date), "", cur.getString(4)));
    		} while (cur.moveToNext());
    	} catch (Exception e){}
    	close();
    	return jobs;
    }
    
    public ArrayList<JobsListItem> getJobsWeek(int clientID){
    	ArrayList<JobsListItem> jobs = new ArrayList<JobsListItem>();
    	open();
    	Cursor cur;
    	if (clientID>0){
    		cur = database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME, CLIENTNAME, STARTDATE, ACTIVE, CLIENTID}, 
    				CLIENTID + "=" + clientID + " AND " + STARTDATE + " BETWEEN date('now', '1 days') AND date('now', '7 days')", null, null, null, "date("+STARTDATE+") ASC");
    	} else {
    		cur = database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME, CLIENTNAME, STARTDATE, ACTIVE, CLIENTID}, 
    				STARTDATE + " BETWEEN date('now', '1 days') AND date('now', '7 days')", null, null, null, "date("+STARTDATE+") ASC");
    	}
    	int icon;
    	try {
    		cur.moveToFirst();
    		do {
    			ClientsConnector cCon = new ClientsConnector(context);
    			if (cCon.isPrivate(database, cur.getInt(5))){
    				icon = R.drawable.user;
    			} else {
    				icon = R.drawable.work_user;
    			}
    			Date date = new Date();
    			date = inDate.parse(cur.getString(3));
    			jobs.add(new JobsListItem(0, cur.getInt(0), icon, cur.getString(2), cur.getString(1),  outDate.format(date), "", cur.getString(4)));
    		} while (cur.moveToNext());
    	} catch (Exception e){}
    	close();
    	return jobs;
    }
    
    public ArrayList<JobsListItem> getJobsLater(int clientID){
    	ArrayList<JobsListItem> jobs = new ArrayList<JobsListItem>();
    	open();
    	Cursor cur;
    	if (clientID>0){
    		cur = database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME, CLIENTNAME, STARTDATE, ACTIVE, CLIENTID}, 
    				CLIENTID + "=" + clientID + " AND " + STARTDATE + " > date('now', '7 days')", null, null, null, "date("+STARTDATE+") ASC");
    	} else {
    		cur = database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME, CLIENTNAME, STARTDATE, ACTIVE, CLIENTID}, 
    				STARTDATE + " > date('now', '7 days')", null, null, null, "date("+STARTDATE+") ASC");
    	}
    	int icon;
    	try {
    		cur.moveToFirst();
    		do {
    			ClientsConnector cCon = new ClientsConnector(context);
    			if (cCon.isPrivate(database, cur.getInt(5))){
    				icon = R.drawable.user;
    			} else {
    				icon = R.drawable.work_user;
    			}
    			Date date = new Date();
    			date = inDate.parse(cur.getString(3));
    			jobs.add(new JobsListItem(0, cur.getInt(0), icon, cur.getString(2), cur.getString(1),  outDate.format(date), "", cur.getString(4)));
    		} while (cur.moveToNext());
    	} catch (Exception e){}
    	close();
    	return jobs;
    }
    
    public Cursor getJobFolder(){
    	return database.query(TABLE_JOBS, new String[]{JOBID, JOBNAME}, null, null, null, null, null);
    }
    
    public int countActiveJobs() {
    	int activeJobs=0;
    	open();
    	Cursor cur = database.query(TABLE_JOBS, null, ACTIVE +"="+"?", new String[]{"true"}, null, null, null);
    	activeJobs = cur.getCount();
    	close();
    	return activeJobs;
    }
        
    // Get one record from table by jobID
    public Cursor getJob(int id) {
    	return database.query(TABLE_JOBS, null, JOBID +"="+ id, null, null, null, null);
    }
    
    // Get one record from table by job name
    public Cursor getJobNameDesc(String jobName) {
    	return database.query(TABLE_JOBS,  new String []{"rowid _id", JOBID, JOBNAME, CLIENTID, CLIENTNAME, ESTIMATEDHOURS, BILLINGMETHOD, REPEAT,  DESCRIPTION},  JOBNAME + " like '%" + jobName + "%' and "+ ACTIVE +"="+"?", new String[]{"true"}, null, null, null);
    }
    
    // Get records for new job based on previous jobs
    public Cursor getJobName(String jobName) {    	
    	//return database.query(TABLE_JOBS,  new String []{"rowid _id", JOBNAME, ESTIMATEDHOURS, BILLINGMETHOD, REPEAT,  DESCRIPTION}, JOBNAME + " like '%" + jobName + "%'" , null, null, null, null);
    	return database.rawQuery("SELECT rowid _id, "+JOBNAME+", "+ESTIMATEDHOURS+", "+BILLINGMETHOD+", "+REPEAT+", "+DESCRIPTION+" FROM jobs WHERE jobName like '%"+jobName+"%' AND startDate BETWEEN  date('now', '-120 days') AND date('now')", null);
    }
	
    // Get job name & description records from table
    public Cursor getJobName() {
    	return database.query(TABLE_JOBS, new String [] {"rowid _id", JOBID, JOBNAME, CLIENTID, CLIENTNAME, DESCRIPTION}, ACTIVE +"="+"?", new String[]{"true"}, null, null, null);
    }
    
    public Double getFixedCost(int id) {
    	double fixedCost = 0;
    	open();
    	Cursor cur = database.query(TABLE_JOBS, new String [] {JOBID, FIXEDCOST}, JOBID + "=" + id, null, null, null, null);
    	if (cur.moveToFirst()){
    		fixedCost = cur.getDouble(1);
    	}
    	close();
    	return fixedCost;
    }
    
    // Get one active job record from table
    public Cursor getActiveJob(int id) {
    	return database.query(TABLE_JOBS, null, CLIENTID +"="+ id +" and "+ ACTIVE +"="+"?" , new String[]{"true"}, null, null, null);
    }
    
    public Cursor getAllJobHours(){
    	return database.query(TABLE_JOBS, new String [] {JOBID, ESTIMATEDHOURS, STARTDATE}, ACTIVE +"="+"?", new String[]{"true"}, null, null, null);
    }
    public Cursor getJobNameByClient(int id) {
    	return database.query(TABLE_JOBS, new String [] {"rowid _id", JOBID, JOBNAME, CLIENTID, CLIENTNAME, DESCRIPTION}, CLIENTID +"="+ id +" and "+ ACTIVE +"="+"?", new String[]{"true"}, null, null, null);
    }
	    
    public boolean clientHasJobs(int id){
    	open();
    	Cursor cur = database.query(TABLE_JOBS, null, CLIENTID +"="+ id, null, null, null, null);
    	try {
    		if (cur.getCount()>0){
    			close();
    			return true;
    		}
    	} catch (Exception e){
    		close();
    		return true;
    	}
    	close();
    	return false;
    }
    
    public Cursor getJobsByClient(int id){
    	return database.query(TABLE_JOBS, null, CLIENTID +"="+ id +" and "+ ACTIVE +"="+"?", new String[]{"true"}, null, null, null);
    }
    
    public ArrayList<Integer> getJobIDsByClient(int id){
    	ArrayList<Integer> ids = new ArrayList<Integer>();
    	open();
    	Cursor cur = database.query(TABLE_JOBS, new String[] {JOBID}, CLIENTID +"="+ id, null, null, null, null);
    	try {
    		cur.moveToFirst();
    		for (int i=1;i<=cur.getCount();i++){
    			ids.add(cur.getInt(0));
    			cur.moveToNext();
    		}
    	} catch (Exception e){
    	}
    	close();
    	return ids;
    }
    
    public int getNextId(){
    	String[] args = {"jobs"};
    	open();
    	Cursor cur = database.rawQuery("SELECT * FROM sqlite_sequence WHERE name=?", args);
    	cur.moveToFirst();
    	int col = cur.getColumnIndex("seq");
    	int lastRow = cur.getInt(col);
    	close();
    	return lastRow;
    }
    
    public void updateClient(int id, int clientID, String name){
    	ContentValues editCon = new ContentValues();
    	editCon.put(CLIENTID, clientID);
    	editCon.put(CLIENTNAME, name);
    
    	open();
    	database.update(TABLE_JOBS, editCon, JOBID+"="+id, null);
    	close();
    }
    
    
    
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_JOBS, editCon, JOBID +"="+ id, null);
    	close();
    }
    
    public void updateFixed(int id, double fixed) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(FIXEDCOST, fixed);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_JOBS, editCon, JOBID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteJob(int id) {
 		open(); 
 		database.delete(TABLE_JOBS, JOBID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_JOBS, null, null);
 		close();
 	}
	
}