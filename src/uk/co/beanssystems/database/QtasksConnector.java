package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class QtasksConnector {

	// QTasks table variables	 
	public static final String TABLE_QTASKS = "qtasks";
	public static final String QTASKID = "qtaskID";
	public static final String QUOTEID = "quoteID";
	public static final String TASKNAME = "taskName";
	public static final String ESTIMATEDDURATION = "estimatedDuration";
	public static final String INCVAT = "incVAT";
	public static final String CHARGEVALUE = "chargeValue";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";
	
	public static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public QtasksConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertQtask(int quoteID , String taskName, double estimatedDuration, String incVAT, double chargeValue) {  
    	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(QUOTEID, quoteID);
		newCon.put(TASKNAME, taskName);
		newCon.put(ESTIMATEDDURATION, estimatedDuration);
		newCon.put(INCVAT, incVAT);
		newCon.put(CHARGEVALUE, chargeValue);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_QTASKS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateQtask(int id, int quoteID , String taskName, double estimatedDuration, String incVAT, double chargeValue) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(QUOTEID, quoteID);
		editCon.put(TASKNAME, taskName);
		editCon.put(ESTIMATEDDURATION, estimatedDuration);
		editCon.put(INCVAT, incVAT);
		editCon.put(CHARGEVALUE, chargeValue);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QTASKS, editCon, QTASKID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllQtasks() {
    	open();
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_QTASKS, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getQtask(int id) {
    	open();
    	return database.query(TABLE_QTASKS, null, QTASKID +"="+ id, null, null, null, null);
    }
    
    public Cursor getTasksByQuote(int id){
    	open();
    	return database.query(TABLE_QTASKS, null, QUOTEID +"="+ id, null, null, null, null);
    }
	
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QTASKS, editCon, QTASKID +"="+ id, null);
    	close();
    }
    
    public void updateAllActive(int id, String active){
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QTASKS, editCon, QUOTEID +"="+ id, null);
    	close();
    }
    
	 // Update the active field
    public void updateOpenActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	database.update(TABLE_QTASKS, editCon, QTASKID +"="+ id, null);
    }
    
    // Remove record from table
 	public void deleteQtask(int id) {
 		open(); 
 		database.delete(TABLE_QTASKS, QTASKID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_QTASKS, null, null);
 		close();
 	}
	
}
