package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class QuotesConnector {
	
	// Quotes table variables	 
	public static final String TABLE_QUOTES = "quotes";
	public static final String QUOTEID = "quoteID";
	public static final String CLIENTID = "clientID";
	public static final String JOBID = "jobID";
	public static final String JOBNAME = "jobName";
	public static final String DATEISSUED = "dateIssued";
	public static final String ESTIMATEDHOURS = "estimatedHours";
	public static final String NOTES = "notes";
	public static final String SUBTOTAL = "subTotal";
	public static final String BASETOTAL = "baseTotal";
	public static final String VATTYPE = "vatType";
	public static final String TOTALCOST = "totalCost";
	public static final String TOTALVAT = "totalVAT";
	public static final String FIXEDCOST = "fixedCost";
	public static final String STATUS = "status";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";	
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public QuotesConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertQuote(int clientID , int jobID, String jobName, String dateIssued, double estimatedHours, String notes, 
    		double subTotal, double baseTotal, String vatType, double  totalCost, double totalVAT, double fixedCost, String status) {  
    	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(CLIENTID, clientID);
		newCon.put(JOBID, jobID);
		newCon.put(JOBNAME, jobName);
		newCon.put(DATEISSUED, dateIssued);
		newCon.put(ESTIMATEDHOURS, estimatedHours);
		newCon.put(NOTES, notes);
		newCon.put(SUBTOTAL, subTotal);
		newCon.put(BASETOTAL, baseTotal);
		newCon.put(VATTYPE, vatType);		
		newCon.put(TOTALCOST, totalCost);
		newCon.put(TOTALVAT, totalVAT);
		newCon.put(FIXEDCOST, fixedCost);
		newCon.put(STATUS, status);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_QUOTES, null, newCon);
		close();
    }
    
    // Update database table
    public void updateQuote(int id, int clientID , int jobID, String jobName, String dateIssued, double estimatedHours, String notes, 
    		double subTotal, double baseTotal, String vatType, double  totalCost, double totalVAT, double fixedCost, String status) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(CLIENTID, clientID);
		editCon.put(JOBID, jobID);
		editCon.put(JOBNAME, jobName);
		editCon.put(DATEISSUED, dateIssued);
		editCon.put(ESTIMATEDHOURS, estimatedHours);
		editCon.put(NOTES, notes);
		editCon.put(SUBTOTAL, subTotal);
		editCon.put(BASETOTAL, baseTotal);
		editCon.put(VATTYPE, vatType);	
		editCon.put(TOTALCOST, totalCost);
		editCon.put(TOTALVAT, totalVAT);
		editCon.put(FIXEDCOST, fixedCost);
		editCon.put(STATUS, status);      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QUOTES, editCon, QUOTEID +"="+ id, null);
    	close();
    }
    
    // Update quote status
    public void updateStatus(int id,  String status) {  
    	ContentValues editCon = new ContentValues();
    	editCon.put(STATUS, status);  
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QUOTES, editCon, QUOTEID +"="+ id, null);
    	close();
    }

    public void updateJobID(int id, int jobID){
    	ContentValues editCon = new ContentValues();
    	editCon.put(JOBID, jobID);
    	
    	open();
    	database.update(TABLE_QUOTES, editCon, QUOTEID+"="+id, null);
    	close();
    }
    
	// Get all the records from table	  
    public Cursor getAllQuotes() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_QUOTES, null, null, null, null, null, DATEISSUED);
    }
    
    // Get all the records from table with status draft & sent	  
    public Cursor getAllQuotesForList() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_QUOTES, null, ACTIVE +"="+"?" + " AND " + STATUS +"="+"?" + " OR " + STATUS +"="+"?", new String[]{"true", "draft", "sent"}, null, null, DATEISSUED);
    }
    
    // Get one record from table
    public Cursor getQuote(int id) {
    	return database.query(TABLE_QUOTES, null, QUOTEID +"="+ id, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getQuoteByJob(int id) {
    	return database.query(TABLE_QUOTES, null, JOBID +"="+ id, null, null, null, null);
    }
    
    public Cursor getQuotesByClient(int id) {
    	return database.query(TABLE_QUOTES, null, CLIENTID +"="+ id + " AND " + ACTIVE +"="+"?", new String[]{"true"}, null, null, null);
    }
    
    // Get records for new job based on previous jobs
    public Cursor getJobName(String jobName) {    	
    	return database.rawQuery("SELECT rowid _id, "+JOBNAME+", "+ESTIMATEDHOURS+", "+NOTES+" FROM quotes WHERE jobName like '%"+jobName+"%' AND dateIssued BETWEEN  date('now', '-120 days') AND date('now')", null);
    }
    
    public ArrayList<Integer> getClientQuotes(int id){
    	ArrayList<Integer> ids = new ArrayList<Integer>();
    	open();
    	Cursor cur = database.query(TABLE_QUOTES, new String[]{QUOTEID, CLIENTID, STATUS}, CLIENTID +"="+ id + " AND "+STATUS+"<>?", new String[]{"converted"}, null, null, null);
    	try {
	    	cur.moveToFirst();
	    	do {
	    		ids.add(cur.getInt(0));
	    	} while (cur.moveToNext());
    	} catch (Exception e){
    		
    	}
    	close();
    	return ids;	
    }
	
    // Get job name & description records from table
    public Cursor getJobName() {
    	return database.query(TABLE_QUOTES, new String [] {"rowid _id", QUOTEID, JOBNAME}, null, null, null, null, null);
    }
    
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QUOTES, editCon, QUOTEID +"="+ id, null);
    	close();
    }
    
	 // Update the active field
    public void updateOpenActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	database.update(TABLE_QUOTES, editCon, QUOTEID +"="+ id, null);
    }
    
    // Remove record from table
 	public void deleteQuote(int id) {
 		open(); 
 		database.delete(TABLE_QUOTES, QUOTEID +"="+ id, null);
 		close();
 	}
 	
 	 public int getNextId(){
     	String[] args = {"quotes"};
     	open();
     	Cursor cur = database.rawQuery("SELECT * FROM sqlite_sequence WHERE name=?", args);
     	cur.moveToFirst();
     	int col = cur.getColumnIndex("seq");
     	int lastRow = cur.getInt(col);
     	close();
     	return lastRow;
     }

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_QUOTES, null, null);
 		close();
 	}
	
}
