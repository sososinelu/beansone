package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SuppliersConnector {
	//Suppliers table variables	 
	
	public static final String TABLE_SUPPLIERS = "suppliers";
	public static final String SUPPLIERID = "supplierID";
	private static final String COMPANY = "company";
	private static final String ADDRESS_0 = "address-0";
	private static final String STREETNUM = "streetNum";
	private static final String STREET = "street";
	private static final String TOWN = "town";
	private static final String COUNTY = "county";
	private static final String POSTCODE = "postcode";
	private static final String MOBILE = "mobile";
	private static final String OFFICE = "office";
	private static final String EMAIL = "email";
	private static final String NOTES = "notes";
	private static final String ACTIVE = "active";
	private static final String EDITDATE = "editDate";
	
	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public SuppliersConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertSupplier(String company, String address_0, String streetNum, String street, 
    		String town, String county, String postcode, String mobile, String office, String email, String notes) {
    	  	
    	ContentValues newCon = new ContentValues();		      
    	newCon.put(COMPANY, company);
    	newCon.put(ADDRESS_0, address_0);
    	newCon.put(STREETNUM, streetNum);
    	newCon.put(STREET, street);
    	newCon.put(TOWN, town);
    	newCon.put(COUNTY, county);
    	newCon.put(POSTCODE, postcode);
    	newCon.put(MOBILE, mobile);
    	newCon.put(OFFICE, office);
    	newCon.put(EMAIL, email);
    	newCon.put(NOTES, notes);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_SUPPLIERS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateSupplier(int id, String company, String address_0, String streetNum, String street, 
    		String town, String county, String postcode, String mobile, String office, String email, String notes) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(COMPANY, company);
    	editCon.put(ADDRESS_0, address_0);
    	editCon.put(STREETNUM, streetNum);
    	editCon.put(STREET, street);
    	editCon.put(TOWN, town);
    	editCon.put(COUNTY, county);
    	editCon.put(POSTCODE, postcode);
    	editCon.put(MOBILE, mobile);
    	editCon.put(OFFICE, office);
    	editCon.put(EMAIL, email);
    	editCon.put(NOTES, notes);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_SUPPLIERS, editCon, SUPPLIERID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllSuppliers() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_SUPPLIERS, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getSupplier(int id) {
    	return database.query(TABLE_SUPPLIERS, null, SUPPLIERID +"="+ id, null, null, null, null);
    }
	
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_SUPPLIERS, editCon, SUPPLIERID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteSupplier(int id) {
 		open(); 
 		database.delete(TABLE_SUPPLIERS, SUPPLIERID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_SUPPLIERS, null, null);
 		close();
 	}
	
}