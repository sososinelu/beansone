package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class TasksConnector {

	// Tasks table variables	 
	public static final String TABLE_TASKS = "tasks";
	public static final String TASKID = "taskID";
	public static final String JOBID = "jobID";
	public static final String INVOICEID = "invoiceID";
	public static final String TASKNAME = "taskName";
	public static final String ACTUALDURATION = "actualDuration";
	public static final String INCVAT = "incVAT";
	public static final String CHARGEVALUE = "chargeValue";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";
	
	public static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public TasksConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertTask(int jobID , int invoiceID, String taskName, double actualDuration, String incVAT, double chargeValue) {  
    	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(JOBID, jobID);
		newCon.put(INVOICEID, invoiceID);
		newCon.put(TASKNAME, taskName);
		newCon.put(ACTUALDURATION, actualDuration);
		newCon.put(INCVAT, incVAT);
		newCon.put(CHARGEVALUE, chargeValue);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_TASKS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateTask(int id, int jobID , int invoiceID, String taskName, double actualDuration, String incVAT, double chargeValue) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(JOBID, jobID);
		editCon.put(INVOICEID, invoiceID);
		editCon.put(TASKNAME, taskName);
		editCon.put(ACTUALDURATION, actualDuration);
		editCon.put(INCVAT, incVAT);
		editCon.put(CHARGEVALUE, chargeValue);
		editCon.put(ACTIVE, "true");	      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_TASKS, editCon, TASKID +"="+ id, null);
    	close();
    }
    
    public void updateInvoiceId(int id, int invoiceID){
    	ContentValues editCon = new ContentValues();
    	editCon.put(INVOICEID, invoiceID);
    	
    	open();
    	database.update(TABLE_TASKS, editCon, TASKID +"="+ id, null);
    	close();
    }

    public void updateIds(int id, int jobID, int invoiceID){
    	ContentValues editCon = new ContentValues();
    	editCon.put(INVOICEID, invoiceID);
    	editCon.put(JOBID, jobID);
    	
    	open();
    	database.update(TABLE_TASKS, editCon, TASKID +"="+ id, null);
    	close();
    }
    
	// Get all the records from table	  
    public Cursor getAllTasks() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_TASKS, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getTask(int id) {
    	open();
    	return database.query(TABLE_TASKS, null, TASKID +"="+ id, null, null, null, null);
    }
	
    public boolean countByJob(int id){
    	boolean result = false;    	
    	open();
    	Cursor cur = database.query(TABLE_TASKS, null, JOBID +"="+ id +" AND "+INVOICEID+"=0 AND "+ACTIVE+" LIKE 'true'", null, null, null, null);
    	try {
    		if (cur.getCount()>0){
    			result = true;
    		}
    	} catch (Exception e){
    	}
    	close();
    	return result;
    }
    
    public Cursor getTasksByJob(int id){
    	open();
    	return database.query(TABLE_TASKS, null, JOBID +"="+ id +" AND "+INVOICEID+"=0 AND "+ACTIVE+" LIKE 'true'", null, null, null, null);
    }
    
    public Cursor getTasksByJobInvoice(int id){
    	open();
    	return database.query(TABLE_TASKS, null, INVOICEID +"="+ id +" AND "+INVOICEID+"<>0", null, null, null, null);
    }
  	//get newest ID
    public int getNextId(){
    	String[] args = {"tasks"};
    	open();
    	Cursor cur = database.rawQuery("SELECT * FROM sqlite_sequence WHERE name=?", args);
    	cur.moveToFirst();
    	int col = cur.getColumnIndex("seq");
    	int lastRow = cur.getInt(col);
    	close();
    	return lastRow;
    }
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_TASKS, editCon, TASKID +"="+ id, null);
    	close();
    }
    
    public void updateActiveUninvoiced(int id, String active){
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_TASKS, editCon, JOBID +"="+ id + " AND "+INVOICEID + "=0", null);
    	close();
    }
    
    public void updateOpenActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	database.update(TABLE_TASKS, editCon, TASKID +"="+ id, null);
    }
    
    // Remove record from table
 	public void deleteTask(int id) {
 		open(); 
 		database.delete(TABLE_TASKS, TASKID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_TASKS, null, null);
 		close();
 	}
	
}