package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ExpSubTypeConnector {
	//ExpSubType table variables	 
	public static final String TABLE_EXPSUBTYPE = "expSubType";
	public static final String EXPSUBTYPEID = "expSubTypeID";
	private static final String EXPTYPEID = "expTypeID";
	private static final String EXPSUBTYPE = "expSubType";
	private static final String CODE = "code";
	private static final String ACTIVE = "active";
	private static final String EDITDATE = "editDate";
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public ExpSubTypeConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertExpSubType(int expTypeID, String expSubType, String code) {
    	  	
    	ContentValues newCon = new ContentValues();		      
    	newCon.put(EXPTYPEID, expTypeID);
    	newCon.put(EXPSUBTYPE, expSubType);
    	newCon.put(CODE, code);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_EXPSUBTYPE, null, newCon);
		close();
    }
    
    // Update database table
    public void updateExpSubType(int id, int expTypeID, String expSubType, String code) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(EXPTYPEID, expTypeID);
    	editCon.put(EXPSUBTYPE, expSubType);
    	editCon.put(CODE, code);	      
    	editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_EXPSUBTYPE, editCon, EXPSUBTYPEID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllExpSubTypes() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_EXPSUBTYPE, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getExpSubType(int id) {
    	return database.query(TABLE_EXPSUBTYPE, null, EXPSUBTYPEID +"="+ id, null, null, null, null);
    }
    //Get all by Root Expense Type
    public Cursor getAllExpSubTypesByRoot(int root) {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_EXPSUBTYPE, new String[] {EXPSUBTYPEID, EXPTYPEID, EXPSUBTYPE, CODE}, EXPTYPEID +"="+root, null, null, null, EXPSUBTYPE + " ASC");
    }
    
    public void updateName(int id, String name) {   	
    	ContentValues editCon = new ContentValues();
		editCon.put(EXPSUBTYPE, name);

    	open();
    	database.update(TABLE_EXPSUBTYPE, editCon, EXPSUBTYPEID +"="+ id, null);
    	close();
    }
    
    
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_EXPSUBTYPE, editCon, EXPSUBTYPEID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteExpSubType(int id) {
 		open(); 
 		database.delete(TABLE_EXPSUBTYPE, EXPSUBTYPEID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_EXPSUBTYPE, null, null);
 		close();
 	}
}