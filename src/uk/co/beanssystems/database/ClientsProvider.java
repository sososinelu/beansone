package uk.co.beanssystems.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class ClientsProvider extends ContentProvider {
	
	private static final String TABLE_CLIENTS = "clients";
	private static final String DB_NAME = "BeansSystems";
	private static final String ID = "_id";
	private DatabaseHandler db;
	private static final String AUTHORITY = "uk.co.beanssystems.data.ClientsProvider";
	public static final int CLIENTS = 100;
	public static final int CLIENT_ID = 110;
	private static final String CLIENTS_BASE_PATH = "clients";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + CLIENTS_BASE_PATH);
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/beans";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/beans";
	private static final UriMatcher sURIMatcher = new UriMatcher( UriMatcher.NO_MATCH);
	static {
	    sURIMatcher.addURI(AUTHORITY, CLIENTS_BASE_PATH, CLIENTS);
	    sURIMatcher.addURI(AUTHORITY, CLIENTS_BASE_PATH + "/#", CLIENT_ID);
	}

	@Override
	public boolean onCreate() {
		db = new DatabaseHandler(getContext(), DB_NAME, null, 1);
		return true;
	}
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	    queryBuilder.setTables(TABLE_CLIENTS);
	    
		int uriType = sURIMatcher.match(uri);
		switch (uriType){
			case CLIENTS://Get All Clients
				break;
			case CLIENT_ID:
				queryBuilder.appendWhere(ID + "="
		                + uri.getLastPathSegment());
				break;
			default:
				throw new IllegalArgumentException("Unknown URI");
		}
		
		Cursor cur = queryBuilder.query(db.getReadableDatabase(),
	            projection, selection, selectionArgs, null, null, sortOrder);
		
		cur.setNotificationUri(getContext().getContentResolver(), uri);
		return cur;
	}
	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		return null;
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		return null;
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		return 0;
	}

}
