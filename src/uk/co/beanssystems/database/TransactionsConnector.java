package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class TransactionsConnector {
	//Transactions table variables	 
	public static final String TABLE_TRANSACTIONS = "transactions";
	public static final String TRANSACTIONID = "transactionID";
	private static final String EXPENSEID = "expenseID";
	private static final String INVOICEID = "invoiceID";
	private static final String JOBID = "jobID";
	private static final String BANKID = "bankID";
	private static final String DATE = "date";
	private static final String AMOUNT = "amount";
	private static final String PAIDTYPE = "paidType";
	private static final String CLEAREDBANK = "clearedBank";
	private static final String ACTIVE = "active";
	private static final String EDITDATE = "editDate";
	
	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public TransactionsConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertTransaction(int expenseID, int invoiceID, int jobID, int bankID, String date, double amount,
    		String paidType, String clearedBank) {
    	  	
    	ContentValues newCon = new ContentValues();		      
    	newCon.put(EXPENSEID,expenseID);
    	newCon.put(INVOICEID,invoiceID);
    	newCon.put(JOBID,jobID);
    	newCon.put(BANKID,bankID);
    	newCon.put(DATE,date);
    	newCon.put(AMOUNT,amount);
    	newCon.put(PAIDTYPE,paidType);
    	newCon.put(CLEAREDBANK,clearedBank);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_TRANSACTIONS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateTransaction(int id, int expenseID, int invoiceID, int jobID, int bankID, String date, double amount,
    		String paidType, String clearedBank) {
    	
    	ContentValues editCon = new ContentValues();
      	editCon.put(EXPENSEID,expenseID);
    	editCon.put(INVOICEID,invoiceID);
    	editCon.put(JOBID,jobID);
    	editCon.put(BANKID,bankID);
    	editCon.put(DATE,date);
    	editCon.put(AMOUNT,amount);
    	editCon.put(PAIDTYPE,paidType);
    	editCon.put(CLEAREDBANK,clearedBank);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_TRANSACTIONS, editCon, TRANSACTIONID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllTransactions() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_TRANSACTIONS, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getTransaction(int id) {
    	return database.query(TABLE_TRANSACTIONS, null, TRANSACTIONID +"="+ id, null, null, null, null);
    }
	
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_TRANSACTIONS, editCon, TRANSACTIONID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteTransaction(int id) {
 		open(); 
 		database.delete(TABLE_TRANSACTIONS, TRANSACTIONID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_TRANSACTIONS, null, null);
 		close();
 	}
	
}