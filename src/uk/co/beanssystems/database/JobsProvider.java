package uk.co.beanssystems.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class JobsProvider extends ContentProvider {
	
	private static final String TABLE_JOBS = "jobs";
	private static final String DB_NAME = "BeansSystems";
	private static final String ID = "_id";
	private DatabaseHandler db;
	private static final String AUTHORITY = "uk.co.beanssystems.data.JobsProvider";
	public static final int JOBS = 100;
	public static final int JOB_ID = 110;
	private static final String JOBS_BASE_PATH = "jobs";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + JOBS_BASE_PATH);
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/beans";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/beans";
	private static final UriMatcher sURIMatcher = new UriMatcher( UriMatcher.NO_MATCH);
	static {
	    sURIMatcher.addURI(AUTHORITY, JOBS_BASE_PATH, JOBS);
	    sURIMatcher.addURI(AUTHORITY, JOBS_BASE_PATH + "/#", JOB_ID);
	}

	@Override
	public boolean onCreate() {
		db = new DatabaseHandler(getContext(), DB_NAME, null, 1);
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	    queryBuilder.setTables(TABLE_JOBS);
	    
		int uriType = sURIMatcher.match(uri);
		switch (uriType){
			case JOBS://Get All Jobs
				break;
			case JOB_ID:
				queryBuilder.appendWhere(ID + "="
		                + uri.getLastPathSegment());
				break;
			default:
				throw new IllegalArgumentException("Unknown URI");
		}
		
		Cursor cur = queryBuilder.query(db.getReadableDatabase(),
	            projection, selection, selectionArgs, null, null, sortOrder);
		
		cur.setNotificationUri(getContext().getContentResolver(), uri);
		return cur;
	}
	
	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		// TODO Auto-generated method stub
		return 0;
	}

}
