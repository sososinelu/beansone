package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ExpTypeConnector {
	//ExpSubType table variables	 
	public static final String TABLE_EXPTYPE = "expType";
	public static final String EXPTYPEID = "expTypeID";
	public static final String EXPTYPE = "expType";
	public static final String CODE = "code";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public ExpTypeConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertExpType(String expType, String code) {
    	  	
    	ContentValues newCon = new ContentValues();		      
    	newCon.put(EXPTYPE, expType);
    	newCon.put(CODE, code);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_EXPTYPE, null, newCon);
		close();
    }
    
    // Update database table
    public void updateExpType(int id, String expType, String code) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(EXPTYPE, expType);
    	editCon.put(CODE, code);	      
    	editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_EXPTYPE, editCon, EXPTYPEID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllExpTypes() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_EXPTYPE, new String[] {EXPTYPEID, EXPTYPE, CODE}, null, null, null, null, EXPTYPE+" ASC");
    }
    
    // Get one record from table
    public Cursor getExpType(int id) {
    	return database.query(TABLE_EXPTYPE, null, EXPTYPEID +"="+ id, null, null, null, null);
    }
	
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_EXPTYPE, editCon, EXPTYPEID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteExpType(int id) {
 		open(); 
 		database.delete(TABLE_EXPTYPE, EXPTYPEID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_EXPTYPE, null, null);
 		close();
 	}
}