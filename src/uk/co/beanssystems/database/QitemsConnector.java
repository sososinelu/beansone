package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class QitemsConnector {
	
	// QItems table variables	 
	public static final String TABLE_QITEMS = "qitems";
	public static final String QITEMID = "qitemID";
	public static final String QUOTEID = "quoteID";
	public static final String SUPPLIERID = "supplierID";
	public static final String ITEMNAME = "itemName";
	public static final String UNITCOST = "unitCost";
	public static final String INCVAT = "incVAT";
	public static final String QUANTITY = "quantity";
	public static final String TOTALCOST = "totalCost";
	public static final String TOTALVAT = "totalVAT";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";	
	
	public static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public QitemsConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertQitem(int quoteID , int supplierID, String itemName, double unitCost, String incVAT, int quantity, 
    		double  totalCost, double totalVAT) {  
    	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(QUOTEID, quoteID);
		newCon.put(SUPPLIERID, supplierID);
		newCon.put(ITEMNAME, itemName);
		newCon.put(UNITCOST, unitCost);
		newCon.put(INCVAT, incVAT);
		newCon.put(QUANTITY, quantity);
		newCon.put(TOTALCOST, totalCost);
		newCon.put(TOTALVAT, totalVAT);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_QITEMS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateQitem(int id, int quoteID , int supplierID, String itemName, double unitCost, String incVAT, int quantity, 
    		double  totalCost, double totalVAT) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(QUOTEID, quoteID);
		editCon.put(SUPPLIERID, supplierID);
		editCon.put(ITEMNAME, itemName);
		editCon.put(UNITCOST, unitCost);
		editCon.put(INCVAT, incVAT);
		editCon.put(QUANTITY, quantity);
		editCon.put(TOTALCOST, totalCost);
		editCon.put(TOTALVAT, totalVAT);      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QITEMS, editCon, QITEMID +"="+ id, null);
    	close();
    }
    
	// Get all the records from table	  
    public Cursor getAllQitems() {
    	open();
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_QITEMS, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getQitem(int id) {
    	open();
    	return database.query(TABLE_QITEMS, null, QITEMID +"="+ id, null, null, null, null);
    }
    
    public Cursor getItemsByQuote(int id){
    	open();
    	return database.query(TABLE_QITEMS, null, QUOTEID +"="+ id, null, null, null, null);
    }
	
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QITEMS, editCon, QITEMID +"="+ id, null);
    	close();
    }
    
    public void updateAllActive(int id, String active){
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_QITEMS, editCon, QUOTEID +"="+ id, null);
    	close();
    }
    
	 // Update the active field
    public void updateOpenActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	database.update(TABLE_QITEMS, editCon, QITEMID +"="+ id, null);
    }
    
    // Remove record from table
 	public void deleteQitem(int id) {
 		open(); 
 		database.delete(TABLE_QITEMS, QITEMID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_QITEMS, null, null);
 		close();
 	}
	
}
