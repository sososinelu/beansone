package uk.co.beanssystems.database;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import android.database.DatabaseUtils.InsertHelper;

public class MasterConnector {
	private static final String DB_NAME = "BeansSystems", EDITDATE = "editDate";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	Context context;
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
	public static final String EXPTYPEID = "expTypeID";
	
	private static final String tableN [] = {
		ClientsConnector.TABLE_CLIENTS,
		JobsConnector.TABLE_JOBS,
		QuotesConnector.TABLE_QUOTES,
		ExpensesConnector.TABLE_EXPENSES,
		ExpTypeConnector.TABLE_EXPTYPE,
		ExpSubTypeConnector.TABLE_EXPSUBTYPE,
		InvoicesConnector.TABLE_INVOICES,
		ItemsConnector.TABLE_ITEMS,
		TransactionsConnector.TABLE_TRANSACTIONS,
		TasksConnector.TABLE_TASKS,
		BanksConnector.TABLE_BANKS,
		SuppliersConnector.TABLE_SUPPLIERS,
		SettingsConnector.TABLE_SETTINGS,
		QtasksConnector.TABLE_QTASKS,
		QitemsConnector.TABLE_QITEMS,
		RatesConnector.TABLE_RATES,
		"sqlite_sequence"
		};
	
	private static final String tableID [] = {
		ClientsConnector.CLIENTID, 
		JobsConnector.JOBID,
		QuotesConnector.QUOTEID,
		ExpensesConnector.EXPENSEID,
		ExpTypeConnector.EXPTYPEID,
		ExpSubTypeConnector.EXPSUBTYPEID,
		InvoicesConnector.INVOICEID,
		ItemsConnector.ITEMID,
		TransactionsConnector.TRANSACTIONID,
		TasksConnector.TASKID,
		BanksConnector.BANKID,
		SuppliersConnector.SUPPLIERID,
		SettingsConnector.USERID,
		QtasksConnector.QTASKID,
		QitemsConnector.QITEMID,
		RatesConnector.RATEID
		};
	
	public MasterConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
		this.context = context;
	}
	
	public void open() throws SQLException {
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	public void emptyTables(){
		open();
		for (int i=0;i<tableN.length;i++){
			if (!SettingsConnector.TABLE_SETTINGS.equals(tableN[i])){
				int rows = database.delete(tableN[i], null, null);
				Log.i("DELETE", String.valueOf(rows)+" deleted from: "+tableN[i]);
			}
		}
		close();
	}
	
	public void restoreDataFast(JSONObject commands){
		for (int n=0;n<tableN.length;n++){
			String tbl = tableN[n].toString();
			String insert="";
			JSONArray values = new JSONArray();
			try{
				JSONObject table = commands.getJSONObject(tableN[n].toLowerCase());
				insert = table.getString("insert");
				values = table.getJSONArray("values");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				open();
				database.beginTransaction();
				SQLiteStatement in = database.compileStatement(insert);
				JSONArray rec = null;
				for (int i=0;i<values.length();i++){
						rec = values.getJSONArray(i);
						for (int x=0;x<rec.length();x++){
							in.bindString(x+1, rec.getString(x));
						}
						in.execute();
						in.clearBindings();
						
				}
				database.setTransactionSuccessful();
			} catch (SQLiteException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				database.endTransaction();
				close();
			}
		}
		restoreJobPictures();
		restoreExpPictures();
		Toast.makeText(context, "Restore Finished", Toast.LENGTH_SHORT).show();
	}
	
	public void restoreJobPictures(){
		File jobs = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "BeansOne" + File.separator + "Jobs");
		if (!jobs.exists()){
			jobs.mkdirs();
		}
		JobsConnector jobCon = new JobsConnector(context);
		jobCon.open();
		Cursor jCur = jobCon.getJobFolder();
		try {
			jCur.moveToFirst();
			do {
				String folder = jCur.getString(0) + "_" + jCur.getString(1).replace(" ", "_");
				File job = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), 
						"BeansOne_OLD"+ File.separator + "Jobs" + File.separator +  folder);
				if (job.exists()){
					File moveJob = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), 
							"BeansOne"+ File.separator + "Jobs" + File.separator + folder);
					if (job.renameTo(moveJob)){
						Log.i("FILEOP", "Move Successful");
					} else {
						Log.i("FILEOP", "Move Failed");
					}
				}
			} while (jCur.moveToNext());
			
			
		} catch (Exception e){
			e.printStackTrace();
		}
		jobCon.close();
	}
	
	public void restoreExpPictures(){
		File exps = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "BeansOne" + File.separator + "Expenses");
		if (!exps.exists()){
			exps.mkdirs();
		}
		File old = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "BeansOne_OLD" + File.separator + "Expenses");
		if (old.exists()){
			ExpensesConnector eCon = new ExpensesConnector(context);
			File[] files = old.listFiles();
			for (File f : files){
				if (eCon.findPhoto(f.getAbsolutePath().replace("BeansOne_OLD", "BeansOne"))){
					File move = new File(f.getAbsolutePath().replace("BeansOne_OLD", "BeansOne"));
					f.renameTo(move);
					Log.i("FILEOP", "File Moved");
				} else {
					Log.i("FILEOP", "File Not Found");
				}
			}
		}
	}
	
	public JSONArray getJSON(JSONObject tables){
		JSONArray jar = new JSONArray();
		open();
		for (int i=0;i<tableN.length;i++){		
			JSONArray records = new JSONArray();
			JSONObject dates = new JSONObject();
			try {
				dates = tables.getJSONObject(tableN[i]);
			} catch (JSONException e2) {
				Log.i(tableN[i], "No Server Records");
			}
			Cursor cur = database.query(tableN[i], null, null, null, null, null, null);
			if (cur.moveToFirst()){
				int cols = cur.getColumnCount() - 1;
				int id;
				Date cDate = new Date();
				Date sDate = cDate;
				boolean newRec = false;	
				do {
					newRec = false;
					try {
						id = cur.getInt(cur.getColumnIndex(tableID[i]));
						sDate = ft.parse(dates.getString(String.valueOf(id)));
						cDate = ft.parse(cur.getString(cur.getColumnIndex(EDITDATE)));
					} catch (ParseException e1) {
						Log.i("Cursor Parse","Couldn't parse from cursor");
					} catch (JSONException e) {
						newRec = true;
					} catch (Exception e){;
						newRec = true;
					}
						
					if (cDate.compareTo(sDate)>0  || newRec==true){
						JSONObject job = new JSONObject();
						try {
							for (int x=0;x<=cols;x++){
										job.put(cur.getColumnName(x), cur.getString(x));					
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					records.put(job);
					}
				} while (cur.moveToNext());
			}
			
			JSONObject table = new JSONObject();
			try {
				table.put(tableN[i], records);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			jar.put(table);
		}
		close();
		return jar;
	}
}
