package uk.co.beanssystems.database;

public class ServerCommands {
	
	public static final String clients =
			"CREATE TABLE clients " +
			"(clientID INTEGER PRIMARY KEY, title VARCHAR(8), forename TEXT, surname TEXT, company TEXT, " +
			"address TEXT, postcode TEXT, mobile TEXT, phone TEXT, email TEXT, notes TEXT, active VARCHAR(5), editDate DATETIME);";

	public static final String quotes = 
			"CREATE TABLE quotes (quoteID INTEGER PRIMARY KEY, clientID INTEGER, jobID INTEGER, jobName TEXT, dateIssued DATE, " +
			"estimatedHours DOUBLE, notes TEXT, subTotal DOUBLE, baseTotal DOUBLE, vatType VARCHAR(5), totalCost DOUBLE, totalVAT DOUBLE," +
			" fixedCost DOUBLE, status VARCHAR(15), active VARCHAR(10), editDate DATETIME);";
	
	public static final String qitems = 
			"CREATE TABLE qitems (qitemID INTEGER PRIMARY KEY, quoteID INTEGER, supplierID INTEGER, itemName TEXT, unitCost DOUBLE, " +
			"incVAT VARCHAR(15), quantity INTEGER, totalCost DOUBLE, totalVAT DOUBLE, active VARCHAR(10), editDate DATETIME);";
	
	public static final String qtasks = 
			"CREATE TABLE qtasks (qtaskID INTEGER PRIMARY KEY, quoteID INTEGER, taskName TEXT, estimatedDuration DOUBLE, " +
			"incVAT VARCHAR(10), chargeValue DOUBLE, active VARCHAR(10), editDate DATETIME);";
	
	public static final String jobs = 
			"CREATE TABLE jobs (jobID INTEGER PRIMARY KEY, clientID INTEGER, clientName TEXT, jobName TEXT, estimatedHours DOUBLE, " +
			"estimatedCost DOUBLE, billingMethod VARCHAR(15), fixedCost DOUBLE, startDate DATE, endDate DATE, repeatNew VARCHAR(12), description TEXT, " +
			"active VARCHAR(10), editDate DATETIME);";
	
	public static final String items = 
			"CREATE TABLE items (itemID INTEGER PRIMARY KEY, jobID INTEGER, supplierID INTEGER, invoiceID INTEGER, itemName TEXT, unitCost DOUBLE, " +
			"incVAT VARCHAR(15), quantity INTEGER, totalCost DOUBLE, totalVAT DOUBLE, active VARCHAR(10), editDate DATETIME);";
	
	public static final String tasks = 
			"CREATE TABLE tasks (taskID INTEGER PRIMARY KEY, jobID INTEGER, invoiceID INTEGER, taskName TEXT, actualDuration DOUBLE, " +
			"incVAT VARCHAR(10), chargeValue DOUBLE, active VARCHAR(10), editDate DATETIME);";
	
	public static final String expenses = 
			"CREATE TABLE expenses (expenseID INTEGER PRIMARY KEY, jobID INTEGER, clientID INTEGER, invoiceID INTEGER, supplierID INTEGER, expCategory INTEGER, " +
			"expType TEXT, date DATE, cost DOUBLE, vat DOUBLE, paid DOUBLE, description TEXT, photo TEXT, active VARCHAR(10), editDate DATETIME);";  
	
	public static final String invoices = 
			"CREATE TABLE invoices (invoiceID INTEGER PRIMARY KEY, clientID INTEGER, jobID INTEGER, invoiceNumber INTEGER, dateIssued DATE, dateDue DATE, " +
			"subTotal DOUBLE, vat DOUBLE, totalDue DOUBLE, baseTotal DOUBLE, vatType VARCHAR(5), outstanding DOUBLE, fixedCost DOUBLE, status VARCHAR(15), " +
			"terms TEXT, notes TEXT, active VARCHAR(10), editDate DATETIME);";
		
	public static final String settings = 
			"CREATE TABLE settings (userID INTEGER PRIMARY KEY, userName TEXT, userCompany TEXT, logo BLOB, vatNum TEXT, vatRate VARCHAR(5), " +
			"address TEXT, postcode TEXT, contactNum TEXT, email TEXT, invoiceComment TEXT, terms TEXT, " +
			"backupType VARCHAR(20), backupPeriod VARCHAR(15), currency VARCHAR(10), moneyDisplay VARCHAR(10), hourlyRate VARCHAR(10), " +
			"active VARCHAR(10), editDate DATETIME);";
	
	public static final String expType = 
			"CREATE TABLE expType (expTypeID INTEGER PRIMARY KEY, expType TEXT, code VARCHAR(3), active VARCHAR(10), editDate DATETIME);";
	
	public static final String expSubType = 
			"CREATE TABLE expSubType (expSubTypeID INTEGER PRIMARY KEY, expTypeID INTEGER, expSubType TEXT, code VARCHAR(3), active VARCHAR(10), " +
			"editDate DATETIME);";
	
	public static final String transactions = 
		"CREATE TABLE transactions (transactionID INTEGER PRIMARY KEY, expenseID INTEGER, invoiceID INTEGER, jobID INTEGER, bankID INTEGER, date DATE, " +
		"amount DOUBLE, paidType VARCHAR(30), clearedBank DATE, active VARCHAR(10), editDate DATETIME);";
	
	public static final String suppliers = 
			"CREATE TABLE suppliers (supplierID INTEGER PRIMARY KEY, company TEXT, address TEXT, mobile TEXT, office TEXT, " +
			"email TEXT, notes TEXT, active TEXT, editDate DATETIME);";
	
	public static final String banks = 
			"CREATE TABLE banks (bankID INTEGER PRIMARY KEY, description TEXT, balance DOUBLE, clearedBalance DOUBLE, clearedDate DOUBLE, " +
			"checkNo INTEGER, payslipNo INTEGER, accountNo INTEGER, sortCode VARCHAR(12), active VARCHAR(10), editDate DATETIME);";

	public static final String sqlite_sequence =
			"CREATE TABLE sqlite_sequence (name TEXT, seq INTEGER);";
	
	public static final String onlineAccount =
			"CREATE TABLE onlineAccount (userID INTEGER PRIMARY KEY, userName TEXT, password TEXT, " +
			"secretQuestion TEXT, accCreated DATETIME, dbName TEXT, dbVersion TEXT, lastBackup DATETIME, lastRestore DATETIME, " +
			"active VARCHAR(10), editDate DATETIME)";
	
	public static final String rates = 
			"CREATE TABLE rates (rateID INTEGER PRIMARY KEY, rateName TEXT, rateValue DOUBLE, " +
			"rateVAT DOUBLE, incVAT VARCHAR(10), rateTime VARCHAR(10), active VARCHAR(10), editDate DATETIME)";

	public static final String[] commands = {clients, quotes, qitems, qtasks, jobs, items, tasks, expenses, invoices, 
		settings, expType, expSubType, transactions, suppliers, banks, sqlite_sequence, onlineAccount, rates};
}