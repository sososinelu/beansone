package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class RatesConnector {
	// Rates table variables	 
	public static final String TABLE_RATES = "rates";
	public static final String RATEID = "rateID";
	public static final String RATENAME = "rateName";
	public static final String RATEVALUE = "rateValue";
	public static final String RATEVAT = "rateVAT";
	public static final String INCVAT = "incVAT";
	public static final String RATETIME = "rateTime";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";		
	public static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public RatesConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertRate(String rateName, double rateValue, double rateVAT, String incVAT, String rateTime) {  
    	ContentValues newCon = new ContentValues();		      
		newCon.put(RATENAME, rateName);
		newCon.put(RATEVALUE, rateValue);
		newCon.put(RATEVAT, rateVAT);
		newCon.put(INCVAT, incVAT);
		newCon.put(RATETIME, rateTime);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_RATES, null, newCon);
		close();
    }
    
    // Update database table
    public void updateRate(int id, String rateName, double rateValue, double rateVAT, String incVAT, String rateTime) {  
    	ContentValues editCon = new ContentValues();		      
    	editCon.put(RATENAME, rateName);
    	editCon.put(RATEVALUE, rateValue);
    	editCon.put(RATEVAT, rateVAT);
    	editCon.put(INCVAT, incVAT);
    	editCon.put(RATETIME, rateTime);
    	editCon.put(ACTIVE, "true");	      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_RATES, editCon, RATEID +"="+ id, null);
    	close();
    }
    
	// Get all the records from table	  
    public Cursor getAllRatess() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_RATES, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getRate(int id) {
    	return database.query(TABLE_RATES, null, RATEID +"="+ id, null, null, null, null);
    }
   
	// Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_RATES, editCon, RATEID +"="+ id, null);
    	close();
    }
    
    //get newest ID
    public int getNextId(){
    	String[] args = {"rates"};
    	open();
    	Cursor cur = database.rawQuery("SELECT * FROM sqlite_sequence WHERE name=?", args);
    	cur.moveToFirst();
    	int col = cur.getColumnIndex("seq");
    	int lastRow = cur.getInt(col);
    	close();
    	return lastRow;
    }
    // Remove record from table
 	public void deleteItem(int id) {
 		open(); 
 		database.delete(TABLE_RATES, RATEID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_RATES, null, null);
 		close();
 	}
}
