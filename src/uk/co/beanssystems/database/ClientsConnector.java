package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ClientsConnector {
	
	// Clients table variables	 
	public static final String TABLE_CLIENTS = "clients";
	public static final String CLIENTID = "clientID";
	public static final String TITLE = "title";
	public static final String FORENAME = "forename";
	public static final String SURNAME = "surname";
	public static final String COMPANY = "company";
	public static final String ADDRESS = "address";
	public static final String POSTCODE = "postcode";
	public static final String MOBILE = "mobile";
	public static final String PHONE = "phone";
	public static final String EMAIL = "email";
	public static final String NOTES = "notes";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";	
	
	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
	
	public ClientsConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertClient(String title, String forename, String surname, String company, String address,
    		 String postcode, String mobile, String phone, String email, String notes) {
    	  	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(TITLE, title);
		newCon.put(FORENAME, forename);
		newCon.put(SURNAME, surname);
		newCon.put(COMPANY, company);
		newCon.put(ADDRESS, address);
		newCon.put(POSTCODE, postcode);
		newCon.put(MOBILE, mobile);
		newCon.put(PHONE, phone);
		newCon.put(EMAIL, email);
		newCon.put(NOTES, notes); 
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_CLIENTS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateClient(int id, String title, String forename, String surname, String company, String address, 
			    		String postcode, String mobile, String phone, String email, String notes) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(TITLE, title);
    	editCon.put(FORENAME, forename);
    	editCon.put(SURNAME, surname);
    	editCon.put(COMPANY, company);
    	editCon.put(ADDRESS, address);
    	editCon.put(POSTCODE, postcode);
    	editCon.put(MOBILE, mobile);
    	editCon.put(PHONE, phone);
    	editCon.put(EMAIL, email);
    	editCon.put(NOTES, notes);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_CLIENTS, editCon, CLIENTID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllClients() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_CLIENTS, null, null, null, null, null, SURNAME);
    }
    
    // Get one record from table by clientID
    public Cursor getClient(int id) {
    	return database.query(TABLE_CLIENTS, null, CLIENTID +"="+ id, null, null, null, null);
    }
    
    // Get one record from table by surname & forename
    public Cursor getClientName(String clientName) {
    	return database.query(TABLE_CLIENTS,  new String []{"rowid _id", FORENAME, SURNAME, EMAIL },  FORENAME + " like '%" + clientName + "%'" +" OR "+ SURNAME + " like '%" + clientName + "%'", null, null, null, null);
    }
    
    public Cursor getClientOrderSurname(){
    	 return database.query(TABLE_CLIENTS, new String [] {"rowid _id", CLIENTID, FORENAME, SURNAME, COMPANY}, ACTIVE + "=?", new String[]{"true"}, null, null, SURNAME);
    }
    
    // Get client name record from table
    public Cursor getClientName() {
    	return database.query(TABLE_CLIENTS, new String [] {"rowid _id",CLIENTID, FORENAME, SURNAME, EMAIL}, null, null, null, null, null);
    }
    
    public Cursor getSingleClientName(int id) {
    	return database.query(TABLE_CLIENTS, new String [] {"rowid _id",CLIENTID, FORENAME, SURNAME, COMPANY}, CLIENTID +"="+ id, null, null, null, null);
    }
    
    public boolean isPrivate(SQLiteDatabase db, int id){
    	Cursor cur = db.query(TABLE_CLIENTS, new String [] {COMPANY}, CLIENTID +"="+ id, null, null, null, null);
    	try {
    		cur.moveToFirst();
    		if ("Private".equals(cur.getString(0))){
    			return true;
    		}
    	} catch (Exception e){}
    	return false;
    }
    
    public int getNextId(){
    	String[] args = {"clients"};
    	open();
    	Cursor cur = database.rawQuery("SELECT * FROM sqlite_sequence WHERE name=?", args);
    	cur.moveToFirst();
    	int col = cur.getColumnIndex("seq");
    	int lastRow = cur.getInt(col);
    	close();
    	return lastRow;
    }
	// Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_CLIENTS, editCon, CLIENTID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteClient(int id) {
 		open(); 
 		database.delete(TABLE_CLIENTS, CLIENTID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_CLIENTS, null, null);
 		close();
 	}
	
}
