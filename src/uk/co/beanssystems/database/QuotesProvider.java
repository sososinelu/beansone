package uk.co.beanssystems.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class QuotesProvider extends ContentProvider {
	private static final String TABLE_QUOTES = "quotes";
	private static final String DB_NAME = "BeansSystems";
	private static final String ID = "_id";
	private DatabaseHandler db;
	private static final String AUTHORITY = "uk.co.beanssystems.data.QuotesProvider";
	public static final int QUOTES = 100;
	public static final int QUOTE_ID = 110;
	private static final String QUOTES_BASE_PATH = "quotes";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + QUOTES_BASE_PATH);
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/beans";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/beans";
	private static final UriMatcher sURIMatcher = new UriMatcher( UriMatcher.NO_MATCH);
	static {
	    sURIMatcher.addURI(AUTHORITY, QUOTES_BASE_PATH, QUOTES);
	    sURIMatcher.addURI(AUTHORITY, QUOTES_BASE_PATH + "/#", QUOTE_ID);
	}
	
	@Override
	public boolean onCreate() {
		db = new DatabaseHandler(getContext(), DB_NAME, null, 1);
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	    queryBuilder.setTables(TABLE_QUOTES);
	    
		int uriType = sURIMatcher.match(uri);
		switch (uriType){
			case QUOTES://Get All Quotes
				break;
			case QUOTE_ID:
				queryBuilder.appendWhere(ID + "="
		                + uri.getLastPathSegment());
				break;
			default:
				throw new IllegalArgumentException("Unknown URI");
		}
		
		Cursor cur = queryBuilder.query(db.getReadableDatabase(),
	            projection, selection, selectionArgs, null, null, sortOrder);
		
		cur.setNotificationUri(getContext().getContentResolver(), uri);
		return cur;
	}
	
	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		return null;
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		return null;
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		return 0;
	}

}
