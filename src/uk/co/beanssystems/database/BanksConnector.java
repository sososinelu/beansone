package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class BanksConnector {
	//Banks table variables	 
	public static final String TABLE_BANKS = "banks";
	public static final String BANKID = "bankID";
	private static final String DESCRIPTION = "description";
	private static final String BALANCE = "balance";
	private static final String CLEAREDBALANCE = "clearedBalance";
	private static final String CLEAREDDATE = "clearedDate";
	private static final String CHECKNO = "checkNo";
	private static final String PAYSLIPNO = "payslipNo";
	private static final String ACCOUNTNO = "accountNo";
	private static final String SORTCODE = "sortCode";
	private static final String ACTIVE = "active";
	private static final String EDITDATE = "editDate";	
	
	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public BanksConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertBank(String description, double balance, double clearedBalance, String clearedDate,
    		int checkNo, int payslipNo, int accountNo, String sortCode) {
    	  	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(DESCRIPTION,description);
		newCon.put(BALANCE,balance);
		newCon.put(CLEAREDBALANCE,clearedBalance);
		newCon.put(CLEAREDDATE,clearedDate);
		newCon.put(CHECKNO,checkNo);
		newCon.put(PAYSLIPNO,payslipNo);
		newCon.put(ACCOUNTNO,accountNo);
		newCon.put(SORTCODE,sortCode);			
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_BANKS, null, newCon);
		close();
    }
    
    // Update database table
    public void updateBank(int id, String description, double balance, double clearedBalance, String clearedDate,
    		int checkNo, int payslipNo, int accountNo, String sortCode) {
    	
    	ContentValues editCon = new ContentValues();
		editCon.put(DESCRIPTION,description);
		editCon.put(BALANCE,balance);
		editCon.put(CLEAREDBALANCE,clearedBalance);
		editCon.put(CLEAREDDATE,clearedDate);
		editCon.put(CHECKNO,checkNo);
		editCon.put(PAYSLIPNO,payslipNo);
		editCon.put(ACCOUNTNO,accountNo);
		editCon.put(SORTCODE,sortCode);			
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_BANKS, editCon, BANKID +"="+ id, null);
    	close();
    }

	// Get all the records from table	  
    public Cursor getAllBanks() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_BANKS, null, null, null, null, null, null);
    }
    
    // Get one record from table
    public Cursor getBank(int id) {
    	return database.query(TABLE_BANKS, null, BANKID +"="+ id, null, null, null, null);
    }
	
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_BANKS, editCon, BANKID +"="+ id, null);
    	close();
    }
    
    // Remove record from table
 	public void deleteBank(int id) {
 		open(); 
 		database.delete(TABLE_BANKS, BANKID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_BANKS, null, null);
 		close();
 	}
	
}