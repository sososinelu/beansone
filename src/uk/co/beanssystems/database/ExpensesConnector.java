package uk.co.beanssystems.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ExpensesConnector {

	// Clients table variables	 
	public static final String TABLE_EXPENSES = "expenses";
	public static final String EXPENSEID = "expenseID";
	public static final String JOBID = "jobID";
	public static final String CLIENTID = "clientID";
	public static final String INVOICEID = "invoiceID";
	public static final String SUPPLIERID = "supplierID";
	public static final String EXPCATEGORY = "expCategory";
	public static final String EXPTYPE = "expType";
	public static final String DATE = "date";
	public static final String COST = "cost";
	public static final String VAT = "vat";
	public static final String PAID = "paid";
	public static final String DESCRIPTION = "description";
	public static final String PHOTO = "photo";
	public static final String ACTIVE = "active";
	public static final String EDITDATE = "editDate";	
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

	private static final String DB_NAME = "BeansSystems";
	private SQLiteDatabase database;
	private DatabaseHandler dbOpenHelper;
	
	public ExpensesConnector(Context context) {
		dbOpenHelper = new DatabaseHandler(context, DB_NAME, null, 1);
	}
	
	// Open the database
	public void open() throws SQLException {
	      //open database in reading/writing mode
	      database = dbOpenHelper.getWritableDatabase();
	} 
	
	// Close the database
	public void close() {
	      if (database != null)
	         database.close();
    }
	
	//Set the editDate variable with the current time stamp
	public String getEditDate() {
		String editDate;
		Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.UK);
		editDate = ft.format(dNow);
		return editDate;
	}
	
	// Insert into database table
    public void insertExpense(int jobID, int clientID, int invoiceID, int supplierID, int expCategory, String expType, String date, 
    		double cost, double vat, double paid, String description, String photo) {
    	  	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(JOBID, jobID);
		newCon.put(CLIENTID, clientID);
		newCon.put(INVOICEID, invoiceID);
		newCon.put(SUPPLIERID, supplierID);
		newCon.put(EXPCATEGORY, expCategory);
		newCon.put(EXPTYPE, expType);
		newCon.put(DATE, date);
		newCon.put(COST, cost);
		newCon.put(VAT, vat);
		newCon.put(PAID, paid);
		newCon.put(DESCRIPTION, description);
		newCon.put(PHOTO, photo);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_EXPENSES, null, newCon);
		close();
    }
    
    // Update database table
    public void updateExpense(int id, int jobID, int clientID, int invoiceID, int supplierID, int expCategory, String expType, String date, 
    		double cost, double vat, double paid, String description, String photo) {
    	
    	ContentValues editCon = new ContentValues();
    	editCon.put(JOBID, jobID);
    	editCon.put(CLIENTID, clientID);
		editCon.put(INVOICEID, invoiceID);
		editCon.put(SUPPLIERID, supplierID);
		editCon.put(EXPCATEGORY, expCategory);
		editCon.put(EXPTYPE, expType);
		editCon.put(DATE, date);
		editCon.put(COST, cost);
		editCon.put(VAT, vat);
		editCon.put(PAID, paid);
		editCon.put(DESCRIPTION, description);
		editCon.put(PHOTO, photo);	      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_EXPENSES, editCon, EXPENSEID +"="+ id, null);
    	close();
    }
    
    public void insertExpense(int supplierID, int expCategory, String expType, String date, 
    		double cost, double vat, double paid, String description, String photo) {
    	  	
    	ContentValues newCon = new ContentValues();		      
		newCon.put(SUPPLIERID, supplierID);
		newCon.put(EXPCATEGORY, expCategory);
		newCon.put(EXPTYPE, expType);
		newCon.put(DATE, date);
		newCon.put(COST, cost);
		newCon.put(VAT, vat);
		newCon.put(PAID, paid);
		newCon.put(DESCRIPTION, description);
		newCon.put(PHOTO, photo);
		newCon.put(ACTIVE, "true");	      
		newCon.put(EDITDATE, getEditDate());
			      
		open();
		database.insert(TABLE_EXPENSES, null, newCon);
		close();
    }
    
    // Update database table
    public void updateExpense(int id, int supplierID, int expCategory, String expType, String date, 
    		double cost, double vat, double paid, String description, String photo) {
    	
    	ContentValues editCon = new ContentValues();
		editCon.put(SUPPLIERID, supplierID);
		editCon.put(EXPCATEGORY, expCategory);
		editCon.put(EXPTYPE, expType);
		editCon.put(DATE, date);
		editCon.put(COST, cost);
		editCon.put(VAT, vat);
		editCon.put(PAID, paid);
		editCon.put(DESCRIPTION, description);
		editCon.put(PHOTO, photo);	      
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_EXPENSES, editCon, EXPENSEID +"="+ id, null);
    	close();
    }
    
    public void updateInvoiceId(int id, int invoiceID){
    	ContentValues editCon = new ContentValues();
    	editCon.put(INVOICEID, invoiceID);
    	
    	open();
    	database.update(TABLE_EXPENSES, editCon, EXPENSEID +"="+ id, null);
    	close();
    }
    
    public void updateIds(int id, int jobID, int clientID, int invoiceID){
    	ContentValues editCon = new ContentValues();
    	editCon.put(INVOICEID, invoiceID);
    	editCon.put(JOBID, jobID);
    	editCon.put(CLIENTID, clientID);
    	
    	open();
    	database.update(TABLE_EXPENSES, editCon, EXPENSEID +"="+ id, null);
    	close();
    }
    
	// Get all the records from table	  
    public Cursor getAllExpenses() {
    	//.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)  - null is the equivalent of *
    	return database.query(TABLE_EXPENSES, null, ACTIVE +"="+"?", new String[]{"true"}, null, null, DATE);
    }
    
    // Get one record from table
    public Cursor getExpense(int id) {
    	return database.query(TABLE_EXPENSES, null, EXPENSEID +"="+ id, null, null, null, null);
    }
    
    public Cursor getExpensesByCategory(int id){
    	return database.query(TABLE_EXPENSES, null, EXPCATEGORY +"="+ id + " AND "+ACTIVE+" LIKE 'true'", null, null, null, null);
    }
    
    public boolean countByJob(int id){
    	boolean result = false;    	
    	open();
    	Cursor cur = database.query(TABLE_EXPENSES, null, JOBID +"="+ id +" AND "+INVOICEID+"=0 AND "+ACTIVE+" LIKE 'true'", null, null, null, null);
    	try {
    		if (cur.getCount()>0){
    			result = true;
    		}
    	} catch (Exception e){
    	}
    	close();
    	return result;
    }
    
    // Get one record from table by job
    public Cursor getExpsByJob(int id) {
    	return database.query(TABLE_EXPENSES, null, JOBID +"="+ id +" AND "+INVOICEID+"=0 AND "+ACTIVE+" LIKE 'true'", null, null, null, null);
    }
	
    public boolean findPhoto(String filename){
    	open();
    	Cursor cur = database.query(TABLE_EXPENSES, new String[]{PHOTO}, PHOTO + "=?", new String[]{filename}, null, null, null);
    	try {
    		if (cur.getCount()>0){
    			close();
    			return true;
    		} else {
    			close();
    			return false;
    		}
    	} catch (Exception e){

    	}
    	close();
    	return false;
    }
    
 // Get one record from table by job
    public Cursor getExpsByJobInvoice(int id) {
    	return database.query(TABLE_EXPENSES, null, INVOICEID +"="+ id +" AND "+INVOICEID+"<>0", null, null, null, null);
    }
	 // Update the active field
    public void updateActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_EXPENSES, editCon, EXPENSEID +"="+ id, null);
    	close();
    }
    
    public void updateActiveUninvoiced(int id, String active){
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	open();
    	database.update(TABLE_EXPENSES, editCon, JOBID +"="+ id + " AND "+INVOICEID + "=0", null);
    	close();
    }
    
	 // Update the active field
    public void updateOpenActive(int id, String active) {   	
    	ContentValues editCon = new ContentValues();
    	editCon.put(ACTIVE, active);
		editCon.put(EDITDATE, getEditDate());

    	database.update(TABLE_EXPENSES, editCon, EXPENSEID +"="+ id, null);
    }
    
  //get newest ID
    public int getNextId(){
    	String[] args = {"expenses"};
    	open();
    	Cursor cur = database.rawQuery("SELECT * FROM sqlite_sequence WHERE name=?", args);
    	cur.moveToFirst();
    	int col = cur.getColumnIndex("seq");
    	int lastRow = cur.getInt(col);
    	close();
    	return lastRow;
    }
    // Remove record from table
 	public void deleteExpense(int id) {
 		open(); 
 		database.delete(TABLE_EXPENSES, EXPENSEID +"="+ id, null);
 		close();
 	}

 	// Drop table
 	public void dropTable() {
 		open(); 
 		database.delete(TABLE_EXPENSES, null, null);
 		close();
 	}
 
}