package uk.co.beanssystems.quotes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.settings.Settings;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class QuotesList extends SherlockActivity {
	private ListView quotesListView;
	private EditText searchEditText;
	private ImageView clearSearch;
	ArrayList<QuotesListItem> quotes = null, quotesFixed = null;
	private ArrayList<String> quotesSearchArray;
	private Cursor quoteCur, clientCur;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes_list);
        ActionBar bar = getSupportActionBar();
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.quotes);
	    bar.setDisplayHomeAsUpEnabled(true);
    }
	
	@Override
	protected void onStart() {
		super.onStart();
		setQuotesList ();	
	}
	
	public void setQuotesList () {
		QuotesConnector quotesCon = new QuotesConnector(this);
        ClientsConnector clientsCon = new ClientsConnector(this);
        quotes = new ArrayList<QuotesListItem>();
        quotesFixed = new ArrayList<QuotesListItem>();
        quotesSearchArray = new ArrayList<String>();
        searchEditText = (EditText)findViewById(R.id.quoListSearch);
		clearSearch = (ImageView)findViewById(R.id.clearSearchQuotes);
        quotesListView = (ListView)findViewById(R.id.quotes_list);
        quotesCon.open();
        clientsCon.open();       
        quoteCur = quotesCon.getAllQuotesForList();
        if(quoteCur.getCount() > 0){    
        	for (int i=0; i<2; i++){
        		// Get the invoices details
        		quoteCur.moveToFirst();
        		// Get the invoice columns index
        		int idCol = quoteCur.getColumnIndex(QuotesConnector.QUOTEID);
    	        int clientIdCol = quoteCur.getColumnIndex(QuotesConnector.CLIENTID);
    	        int jobNameCol = quoteCur.getColumnIndex(QuotesConnector.JOBNAME);
    	        int dateIssuedCol = quoteCur.getColumnIndex(QuotesConnector.DATEISSUED);
    	        int totalCostCol = quoteCur.getColumnIndex(QuotesConnector.TOTALCOST);
    			int statusCol = quoteCur.getColumnIndex(QuotesConnector.STATUS);
    			int fixedCostCol = quoteCur.getColumnIndex(QuotesConnector.FIXEDCOST);
    			
        		do{
        			// Get the invoice data 
        			int id = quoteCur.getInt(idCol);
        			int clientId = quoteCur.getInt(clientIdCol);
        			String jobName = quoteCur.getString(jobNameCol);
                    Calendar cal = Calendar.getInstance();
                    try {
                    	cal = convertStringToDate(quoteCur.getString(dateIssuedCol));
                    } catch (Exception e){
                    	//
                    }
                    double totalCost = quoteCur.getDouble(totalCostCol);
                	double fixedCost = quoteCur.getDouble(fixedCostCol);
                    String status = quoteCur.getString(statusCol);
                    
                    double displayAmount = (fixedCost!=0) ? fixedCost : totalCost;
                    // Get the client details
        			clientCur = clientsCon.getClient(clientId);
        			clientCur.moveToFirst();
        			// Get the client columns index
        			int clientForenameCol = clientCur.getColumnIndex(ClientsConnector.FORENAME);
        			int clientSurnameCol = clientCur.getColumnIndex(ClientsConnector.SURNAME);
        			int clientCompanyCol = clientCur.getColumnIndex(ClientsConnector.COMPANY);
        			
        			// Get the client name data 
        			String clientName = clientCur.getString(clientForenameCol) + " " + clientCur.getString(clientSurnameCol);
        			String company = clientCur.getString(clientCompanyCol);
        			
        			String string = clientName +" "+jobName;
        			
        			int icon;
    	        	if (company.compareTo("Private")==0){
    	        		icon = R.drawable.user;
    	        	} else {
    	        		icon = R.drawable.work_user;
    	        	}
	        	
                  	if (status.equals("draft") && i==0){
                  		quotesSearchArray.add(string.toLowerCase());
                    	QuotesListItem quote = new QuotesListItem(id, icon, clientId, clientName, jobName, outDate.format(cal.getTime()), String.valueOf(displayAmount), status);
                    	quotes.add(quote);
                    	quotesFixed.add(quote);
                    	System.out.println("DRAFT ADDED");
                  	} else if (status.equals("sent") && i==1){
                  		quotesSearchArray.add(string.toLowerCase());
                  		QuotesListItem quote = new QuotesListItem(id, icon, clientId, clientName, jobName, outDate.format(cal.getTime()), String.valueOf(displayAmount), status);
                    	quotes.add(quote);
                    	quotesFixed.add(quote);
                    	System.out.println("SENT ADDED");
                  	}
                  	
                } while(quoteCur.moveToNext());
        	}
        }
    	quotesListView.setEmptyView(findViewById(R.id.noQuotesListLayout));	
    	QuotesAdapter quotesAdapter = new QuotesAdapter(this, R.layout.activity_quotes_list_row, quotes);
    	quotesListView.setAdapter(quotesAdapter);
    	
    	quotesListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				QuotesListItem ili = (QuotesListItem) quotesListView.getItemAtPosition(position);
				Intent i = new Intent(getApplicationContext(), ViewQuote.class);
				i.putExtra("passQuoteID", ili.id);
				i.putExtra("passStatus", ili.status);
				startActivity(i);
			}
    	});
        
       quotesCon.close();
       clientsCon.close();
        
       searchEditText.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {	
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				quotes.clear();
				
				String search = searchEditText.getText().toString().toLowerCase();
				
				if(searchEditText.getText().toString().length() == 0){
					clearSearch.setVisibility(View.GONE);
				}else{
					clearSearch.setVisibility(View.VISIBLE);
				}
				
				boolean exist = false;		

				int searchListLength = quotesSearchArray.size();
				for (int i = 0; i < searchListLength; i++) {
					if (quotesSearchArray.get(i).contains(search)) {	
						add (i);
						exist = true;
					}
				} 

				if (exist == false) {
					Toast.makeText(getApplicationContext(),
							search + " Not Found" + "",
							Toast.LENGTH_SHORT).show();
				}
				
				QuotesAdapter quotesAdapterT = new QuotesAdapter(QuotesList.this, R.layout.activity_quotes_list_row, quotes);
	        	quotesListView.setAdapter(quotesAdapterT);
			}
		});
	}
	
	public void add (int i){
		QuotesListItem q = (QuotesListItem) quotesFixed.get(i); 
		quotes.add(q);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    MenuItem sort1 = menu.findItem(R.id.menuSort1);
	    MenuItem sort2 = menu.findItem(R.id.menuSort2);
	    MenuItem sort3 = menu.findItem(R.id.menuSort3);
	    MenuItem sort4 = menu.findItem(R.id.menuSort4);
	    sort1.setTitle("Draft");
	    sort2.setTitle("Sent");
	    sort3.setVisible(false);
	    sort4.setVisible(false);
	    return true;
	}
	  
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	    	Intent intent = new Intent(this, MainActivity.class);            
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	        startActivity(intent);
	        finish();
	    	return true;	    	
	    case R.id.search_option:
	    	RelativeLayout item1 = (RelativeLayout)findViewById(R.id.quoListLayout);
	    	if(item1.getVisibility() == View.GONE){
	    		item1.setVisibility(View.VISIBLE);
	    	}else{
	    		item1 .setVisibility(View.GONE);
	    		searchEditText.setText("");
	    	}  	
	    	return true;
	    case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  
	    case R.id.about_option:
	    	return true;	    	
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    case R.id.menuSortAll:
	    	sortList("all");
	    	return true;
	    case R.id.menuSort1:
	    	sortList("draft");
	    	return true;
	    case R.id.menuSort2:
	    	sortList("sent");
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	public void sortList (String sort){
		quotes.clear();
    	for (int i = 0; i < quotesFixed.size(); i++) {
    		if(sort.equals("all")){
    			add (i);
    		}else{
    			QuotesListItem q = (QuotesListItem) quotesFixed.get(i); 
    			if (q.status.equals(sort)) {	
    				add (i);
    			}
    		}
		} 
    	QuotesAdapter quotesAdapterS = new QuotesAdapter(QuotesList.this, R.layout.activity_quotes_list_row, quotes);
    	quotesListView.setAdapter(quotesAdapterS);
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.clearSearchQuotes:			
			searchEditText.setText("");
			break;
		case R.id.newQuotesListButton:			
			Intent i = new Intent(this, NewQuote.class);
	    	startActivity(i);
		break;
		  
		default:
			break;
		}
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance();
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    cal.roll(Calendar.MONTH, -1);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
}
