package uk.co.beanssystems.quotes;

public class QuotesListItem {
	public int id;
	public int icon;
	public int clientId;
    public String client;
    public String jobName;
    public String dateIssued;
    public String estimatedSum;
    public String status;
    
	public QuotesListItem() {
		super();
	}
	
	public QuotesListItem(int id, int icon, int clientId, String client, String jobName, String dateIssued, String estimatedSum, String status) {
		super();
		this.id = id;
		this.icon = icon;
		this.clientId = clientId;
		this.client = client;
		this.jobName = jobName;
		this.dateIssued = dateIssued;
		this.estimatedSum = estimatedSum;
		this.status = status;  
	}
}