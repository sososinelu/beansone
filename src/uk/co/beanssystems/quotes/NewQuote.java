package uk.co.beanssystems.quotes;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.beanssystems.adapter.TaskItemExp;
import uk.co.beanssystems.adapter.ViewItemFrag;
import uk.co.beanssystems.adapter.ViewTaskFrag;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.PDFMaker;
import uk.co.beanssystems.beansone.QuotePDF;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.QitemsConnector;
import uk.co.beanssystems.database.QtasksConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.database.QuotesProvider;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.jobs.Item;
import uk.co.beanssystems.jobs.Task;
import uk.co.beanssystems.popups.AddItemPopup;
import uk.co.beanssystems.popups.AddTaskPopup;
import uk.co.beanssystems.popups.ChooseVATPopup;
import uk.co.beanssystems.popups.SelectClientPopup;
import uk.co.beanssystems.settings.Settings;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class NewQuote extends SherlockFragmentActivity implements LoaderManager.LoaderCallbacks<Cursor>{
	EditText clientNameEditText ,jobNameEditText ,dateIssued, notes, fixedCost;
	TextView subtotal, taxes, total, workCount, itemsCount, barTitle;
	ListView jobNameList;
	RelativeLayout item1;
	QuotesConnector job = null;
	Button sendBtn;
	LinearLayout popup;
	SimpleCursorAdapter mAdapter;
	String jobName, hours="", fixed="", emailSend;  
	TranslateAnimation animationIn, animationOut;
	Drawable arrowUp, arrowDown;
	CheckBox fixedChk;
	boolean isFixed = false, edit = false, emailStarted=false;
	Context context;
	private CursorAdapter adapter;
	private Cursor qtasksCur, qitemsCur;
	private static final String ID = "rowid _id";
	private ComponentName editMode;
	private int clientId, jobId=0, quoteID, jobNameSize = 0;
	private double totalTasks, totalItems, finalSubtotal, finalTotal, finalVat = 0.0, amount = 0;
	private double globalVAT = 20.0, baseTotal;
	private String status="draft", terms = "", email, file, vatType = "none";
	NumberFormat nf;
	ArrayList<String> estHrsSpinnerArray = new ArrayList<String>();
	ArrayList<Task> tasks = new ArrayList<Task>(); 
	ArrayList<Item> items = new ArrayList<Item>();
	ArrayList<Task> delTasks = new ArrayList<Task>();
	ArrayList<Item> delItems = new ArrayList<Item>();	
//	ArrayList<Integer> deleteTasks = new ArrayList<Integer>();
//	ArrayList<Integer> deleteItems = new ArrayList<Integer>();
    DecimalFormat df = new DecimalFormat("0.#");
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_quote);
		editMode = getCallingActivity();
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.quotes);
	    bar.setDisplayHomeAsUpEnabled(true);
	 	nf = NumberFormat.getCurrencyInstance(Locale.UK);
		if (savedInstanceState != null){
			ArrayList<Task> tempTasks = (ArrayList<Task>) savedInstanceState.getSerializable("tasks");
			for (int i = 0;i<tempTasks.size();i++){
				Task tsk = tempTasks.get(i);
				tasks.add(tsk);
			}
			ArrayList<Item> tempItems = (ArrayList<Item>) savedInstanceState.getSerializable("items");
			for (int i = 0;i<tempItems.size();i++){
				Item it = tempItems.get(i);
				items.add(it);
			}
		}
		
		clientNameEditText = (EditText) findViewById(R.id.clientNewQuoteEditText);
		jobNameEditText = (EditText) findViewById(R.id.jobNewQuoteEditText);
		dateIssued = (EditText) findViewById(R.id.dateIssuedNewQuoteEditText);
		notes = (EditText) findViewById(R.id.notesNewQuoteEditText);
		subtotal = (TextView) findViewById(R.id.subTotalValueNewQuoteTextView);
		taxes = (TextView) findViewById(R.id.taxesValueNewQuoteTextView);
		total = (TextView) findViewById(R.id.totalValueNewQuoteTextView);
		jobNameList = (ListView) findViewById(R.id.suggestionsQList);
		popup = (LinearLayout)findViewById(R.id.sendPopupNewQuote);
        sendBtn = (Button) findViewById(R.id.sendButtonNewQuote);
        workCount = (TextView) findViewById(R.id.workCountNewQuoteTextView);
        itemsCount = (TextView) findViewById(R.id.itemCountNewQuoteTextView);
        fixedChk = (CheckBox) findViewById(R.id.fixedCostChkNewQuote);
        fixedCost = (EditText) findViewById(R.id.fixedCostAmountNewQuoteEditText);
        
        Calendar today = Calendar.getInstance();
        dateIssued.setText(outDate.format(today.getTime()));
        
		//Slide Up from 100% below itself
        animationIn = new TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
        animationIn.setDuration(200);

	    //Slide Out to 100% below itself
        animationOut = new TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f);
        animationOut.setDuration(200);
        
		arrowDown = getResources().getDrawable(R.drawable.arrow_down);
		arrowUp = getResources().getDrawable(R.drawable.arrow_up);
		
		fixedChk.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton view, boolean state) {
				if (state){
					fixedChk.setText("");
					fixedCost.setVisibility(View.VISIBLE);
					isFixed = true;
					setTotals();
				} else {
					fixedChk.setText(getResources().getString(R.string.fixedCost));
					fixedCost.setVisibility(View.GONE);
					isFixed = false;
					fixedCost.setText("");
					setTotals();
				}
			}
		});
		
		fixedCost.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {	
				
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}
			
			public void afterTextChanged(Editable s) {
				setTotals();
			}	
		});
		
		// Edit Intent		
		Intent in = getIntent();
		if ("ViewQuote".equals(in.getStringExtra("source"))){
			edit = true;
			sendBtn.setVisibility(View.INVISIBLE);
			baseTotal = in.getDoubleExtra("baseTotal", 0.0);
			vatType = in.getStringExtra("vatType");
			clientId = in.getIntExtra("clientID", 0);
			quoteID = in.getIntExtra("quoteID", 0);
			jobId = in.getIntExtra("jobID",0);
			clientNameEditText.setText(in.getStringExtra("clientName"));
			clientNameEditText.setEnabled(false);
			jobNameEditText.setText(in.getStringExtra("jobName"));
			Calendar cal = Calendar.getInstance();
			try {
				cal = convertStringToDate(in.getStringExtra("dateIssued"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			double fixedCostVal = in.getDoubleExtra("fixedCost", 0);
			if (fixedCostVal!=0){
				fixedCost.setText(String.valueOf(fixedCostVal));
				fixedChk.setText("");
				fixedChk.setChecked(true);
				fixedCost.setVisibility(View.VISIBLE);
				isFixed = true;
				setTotals();
			}
			dateIssued.setText(outDate.format(cal.getTime()));
			if (editMode==null){
				status = "converted";
			} else {
				status = in.getStringExtra("status").toLowerCase();
			}
			
			ArrayList<Task> tempTasks = (ArrayList<Task>) in.getSerializableExtra("tasks");
			for (int i=0;i<tempTasks.size();i++){
				Task tsk = tempTasks.get(i);
				tasks.add(tsk);
				if(tsk.taskName.equals("Callout")){
					totalTasks += tsk.value;
				}else{
					totalTasks += tsk.duration * tsk.value;
				}	
			}
			
			ArrayList<Item> tempItems = (ArrayList<Item>) in.getSerializableExtra("items");
			for (int i=0;i<tempItems.size();i++){
				Item it = tempItems.get(i);
				items.add(it);
				totalItems += it.cost * it.quantity;
			}
			setTotals();
		} else if ("ViewClient".equals(in.getStringExtra("source"))){
			clientId = in.getIntExtra("clientID", 0);
			ClientsConnector cliCon = new ClientsConnector(this);
			cliCon.open();
			try {
				Cursor cur = cliCon.getSingleClientName(clientId);
				cur.moveToFirst();
				int fornameCol = cur.getColumnIndex(ClientsConnector.FORENAME);
				int surnameCol = cur.getColumnIndex(ClientsConnector.SURNAME);
				clientNameEditText.setText(cur.getString(fornameCol)+ " " + cur.getString(surnameCol));
			} catch (Exception e){
				e.printStackTrace();
				System.out.println("CLIENT ID ERROR");
			}
			cliCon.close();
		} else if ("ViewJobQuote".equals(in.getStringExtra("source"))){
			edit = true;
			baseTotal = in.getDoubleExtra("baseTotal", 0.0);
			vatType = "none";
			clientNameEditText.setText(in.getStringExtra("clientName"));
			jobNameEditText.setText(in.getStringExtra("jobName"));
			clientId = in.getIntExtra("clientID", 0);
			jobId = in.getIntExtra("jobID", 0);
			ArrayList<Task> tempTasks = (ArrayList<Task>) in.getSerializableExtra("tasks");
			for (int i=0;i<tempTasks.size();i++){
				Task tsk = tempTasks.get(i);
				tasks.add(tsk);
				if(tsk.taskName.equals("Callout")){
					totalTasks += tsk.value;
				}else{
					totalTasks += tsk.duration * tsk.value;
				}
			}
			
			ArrayList<Item> tempItems = (ArrayList<Item>) in.getSerializableExtra("items");
			for (int i=0;i<tempItems.size();i++){
				Item it = tempItems.get(i);
				items.add(it);
				totalItems += it.cost * it.quantity;
			}
			if(tasks.size() > 0){
			}
			setTotals();
			status = "converted";
			clientNameEditText.setOnClickListener(null);
			clientNameEditText.setFocusable(false);
			jobNameEditText.setOnClickListener(null);
			jobNameEditText.setFocusable(false);
		}
		
		if (edit==false){		
			adapter = new SimpleCursorAdapter(this,
					android.R.layout.simple_list_item_1, 
					null, 
					new String[]{QuotesConnector.JOBNAME}, 
					new int[]{android.R.id.text1,}, 
					Adapter.NO_SELECTION);
			jobNameList.setAdapter(adapter);
			Bundle b = new Bundle();
			b.putString("str", "");
			getSupportLoaderManager().initLoader(0, b, this);
			context = this;
			item1 = (RelativeLayout)findViewById(R.id.suggestionsQLayout);
			// Text changed listener for job name
			jobNameEditText.addTextChangedListener(new TextWatcher() {
				public void onTextChanged(CharSequence s, int start, int before, int count) {	 
				}
				
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				}
				
				public void afterTextChanged(Editable s) {
					adapter.swapCursor(null);
					Bundle bundle = new Bundle();
		    		bundle.putString("str", s.toString());
		        	getSupportLoaderManager().restartLoader(0, bundle, (LoaderCallbacks<Cursor>) context);
					
					jobName = jobNameEditText.getText().toString().toLowerCase(); 		
	//				if (jobName.length()==0){
	//					item1.setVisibility(View.GONE);
	//				} else if(jobName.length() == 3 && jobNameSize == 0){
	//					item1.setVisibility(View.VISIBLE);
	//				} else if (jobNameSize != jobName.length() && jobName.length() >= 3){
	//					item1.setVisibility(View.VISIBLE);
	//				} else if (jobNameSize == jobName.length()){
	//					item1.setVisibility(View.VISIBLE);
	//				 } else if (jobName.length() < 3){
	//					item1.setVisibility(View.GONE);
	//				}
				}	
			});
			jobNameList.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					String projection[] = {ID, QuotesConnector.QUOTEID, QuotesConnector.JOBNAME, QuotesConnector.ESTIMATEDHOURS, QuotesConnector.NOTES, QuotesConnector.DATEISSUED};
				    Cursor cur = getContentResolver().query(Uri.withAppendedPath(QuotesProvider.CONTENT_URI, String.valueOf(id)), projection, null, null, null);
				    if (cur.moveToFirst()) {
				    	jobNameEditText.setText(cur.getString(2));				
						notes.setText(cur.getString(4));
						tasks = new ArrayList<Task>();
						items = new ArrayList<Item>();
						totalItems = 0;
						totalTasks = 0;
						getTasks(cur.getInt(1));
						getItems(cur.getInt(1));
						jobNameSize = jobName.length();
						item1.setVisibility(View.GONE);
					    cur.close();
				    } else {
				    	cur.close();
				    }
				}
	        });
		}
	}
	
	public void onResume(){
		super.onPause();
		if (emailStarted){
    		if (!status.equals("paid")){
				QuotesConnector qCon = new QuotesConnector(this);
				qCon.updateStatus(qCon.getNextId(), "sent");
				qCon.close();	
	    		finish();
    		}
			emailStarted = false;
		}
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		emailStarted=false;
	}
	
	public String stripDecimal(String str){
		String[] amount =str.split("\\.");
		if(amount[1].equals("0")){
			System.out.println("Entered Integer: "+amount[0]);
			return amount[0];
		}else{
			return amount[0]+"."+amount[1];
		}
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int I, Bundle bundle) {
		String search = bundle.getString("str").replace("'", "''");
		CursorLoader curLoader = null;
		curLoader = new CursorLoader(this, 
				Uri.parse(QuotesProvider.CONTENT_URI.toString()), //CONTENT URI
				new String[]{ID, QuotesConnector.QUOTEID, QuotesConnector.JOBNAME, QuotesConnector.ESTIMATEDHOURS, QuotesConnector.NOTES, QuotesConnector.DATEISSUED}, //PROJECTION
				QuotesConnector.JOBNAME +" like '%"+search+"%' AND dateIssued BETWEEN date('now', '-120 days') AND date('now')", //SELECTION
				null, //SELECTION ARGUMENTS
				null); //SORT ORDER
		return curLoader;
	}
	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		if (cursor.getCount()!=0 && jobNameEditText.length()>=3 && jobNameSize != jobNameEditText.length()){
			item1.setVisibility(View.VISIBLE);
		} else {
			item1.setVisibility(View.GONE);
		}
		adapter.swapCursor(cursor);
	}
	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {
		adapter.swapCursor(null);
	}
	
	public void getTasks(int id){
		QtasksConnector qtasksCon = new QtasksConnector(this);
		qtasksCon.open();
		try{
			qtasksCur = qtasksCon.getTasksByQuote(id);
			qtasksCur.moveToFirst();
			int taskNameCol = qtasksCur.getColumnIndex(QtasksConnector.TASKNAME);
			int estimatedDurationCol = qtasksCur.getColumnIndex(QtasksConnector.ESTIMATEDDURATION);
			int chargeValueCol = qtasksCur.getColumnIndex(QtasksConnector.CHARGEVALUE);
			
			do{		
				Task tsk = new Task(qtasksCur.getString(taskNameCol), qtasksCur.getDouble(estimatedDurationCol), qtasksCur.getDouble(chargeValueCol));
				tasks.add(tsk);
				System.out.println("TASK ADDED");
				if(tsk.taskName.equals("Callout")){
					totalTasks += tsk.value;
				}else{
					totalTasks += tsk.duration * tsk.value;
				}
			}while(qtasksCur.moveToNext());
			setTotals();
		} catch (Exception e) { 
	    	System.out.println("++++++No Works+++++");
	    }
		qtasksCon.close();
	}
	
	public void getItems(int id){
		QitemsConnector qitemsCon = new QitemsConnector(this);
		qitemsCon.open();
		try{
			qitemsCur = qitemsCon.getItemsByQuote(id);
			qitemsCur.moveToFirst();
			int itemNameCol = qitemsCur.getColumnIndex(QitemsConnector.ITEMNAME);
			int unitCostCol = qitemsCur.getColumnIndex(QitemsConnector.UNITCOST);
			int quantityCol = qitemsCur.getColumnIndex(QitemsConnector.QUANTITY);
			
			do {
				Item it = new Item( qitemsCur.getString(itemNameCol), qitemsCur.getDouble(unitCostCol), qitemsCur.getInt(quantityCol)); 			
				items.add(it);	
				System.out.println("ITEM ADDED");
				totalItems += it.quantity * it.cost;		
			}while (qitemsCur.moveToNext());	
			setTotals();
		} catch (Exception e) { 
        	System.out.println("++++++No Items+++++");
        }
		qitemsCon.close();
	}
	
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
      //calculate();
        Toast.makeText(getApplicationContext(), "CHANGED", Toast.LENGTH_SHORT).show();
    }
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("tasks", tasks);
        outState.putSerializable("items", items);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_options, menu);
		if (getCallingActivity()!=null){
			MenuItem it = menu.findItem(R.id.add_option);
			it.setTitle("Save");
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_option:
			item.setEnabled(false);
			if(jobNameEditText.getText().length() == 0 ||  clientNameEditText.getText().length() == 0 || dateIssued.getText().length() == 0 ){
				item.setEnabled(true);
				validation(jobNameEditText);
				validation(clientNameEditText);
				validation(dateIssued);	
			}else if(tasks.size() == 0 && items.size() == 0 && fixedCost.getText().toString().equals("")){
				item.setEnabled(true);
				
				Toast.makeText(getApplicationContext(), "Set a fixed cost or add Work & Items!", Toast.LENGTH_SHORT).show();
			}else{
				baseTotal = finalTotal;
				Intent i = new Intent(this, ChooseVATPopup.class);
				i.putExtra("source", 0);
				i.putExtra("total", baseTotal);
				i.putExtra("vatType", vatType);
				startActivityForResult(i, 4);
				item.setEnabled(true);
			}
	    	return true;	
		case android.R.id.home:
			final Builder builder = new AlertDialog.Builder(this);			
			TextView title = new TextView(this);
			title.setText("Quote details not saved!");
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			//title.setTextColor(getResources().getColor(R.color.greenBG));
			title.setTextSize(20);
			builder.setCustomTitle(title);
			TextView msg = new TextView(this);
			msg.setText("Are you sure you want to go back?");
			msg.setPadding(10, 10, 10, 10);
			msg.setGravity(Gravity.CENTER);
			msg.setTextSize(18);
			builder.setView(msg);
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					delTasks.clear();
					delItems.clear();
					ViewItemFrag.delItems.clear();
					ViewTaskFrag.delTasks.clear();
			        finish();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			AlertDialog dialog = builder.create();
			if (tasks.size() !=0 || items.size() !=0  || jobNameEditText.getText().length()!=0
					|| notes.getText().length()!=0 || fixedCost.getText().length()!=0){
				dialog.show();
			}else{
				finish();
			}
	    	return true;
		case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  
	    case R.id.about_option:
	    	return true;
	    case R.id.help_option:
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void validation (final EditText text){
		if( text.getText().length() == 0 ){
			   text.setError( "Input is required!" );
		}else{
			text.setError(null);
		}
		
		text.addTextChangedListener(new TextWatcher() {	
			public void afterTextChanged(Editable s) {
				if( text.getText().length() == 0 ){
					text.setError( "Input is required!" );
				}else{
					text.setError(null);
				}
			}
		 
			public void beforeTextChanged(CharSequence s, int start, 
		     int count, int after) {	  
			}
		 
			public void onTextChanged(CharSequence s, int start, 
		     int before, int count) {  
			}
		});
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {		
		switch (requestCode){
		case 0:
			if (resultCode==RESULT_OK){
				email = data.getStringExtra("email");
				clientId = data.getIntExtra("clientID", 0);			  
				clientNameEditText.setText(data.getStringExtra("clientName"));
				System.out.println("++++++++++++++++++CLIENT ADDED++++++++++++++++++++++");
			}
			break;
		case 1:
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			if (resultCode==RESULT_OK){
				int taskID = 0;
				Task task = new Task(taskID, data.getStringExtra("taskName"), Double.parseDouble(data.getStringExtra("duration")), 
						Double.parseDouble(data.getStringExtra("value")));
				tasks.add(task);
				if(task.taskName.equals("Callout")){
					totalTasks += task.value;
				}else{
					totalTasks += task.duration * task.value;
				}
				setTotals();
			}
			break;
		case 2:
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			if (resultCode==RESULT_OK){
				int itemID = 0;		
				Item item = new Item(itemID, data.getStringExtra("itemName"), Double.parseDouble(data.getStringExtra("cost")), Integer.parseInt(data.getStringExtra("quantity")), 
						Double.parseDouble(data.getStringExtra("cost")) * Integer.parseInt(data.getStringExtra("quantity")));
				items.add(item);
				totalItems += item.cost * item.quantity;
				setTotals();			
			}
			break;
		case 3:
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			if (resultCode==RESULT_OK){
				System.out.println("On Activity Result - New Quote");
				tasks = (ArrayList<Task>) data.getSerializableExtra("tasksArray");
				items = (ArrayList<Item>) data.getSerializableExtra("itemsArray");
				delTasks = (ArrayList<Task>) data.getSerializableExtra("delTasksArray");
				delItems = (ArrayList<Item>) data.getSerializableExtra("delItemsArray");
				if(tasks.size() == 0 ){
					totalTasks = 0;
				}else{
					totalTasks = 0;
					for(int i=0; i<tasks.size();i++){
				    	Task task = tasks.get(i);
				    	if(task.taskName.equals("Callout")){
							totalTasks += task.value;
						}else{
							totalTasks += task.duration * task.value;
						}
				    }
				}
				
				if(items.size() == 0){
					totalItems = 0;
				}else{
					totalItems = 0;
					for(int i=0; i<items.size();i++){
				    	Item item = items.get(i);
				    	totalItems += item.cost * item.quantity;
				    }
				}
				setTotals();			
			}
			break;
		case 4:
			if (resultCode==RESULT_OK){
				vatType = data.getStringExtra("vatType");
				finalSubtotal = data.getDoubleExtra("subtotal", 0.0);
				finalVat = data.getDoubleExtra("vat", 0.0);
				finalTotal = data.getDoubleExtra("total", 0.0);
				Log.i("Totals", finalSubtotal + " | "+ finalVat + " | " + finalTotal);
				addQuote();
				finish();
			}
			break;
		case 5:
			if(resultCode==RESULT_OK){
				vatType = data.getStringExtra("vatType");
				finalSubtotal = data.getDoubleExtra("subtotal", 0.0);
				finalVat = data.getDoubleExtra("vat", 0.0);
				finalTotal = data.getDoubleExtra("total", 0.0);
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
				popup.startAnimation(animationOut);
				popup.setVisibility(View.GONE);
				addQuote();
				ClientsConnector cliCon = new ClientsConnector(this);
				cliCon.open();
				Cursor cur = cliCon.getClient(clientId);
				cur.moveToFirst();
				int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
				String email = cur.getString(emailCol).replace(" ", "");
				cliCon.close();
				if (!email.equals("") && email.contains("@") && email.contains(".")){
					String client = clientNameEditText.length()<8 ? clientNameEditText.getText().toString() : clientNameEditText.getText().toString().substring(0, 7);
					String job = jobNameEditText.length()<8 ? jobNameEditText.getText().toString() : jobNameEditText.getText().toString().substring(0, 7);
					emailSend = email;
					String filename = quoteID + "_quo_"+client+"_"+job+".pdf";
					QuotePDF obj = new QuotePDF(clientId, jobNameEditText.getText().toString(), dateIssued.getText().toString(), finalSubtotal, finalVat, finalTotal);
					PDFMaker pdf = new PDFMaker(this, 0, filename, "Thank you for your business", obj);
		    		if (pdf.makeFile()){
		    			file = pdf.build();
		    			checkConStatus();
						sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()))); 
		    		} else {
		    			Toast.makeText(this, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();
		    		}
	    		
		    	} else {
					Toast.makeText(this, "You do not have an email address for this client. Please check client email and resend", Toast.LENGTH_LONG).show();
				}
			}
			
			break;
		case 6:
			if(resultCode==RESULT_OK){
				vatType = data.getStringExtra("vatType");
				finalSubtotal = data.getDoubleExtra("subtotal", 0.0);
				finalVat = data.getDoubleExtra("vat", 0.0);
				finalTotal = data.getDoubleExtra("total", 0.0);
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
				popup.startAnimation(animationOut);
				popup.setVisibility(View.GONE);
				String client = clientNameEditText.length()<8 ? clientNameEditText.getText().toString() : clientNameEditText.getText().toString().substring(0, 7);
				String job = jobNameEditText.length()<8 ? jobNameEditText.getText().toString() : jobNameEditText.getText().toString().substring(0, 7);
				String filename = quoteID + "_quo_"+client+"_"+job+".pdf";			
				QuotePDF obj = new QuotePDF(clientId, jobNameEditText.getText().toString(), dateIssued.getText().toString(), finalSubtotal, finalVat, finalTotal);
				PDFMaker pdf = new PDFMaker(this, 0, filename, "Thank you for your business", obj);
	    		if (pdf.makeFile()){
	    			file = pdf.build();
		        	startPDF();
				} else {
					Toast.makeText(this, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();
				}
			}
    		break;
		default:
			break;
		}
	}

	public void onClick(View view){ 
		switch (view.getId()){	
		case com.actionbarsherlock. R.id .abs__action_bar_title:
			Log.i("<<<<<< WORKS", "<<<<<<<<<<<< :D >>>>>>>>>>>>>>>");
			break;
		case R.id.clientNewQuoteEditText:
			Intent inClient = new Intent(this, SelectClientPopup.class);
			startActivityForResult(inClient, 0);
			break;
		case R.id.dateIssuedNewQuoteEditText:
			Calendar cal = Calendar.getInstance();
			DatePickerDialog datePickDiag = new DatePickerDialog(
					NewQuote.this, odsl, cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			datePickDiag.show();
		    break;
		case R.id.addWorkNewQuoteButton:
			Intent inTask = new Intent(this, AddTaskPopup.class);
			startActivityForResult(inTask, 1);
			break;		
		case R.id.addItemNewQuoteButton:
			Intent inItem = new Intent(this, AddItemPopup.class);
			startActivityForResult(inItem, 2);
			break;
		case R.id.viewWorkNewQuoteButton:
			System.out.println("NEW QUOTE - TASKS ?>>>> "+tasks.size());
			passTasksItems(0);
			break;
		case R.id.workCountNewQuoteTextView:
			 System.out.println("NEW QUOTE - TASKS ?>>>> "+tasks.size());
			 passTasksItems(0);
			break;
		case R.id.viewItemNewQuoteButton:
			System.out.println("NEW QUOTE - ITEMS ?>>>> "+items.size());
			passTasksItems(1);
			break;
		case R.id.itemCountNewQuoteTextView:
			 System.out.println("NEW QUOTE - ITEMS ?>>>> "+items.size());
			 passTasksItems(1);
			 break;
		case R.id.sendButtonNewQuote:	        
			if (popup.getVisibility()==View.GONE){
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDown, null);
				popup.startAnimation(animationIn);
				popup.setVisibility(View.VISIBLE);
			} else {
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
				popup.startAnimation(animationOut);
				popup.setVisibility(View.GONE);
			}
			break;
		case R.id.emailLabelTextView:
			if( jobNameEditText.getText().length() == 0 || clientNameEditText.getText().length() == 0 || dateIssued.getText().length() == 0 ){
				validation(jobNameEditText);
				validation(clientNameEditText);
				validation(dateIssued);	
			}else if(tasks.size() == 0 && items.size() == 0 && fixedCost.getText().toString().equals("")){
				Toast.makeText(getApplicationContext(), "Set a fixed cost or add Work & Items!", Toast.LENGTH_SHORT).show();
			}else {
				//baseTotal = finalTotal;
				Intent i = new Intent(this, ChooseVATPopup.class);
				i.putExtra("source", 0);
				i.putExtra("total", baseTotal);
				i.putExtra("vatType", vatType);
				startActivityForResult(i, 5);
			}
			break;
		case R.id.printLabelTextView:
			Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
//				if( jobNameEditText.getText().length() == 0 ||  clientNameEditText.getText().length() == 0 || dateIssued.getText().length() == 0 ){
//					validation(jobNameEditText);
//					validation(clientNameEditText);
//					validation(dateIssued);	
//				}else{
//					sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
//					popup.startAnimation(animationOut);
//					popup.setVisibility(View.GONE);
//					makePDF();
//		    		printPDF();
//				}
//			}
			break;
		case R.id.pdfLabelTextView:
			if( jobNameEditText.getText().length() == 0 ||  clientNameEditText.getText().length() == 0 || dateIssued.getText().length() == 0 ){
				validation(jobNameEditText);
				validation(clientNameEditText);
				validation(dateIssued);	
			} else {
				//baseTotal = finalTotal;
				Intent i = new Intent(this, ChooseVATPopup.class);
				i.putExtra("source", 0);
				i.putExtra("total", baseTotal);
				i.putExtra("vatType", vatType);
				startActivityForResult(i, 6);		
			}
			break;
		default:
			break;
		}
	}
	
	public void passTasksItems (int currentItem){
		Intent in = new Intent(this, TaskItemExp.class);
		in.putExtra("tasksArray", tasks);
		in.putExtra("itemsArray", items);		
		in.putExtra("currentItem", currentItem);
		in.putExtra("loadFrags", 2);
		in.putExtra("source", 1);
		startActivityForResult(in, 3);
	}
	
	final OnDateSetListener odsl = new OnDateSetListener() {
			public void onDateSet(DatePicker arg0, int year, int month,
					int dayOfMonth) {
				month++;
				TextView txt = (TextView) findViewById(R.id.dateIssuedNewQuoteEditText);
				String dayStr = String.valueOf(dayOfMonth);
				String monthStr = String.valueOf(month);

				if (dayStr.length()==1){
					dayStr = "0"+ dayStr;
				}
				if (monthStr.length()==1){
					monthStr = "0"+ monthStr;
				}
				txt.setText(dayStr + "/" + monthStr + "/" + year);
			}
		};

	public void setTotals (){
		if (isFixed){
			amount = 0;
			try {
				amount = Double.parseDouble(fixedCost.getText().toString());
			} catch(Exception e){
				
			}
			total.setText("  Total: "+String.valueOf(nf.format(amount)));
			finalTotal = amount;
		} else {
			finalTotal = totalTasks + totalItems; 
		}
		baseTotal = finalTotal;
		if("inc".equals(vatType)){
			subtotal.setVisibility(View.VISIBLE);
			taxes.setVisibility(View.VISIBLE);
			subtotal.setText("Sub: "+String.valueOf(nf.format(finalTotal - (globalVAT / (globalVAT + 100.0) * finalTotal))));
	    	taxes.setText("VAT: "+String.valueOf(nf.format((globalVAT / (globalVAT + 100.0) * finalTotal))));
	    	total.setText("  Total: "+String.valueOf(nf.format(finalTotal)));
		}else if("add".equals(vatType)){
			subtotal.setVisibility(View.VISIBLE);
			taxes.setVisibility(View.VISIBLE);
			subtotal.setText("Sub: "+String.valueOf(nf.format(finalTotal)));
	    	taxes.setText("VAT: "+String.valueOf(nf.format((finalTotal * (globalVAT + 100.0) / 100.0) - finalTotal)));
	    	total.setText("  Total: "+String.valueOf(nf.format(finalTotal * (globalVAT + 100.0) / 100.0)));
		}else{
			subtotal.setVisibility(View.GONE);
			taxes.setVisibility(View.GONE);
			total.setText("  Total: "+String.valueOf(nf.format(finalTotal)));
		}
		
		double hours=0.0;
		for (int i=0;i<tasks.size();i++){
			hours += tasks.get(i).duration;
		}
		workCount.setText("Work: "+String.valueOf(hours)+" Hrs");
		int quantity=0;
		for (int i=0;i<items.size();i++){
			quantity += items.get(i).quantity;
		}
		itemsCount.setText("Items: "+String.valueOf(quantity));
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance(Locale.UK);
	    Date date = outDate.parse(strDate);
	    cal.setTime(date);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
	
	public void addQuote(){
		Calendar me = Calendar.getInstance(Locale.UK);
		try {
			me = convertStringToDate(dateIssued.getText().toString());
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		String dateIn = inDate.format(me.getTime());
		QuotesConnector quCon = new QuotesConnector(this);
		QtasksConnector tCon = new QtasksConnector(this);
		QitemsConnector iCon = new QitemsConnector(this);
		double hours = 0.0;
		double fixedCostLocal = (isFixed) ? Double.valueOf(fixedCost.getText().toString()) : 0;
		if (editMode==null){
			try{
				// New Mode
				quCon.insertQuote(clientId , jobId, jobNameEditText.getText().toString(), dateIn, hours, notes.getText().toString(), 
						finalSubtotal, baseTotal, vatType, finalTotal, finalVat, fixedCostLocal, status);			
				int thisQuote = quCon.getNextId();
				quoteID = thisQuote;
				System.out.println("++++++++++++QUOTE ADDED++++++++++++++++");
				
				// Insert QTasks
				for (int i=0;i<tasks.size();i++){
					Task task = tasks.get(i);
					tCon.insertQtask(thisQuote, task.taskName , task.duration, "", task.value);
					System.out.println("++++++++ TASK > DATABASE");
				}
				// Insert QItems
				for (int i=0;i<items.size();i++){
					Item item = items.get(i);	
					iCon.insertQitem( thisQuote, 0, item.itemName, item.cost, "" , item.quantity, item.total, 0.0);
					System.out.println("++++++++ ITEM > DATABASE");
				}  
			} catch(Exception e){
				System.out.println("++++++ NEW QUOTE ERROR +++++ "+e.getLocalizedMessage().toString());
	   	  	}
		} else {
			//Edit Mode
			status = "draft";
			quCon.updateQuote(quoteID, clientId , jobId, jobNameEditText.getText().toString(), dateIn, hours, notes.getText().toString(),
					finalSubtotal, baseTotal, vatType, finalTotal, finalVat, fixedCostLocal, status);
			System.out.println("upated record, Fixed= "+ fixedCostLocal);
			for (Task tsk : tasks){
				if (tsk.taskID==0){
					tCon.insertQtask(quoteID, tsk.taskName, tsk.duration, "", tsk.value);
				}else{
					tCon.updateQtask(tsk.taskID, quoteID, tsk.taskName, tsk.duration, "", tsk.value);
				}
			}
			
			for (Item it : items){
				double tCost = it.cost*it.quantity;
				if (it.itemID==0){
					iCon.insertQitem(quoteID, 0, it.itemName, it.cost,"" , it.quantity, tCost, 0);
				}else{
					iCon.updateQitem(it.itemID, quoteID, 0, it.itemName, it.cost, "", it.quantity, tCost, 0.0);
				}
			}		
			
			for(Task t : delTasks){
				tCon.deleteQtask(t.taskID);
			}
			
			for(Item i : delItems){
				iCon.deleteQitem(i.itemID);
			}
			
		}
		quCon.close();
		tCon.close();
		iCon.close();
	}
	
	public void startPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		
		if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } 
            catch (ActivityNotFoundException e) {
                Toast.makeText(this, "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
            }
		}
	}
	
	public void checkConStatus (){
		ConnectivityManager conMngr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		android.net.NetworkInfo wifi = conMngr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		android.net.NetworkInfo mobile = conMngr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if(wifi.isConnected() || mobile.isConnected()){
			emailPDF();
		}else{
			final Builder builder = new AlertDialog.Builder(this);			
			TextView title = new TextView(this);
			title.setText("No Internet Connection!");
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			//title.setTextColor(getResources().getColor(R.color.greenBG));
			title.setTextSize(20);
			builder.setCustomTitle(title);
			TextView msg = new TextView(this);
			msg.setText("No Internet connection is available. If you choose to send the Quote, the email will stay in your Email Client Outbox till the device gets connected to the internet. Send email?");
			msg.setPadding(10, 10, 10, 10);
			msg.setGravity(Gravity.CENTER);
			msg.setTextSize(18);
			builder.setView(msg);
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					emailPDF();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}
	
	public void emailPDF(){
		emailStarted = true;
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		String[] mailto = {emailSend};
		i.putExtra(Intent.EXTRA_EMAIL, mailto);
		i.putExtra(Intent.EXTRA_SUBJECT, "Your Quote");
		i.putExtra(Intent.EXTRA_TEXT, "Hello "+clientNameEditText.getText().toString()+", your quote has been attached to this email.");
		i.putExtra(Intent.EXTRA_STREAM,
			Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ java.io.File.separator + "beansone"+ java.io.File.separator + file)));
		i.setType("application/pdf");
	    startActivity(Intent.createChooser(i, "Send Email"));
	}
	
	public void printPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		Intent printIntent = new Intent(this, PrintDialog.class);
    	printIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
    	printIntent.putExtra("title", "Quote for "+ clientNameEditText.getText().toString());
    	startActivity(printIntent);
	}
}