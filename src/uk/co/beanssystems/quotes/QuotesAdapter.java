package uk.co.beanssystems.quotes;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import uk.co.beanssystems.beansone.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class QuotesAdapter extends ArrayAdapter<QuotesListItem> {
	Context context;
	int layoutResourceId;
	ArrayList<QuotesListItem> items = null;
	
	public QuotesAdapter (Context context, int layoutResourceId, ArrayList<QuotesListItem> items){
		super(context, layoutResourceId, items);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.items = items;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
        QuotesListItemHolder holder = null;
        // Set currency format 
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.UK);
        if(row == null){
	        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	        row = inflater.inflate(layoutResourceId, parent, false); 
	        holder = new QuotesListItemHolder();
	        holder.icon = (ImageView)row.findViewById(R.id.imageQuoteListRowImageView);
	        holder.client = (TextView)row.findViewById(R.id.clientQuoteListRowTextView);
	        holder.jobName = (TextView)row.findViewById(R.id.jobQuoteListRowEditText);
	        holder.dateIssued = (TextView)row.findViewById(R.id.dateIssuedQuoteListRowTextView);
	        holder.estimatedSum = (TextView)row.findViewById(R.id.estimSumQuoteListRowTextView);
	        holder.status = (TextView) row.findViewById(R.id.statusQuoteListRowTextView);
	        row.setTag(holder);
	    }
        else{
        	holder = (QuotesListItemHolder)row.getTag();
	    }

        QuotesListItem quote = items.get(position);
        holder.icon.setImageResource(quote.icon);
        holder.client.setText(quote.client);
        holder.jobName.setText(quote.jobName);
        holder.dateIssued.setText(quote.dateIssued);
        holder.estimatedSum.setText(nf.format(Double.valueOf(quote.estimatedSum)));
        holder.status.setText(quote.status);
		if(quote.status.equals("draft")){
			holder.status.setBackgroundResource(R.color.yellow2);
		}else if(quote.status.equals("sent")){
			holder.status.setBackgroundResource(R.color.royalBlue);
		}
        return row;
	}
	
	static class QuotesListItemHolder {
	       ImageView icon;
	       TextView client;
	       TextView jobName;
	       TextView dateIssued;
	       TextView estimatedSum;
	       TextView status;
	   }
}
