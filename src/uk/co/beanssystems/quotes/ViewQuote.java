package uk.co.beanssystems.quotes;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.PDFMaker;
import uk.co.beanssystems.beansone.QuotePDF;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.QitemsConnector;
import uk.co.beanssystems.database.QtasksConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.jobs.Item;
import uk.co.beanssystems.jobs.NewJob;
import uk.co.beanssystems.jobs.Task;
import uk.co.beanssystems.popups.DeleteDialog;
import uk.co.beanssystems.settings.Settings;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class ViewQuote extends SherlockActivity {
	private Cursor tasksCur, itemsCur;
	double globalVAT = 20;
	private ArrayList<Task> tasks = new ArrayList<Task>();
	private ArrayList<Item> items = new ArrayList<Item>();	
	private int clientID, jobID = 0, quoteID, launchIntent = 0;
	private String jobName, status, source="", emailSend, clientName, company, mobile, email, address, postcode, file, dateIssued, vatType;
	private double estimatedHours, totalTasks, totalItems, subTotal, baseTotal, totalCost, totalVAT, fixedCost;
	private boolean emailStarted=false, isFixed = false;
	private TextView totalSumTextView, statusTextView, companyTextView, jobTextView, dateIssuedTextView, 
		fixedLabel, vatLabel, subtotalLabel, noIWTextView;
	private Button clientButton, sendButton;  
	View main, tasksView, itemsView, expView;
	LayoutInflater inflater;
	NumberFormat nf;
	LinearLayout linear, sendLayout, vatLabelLayout, subTotalLabelLayout;
	RelativeLayout quickLayout;	
	Drawable arrowDw, arrowUp;
	TranslateAnimation animationInSend, animationOutSend, animationInQuick, animationOutQuick;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	QuotesConnector qCon = new QuotesConnector(this);
	QtasksConnector qtasksCon = new QtasksConnector(this);
	QitemsConnector qitemsCon = new QitemsConnector(this);
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		main = inflater.inflate(R.layout.activity_view_quote, null);
		setContentView(main);
		nf = NumberFormat.getCurrencyInstance(Locale.UK);		
		
		try{
			source = getCallingActivity().getShortClassName().replace("uk.co.beanssystems.jobs.", "");
		} catch (Exception e){
			e.printStackTrace();
		}
		
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.quotes);
	    bar.setDisplayHomeAsUpEnabled(true);
	    
	    animationInSend = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
			    Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
	    animationOutSend = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f );
	     animationInQuick = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
			    Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
		animationOutQuick = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -1.0f );
		quickLayout = (RelativeLayout) findViewById(R.id.quickContactPopupViewQuote);
	    sendLayout = (LinearLayout) findViewById(R.id.sendPopupViewQuote);	    
	    arrowDw = getApplicationContext().getResources().getDrawable(R.drawable.arrow_down);
	    arrowUp = getApplicationContext().getResources().getDrawable(R.drawable.arrow_up);
	    totalSumTextView = (TextView) findViewById(R.id.totalDueViewQuoteTextView);
	    statusTextView = (TextView) findViewById(R.id.statusViewQuoteTextView); 
	    clientButton = (Button) findViewById(R.id.clientViewQuoteButton);
	    sendButton = (Button) findViewById(R.id.sendJobViewQuoteButton);
	    companyTextView = (TextView) findViewById(R.id.companyViewQuoteTextView);
	    jobTextView = (TextView) findViewById(R.id.jobViewQuoteTextView);
	    dateIssuedTextView = (TextView) findViewById(R.id.issuedDateViewQuoteTextView);
	    vatLabel = (TextView) findViewById(R.id.vatValueLabelViewQuote);
	    vatLabelLayout = (LinearLayout) findViewById(R.id.vatLayoutViewQuote);
	    subtotalLabel = (TextView) findViewById(R.id.subtotalValueLabelViewQuote);
	    subTotalLabelLayout = (LinearLayout) findViewById(R.id.subtotalLayoutViewQuote);
	    fixedLabel = (TextView) findViewById(R.id.fixedPriceLabelViewQuote);
	    noIWTextView = (TextView) findViewById(R.id.noIWViewQuoteTextView);
		Intent ili = getIntent();
	    quoteID = ili.getIntExtra("passQuoteID", -1);
	    status = ili.getStringExtra("passStatus");
	    if(status.equals("draft")){
			statusTextView.setBackgroundResource(R.color.yellow2);
		}else if(status.equals("sent")){
			statusTextView.setBackgroundResource(R.color.royalBlue);
		}      
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.menu_options_view, menu);
	    return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.edit_option:
		   Intent inQuote = new Intent(this, NewQuote.class);
		   inQuote.putExtra("source", "ViewQuote");
		   inQuote.putExtra("quoteID", quoteID);
     	   inQuote.putExtra("jobID", jobID);
     	   inQuote.putExtra("status", status);
     	   inQuote.putExtra("clientID", clientID);
     	   inQuote.putExtra("jobName", jobName);
     	   inQuote.putExtra("clientName", clientName);
     	   inQuote.putExtra("dateIssued", dateIssuedTextView.getText().toString());
     	   String eH = Double.toString(estimatedHours);
     	   inQuote.putExtra("estimatedHours", estimatedHours);
     	   inQuote.putExtra("tasks", tasks);
     	   inQuote.putExtra("items", items);
     	   inQuote.putExtra("fixedCost", fixedCost);
     	   inQuote.putExtra("vatType", vatType);
     	   startActivityForResult(inQuote,8);
			return true;
		
		case R.id.delete_option:
			Intent inDel = new Intent(this, DeleteDialog.class);
			inDel.putExtra("title", "Quote");
			startActivityForResult(inDel,0);
			return true;
	    case android.R.id.home:
	    	if ("ViewJob".equals(source)){
	    		setResult(0);
	    		finish();
	    	} else {
		        finish();
		    	return true;
	    	}
	    case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  	        
	    case R.id.about_option:
	    	return true;	    	
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	public void onResume(){
		super.onPause();
		if (emailStarted){
			QuotesConnector qCon = new QuotesConnector(this);
    		qCon.updateStatus(quoteID, "sent");
    		qCon.close();
    		status = "sent";
    		statusTextView.setText("Sent");
			statusTextView.setBackgroundResource(R.color.royalBlue);
			emailStarted = false;
		} else {
			loadQuote();
		}
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		emailStarted=false;
	}
	
	protected void onActivityResult (int requestCode, int resultCode, Intent data){
		switch(requestCode){
		case 0:
			if(resultCode == RESULT_OK){
				qCon.updateActive(quoteID, "dead");
				for(int i=0; i < tasks.size(); i++){
					Task t = tasks.get(i);
					qtasksCon.updateActive(t.taskID, "dead");
				}
				for(int j=0; j < items.size(); j++){
					Item i = items.get(j);
					qitemsCon.updateActive(i.itemID, "dead");
				}
				finish();
			}else{
				Toast.makeText(this, "Delete Cancelled!", Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			break;
		}
	}

	public void onClick(View view){
		switch (view.getId()){
		case R.id.clientViewQuoteButton:		    	    
		    animationInQuick.setDuration(200);
		    animationOutQuick.setDuration(200);		    
		    if(quickLayout.getVisibility() == View.GONE){
		    	clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
		    	quickLayout.setVisibility(View.VISIBLE);
		    	quickLayout.startAnimation(animationInQuick);
	    	}else{
	    		clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
	    		quickLayout.startAnimation(animationOutQuick);
	    		quickLayout.setVisibility(View.GONE);
	    	} 
			break;			
		case R.id.sendJobViewQuoteButton:	
			//Slide Up from 100% below itself		    
		    animationInSend.setDuration(200);
		    animationOutSend.setDuration(200);	    
		    if(sendLayout.getVisibility() == View.GONE){
		    	sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
		    	sendLayout.setVisibility(View.VISIBLE);
		    	sendLayout.startAnimation(animationInSend);
	    	}else{
	    		sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
	    		sendLayout.startAnimation(animationOutSend);
	    		sendLayout .setVisibility(View.GONE);
	    	} 
			break;		
		case R.id.makeJobViewQuoteButton:			
				launchIntent = 1;
				launchIntent(); 
			break;
		case R.id.mobileQCViewQuoteTextView:
			try {
				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
				quickLayout.startAnimation(animationOutQuick);
				quickLayout.setVisibility(View.GONE);
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("tel", mobile, null)));
			} catch (Exception e){
			}
			break;			
		case R.id.smsQCViewQuoteTextView:
			try {
				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
				quickLayout.startAnimation(animationOutQuick);
				quickLayout.setVisibility(View.GONE);
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", mobile, null)));
			} catch (Exception e){
			}
			break;					
		case R.id.emailQCViewQuoteTextView:
			try {
				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
				quickLayout.startAnimation(animationOutQuick);
				quickLayout.setVisibility(View.GONE);
				Intent intent = new Intent(Intent.ACTION_SEND);
	            intent.setType("plain/text");
	            intent.putExtra(Intent.EXTRA_EMAIL,new String[] {email});
//	            intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of the mail");
//	            intent.putExtra(Intent.EXTRA_TEXT, "body of the mail");
	            startActivity(Intent.createChooser(intent, "Title of the chooser dialog"));
			} catch (Exception e){
			}
			break;
		case R.id.mapQCViewQuoteTextView:
			try {
				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
				quickLayout.startAnimation(animationOutQuick);
				quickLayout.setVisibility(View.GONE);
				String address1 = address + postcode; // Get address
				address1 = address1.replace(" ", "+");
				Intent geoIntent = new Intent (android.content.Intent.ACTION_VIEW, Uri.parse ("geo:0,0?q=" + address1)); // Prepare intent
				startActivity(geoIntent);	// Initiate lookup
			} catch (Exception e){
				Toast.makeText(getBaseContext(), "No Application Available to Display Maps!!", Toast.LENGTH_SHORT).show();
				
			}
			break;			
		case R.id.emailLabelViewQuoteTextView:
			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
			sendLayout.startAnimation(animationOutSend);
			sendLayout.setVisibility(View.GONE);
			ClientsConnector cliCon = new ClientsConnector(this);
			cliCon.open();
			Cursor cur = cliCon.getClient(clientID);
			cur.moveToFirst();
			int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
			String email = cur.getString(emailCol).replace(" ", "");
			cliCon.close();
			if (!email.equals("") && email.contains("@")){
				String client = clientName.length()<8 ? clientName : clientName.substring(0, 7);
				String job = jobName.length()<8 ? jobName : jobName.substring(0, 7);
				emailSend = email;
				String filename = quoteID + "_quo_"+client+"_"+job+".pdf";
				QuotePDF obj = new QuotePDF(clientID, jobName, dateIssued, subTotal, totalVAT, totalCost);
				PDFMaker pdf = new PDFMaker(this, 0, filename, "Thank you for your business", obj);
	    		if (pdf.makeFile()){
	    			file = pdf.build();
	    			checkConStatus();
					sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
				} else {
					Toast.makeText(this, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();	
				}
			} else {
			Toast.makeText(this, "You do not have an email address for this client. Please check client email and resend", Toast.LENGTH_LONG).show();
			}
			break;	
		case R.id.printLabelViewQuoteTextView:
			//Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
//			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
//			sendLayout.startAnimation(animationOutSend);
//			sendLayout.setVisibility(View.GONE);
//			makePDF();
//    		printPDF();
			break;			
		case R.id.pdfLabelViewQuoteTextView:
			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
			sendLayout.startAnimation(animationOutSend);
			sendLayout.setVisibility(View.GONE);
			String client = clientName.length()<8 ? clientName : clientName.substring(0, 7);
			String job = jobName.length()<8 ? jobName : jobName.substring(0, 7);
			String filename = quoteID + "_quo_"+client+"_"+job+".pdf";
			QuotePDF obj = new QuotePDF(clientID, jobName, dateIssued, subTotal, totalVAT, totalCost);
			PDFMaker pdf = new PDFMaker(this, 0, filename, "Thank you for your business", obj);
    		if (pdf.makeFile()){
    			file = pdf.build();
				startPDF();
			} else {
				Toast.makeText(this, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();	
			}
			break;		  
		default:
			break;
		}
	}
	
	public void launchIntent () {
		Intent intent = new Intent(this, NewJob.class);
		if(launchIntent == 1){
			intent.putExtra("tasks", tasks);
			intent.putExtra("items", items);
		    intent.putExtra("passQuoteId", quoteID);
		    intent.putExtra("passClientId", clientID);
		    intent.putExtra("passJobName", jobName);
		    intent.putExtra("passEstimatedHours", estimatedHours);
		    intent.putExtra("totalTasks", totalTasks);
		    intent.putExtra("totalItems", totalItems);
		    intent.putExtra("totalCost", totalCost);
		    intent.putExtra("subTotal", subTotal);
		    intent.putExtra("totalVAT", totalVAT);
		    intent.putExtra("baseTotal", baseTotal);
		    intent.putExtra("vatType", vatType);
		    if (isFixed){
		    	intent.putExtra("passFixedCost", fixedCost);
		    }
		    startActivity(intent);
		}else{
			startActivity(intent);
		}  
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance();
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    cal.roll(Calendar.MONTH, -1);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
	
	public void loadQuote(){
		linear = (LinearLayout)main.findViewById(R.id.parentLayoutViewQuote);
		linear.removeAllViews();
		tasksView = inflater.inflate(R.layout.activity_view_quote_holder, null);
		itemsView = inflater.inflate(R.layout.activity_view_quote_holder, null);
		QuotesConnector qCon = new QuotesConnector(this);
		qCon.open();
		Cursor qCur = qCon.getQuote(quoteID);
		qCur.moveToFirst();
		int clientIDCol = qCur.getColumnIndex(QuotesConnector.CLIENTID);
		int jobNameCol = qCur.getColumnIndex(QuotesConnector.JOBNAME);
		int dateIssuedCol = qCur.getColumnIndex(QuotesConnector.DATEISSUED);
		int estimatedHoursCol = qCur.getColumnIndex(QuotesConnector.ESTIMATEDHOURS);
		int subtotalCol = qCur.getColumnIndex(QuotesConnector.SUBTOTAL);
		int basetotalCol = qCur.getColumnIndex(QuotesConnector.BASETOTAL);
		int vatTypeCol = qCur.getColumnIndex(QuotesConnector.VATTYPE);
		int totalCostCol = qCur.getColumnIndex(QuotesConnector.TOTALCOST);
		int totalVATCol = qCur.getColumnIndex(QuotesConnector.TOTALVAT);
		int fixedCostCol = qCur.getColumnIndex(QuotesConnector.FIXEDCOST);	
		int statusCol = qCur.getColumnIndex(QuotesConnector.STATUS);
		
		estimatedHours = qCur.getDouble(estimatedHoursCol);
		totalCost = qCur.getDouble(totalCostCol);
		fixedCost = qCur.getDouble(fixedCostCol);
		vatType = qCur.getString(vatTypeCol);
		subTotal = qCur.getDouble(subtotalCol);
		baseTotal = qCur.getDouble(basetotalCol);
		totalVAT = qCur.getDouble(totalVATCol);
		
		double displayAmount = 0;
		if (fixedCost!=0){
			displayAmount = subTotal + totalVAT;
			fixedLabel.setVisibility(View.VISIBLE);
			isFixed = true;
		} else {
			displayAmount = totalCost;
			fixedLabel.setVisibility(View.GONE);
			isFixed = false;
		}
		if(vatType.equals("none")){
			vatLabelLayout.setVisibility(View.GONE);
			subTotalLabelLayout.setVisibility(View.GONE);
		}else{
			vatLabelLayout.setVisibility(View.VISIBLE);
			subTotalLabelLayout.setVisibility(View.VISIBLE);
			vatLabel.setText(String.valueOf(nf.format(totalVAT)));
			subtotalLabel.setText(String.valueOf(nf.format(subTotal)));
		}
		totalSumTextView.setText(nf.format(displayAmount));
		jobName = qCur.getString(jobNameCol);
		jobTextView.setText(jobName);
		dateIssued = qCur.getString(dateIssuedCol);
		Calendar cal = Calendar.getInstance();
		try {
			cal = convertStringToDate(dateIssued);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		dateIssuedTextView.setText(outDate.format(cal.getTime()));
		statusTextView.setText(qCur.getString(statusCol));
		clientID = qCur.getInt(clientIDCol);
		qCon.close();
		tasks = new ArrayList<Task>();
		items = new ArrayList<Item>();
		getClient(clientID);
		getItems(quoteID);
		getTasks(quoteID);	
		file = clientName.replaceAll(" ", "_")+ "_"+jobName.replaceAll(" ", "_") +"_quote.pdf";
		if(tasks.size() == 0 && items.size() == 0){
			linear.addView(noIWTextView);
		}
	}
	
	public void getClient(int id){		
		if (id != 0){
			ClientsConnector cliCon = new ClientsConnector(this);
			cliCon.open();
			try{
				Cursor cur = cliCon.getClient(id);
				cur.moveToFirst();
				int forenameCol = cur.getColumnIndex(ClientsConnector.FORENAME);
				int surnameCol = cur.getColumnIndex(ClientsConnector.SURNAME);
				int companyNameCol = cur.getColumnIndex(ClientsConnector.COMPANY);
				int mobileCol = cur.getColumnIndex(ClientsConnector.MOBILE);
				int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
				int addressCol = cur.getColumnIndex(ClientsConnector.ADDRESS);
				int postcodeCol = cur.getColumnIndex(ClientsConnector.POSTCODE);			
				clientName = cur.getString(forenameCol) + " " + cur.getString(surnameCol);
				company = cur.getString(companyNameCol);
				companyTextView.setText(company);
				mobile = cur.getString(mobileCol);
				email = cur.getString(emailCol);
				address = cur.getString(addressCol);
				postcode = cur.getString(postcodeCol);
				clientButton.setText(clientName);
			} catch (Exception e) { 
	        	System.out.println("++++++No Clients+++++ "+e.getLocalizedMessage().toString());
	        }
			cliCon.close();
		}
	}
	
	public void getTasks(int id) {
		totalTasks = 0;
		qtasksCon.open();
		try{		
			tasksCur = qtasksCon.getTasksByQuote(id);	
			if(tasksCur.getCount() != 0){
				((TextView) tasksView.findViewById(R.id.typeViewQuoteHolderTextView)).setText("Work");
		        linear.addView(tasksView);
				tasksCur.moveToFirst();
				// Get the Task columns
				int taskIdCol = tasksCur.getColumnIndex(QtasksConnector.QTASKID); 
				int taskNameCol = tasksCur.getColumnIndex(QtasksConnector.TASKNAME);
				int estimatedDurationCol = tasksCur.getColumnIndex(QtasksConnector.ESTIMATEDDURATION);
				int chargeValueCol = tasksCur.getColumnIndex(QtasksConnector.CHARGEVALUE);			
				do {
					Task task = new Task(tasksCur.getInt(taskIdCol), tasksCur.getString(taskNameCol),  tasksCur.getDouble(estimatedDurationCol), tasksCur.getDouble(chargeValueCol));
					RelativeLayout inflatedView = (RelativeLayout) View.inflate(this, R.layout.activity_view_quote_child, null);
					((TextView) inflatedView.findViewById(R.id.typeNameViewQuoteTextView)).setText(task.taskName);
					((TextView) inflatedView.findViewById(R.id.quantityViewQuoteTextView)).setText(" "+String.valueOf(task.duration)+" Hrs");
					((TextView) inflatedView.findViewById(R.id.unitViewQuoteTextView)).setText("Rate = ");
					((TextView) inflatedView.findViewById(R.id.unitCostViewQuoteTextView)).setText(String.valueOf(nf.format(task.value)));
			     	((LinearLayout) tasksView.findViewById(R.id.taskItemExpLayoutChild)).addView(inflatedView);
			     	if(task.taskName.equals("Callout")){
						totalTasks += task.value;
					}else{
						totalTasks += task.duration * task.value;
					}   	     	
					tasks.add(task);
				}while(tasksCur.moveToNext());	
				((TextView) tasksView.findViewById(R.id.totalViewQuoteTextView)).setText(String.valueOf(nf.format(totalTasks)));
			}
		} catch (Exception e) { 
	    	System.out.println("++++++No QTasks+++++ "+e.getLocalizedMessage().toString());
	    }
		qtasksCon.close();
	}
	
	public void getItems(int id) {
		totalItems = 0;
		qitemsCon.open();
		try{
			itemsCur = qitemsCon.getItemsByQuote(id);
			if(itemsCur.getCount() != 0){		
				((TextView) itemsView.findViewById(R.id.typeViewQuoteHolderTextView)).setText("Items");	        
		        linear.addView(itemsView);
				itemsCur.moveToFirst();
				// Get the Item columns
				int itemIdCol = itemsCur.getColumnIndex(QitemsConnector.QITEMID);
				int itemNameCol = itemsCur.getColumnIndex(QitemsConnector.ITEMNAME);
				int unitCostCol = itemsCur.getColumnIndex(QitemsConnector.UNITCOST);
				int quantityCol = itemsCur.getColumnIndex(QitemsConnector.QUANTITY);
				int totalCostCol = itemsCur.getColumnIndex(QitemsConnector.TOTALCOST);			
				do {
					Item item = new Item(itemsCur.getInt(itemIdCol),  itemsCur.getString(itemNameCol), itemsCur.getDouble(unitCostCol),
							itemsCur.getInt(quantityCol), itemsCur.getDouble(totalCostCol));
					RelativeLayout inflatedView = (RelativeLayout) View.inflate(this, R.layout.activity_view_quote_child, null);
					((TextView) inflatedView.findViewById(R.id.typeNameViewQuoteTextView)).setText(item.itemName);
					((TextView) inflatedView.findViewById(R.id.quantityViewQuoteTextView)).setText(" X "+String.valueOf(item.quantity));
					((TextView) inflatedView.findViewById(R.id.unitViewQuoteTextView)).setText("Unit = ");
					((TextView) inflatedView.findViewById(R.id.unitCostViewQuoteTextView)).setText(String.valueOf(nf.format(item.cost)));
			     	((LinearLayout) itemsView.findViewById(R.id.taskItemExpLayoutChild)).addView(inflatedView);
			     	double totalCost = itemsCur.getDouble(totalCostCol);
					totalItems += totalCost;
			     	items.add(item);
				}while (itemsCur.moveToNext());
				((TextView) itemsView.findViewById(R.id.totalViewQuoteTextView)).setText(String.valueOf(nf.format(totalItems)));
			}
		} catch (Exception e) { 
	    	System.out.println("++++++No QItems+++++ "+e.getLocalizedMessage().toString());
	    }
		qitemsCon.close();
	}
	
	public void startPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		
		if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } 
            catch (ActivityNotFoundException e) {
                Toast.makeText(this, "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
            }
		}
	}
	
	public void checkConStatus (){
		ConnectivityManager conMngr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		android.net.NetworkInfo wifi = conMngr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		android.net.NetworkInfo mobile = conMngr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if(wifi.isConnected() || mobile.isConnected()){
			emailPDF();
		}else{
			final Builder builder = new AlertDialog.Builder(this);			
			TextView title = new TextView(this);
			title.setText("No Internet Connection!");
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			//title.setTextColor(getResources().getColor(R.color.greenBG));
			title.setTextSize(20);
			builder.setCustomTitle(title);
			TextView msg = new TextView(this);
			msg.setText("No Internet connection is available. If you choose to send the Quote, the email will stay in your Email Client Outbox till the device gets connected to the internet. Send email?");
			msg.setPadding(10, 10, 10, 10);
			msg.setGravity(Gravity.CENTER);
			msg.setTextSize(18);
			builder.setView(msg);
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					emailPDF();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}
	
	public void emailPDF(){
		emailStarted = true;
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		String[] mailto = {emailSend};
		i.putExtra(Intent.EXTRA_EMAIL, mailto);
		i.putExtra(Intent.EXTRA_SUBJECT, "Your Quote");
		i.putExtra(Intent.EXTRA_TEXT, "Hello "+clientName+", your quote has been attached to this email.");
		i.putExtra(Intent.EXTRA_STREAM,
			Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ java.io.File.separator + "beansone"+ java.io.File.separator + file)));
		i.setType("application/pdf");
	    startActivity(Intent.createChooser(i, "Send Email"));
	}
	
	public void printPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		Intent printIntent = new Intent(this, PrintDialog.class);
    	printIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
    	printIntent.putExtra("title", "Quote for "+clientName);
    	startActivity(printIntent);
	}

}