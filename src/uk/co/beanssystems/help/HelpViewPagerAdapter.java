package uk.co.beanssystems.help;

import uk.co.beanssystems.beansone.R;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

public class HelpViewPagerAdapter extends PagerAdapter {
	LayoutInflater inflater;

    public int getCount() {
        return 12;
    }
    
    public Object instantiateItem(View collection, int position) {
        inflater = (LayoutInflater) collection.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int resId = 0;
        
        switch (position) {
        case 0:
            resId = R.layout.activity_help_menu;
            break;
        case 1:
            resId = R.layout.activity_help_terms;
            break;
        case 2:
            resId = R.layout.activity_help_add_client;
            break;
        case 3:
            resId = R.layout.activity_help_add_quote;
            break;
        case 4:
            resId = R.layout.activity_help_add_job;
            break;
        case 5:
            resId = R.layout.activity_help_add_invoice;
            break;
        case 6:
            resId = R.layout.activity_help_add_expense;
            break;
        case 7:
            resId = R.layout.activity_help_add_task_item;
            break;
        case 8:
            resId = R.layout.activity_help_view_task_item;
            break;
        case 9:
            resId = R.layout.activity_help_camera;
            break; 
        case 10:
            resId = R.layout.activity_help_quick_contact;
            break; 
        case 11:
            resId = R.layout.activity_help_send;
            break;           
        }
        View view = inflater.inflate(resId, null);
        ((ViewPager) collection).addView(view, 0);
        
        return view;
    }
    
    
    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }
    
    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }
    
    @Override
    public Parcelable saveState() {
        return null;
    }
}
