package uk.co.beanssystems.help;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.settings.Settings;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class Help extends SherlockActivity {
	ViewPager myPager;
	HelpViewPagerAdapter vAdapter;
	String source="";
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		
		try{
			Intent i = getIntent();
	        source = i.getStringExtra("source");
        } catch (Exception e) {
        	e.printStackTrace();
        }
		
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.help);
	    bar.setDisplayHomeAsUpEnabled(true);
		
		vAdapter = new HelpViewPagerAdapter();
	    myPager = (ViewPager) findViewById(R.id.viewPagerSetup);
	    myPager.setAdapter(vAdapter);
	    myPager.setOffscreenPageLimit(2);
	    myPager.setCurrentItem(0);
	}
	
	  @Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.menu_options, menu);
			MenuItem it = menu.findItem(R.id.add_option);
			it.setTitle("Help Menu");
	    return true;
	  }
	
	public void onClick(View v){
		switch(v.getId()){
		case R.id.glossaryHelpMenu:
			myPager.setCurrentItem(1);
			break;	
		case R.id.clientsHelpMenu:
			myPager.setCurrentItem(2);
			break;			
		case R.id.quotesHelpMenu:
			myPager.setCurrentItem(3);
			break;			
		case R.id.jobsHelpMenu:
			myPager.setCurrentItem(4);
			break;			
		case R.id.invoicesHelpMenu:
			myPager.setCurrentItem(5);
			break;			
		case R.id.expensesHelpMenu:
			myPager.setCurrentItem(6);
			break;			
		case R.id.addTasksItemsHelpMenu:
			myPager.setCurrentItem(7);
			break;		
		case R.id.viewTasksItemsHelpMenu:
			myPager.setCurrentItem(8);
			break;			
		case R.id.cameraHelpMenu:
			myPager.setCurrentItem(9);
			break;			
		case R.id.quickContactHelpMenu:
			myPager.setCurrentItem(10);
			break;
		case R.id.sendHelpMenu:
			myPager.setCurrentItem(11);
			break;	
		default:break;
		}
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	    	if("Setup".equals(source)){
	    		Intent iM = new Intent (this, MainActivity.class);
	    		startActivity(iM);
	    		finish();
	    	}else{
	    		finish();
	    	}
	    	return true;
	    case R.id.add_option:
	    	myPager.setCurrentItem(0);
	    	return true;
	    case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
}
