package uk.co.beanssystems.popups;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.jobs.ViewJobInfo;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockActivity;

public class EnterFixedPricePopup extends SherlockActivity {
	private EditText price;
	private Button okPriceButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_fixed_price_enter);
		price = (EditText)findViewById(R.id.fixedPricePopupEditText);
		okPriceButton = (Button)findViewById(R.id.okPricePopupButton);
	}
	
	public void onClick(View v){
		switch (v.getId()){
			case R.id.okPricePopupButton:
				okPriceButton.setEnabled(false);
				Intent iOk = new Intent(this, ViewJobInfo.class);
				iOk.putExtra("fixedPrice", Double.parseDouble(price.getText().toString()));
				setResult(RESULT_OK, iOk);
				finish();
				break;
				
			case R.id.cancelPricePopupButton:
				setResult(RESULT_CANCELED);
				finish();
				break;
				
			default:break;
		}
	}
}
