package uk.co.beanssystems.popups;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.RatesConnector;
import uk.co.beanssystems.jobs.Rate;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class AddRatePopup extends Activity {
	private EditText rateName, rateCharge;
	private Button addRateDeleteButton, addRateButton;
	private Spinner calloutHrsSpinner;
	private boolean incVAT = true, edit = false;
	private LinearLayout calloutTimeHolder;
	private int rateID;
	double globalVAT = 20.0;
	private Rate rate;
	ArrayList<String> calloutTimeSpinnerArray = new ArrayList<String>();
	private RatesConnector rCon = new RatesConnector(AddRatePopup.this);
	DecimalFormat df ;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_add_rate);
		df = new DecimalFormat("#.##");
		rateName = (EditText) findViewById(R.id.rateNameAddRateEditText);
		rateCharge = (EditText) findViewById(R.id.chargeAddRateEditText);
		addRateDeleteButton = (Button) findViewById(R.id.delAddRateButton);
		addRateButton = (Button) findViewById(R.id.okAddRateButton);
		calloutHrsSpinner = (Spinner) findViewById(R.id.calloutTimeAddRateSpinner);
		calloutTimeHolder = (LinearLayout) findViewById(R.id.calloutTimeHolderAddRate);
		// Edit
		Intent in = getIntent();
		if(in.getIntExtra("edit", 0) > 0){
			rate = (Rate) in.getSerializableExtra("rateObj");
			rateID = rate.rateID;
			rateName.setText(rate.rateName);
			rateCharge.setText(String.valueOf(df.format(rate.rateValue)));
			addRateDeleteButton.setVisibility(View.VISIBLE); 
			edit = true;
			addRateButton.setText("Update");
			if(rate.rateName.equals("Callout")){
				calloutTimeHolder.setVisibility(View.VISIBLE);
				setCalloutTimeSpinner();
				// Set quantity spinner
				ArrayAdapter durAdap = (ArrayAdapter) calloutHrsSpinner.getAdapter(); // Cast to an ArrayAdapter
	            int spinnerPosition = durAdap.getPosition(String.valueOf(rate.rateTime));
	            calloutHrsSpinner.setSelection(spinnerPosition); // Set the default according to value
	            rateName.setFocusable(false);
	            rateName.setFocusableInTouchMode(false);
			}else if(rate.rateName.equals("Standard")){
				 rateName.setFocusable(false);
		         rateName.setFocusableInTouchMode(false);
			}
		}
	}
	
    public void setCalloutTimeSpinner () {
    	calloutTimeSpinnerArray.clear();
		for(int i=0; i <= 20; i++){
			calloutTimeSpinnerArray.add(String.valueOf(i));
		}
		ArrayAdapter<String> spinnerArraAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, calloutTimeSpinnerArray);
		spinnerArraAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		calloutHrsSpinner.setAdapter(spinnerArraAdapter);	
		calloutHrsSpinner.setOnTouchListener(spinnerTouchEvent);
	}
    
	private View.OnTouchListener spinnerTouchEvent = new View.OnTouchListener() {
	    public boolean onTouch(View v, MotionEvent event) {
	        if (event.getAction() == MotionEvent.ACTION_UP) {
	        	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	        	imm.hideSoftInputFromWindow(calloutHrsSpinner.getWindowToken(), 0);
	        }
	        return false;
	    }
	};
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.okAddRateButton:
			addRateButton.setEnabled(false);
			if (rateName.getText().length()==0 || rateCharge.getText().length()==0){
				addRateButton.setEnabled(true);
				validation(rateName);
				validation(rateCharge);
			}else if(edit){
				String rateTime = "";
				if(rate.rateName.equals("Callout")){
					rateTime = calloutHrsSpinner.getSelectedItem().toString();
				}
				rCon.updateRate(rateID, rateName.getText().toString(), Double.valueOf(stripDecimal(rateCharge.getText().toString())), 0.0, "", rateTime);
				Intent i = new Intent();	
				setResult(RESULT_OK, i);
				finish();
			}else {
			  	String rate = rateName.getText().toString();
				double rateVal = Double.valueOf(stripDecimal(rateCharge.getText().toString()));				
				RatesConnector rateCon = new RatesConnector(this);
				rateCon.insertRate(rate, rateVal, 0.0, "", "rateTime");
				Intent i = new Intent();	
				i.putExtra("rateName", rate);
				i.putExtra("rateVal", rateVal);
				setResult(RESULT_OK, i);
		  		finish();
			}
			break;
		case R.id.cancelAddRateButton:
			Intent i = new Intent();
			setResult(RESULT_CANCELED, i);
			finish();
			break;
		case R.id.delAddRateButton:		
	    	// Create AlertDialog
			final Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Delete Rate");
			builder.setMessage("Are you sure you want to delete the rate?");
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {				
					rCon.deleteItem(rateID);
					Intent in = new Intent();
					setResult(RESULT_OK, in);
			  		finish();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
			break;
		case R.id.esthoursLabelTextView:
			((Spinner) findViewById(R.id.calloutTimeAddRateSpinner)).performClick();
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        	imm.hideSoftInputFromWindow(calloutHrsSpinner.getWindowToken(), 0);
			break;
 
		default:
			break;
		}
	}
	
	public String stripDecimal(String str){
		String[] amount= str.split("\\.");
		try{
			amount = str.split("\\.");
			amount[1] = amount[1].substring(0, 2);
		} catch (Exception e){
			System.out.println("Entered Integer");
			return amount[0];
		}
		return amount[0]+"."+amount[1];
	}
	
	public void validation (final EditText text){
		if(text.getText().length()==0){
			text.setError("Please Complete Field");
		} else {
			text.setError(null,null);
		}
		text.addTextChangedListener(new TextWatcher() {	 
		   public void afterTextChanged(Editable s) {
			   if(text.getText().length()==0){
				   text.setError("Please Complete Field");
			   }else{
				   text.setError(null,null);
			   }
		   }
		 
		   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		 
		   public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
}
