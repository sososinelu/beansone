package uk.co.beanssystems.popups;

import java.io.File;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import uk.co.beanssystems.beansone.R;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class AddExpensePopup extends Activity {
	Spinner expenseType;
	EditText date, cost, description;
	Button addExpDeleteButton, addExpButton;
	ImageView thumb;
	private Uri storedUri = null,  tempUri = null;
	private double globalVAT = 20.0;
	private int childPos = -1;
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		overridePendingTransition(R.anim.slidein, R.anim.slideout);
		setContentView(R.layout.popup_new_job_add_expense);

		expenseType = (Spinner)findViewById(R.id.expenseTypeAddExpenseViewJobSpinner);
		date = (EditText)findViewById(R.id.dateAddExpenseViewJobEditText);
		cost = (EditText)findViewById(R.id.costAddExpenseViewJobEditText);
		description = (EditText)findViewById(R.id.descriptionAddExpenseViewJobEditText);
		thumb = (ImageView)findViewById(R.id.cameraThumbAddExpenseViewJobImageView);
		addExpDeleteButton = (Button) findViewById(R.id.delAddExpPopupButton);
		addExpButton = (Button) findViewById(R.id.okAddExpenseViewJobButton);
		
		// Set todays date in Start Date edit text
		Calendar today = Calendar.getInstance();
		date.setText(outDate.format(today.getTime()));
		
		Intent in = getIntent();
		if(in.getIntExtra("childPos", -1) >=0){		
			childPos = in.getIntExtra("childPos", -1);
			ArrayAdapter myAdap = (ArrayAdapter) expenseType.getAdapter(); // Cast to an ArrayAdapter
            int spinnerPosition = myAdap.getPosition(in.getStringExtra("expType"));
            //set the default according to value
            expenseType.setSelection(spinnerPosition);	
			date.setText(in.getStringExtra("expDate"));
			cost.setText(in.getStringExtra("expCost"));
			description.setText(in.getStringExtra("expDescription"));
			String path = in.getStringExtra("photoPath");
			System.out.println("Photo Path >>>>>>>>>>>>"+path);
			try{
				if (path.length()>0){
					storedUri = Uri.parse(in.getStringExtra("photoPath"));
					loadThumb();
				} else {
					storedUri = null;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
				
			addExpDeleteButton.setVisibility(View.VISIBLE); 
		}
	}
	
	public String stripDecimal(String str){
		String[] amount;
		try{
			amount = str.split("\\.");
			amount[1] = amount[1].substring(0, 2);
		} catch (Exception e){
			System.out.println("Entered Integer");
			return str;
		}
		return amount[0]+"."+amount[1];
	}
	
	public void onClick(View v){
		switch (v.getId()){
		case R.id.dateAddExpenseViewJobEditText:
			Calendar cal = Calendar.getInstance();
			DatePickerDialog datePickDiag = new DatePickerDialog(this, odsl, cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			datePickDiag.show();
			break;
		case R.id.okAddExpenseViewJobButton:
			addExpButton.setEnabled(false);
			if (date.getText().length()==0 || cost.getText().length()==0 || description.getText().length()==0){
				addExpButton.setEnabled(true);
				validation(date);
				validation(cost);
				validation(description);
			} else {			
				String expType = expenseType.getSelectedItem().toString();
				String expDate = date.getText().toString();
				Date date = new Date();
				try {
					date = outDate.parse(expDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				String value = stripDecimal(cost.getText().toString());
				double expCost = Double.valueOf(value);
				String expDescription = description.getText().toString();
				Intent in = new Intent();
				in.putExtra("expType", expType);
				in.putExtra("expDate", inDate.format(date));
				in.putExtra("expCost", expCost);
				in.putExtra("expDescription", expDescription);
				if (storedUri!=null){
					in.putExtra("photoPath", storedUri.getPath());
				} else {
					in.putExtra("photoPath", "");
				}
				in.putExtra("childPos", childPos);
				setResult(RESULT_OK, in);
				finish();
				overridePendingTransition(R.anim.slidein, R.anim.slideout);
			}
			break;
	  case R.id.cancelAddExpenseViewJobButton:
		  	if(storedUri!=null){
				File file = new File(storedUri.getPath());
				file.delete();
			}
		  Intent i2 = new Intent();
		  setResult(RESULT_CANCELED, i2);
		  finish();
		  overridePendingTransition(R.anim.slidein, R.anim.slideout);
		  break;
	  case R.id.cameraThumbAddExpenseViewJobImageView:
		  	Intent inCam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			tempUri = this.getOutputMediaFileUri();
			inCam.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
		    startActivityForResult(inCam, 5);
		    break;
	  case R.id.delAddExpPopupButton:
			Intent i = new Intent();
			i.putExtra("childPos", childPos);
			setResult(-2, i);
			finish();
			overridePendingTransition(R.anim.slidein, R.anim.slideout);
			break;
		default:
			break;
		}
	}
	
	public void validation (final EditText text){
		if(text.getText().length()==0){
			text.setError("Please Complete Field");
		} else {
			text.setError(null,null);
		}
		text.addTextChangedListener(new TextWatcher() {	 
		   public void afterTextChanged(Editable s) {
			   if(text.getText().length()==0){
				   text.setError("Please Complete Field");
			   }else{
				   text.setError(null,null);
			   }
		   }
		 
		   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		 
		   public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode){
		case 5:
			//camera
			if (resultCode == RESULT_OK){
				if (storedUri!=null){
					try{
						File file = new File(storedUri.getPath());
						file.delete();
					} catch (Exception e){
						System.out.println("Could not delete file: "+storedUri.getPath());
					}
					sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
					storedUri = tempUri;
					loadThumb();
				} else {
					storedUri = tempUri;
					loadThumb();
				}
			} else if (resultCode == RESULT_CANCELED) {
				tempUri = null;
	        }
		}
	}
	
	public void loadThumb(){
		String imageInSD = storedUri.getPath();
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = 8;
		Bitmap bitmap = BitmapFactory.decodeFile(imageInSD, opts);
		thumb.setImageBitmap(bitmap);
	}
	
	private Uri getOutputMediaFileUri(){
	      return Uri.fromFile(getOutputMediaFile());
	}
	private static File getOutputMediaFile(){
	    File mediaStorageDir = new File(
	    		Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "BeansOne" + File.separator + "Expenses");
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            System.out.println("Beans, failed to create directory");
	            return null;
	        }
	    }
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    mediaFile = new File(mediaStorageDir.getPath() + File.separator +"EXP_"+ timeStamp + ".jpg");
	    return mediaFile;
	}
	 final OnDateSetListener odsl = new OnDateSetListener() {
			public void onDateSet(DatePicker arg0, int year, int month,
					int dayOfMonth) {
				month++;
				TextView txt = (TextView) findViewById(R.id.dateAddExpenseViewJobEditText);
				String dayStr = String.valueOf(dayOfMonth);
				String monthStr = String.valueOf(month);

				if (dayStr.length()==1){
					dayStr = "0"+ dayStr;
				}
				if (monthStr.length()==1){
					monthStr = "0"+ monthStr;
				}
				txt.setText(dayStr + "/" + monthStr + "/" + year);
			}
		};
}
