package uk.co.beanssystems.popups;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.AccountConnector;
import uk.co.beanssystems.database.RatesConnector;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class ChangePasswordPopup extends SherlockActivity {
	private EditText oldPass, newPass, confirmPass;
	private TextView error;
	String errorMsg = "";
	private Pattern pattern;
	private Matcher matcher;
	Context context;
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z]).{6,20})";
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_change_password);
		context = this;
		oldPass = (EditText) findViewById(R.id.oldPassValueChangePasswordEditText);
		newPass = (EditText) findViewById(R.id.newPassValueChangePasswordEditText);
		confirmPass = (EditText) findViewById(R.id.confirmPassValueChangePasswordEditText);
		error = (TextView) findViewById(R.id.errorTextChangePassword);
	}
	
	public boolean validate(){
		errorMsg="";
		if (oldPass.getText().length()==0 || newPass.getText().length()==0 || confirmPass.getText().length()==0){
			error.setVisibility(View.VISIBLE);
			error.setText("Please complete all fields");
			return false;
		}
		
		String nPass = newPass.getText().toString();
		String cPass = confirmPass.getText().toString();
		if (!nPass.equals(cPass)){
			error.setVisibility(View.VISIBLE);
			error.setText("Your new passwords do not match");
			return false;
		}
		
		pattern = Pattern.compile(PASSWORD_PATTERN);
		String p = newPass.getText().toString();
		matcher = pattern.matcher(p);
		if (!matcher.matches()){
			error.setVisibility(View.VISIBLE);
			error.setText("Invalid New Password\nMust be 6-20 characters containing both numbers and letters");
			return false;
		}
		return true;
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.okAddRateButton:
				if (validate()){
					JSONObject request = new JSONObject();
					AccountConnector accCon = new AccountConnector(this);
					
					try {
						request.put("username", accCon.getUsername());
						request.put("oldPass", oldPass.getText().toString());
						request.put("newPass", newPass.getText().toString());
						
						ChangePassTask task = new ChangePassTask();
						task.execute("http://109.235.145.88/scripts/change.php", request.toString());
					} catch (JSONException e){
						
					}
					
					
				}
			break;
		case R.id.cancelAddRateButton:
			finish();
			break;
		default:
			break;
		}
	}
	
	private class ChangePassTask extends AsyncTask<String, Void, JSONObject>{
		@Override
		protected JSONObject doInBackground(String... params) {
			String result = "";
			JSONObject request;
			JSONObject resp = new JSONObject();
			try {
				request = new JSONObject(params[1]);
				HttpClient httpClient = new DefaultHttpClient();
		        HttpPost httpPost = new HttpPost(params[0]);
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("json", request.toString()));
		        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        HttpResponse response = httpClient.execute(httpPost);
		        HttpEntity entity = response.getEntity();
		        InputStream is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        result=sb.toString();
		        Log.i("Result String", result);
		        resp = new JSONObject(result);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return resp;
		}
		
		@Override
	    protected void onPostExecute(JSONObject result) {
			String code="0", msg="", pass="";
			try {
				code = result.getString("code");
				msg = result.getString("msg");
				pass = result.getString("pass");
			} catch (JSONException e){
				e.printStackTrace();
			}
			if ("1".equals(code) && pass.length()>5){
				AccountConnector accCon = new AccountConnector(context);
				accCon.updatePassword(pass);
				Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
				finish();
			} else {
				Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
			}
			
			
	    }
	}
}
