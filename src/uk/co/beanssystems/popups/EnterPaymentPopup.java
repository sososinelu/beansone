package uk.co.beanssystems.popups;

import java.text.NumberFormat;
import java.util.Locale;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.InvoicesConnector;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class EnterPaymentPopup extends Activity {
	private EditText paymentEditText;
	private TextView outstandingTextView;
	private Button okPaymentButton;
	private int invoiceID;
	private double outstandingSum;
	NumberFormat nf;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_view_invoice_payment);
		// Set currency format 
		nf = NumberFormat.getCurrencyInstance(Locale.UK);		
		Intent ili = getIntent();
        invoiceID = ili.getIntExtra("passInvoiceID", -1);
        outstandingSum = ili.getDoubleExtra("passOutstandingSum", -1);
        okPaymentButton = (Button) findViewById(R.id.okPaymentPopupButton);
        outstandingTextView = (TextView) findViewById(R.id.outstandingPaymentPopupTextView);
        paymentEditText = (EditText) findViewById(R.id.paymentPopupEditText);       
        outstandingTextView.setText(String.valueOf(nf.format(outstandingSum)));
	}
	
	public String stripDecimal(String str){
		String[] amount;
		try{
			amount = str.split("\\.");
			amount[1] = amount[1].substring(0, 2);
		} catch (Exception e){
			System.out.println("Entered Integer");
			return str;
		}
		return amount[0]+"."+amount[1];
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.okPaymentPopupButton:
			okPaymentButton.setEnabled(false);
			if (paymentEditText.getText().toString().length()==0){
				okPaymentButton.setEnabled(true);
				paymentEditText.setError("Please Complete Field");
			} else if (Double.valueOf(paymentEditText.getText().toString())==0){
				okPaymentButton.setEnabled(true);
				paymentEditText.setError("Amount must be more than zero");
			} else {
				okPaymentButton.setEnabled(true);
				paymentEditText.setError(null, null);
				String payString = stripDecimal(paymentEditText.getText().toString());
				double payment = Double.valueOf(payString);
				InvoicesConnector invCon = new InvoicesConnector(this);
				if(payment > outstandingSum){
					Toast.makeText(getApplicationContext(), "The payment is bigger then the outstanding value!",
							Toast.LENGTH_LONG).show();
				}else{
					try{
						invCon.updateOutstandingSum(invoiceID, (outstandingSum-payment));
					} catch(Exception e){
						System.out.println("++++++ERROR+++++ "+e.getLocalizedMessage().toString());
			   	  	}
					Intent i = new Intent();
				  	i.putExtra("passOutstandingSum", (outstandingSum-payment));
				  	setResult(RESULT_OK, i);
				  	finish();
				}
			}
			break;
		case R.id.cancelPaymentPopupButton:
			finish();
			break;
		default:
			break;
		}
	}
}
