package uk.co.beanssystems.popups;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.clients.NewClient;
import uk.co.beanssystems.database.ClientsProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

public class SelectClientPopup extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor>  {
	private static final String ID = "rowid _id", CLIENTID = "clientID", FORENAME = "forename", SURNAME = "surname", EMAIL = "email";
	EditText searchEditText;
	private ComponentName sourceClass;
	private CursorAdapter adapter;
	private Context context;
	private static final int LOADER_ID = 0x01;
	private static final int SEARCH_LOADER = 0x02;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_new_invoice_client);
		sourceClass = getCallingActivity();
		searchEditText = (EditText) findViewById(R.id.searchClientPopupEditText);
		context = this;
		
		getSupportLoaderManager().initLoader(LOADER_ID, null, this);
		
		adapter = new SimpleCursorAdapter(this,
				android.R.layout.simple_list_item_2, 
				null, 
				new String[]{FORENAME,SURNAME}, 
				new int[]{android.R.id.text1, android.R.id.text2}, 
				Adapter.NO_SELECTION);
		
		 ListView listView = (ListView) findViewById(android.R.id.list);
	     listView.setAdapter(adapter);
	     listView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {
				String projection[] = { ID, CLIENTID, FORENAME, SURNAME, EMAIL};
			    Cursor cur = getContentResolver().query(Uri.withAppendedPath(ClientsProvider.CONTENT_URI, String.valueOf(id)), projection, null, null, null);
			    if (cur.moveToFirst()) {
			        Intent i = new Intent(context, sourceClass.getClass());
			        i.putExtra("clientID", cur.getInt(1));
				    i.putExtra("clientName", cur.getString(2) + " " + cur.getString(3));
				    i.putExtra("email", cur.getString(4));
				    cur.close();
				    setResult(RESULT_OK, i);
				    finish();
			    } else {
			    	cur.close();
			    }
			}
	     });

	     searchEditText.addTextChangedListener(new TextWatcher() {
		        public void onTextChanged(CharSequence s, int start, int before, int count) {
		        }
		        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		        }
		        public void afterTextChanged(Editable s) {
		        	Bundle bundle = new Bundle();
		    		bundle.putString("str", s.toString());
		        	getSupportLoaderManager().restartLoader(SEARCH_LOADER, bundle, (LoaderCallbacks<Cursor>) context);
		        }
		    });
	}
	
	public void onClick(View view){
		  switch (view.getId()){
		  case R.id.addClientPopupButton:
			  Intent inNewClient = new Intent(this, NewClient.class);
			  startActivityForResult(inNewClient, 0);
			  break;
		  default:
			  break;
		  }
	  }
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		  switch (requestCode){
			  case 0:
				  if (resultCode==RESULT_OK){
					  setResult(RESULT_OK, data);
					  finish();
				  } else if (resultCode==RESULT_CANCELED){
					  //
				  }
				  break;
			  default:
				  break;
		  }
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
		CursorLoader curLoader = null;
		switch(i){
			case LOADER_ID:
				curLoader = new CursorLoader(this, 
						Uri.parse(ClientsProvider.CONTENT_URI.toString()), //CONTENT URI
						new String[]{ID, CLIENTID, FORENAME, SURNAME}, //PROJECTION
						null, //SELECTION
						null, //SELECTION ARGUMENTS
						null); //SORT ORDER
				break;
			case SEARCH_LOADER:
				curLoader = new CursorLoader(this, 
						Uri.parse(ClientsProvider.CONTENT_URI.toString()), //CONTENT URI
						new String[]{ID, CLIENTID, FORENAME, SURNAME}, //PROJECTION
						FORENAME + " like '%" + bundle.getString("str") + "%'" +" OR "+ SURNAME + " like '%" + bundle.getString("str") + "%'", //SELECTION
						null, //SELECTION ARGUMENTS
						null); //SORT ORDER
				break;
			default:
				break;
		}
		return curLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
        adapter.swapCursor(null);
	}
}