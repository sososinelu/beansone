package uk.co.beanssystems.popups;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.AccountConnector;
import uk.co.beanssystems.database.MasterConnector;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class SyncPopup extends Activity {
	TextView accLabel;
	ProgressBar progress;
	String dbName="";
	JSONObject tables = new JSONObject();
	Context context;
	String oldStamp="", newStamp, username="";
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.popup_sync);
	    context = this;
	    accLabel = (TextView) findViewById(R.id.accLabelSyncPopupTextView);
	    progress =  (ProgressBar) findViewById(R.id.progressSyncPopupBar);
	    
	    AccountConnector accCon = new AccountConnector(this);
	    dbName = accCon.getDatabase();
	    oldStamp = accCon.getLastSync();
	    Date date = new Date();
		accCon.updateLastSync(sdf.format(date));
	    username = accCon.getUsername();
	    GetDatesTask datesTask = new GetDatesTask();
		datesTask.execute(new String[] {});
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.cancelSyncPopupButton:
				finish();
			break;
		default:
			break;
		}
	}
	
	private class GetDatesTask extends AsyncTask<String, Void, JSONObject>{
		@Override
		protected JSONObject doInBackground(String... params) {
			String result = "";
			JSONObject request = new JSONObject();
			JSONObject tablesLocal = new JSONObject();
			try{
				request.put("database", dbName);
				HttpClient httpClient = new DefaultHttpClient();
		        HttpPost httpPost = new HttpPost("http://109.235.145.88/scripts/verify.php");
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("json", request.toString()));
		        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        
		        HttpResponse response = httpClient.execute(httpPost);
		        HttpEntity entity = response.getEntity();
		        InputStream is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        result=sb.toString();
		        JSONObject jObj = new JSONObject(result);
		        tablesLocal = jObj.getJSONObject("tables"); 
			} catch (JSONException e){
				Log.i("verify.php Result:",result);
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return tablesLocal;
		}
		
		@Override
	    protected void onPostExecute(JSONObject result) {
			tables = result;
			SendDataTask task = new SendDataTask();
			task.execute(new String[] {});
	    }
	}
	private class SendDataTask extends AsyncTask<String, Integer, JSONObject>{
		ProgressBar progressBar;
		int myProgress;
		@Override
		protected JSONObject doInBackground(String... params) {
			
			Context context = getBaseContext();
			JSONObject jObj = new JSONObject();
			JSONArray jar = new JSONArray();
			MasterConnector mCon = new MasterConnector(context);
			jar = mCon.getJSON(tables);
			Log.i("JSON TO SERVER", jar.toString());
			JSONObject job = new JSONObject();
			try {
				job.put("database", dbName);
				job.put("DBVersion", "1.0");
				job.put("tables", jar);
				job.put("username", username);
				myProgress=40;
				publishProgress(myProgress);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			String result= "";
			String msg = "";
			HttpClient httpClient = new DefaultHttpClient();
	        HttpPost httpPost = new HttpPost("http://109.235.145.88/scripts/store.php");
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("json", job.toString()));
	        try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
		        httpClient.execute(httpPost);
		        
		        HttpResponse response = httpClient.execute(httpPost);
		        HttpEntity entity = response.getEntity();
		        myProgress = 80;
		        publishProgress(myProgress);
		        InputStream is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        result=sb.toString();
		        Log.i("STORERAW", result);
	            jObj = new JSONObject(result);
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				Log.i("store.php Result:",result);
	            Log.e("JSON Parser", "Error parsing data " + e.toString());
	        }  
			return jObj;
		}
		
	 @Override
	  protected void onPreExecute() {
	   progressBar = (ProgressBar) findViewById(R.id.progressSyncPopupBar);
	   myProgress = 0;
	  }
	 
	 @Override
	  protected void onProgressUpdate(Integer... values) {
	   progressBar.setProgress(values[0]);
	  }
		protected void onPostExecute(JSONObject result){
			int code=0;
			String msg="Sync Error";
			try{
				code = Integer.parseInt(result.getString("code"));
				msg = result.getString("msg");
			} catch (JSONException e){
				
			}
			AccountConnector accCon = new AccountConnector(context);
			switch (code){
				case 0://Error Code
					accCon.updateLastSync(oldStamp);
					break;
				case 1://Success Code
					myProgress = 100;
					publishProgress(myProgress);
					Date date = new Date();
					accCon.updateLastSync(sdf.format(date));
					break;
				default:break;
			}
			Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
			finish();
		}
	}	
}
