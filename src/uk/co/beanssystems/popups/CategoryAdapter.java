package uk.co.beanssystems.popups;

import java.util.ArrayList;

import uk.co.beanssystems.beansone.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CategoryAdapter extends ArrayAdapter<Category> {
	Context context;
	int layoutResourceId;
	ArrayList<Category> cats = null;
	
	public CategoryAdapter (Context context, int layoutResourceId, ArrayList<Category> cats){
		super(context, layoutResourceId, cats);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.cats = cats;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		CategoryHolder holder = null;
		
		Category cat = cats.get(position);		
		if(row == null){
	        	LayoutInflater inflater = ((Activity)context).getLayoutInflater();
		        row = inflater.inflate(layoutResourceId, parent, false);
		        holder = new CategoryHolder();
		        holder.title = (TextView) row.findViewById(R.id.headerTitleCategory);
		        row.setTag(holder);
	    } else {
        	holder = (CategoryHolder)row.getTag();
       	}
		
		TextView txt = (TextView) row.findViewById(R.id.headerTitleCategory);
		holder.title.setText(cat.category);

		if (cat.rowType==0){
			txt.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.green_button));
			txt.setPadding(5, 0, 0, 2);
			txt.setGravity(Gravity.LEFT);
			txt.setGravity(Gravity.CENTER_VERTICAL);
			txt.setTextSize(18);
			txt.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
		} else {
			txt.setBackgroundColor(Color.TRANSPARENT);
			txt.setPadding(0, 1, 0, 1);
			txt.setGravity(Gravity.CENTER);
			txt.setTextSize(22);
		}
		return row;
	}

	static class CategoryHolder {
		TextView title;
	}
}
