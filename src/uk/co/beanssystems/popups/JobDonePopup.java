package uk.co.beanssystems.popups;

import uk.co.beanssystems.beansone.R;
import android.os.Bundle;
import android.view.View;

import com.actionbarsherlock.app.SherlockActivity;

public class JobDonePopup extends SherlockActivity {

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_job_done);
	}
	
	public void onClick(View v){
		switch (v.getId()){
		case R.id.okButtonJobDonePopup:
			setResult(RESULT_OK);
			finish();
			break;
		
		case R.id.cancelButtonJobDonePopup:
			setResult(RESULT_CANCELED);
			finish();
			break;
			
		default:break;
		}
	}
}
