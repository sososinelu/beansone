package uk.co.beanssystems.popups;

import java.util.ArrayList;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ExpSubTypeConnector;
import uk.co.beanssystems.database.ExpTypeConnector;
import uk.co.beanssystems.expenses.NewExpense;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.SherlockActivity;

public class ExpenseTypePopup extends SherlockActivity {
	private ArrayList<Category> cats;
	ListView catList;
	private static final String EXPTYPEID = "expTypeID";
	private static final String EXPTYPE = "expType";
	private static final String EXPSUBTYPEID = "expSubTypeID", EXPSUBTYPE = "expSubType";
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_expense_type_category);
		loadExpenses();
	}
	
	public void loadExpenses(){
		catList = (ListView) findViewById(R.id.expense_category_list);
		cats = new ArrayList<Category>();
		ExpTypeConnector expCon = new ExpTypeConnector(this);
		expCon.open();
		ExpSubTypeConnector expSubCon = new ExpSubTypeConnector(this);
		expSubCon.open();
		try {
			Cursor parentCur = expCon.getAllExpTypes();
			parentCur.moveToFirst();
			int expTypeIdCol = parentCur.getColumnIndex(EXPTYPEID);
			int expTypeCol = parentCur.getColumnIndex(EXPTYPE);
			int expTypeID;
			String expType;
			//Get root and add their children
			do {
				expTypeID = parentCur.getInt(expTypeIdCol);
				expType = parentCur.getString(expTypeCol).trim();
				
				Cursor childCur = expSubCon.getAllExpSubTypesByRoot(expTypeID);
				if (childCur.getCount()!=0){
					Category p = new Category(0, expTypeID, expType);
					cats.add(p);
				}
				childCur.moveToFirst();
				int expSubTypeIdCol = childCur.getColumnIndex(EXPSUBTYPEID);
				int expSubTypeCol = childCur.getColumnIndex(EXPSUBTYPE);
				do {
					Category c = new Category(1, childCur.getInt(expSubTypeIdCol), childCur.getString(expSubTypeCol));
					cats.add(c);
				} while (childCur.moveToNext());
				
			} while (parentCur.moveToNext());
		} catch (Exception e){
			e.printStackTrace();
		}
		
		expSubCon.close();
		expCon.close();
		
		CategoryAdapter adapter = new CategoryAdapter(this, R.layout.category_parent_header, cats);
        catList.setAdapter(adapter);
        catList.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Category cat = (Category) catList.getItemAtPosition(position);
				if (cat.rowType!=0){
					Intent i = new Intent(getApplicationContext(), NewExpense.class);
					i.putExtra("expenseID", cat.expenseID);
					i.putExtra("expenseType", cat.category);
					setResult(RESULT_OK, i);
					finish();
				}
			}
        });
	}
	
	public void onClick(View view){
		switch(view.getId()){
		case R.id.cancelCatTypePopup:
			setResult(RESULT_CANCELED);
			finish();
			break;
		default:break;
		}
	}
}
