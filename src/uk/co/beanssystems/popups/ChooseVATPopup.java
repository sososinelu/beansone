package uk.co.beanssystems.popups;

import java.text.NumberFormat;
import java.util.Locale;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.invoices.NewInvoice;
import uk.co.beanssystems.quotes.NewQuote;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

public class ChooseVATPopup extends SherlockActivity {
	private double subtotal, vat, total;
	private static final int QUOTE = 0, INVOICE = 1;
	private int source;
	TextView subtotalView, vatView, totalView;
	RadioGroup vatGroup;
	NumberFormat nf;
	private double globalVAT = 20.0;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_choose_vat);
		nf = NumberFormat.getCurrencyInstance(Locale.UK);
		Intent i = getIntent();
		
		subtotalView = (TextView) findViewById(R.id.subTotalValueChooseVatTextView);
		vatView = (TextView) findViewById(R.id.vatValueChooseVatTextView);
		totalView = (TextView) findViewById(R.id.totalValueChooseVatTextView);
		vatGroup = (RadioGroup) findViewById(R.id.vatGroup);
		
		vatGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
		    public void onCheckedChanged(RadioGroup group, int id){
		       switch (id){
			       case R.id.noVatOption:
			    	    subtotalView.setText(nf.format(total));
				   		vatView.setText(nf.format(0.0));
				   		totalView.setText(nf.format(total));
			    	   break;
			       case R.id.incVatOption:
			    	   subtotalView.setText(nf.format(total - (globalVAT / (globalVAT + 100.0) * total)));
				   	   vatView.setText(nf.format((globalVAT / (globalVAT + 100.0) * total)));
				       totalView.setText(nf.format(total));
			    	   break;
			       case R.id.addVatOption:
			    	   subtotalView.setText(nf.format(total));
			   		   vatView.setText(nf.format((total * (globalVAT + 100.0) / 100.0) - total));
			   		   totalView.setText(nf.format(total * (globalVAT + 100.0) / 100.0));
			    	   break;
			       default:break;
		       }
		    }
		});
		
		source = i.getIntExtra("source", 0);
		total = i.getDoubleExtra("total", 0.0);
		
		if("inc".equals(i.getStringExtra("vatType"))){
			vatGroup.check(R.id.incVatOption);	
		}else if("add".equals(i.getStringExtra("vatType"))){
			vatGroup.check(R.id.addVatOption);	
		}else if("none".equals(i.getStringExtra("vatType"))){
			vatGroup.check(R.id.addVatOption);
			vatGroup.check(R.id.noVatOption);	
		}
	}
	
	public void onClick(View view){
		Intent in;
		if (source==QUOTE){
			in = new Intent(this, NewQuote.class);
		} else {
			in = new Intent(this, NewInvoice.class);
		}
		
		switch (view.getId()){
		case R.id.okAddChooseVATButton:
			switch(vatGroup.getCheckedRadioButtonId()){
			case R.id.noVatOption:
			   in.putExtra("vatType", "none");
	    	   in.putExtra("subtotal", total);
	    	   in.putExtra("vat", 0.0);
	    	   in.putExtra("total", total);
	    	   setResult(RESULT_OK, in);
	    	   finish();
	    	   break;
	       case R.id.incVatOption:
	    	   in.putExtra("vatType", "inc");
	    	   in.putExtra("subtotal", total - (globalVAT / (globalVAT + 100.0) * total));
	    	   in.putExtra("vat", (globalVAT / (globalVAT + 100.0) * total));
	    	   in.putExtra("total", total);
	    	   setResult(RESULT_OK, in);
	    	   finish();
	    	   break;
	       case R.id.addVatOption:
	    	   in.putExtra("vatType", "add");
	    	   in.putExtra("subtotal", total);
	    	   in.putExtra("vat", (total * (globalVAT + 100.0) / 100.0) - total);
	    	   in.putExtra("total", total * (globalVAT + 100.0) / 100.0);
	    	   setResult(RESULT_OK, in);
	    	   finish();
	    	   break;
			default:break;
			}
			break;
		case R.id.cancelChooseVATButton:
			finish();
			break;
		default:
			break;
		}
	}
	
}