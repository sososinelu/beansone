package uk.co.beanssystems.popups;

import uk.co.beanssystems.beansone.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

public class DeleteDialog extends SherlockActivity {
	Intent i;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delete_dialog);
		TextView title = (TextView)findViewById(R.id.titleDeleteDialog);
		i = getIntent();
		try {
			title.setText("Delete "+i.getStringExtra("title")+"?");
		} catch (Exception e){
			
		}
	}
	
	public void onClick(View view){
		switch (view.getId()){
			case R.id.yesDeleteDialogButton:
				setResult(RESULT_OK);
				finish();
				break;
			case R.id.noDeleteDialogButton:
				setResult(RESULT_CANCELED);
				finish();
				break;
			default:break;
		}
	}
}
