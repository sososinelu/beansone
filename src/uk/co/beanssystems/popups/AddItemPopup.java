package uk.co.beanssystems.popups;

import java.util.ArrayList;

import uk.co.beanssystems.beansone.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class AddItemPopup extends Activity {
	private EditText itemName, cost;
	private Button addItemOk, addItemDelete;
	private Spinner quantitySpinner; 
	ArrayList<String> quantitySpinnerArray = new ArrayList<String>();
	private int childPos = -1;
	double globalVAT = 20.0, itemVAT1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slidein, R.anim.slideout);
		setContentView(R.layout.popup_new_job_add_item);
		itemName = (EditText) findViewById(R.id.itemNameAddItemNewJobEditText);
		cost = (EditText) findViewById(R.id.costAddItemNewJobEditText);
		addItemOk = (Button) findViewById(R.id.okAddItemNewJobButton);
		addItemDelete  = (Button) findViewById(R.id.delAddItemNewJobButton);
		quantitySpinner = (Spinner) findViewById(R.id.quantityAddItemSpinner); 		
		setQuantity();
		Intent in = getIntent();
		if(in.getIntExtra("childPos", -1) >=0){
			itemName.setText(in.getStringExtra("itemName"));
			cost.setText(String.valueOf(in.getDoubleExtra("cost", 0)));
			// Set quantity spinner
			ArrayAdapter durAdap = (ArrayAdapter) quantitySpinner.getAdapter(); // Cast to an ArrayAdapter
            int spinnerPosition = durAdap.getPosition(String.valueOf(in.getIntExtra("quantity", 0)));
            quantitySpinner.setSelection(spinnerPosition); // Set the default according to value
			childPos = in.getIntExtra("childPos", -1);			
			addItemDelete.setVisibility(View.VISIBLE); 
		}
	}
	
	public void setQuantity () {
		for(int i=1; i <= 30; i++){
			quantitySpinnerArray.add(String.valueOf(i));
			//quantitySpinnerArray.add(String.valueOf(Double.valueOf(i)+0.5));
		}
		ArrayAdapter<String> spinnerArraAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, quantitySpinnerArray);
		spinnerArraAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		quantitySpinner.setAdapter(spinnerArraAdapter);
		quantitySpinner.setOnTouchListener(spinnerTouchEvent);
	}
	
	private View.OnTouchListener spinnerTouchEvent = new View.OnTouchListener() {
	    public boolean onTouch(View v, MotionEvent event) {
	        if (event.getAction() == MotionEvent.ACTION_UP) {
	        	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	        	imm.hideSoftInputFromWindow(quantitySpinner.getWindowToken(), 0);
	        }
	        return false;
	    }
	};
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.okAddItemNewJobButton:
			addItemOk.setEnabled(false);
			if (itemName.getText().length()==0 || cost.getText().length()==0){
				addItemOk.setEnabled(true);
				validation(itemName);
				validation(cost);
			} else {
				String item = itemName.getText().toString();
				String costVal = stripDecimal(cost.getText().toString());
				String quant = quantitySpinner.getSelectedItem().toString();
				Intent i = new Intent();	
				i.putExtra("itemName", item);
				i.putExtra("cost", costVal);
				i.putExtra("quantity", quant);
				i.putExtra("childPos", childPos);
				setResult(RESULT_OK, i);
		  		finish();
		  		overridePendingTransition(R.anim.slidein, R.anim.slideout);
			}
			break;
		case R.id.quantityLabelAddItemTextView:
			((Spinner) findViewById(R.id.quantityAddItemSpinner)).performClick();
			InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        	im.hideSoftInputFromWindow(quantitySpinner.getWindowToken(), 0);
			break;
		case R.id.quantityAddItemImageView:
			((Spinner) findViewById(R.id.quantityAddItemSpinner)).performClick();
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        	imm.hideSoftInputFromWindow(quantitySpinner.getWindowToken(), 0);
			break;
		case R.id.cancelAddItemNewJobButton:
			finish();
			overridePendingTransition(R.anim.slidein, R.anim.slideout);
			break;
		case R.id.delAddItemNewJobButton:
			Intent i = new Intent();
			i.putExtra("childPos", childPos);
			setResult(-2, i);
			finish();
			overridePendingTransition(R.anim.slidein, R.anim.slideout);
			break;
		default:
			break;
		}
	}
	
	public String stripDecimal(String str){
		String[] amount;
		try{
			amount = str.split("\\.");
			amount[1] = amount[1].substring(0, 2);
		} catch (Exception e){
			System.out.println("Entered Integer");
			return str;
		}
		return amount[0]+"."+amount[1];
	}
	
	public void validation (final EditText text){
		if(text.getText().length()==0){
			text.setError("Please Complete Field");
		} else {
			text.setError(null,null);
		}
		text.addTextChangedListener(new TextWatcher() {	 
		   public void afterTextChanged(Editable s) {
			   if(text.getText().length()==0){
				   text.setError("Please Complete Field");
			   }else{
				   text.setError(null,null);
			   }
		   }
		 
		   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		 
		   public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
}
