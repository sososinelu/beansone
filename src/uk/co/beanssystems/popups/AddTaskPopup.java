package uk.co.beanssystems.popups;

import java.util.ArrayList;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.RatesConnector;
import uk.co.beanssystems.jobs.Rate;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddTaskPopup extends Activity {
	private EditText taskName;
	private TextView includesTimeTextView;
	private Button addTaskDeleteButton, addTaskButton;
	private Spinner rateSpinner, durationSpinner;
	Cursor ratesCur;
	private ArrayList<Rate> rates = new ArrayList<Rate>();
	ArrayList<String> rateSpinnerArray = new ArrayList<String>();
	ArrayList<String> durationSpinnerArray = new ArrayList<String>();
	private int childPos = -1;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slidein, R.anim.slideout);
		setContentView(R.layout.popup_new_job_add_task);
		taskName = (EditText) findViewById(R.id.taskNameAddTaskNewJobEditText);
		addTaskDeleteButton = (Button) findViewById(R.id.delAddTaskNewJobButton);
		addTaskButton = (Button) findViewById(R.id.okAddTaskNewJobButton);
		rateSpinner = (Spinner) findViewById(R.id.rateAddTaskSpinner); 
		durationSpinner = (Spinner) findViewById(R.id.durationAddTaskSpinner);
		includesTimeTextView = (TextView) findViewById(R.id.timeLabelAddWorkTextView);
		setDuration();
		getRates();
		
		Intent in = getIntent();
		if(in.getIntExtra("childPos", -1) >=0){
			taskName.setText(in.getStringExtra("taskName"));
			// Set duration spinner
			ArrayAdapter durAdap = (ArrayAdapter) durationSpinner.getAdapter(); // Cast to an ArrayAdapter
            int spinnerPosition = durAdap.getPosition(stripDecimal(String.valueOf(in.getDoubleExtra("duration", 0))));
            durationSpinner.setSelection(spinnerPosition); // Set the default according to value
            // Set rates spinner
            for(int j=0; j < rates.size(); j++){
				Rate rate = rates.get(j);
				if(in.getDoubleExtra("value", 0) == rate.rateValue){
					ArrayAdapter<String> rateAdap = (ArrayAdapter<String>) rateSpinner.getAdapter(); // Cast to an ArrayAdapter
		            int spinPosition = rateAdap.getPosition(rate.rateName+" �"+stripDecimal(String.valueOf(rate.rateValue)));
		            rateSpinner.setSelection(spinPosition); // Set the default according to value
		        }else{
		        	System.out.println("No Rate!");
		        }
			}
			childPos = in.getIntExtra("childPos", -1);
			addTaskDeleteButton.setVisibility(View.VISIBLE); 
		}
		
		rateSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		        if(rateSpinner.getSelectedItem().toString().equals("<Add Rate>")){
		        	Intent i = new Intent(AddTaskPopup.this, AddRatePopup.class);
		        	startActivityForResult(i, 0);
		        }else if(rateSpinner.getSelectedItem().toString().contains("Callout")){
		        	for(int k=0; k < rates.size(); k++){
						Rate rate = rates.get(k);
						if(rate.rateName.equals("Callout")){
							// Set time spinner
							ArrayAdapter<String> timeAdap = (ArrayAdapter<String>) durationSpinner.getAdapter(); // Cast to an ArrayAdapter
				            int spinPosition = timeAdap.getPosition(rate.rateTime);
				            durationSpinner.setSelection(spinPosition); // Set the default according to value
				            taskName.setText(rate.rateName); // Set work name
				            includesTimeTextView.setText("Includes Time"); // Set time label
				        }else{
				        	System.out.println("No Rate!");
				        }
					}
		        } else if (childPos==-1 || "Callout".equals(taskName.getText().toString())) {
		        	taskName.setText(""); // Set work name
		        	includesTimeTextView.setText("Select Time"); // Set time label
		        	durationSpinner.setSelection(0);
		        }
		    }
		    public void onNothingSelected(AdapterView<?> parentView) {
		    }
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();		
	}
	
	public void setDuration () {
		durationSpinnerArray.clear();
		for(int i=0; i < 12; i++){
			durationSpinnerArray.add(String.valueOf(i));
			durationSpinnerArray.add(String.valueOf(Double.valueOf(i)+0.5));
		}
		ArrayAdapter<String> spinnerArraAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, durationSpinnerArray);
		spinnerArraAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		durationSpinner.setAdapter(spinnerArraAdapter);
		durationSpinner.setOnTouchListener(spinnerTouchEvent);
	}
	
	public void getRates () {
		rates.clear();
		rateSpinnerArray.clear();
		RatesConnector ratesCon = new RatesConnector(this);
		ratesCon.open();
		try{
			ratesCur = ratesCon.getAllRatess();
			ratesCur.moveToFirst();
			int rateIDCol = ratesCur.getColumnIndex(RatesConnector.RATEID);
			int rateNameCol = ratesCur.getColumnIndex(RatesConnector.RATENAME);
			int rateValueCol = ratesCur.getColumnIndex(RatesConnector.RATEVALUE);
			int incTimeCol = ratesCur.getColumnIndex(RatesConnector.RATETIME);			
			do{		
				Rate rate = new Rate(ratesCur.getInt(rateIDCol), ratesCur.getString(rateNameCol), ratesCur.getDouble(rateValueCol), ratesCur.getString(incTimeCol)); 
				rates.add(rate);
				System.out.println("RATE ADDED");
				rateSpinnerArray.add(rate.rateName+" �"+stripDecimal(String.valueOf(rate.rateValue)));
			}while(ratesCur.moveToNext());
		} catch (Exception e) { 
	    	System.out.println("++++++No Rates+++++");
	    }
		rateSpinnerArray.add("<Add Rate>");
		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, rateSpinnerArray);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		rateSpinner.setAdapter(spinnerArrayAdapter);
		rateSpinner.setOnTouchListener(spinnerTouchEvent);
		ratesCon.close();
	}
	
	private View.OnTouchListener spinnerTouchEvent = new View.OnTouchListener() {
	    public boolean onTouch(View v, MotionEvent event) {
	        if (event.getAction() == MotionEvent.ACTION_UP) {
	        	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	        	imm.hideSoftInputFromWindow(rateSpinner.getWindowToken(), 0);
	        	imm.hideSoftInputFromWindow(durationSpinner.getWindowToken(), 0);
	        }
	        return false;
	    }
	};
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.okAddTaskNewJobButton:
			addTaskButton.setEnabled(false);
			if (taskName.getText().length()==0 || durationSpinner.getSelectedItem().toString() .equals("0")) {
				addTaskButton.setEnabled(true);
				validation(taskName);
				if(durationSpinner.getSelectedItem().toString() .equals("0")){
					Toast.makeText(this, "Select Duration!", Toast.LENGTH_SHORT).show();
				}
			} else {				
				Intent i = new Intent();
				String task = taskName.getText().toString();
				String dur = durationSpinner.getSelectedItem().toString();
				String charge = "charge";
				String valu = "";
				for(int j=0; j < rates.size(); j++){
					Rate rate = rates.get(j);
					if(rateSpinner.getSelectedItem().toString().equals(rate.rateName+" �"+stripDecimal(String.valueOf(rate.rateValue)))){
						valu = String.valueOf(rate.rateValue);
						System.out.println("<<<<<<<<<"+valu+">>>>>>>>>");
			        }else{
			        	System.out.println("No Rate!");
			        }
				}
				i.putExtra("taskName", task);
				i.putExtra("duration", dur);
				i.putExtra("chargeType", charge);
				i.putExtra("value", valu);
				i.putExtra("childPos", childPos);
			  	setResult(RESULT_OK, i);
			  	finish();
			  	overridePendingTransition(R.anim.slideout, R.anim.slideout);
			}
			break;
		case R.id.cancelAddTaskNewJobButton:
			finish();
			overridePendingTransition(R.anim.slideout, R.anim.slideout);
			break;
		case R.id.delAddTaskNewJobButton:
			Intent i = new Intent();
			i.putExtra("childPos", childPos);
			setResult(-2, i);
			finish();
			overridePendingTransition(R.anim.slideout, R.anim.slideout);
			break;
		case R.id.rateLabelAddWorkTextView:
			((Spinner) findViewById(R.id.rateAddTaskSpinner)).performClick();
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        	imm.hideSoftInputFromWindow(rateSpinner.getWindowToken(), 0);
			break;
		case R.id.timeLabelAddWorkTextView:
			((Spinner) findViewById(R.id.durationAddTaskSpinner)).performClick();
			InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        	im.hideSoftInputFromWindow(durationSpinner.getWindowToken(), 0);
			break;
		
		default:
			break;
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {		
		switch (requestCode){
		case 0:
			if (resultCode==RESULT_OK){
				getRates();
				try{
					String rateName = data.getStringExtra("rateName");
					double rateVal = data.getDoubleExtra("rateVal", 0);
					ArrayAdapter myAdap = (ArrayAdapter) rateSpinner.getAdapter(); // Cast to an ArrayAdapter
		            int spinnerPosition = myAdap.getPosition(rateName+" �"+stripDecimal(String.valueOf(rateVal)));
		            rateSpinner.setSelection(spinnerPosition); // Set the default according to value
				}catch(Exception ex){
					
				}
			}else{
				rateSpinner.setSelection(0);
			}
			break;
		default:
			break;
		}
	}
	
	public String stripDecimal(String str){
		String[] amount =str.split("\\.");
		try{
			amount[1] = amount[1].substring(0, 2);
		} catch (Exception e){

		}
		if(amount[1].equals("0")){
			System.out.println("Entered Integer: "+amount[0]);
			return amount[0];
		}else{
			return amount[0]+"."+amount[1];
		}
	}
	
	public void validation (final EditText text){
		if(text.getText().length()==0){
			text.setError("Please Complete Field");
		} else {
			text.setError(null,null);
		}
		text.addTextChangedListener(new TextWatcher() {	 
		   public void afterTextChanged(Editable s) {
			   if(text.getText().length()==0){
				   text.setError("Please Complete Field");
			   }else{
				   text.setError(null,null);
			   }
		   }
		 
		   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		 
		   public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
}
