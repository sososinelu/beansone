package uk.co.beanssystems.popups;

public class Category {
	public int rowType, expenseID;
	public String category;
	
	public Category () {
		super();
	}

	public Category (int rowType, int expenseID, String category) {
		super();
		this.expenseID = expenseID;
		this.rowType = rowType;
		this.category = category;
	}
}
