package uk.co.beanssystems.popups;

import uk.co.beanssystems.beansone.R;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class FullscreenImage extends SherlockActivity {
	ImageView image;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_image);
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(true);
        
	    bar.setDisplayHomeAsUpEnabled(true);
		image = (ImageView) findViewById(R.id.fullscreenImage);
		if(getIntent().getIntExtra("from", 0) == 1){
			bar.setLogo(R.drawable.jobs);
		}else if ((getIntent().getIntExtra("from", 0) == 2)){
			bar.setLogo(R.drawable.coinstack);
		}
		String targetPath = getIntent().getStringExtra("path");
		image.setImageURI(Uri.parse(targetPath));
		image.refreshDrawableState();
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case android.R.id.home:
	        finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}     
}
