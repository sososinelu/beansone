package uk.co.beanssystems.settings;

import java.text.SimpleDateFormat;


import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.AccountConnector;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.popups.SyncPopup;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class Settings extends SherlockActivity {
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String DBNAME = "dbName";
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
	    
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.settings);
	    bar.setDisplayHomeAsUpEnabled(true);
	}
	
	public void onClick(View v){
		switch(v.getId()){
		case R.id.userProfileSettingsImageView:
			Intent inUser = new Intent(this, MyDetails.class);
			startActivity(inUser);
			break;
		case R.id.financeSettingsImageView:
//			Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
			Intent inFinance = new Intent(this, FinanceSettings.class);
			startActivity(inFinance);
			break;
		case R.id.syncSettingsImageView:
			//Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
			Intent inSync = new Intent(this, SyncSettings.class);
			startActivity(inSync);
			break;
		case R.id.expensesSettingsImageView:
//			Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
			Intent inExp = new Intent(this, ExpensesSettings.class);
			startActivity(inExp);
			break;
		case R.id.syncNowSettingsImageView:
			AccountConnector accCon = new AccountConnector(this);
			String dbName = accCon.getDatabase();
			if ("".equals(dbName) || dbName==null){
				Toast.makeText(this, "You must create an account first", Toast.LENGTH_SHORT).show();
				Intent in = new Intent(this, UserAccount.class);
				startActivity(in);
			} else {
				if (isConnected()){
					Intent inSyncPop = new Intent(this, SyncPopup.class);
					startActivity(inSyncPop);
				}
			}
			break;
		case R.id.helpSettingsImageView:
			Intent inHelp = new Intent(this, Help.class);
			startActivity(inHelp);
			break;
		default:break;
		}
	}	
	public boolean isConnected(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
		        return true;
		    }
		    Toast.makeText(this, "You must have a network connection to do this", Toast.LENGTH_LONG).show();
		    return false;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    menu.removeItem(R.id.search_option);
	    menu.removeItem(R.id.settings_option);
	    menu.removeItem(R.id.menuSort);
	    return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	        finish();
	    	return true;
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
}
