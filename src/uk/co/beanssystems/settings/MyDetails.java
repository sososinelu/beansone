package uk.co.beanssystems.settings;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.clients.ClientList;
import uk.co.beanssystems.clients.ViewClient;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.help.Help;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MyDetails extends SherlockActivity {
	EditText nameEditText, numberEditText, companyEditText, addressEditText, postcodeEditText, emailEditText;
	String name; 
	Cursor settCur;
	SettingsConnector settCon;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_my_details);
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.work_user);
	    bar.setDisplayHomeAsUpEnabled(true);
	    
	    settCon= new SettingsConnector(this);
	    nameEditText = (EditText) findViewById(R.id.nameMyDetailsEditText);
		numberEditText = (EditText) findViewById(R.id.numberMyDetailsEditText);
		companyEditText = (EditText) findViewById(R.id.companyMyDetailsEditText);
		addressEditText = (EditText) findViewById(R.id.addressMyDetailsEditText);
		postcodeEditText = (EditText) findViewById(R.id.postcodeMyDetailsEditText);
		emailEditText = (EditText) findViewById(R.id.emailMyDetailsEditText);
	    
		loadMyDetails();
	}
	
	public void loadMyDetails () {
		try{		
			settCon.open();
			settCur = settCon.getUserDetails();		
			settCur.moveToFirst();
			int userNameCol = settCur.getColumnIndex(SettingsConnector.USERNAME);
			int numberCol = settCur.getColumnIndex(SettingsConnector.CONTACTNUM);
			int companyCol = settCur.getColumnIndex(SettingsConnector.USERCOMPANY);
			int addressCol = settCur.getColumnIndex(SettingsConnector.ADDRESS);
			int postcodeCol = settCur.getColumnIndex(SettingsConnector.POSTCODE);
			int emailCol = settCur.getColumnIndex(SettingsConnector.EMAIL);
			
			nameEditText.setText(settCur.getString(userNameCol));
			numberEditText.setText(settCur.getString(numberCol));
			companyEditText.setText(settCur.getString(companyCol));
			addressEditText.setText(settCur.getString(addressCol));
			postcodeEditText.setText(settCur.getString(postcodeCol));
			emailEditText.setText(settCur.getString(emailCol));
			settCon.close();
		} catch (Exception e) { 
        	e.printStackTrace();
        }
		
	}
	
	public void saveMyDetails () {
		try{
			settCon.updateUserDetails(1, nameEditText.getText().toString(),  numberEditText.getText().toString(), companyEditText.getText().toString(), addressEditText.getText().toString(), postcodeEditText.getText().toString(), emailEditText.getText().toString());
		} catch (Exception e) { 
			e.printStackTrace();
        }
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.add_option:
			item.setEnabled(false);
	    	if (emailEditText.getText().length()!=0){
	    		String e = emailEditText.getText().toString();
	    		if (!e.contains("@") || !e.contains(".")){
	    			item.setEnabled(true);
	    			emailEditText.setError("Invalid Email Address");
		    		return false;
	    		}
	    	}   	  	    	  	    	  	    	  
	    	  
	    	if (nameEditText.getText().length()==0){
	    		item.setEnabled(true);
	    		validation(nameEditText);
	    		return false;
	      	}else{
	      		saveMyDetails();
	      		finish();
	      		return true;
	      	}
	    case android.R.id.home:
	    	final Builder builder = new AlertDialog.Builder(this);			
			 TextView title = new TextView(this);
			 title.setText("My details not saved!");
			 title.setPadding(10, 10, 10, 10);
			 title.setGravity(Gravity.CENTER);
			 //title.setTextColor(getResources().getColor(R.color.greenBG));
			 title.setTextSize(20);
			 builder.setCustomTitle(title);
			 TextView msg = new TextView(this);
			 msg.setText("Are you sure you want to go back?");
			 msg.setPadding(10, 10, 10, 10);
			 msg.setGravity(Gravity.CENTER);
			 msg.setTextSize(18);
			 builder.setView(msg);
			 builder.setCancelable(true);
			 builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				 public void onClick(DialogInterface dialog, int which) {
			         finish();
				 }
			 });
			 builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				 public void onClick(DialogInterface dialog, int which) {
				 }
			 });
			 AlertDialog dialog = builder.create();
			 if (nameEditText.getText().length()!=0 || numberEditText.getText().length()!=0 || companyEditText.getText().length()!=0 || 
					 addressEditText.getText().length()!=0 || postcodeEditText.getText().length()!=0 || emailEditText.getText().length()!=0){
				 dialog.show();
			 }else{
				 finish();
			 }
	    	return true;
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_options, menu);
		MenuItem it = menu.findItem(R.id.add_option);
		it.setTitle("Save");
	    return true;
	}
	
	public void validation (final EditText text){
		if(text.getText().length()==0){
			text.setError("Please Complete Field");
		} else {
			text.setError(null,null);
		}
		text.addTextChangedListener(new TextWatcher() {	 
		   public void afterTextChanged(Editable s) {
			   if(text.getText().length()==0){
				   text.setError("Please Complete Field");
			   }else{
				   text.setError(null,null);
			   }
		   }
		 
		   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		 
		   public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
}
