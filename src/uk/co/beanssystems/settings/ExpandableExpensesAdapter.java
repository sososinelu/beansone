package uk.co.beanssystems.settings;

import java.util.ArrayList;

import uk.co.beanssystems.beansone.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpandableExpensesAdapter extends BaseExpandableListAdapter {
	private Context context;
	private ArrayList<ParentInfo> expandList;
	String text;
	
	public ExpandableExpensesAdapter(Context context, ArrayList<ParentInfo> expandList) {
		this.context = context;
		this.expandList = expandList;
	}
	
	public Object getChild(int groupPosition, int childPosition) {
		ArrayList<ChildInfo> list = expandList.get(groupPosition).getHeaderList();
		return list.get(childPosition);
	}
	
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}
	
	public Object removeChild(int groupPosition, int childPosition) {
		ArrayList<ChildInfo> list = expandList.get(groupPosition).getHeaderList();
		return list.remove(childPosition);
	}
	 
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup parent) {	   
		ChildInfo childInfo = (ChildInfo) getChild(groupPosition, childPosition);
		
		//Inflate new view
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.child_expense, null);
		}
		
		TextView childItem = (TextView) view.findViewById(R.id.childExpenseTextView);
		childItem.setText(childInfo.getName().trim());

		ImageView delImg = (ImageView) view.findViewById(R.id.deleteCross);
		if (childInfo.getFixed()==0){
			delImg.setVisibility(View.VISIBLE);
			delImg.setTag(childInfo);
		} else {
			delImg.setVisibility(View.INVISIBLE);
		}
		
		ImageView editImg = (ImageView) view.findViewById(R.id.editPencil);
		editImg.setTag(childInfo);
		return view;
	}
	
	public int getChildrenCount(int groupPosition) {
		ArrayList<ChildInfo> list = expandList.get(groupPosition).getHeaderList();
		return list.size();
	}
	
	public Object getGroup(int groupPosition) {
		return expandList.get(groupPosition);
	}
	
	public Object removeGroup(int groupPosition) {
		return expandList.remove(groupPosition);
	}
	 
	public int getGroupCount() {
		return expandList.size();
	}
	 
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}
	 
	public View getGroupView(int groupPosition, boolean isLastChild, View view, ViewGroup parent) {	   
		ParentInfo parentInfo = (ParentInfo) getGroup(groupPosition);
		
		//Inflate new view
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.parent_expense, null);
		}
		
		//Set Views
		TextView heading = (TextView) view.findViewById(R.id.parentExpenseTextView);
		heading.setText(parentInfo.getName().trim());
		
		ImageView addImg = (ImageView) view.findViewById(R.id.addExpenseSettingsImageView);
		addImg.setTag(parentInfo);
	    return view;
	}
	 
	public boolean hasStableIds() {
		return true;
	}
	 
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
