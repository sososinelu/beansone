package uk.co.beanssystems.settings;

public class ChildInfo {
	private int id;
	private String name = "";
	private int childPosition = 0; 
	private int parentPosition = 0; 
	private int fixed = 0;
	
	public int getID(){
		return id;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setChildPosition(int pos){
		this.childPosition = pos;
	}
	
	public int getChildPosition(){
		return childPosition;
	}
	
	public void setParentPosition(int pos){
		this.parentPosition = pos;
	}
	
	public int getParentPosition(){
		return parentPosition;
	}
	
	public void setFixed(int fix){
		this.fixed = fix;
	}
	
	public int getFixed(){
		return fixed;
	}
}
