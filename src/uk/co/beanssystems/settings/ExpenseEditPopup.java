package uk.co.beanssystems.settings;

import uk.co.beanssystems.beansone.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockActivity;

public class ExpenseEditPopup extends SherlockActivity {
	private EditText name;
	private int parentID, childID, group, source;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_settings_expense_edit_popup);
		name = (EditText)findViewById(R.id.expenseNameSettingsEditText);
		Intent i = getIntent();

		source = i.getIntExtra("source", 0);
		if (source==0){
			name.setText(i.getStringExtra("name"));
			childID = i.getIntExtra("childID", 0);
			group = i.getIntExtra("group", 0);
		} else {
			parentID = i.getIntExtra("parentID", 0);
			group = i.getIntExtra("group", 0);
		}
		
	}
	
	public void onClick(View v){
		switch (v.getId()){
		case R.id.okExpenseEditButton:
			Intent i = new Intent(this, ExpensesSettings.class);
			if (source==0){
				i.putExtra("source", source);
				i.putExtra("childID", childID);
				i.putExtra("nameReturned", name.getText().toString());
				i.putExtra("group", group);
			} else {
				i.putExtra("source", source);
				i.putExtra("parentID", parentID);
				i.putExtra("nameReturned", name.getText().toString());
				i.putExtra("group", group);
			}
			setResult(RESULT_OK, i);
			finish();
			break;
		case R.id.cancelExpenseEditButton:
			setResult(RESULT_CANCELED);
			finish();
			break;
		default:break;
		}
	}
}
