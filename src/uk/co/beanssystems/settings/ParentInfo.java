package uk.co.beanssystems.settings;

import java.util.ArrayList;

public class ParentInfo {
	private int id;
	private String name;
	private int position=0;
	private ArrayList<ChildInfo> parentList = new ArrayList<ChildInfo>();;
	
	public int getID() {
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int pos) {
		this.position = pos;
	}
	
	public ArrayList<ChildInfo> getHeaderList() {
		return parentList;
	}
	
	public void setHeaderList(ArrayList<ChildInfo> headerList) {
		this.parentList = headerList;
	}
}
