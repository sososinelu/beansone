package uk.co.beanssystems.settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.AccountConnector;
import uk.co.beanssystems.database.MasterConnector;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.popups.AddTaskPopup;
import uk.co.beanssystems.popups.SyncPopup;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class SyncSettings extends SherlockActivity {
	private static final String BACKUPTYPE = "backupType", BACKUPPERIOD = "backupPeriod", LASTBACKUP = "lastBackup";
	private String lastBackup = "", username;
	Spinner intervalSpinner;
	ToggleButton wifiOnlyToggle;
	TextView lastSyncTextView;
	Cursor settCur;
	SettingsConnector settCon;
	Context context;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_sync);
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.sync);
	    bar.setDisplayHomeAsUpEnabled(true);
	    context = this;
	    settCon = new SettingsConnector(this);
	    intervalSpinner = (Spinner) findViewById(R.id.intervalSyncSpinner);
	    wifiOnlyToggle = (ToggleButton) findViewById(R.id.wifiOnlySyncToggle);
	    lastSyncTextView = (TextView) findViewById(R.id.lastSyncLabelSyncTextView);
	    
	    loadSyncDetails();
	}
	
	public void loadSyncDetails () {
		try{
			settCon.open();
			settCur = settCon.getSync();
			settCur.moveToFirst();
			int intervalCol = settCur.getColumnIndex(BACKUPPERIOD);
			int wifiOnlyCol = settCur.getColumnIndex(BACKUPTYPE);
			
			ArrayAdapter myAdap = (ArrayAdapter) intervalSpinner.getAdapter(); // Cast to an ArrayAdapter
	        int spinnerPosition = myAdap.getPosition(settCur.getString(intervalCol));
	        intervalSpinner.setSelection(spinnerPosition);
	        if(settCur.getString(wifiOnlyCol).equals("yes")){
	        	wifiOnlyToggle.setChecked(true);
	        }else{
	        	wifiOnlyToggle.setChecked(false);
	        }
			AccountConnector accCon = new AccountConnector(this);
	        lastBackup = accCon.getLastSync();
	        lastSyncTextView.setText(lastBackup);
		} catch (Exception e) { 
			e.printStackTrace();
        }
	}
	public boolean isConnected(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
		        return true;
		    }
		    Toast.makeText(this, "You must have a network connection to do this", Toast.LENGTH_LONG).show();
		    return false;
	}
	public void setSyncDetails () {
		try{
			String state = "";
			if(wifiOnlyToggle.isChecked() == true){
				state = "yes";
			}else{
				state = "no";
			}
			settCon.updateSync(state, intervalSpinner.getSelectedItem().toString());
		} catch (Exception e) { 
			e.printStackTrace();
        }
	}
	
	public void onClick(View v){
		switch(v.getId()){
		case R.id.yourAccSyncImageButton:
			Intent in = new Intent(this, UserAccount.class);
			startActivity(in);
			break;
			
		case R.id.restoreSyncImageButton:
			AccountConnector accCon2 = new AccountConnector(this);
			String dbName2 = accCon2.getDatabase();
			username = accCon2.getUsername();
			if ("".equals(dbName2) || dbName2==null){
				Toast.makeText(this, "You must login to your account first", Toast.LENGTH_SHORT).show();
				Intent inAcc = new Intent(this, UserAccount.class);
				startActivity(inAcc);
			} else {			
				if (isConnected()){
					 AlertDialog.Builder builder = new AlertDialog.Builder(this);
					 builder.setTitle("Warning!");
					 builder.setMessage("This feature will restore all data since the last backup associated with this account." +
						 		"All subsequent data will be lost! Proceed?");
					 builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				             public void onClick(DialogInterface dialog, int which) {
				            	 RestoreDataTask task = new RestoreDataTask();
				            	 task.execute(null,null,null);
				             }
				     });
					 builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			             public void onClick(DialogInterface dialog, int which) {
		
			             }
					 });
		             AlertDialog alert = builder.create();
		             alert.show();
		             TextView messageView = (TextView)alert.findViewById(android.R.id.message);
		             messageView.setGravity(Gravity.CENTER);
				}
			}
			break;
			
		case R.id.syncNowSyncImageButton:
			if  (isConnected()){
				AccountConnector accCon = new AccountConnector(this);
				String dbName = accCon.getDatabase();
				if ("".equals(dbName) || dbName==null){
					Toast.makeText(this, "You must create an account first", Toast.LENGTH_SHORT).show();
					Intent inAcc = new Intent(this, UserAccount.class);
					startActivity(inAcc);
				} else {
					Intent inSyncPop = new Intent(this, SyncPopup.class);
					startActivity(inSyncPop);
				}
			}
			break;
		default:break;
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    menu.removeItem(R.id.search_option);
	    menu.removeItem(R.id.settings_option);
	    menu.removeItem(R.id.menuSort);
	    return true;
	}
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	    	setSyncDetails();
	        finish();
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	public class RestoreDataTask extends AsyncTask<String, Integer, JSONObject>{
		 ProgressDialog prog;
		@Override
		protected JSONObject doInBackground(String... params) {
			Context context = getBaseContext();
			JSONObject jObj = new JSONObject();
			AccountConnector accCon = new AccountConnector(context);
			String dbName = accCon.getDatabase();
			
			JSONObject job = new JSONObject();
			try {
				job.put("database", dbName);
				job.put("DBVersion", "1.0");
				job.put("username", username);
				publishProgress(40);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			String result= "";
			HttpClient httpClient = new DefaultHttpClient();
	        HttpPost httpPost = new HttpPost("http://109.235.145.88/scripts/restore.php");
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("json", job.toString()));
	        try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
		        httpClient.execute(httpPost);
		        HttpResponse response = httpClient.execute(httpPost);
		        publishProgress(80);
		        HttpEntity entity = response.getEntity();
		        InputStream is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        result=sb.toString();
	            jObj = new JSONObject(result);
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				Log.i("store.php Result:",result);
	            Log.e("JSON Parser", "Error parsing data " + e.toString());
	        }  
			return jObj;
		}
		@Override
		protected void onPreExecute() {
			    prog = new ProgressDialog(context);
			    prog.setTitle("Restore");
			    prog.setMessage("Please wait as your data is being restored");       
			    prog.setIndeterminate(false);
			    prog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			    prog.show();
		}
		
		protected void onProgressUpdate(Integer... values) {
		        prog.setProgress(values[0]);
		}
		
		protected void onPostExecute(JSONObject result){
			int code=0;
			String msg="Restore Error";
			MasterConnector mCon = new MasterConnector(context);
			JSONObject tables = new JSONObject();
			try{
				code = Integer.parseInt(result.getString("code"));
				msg = result.getString("msg");
				tables = result.getJSONObject("commands");
				mCon.emptyTables();
				mCon.restoreDataFast(tables);
				publishProgress(100);
				prog.dismiss();
			} catch (JSONException e){
				Log.i("onPostExecute", "json parsing error");
			}
		}
	}	
}
