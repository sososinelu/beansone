package uk.co.beanssystems.settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.beansone.R.color;
import uk.co.beanssystems.database.AccountConnector;
import uk.co.beanssystems.database.MasterConnector;
import uk.co.beanssystems.database.ServerCommands;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.popups.ChangePasswordPopup;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class UserAccount extends SherlockActivity {
	private static final String DBNAME = "dbName", EMAIL = "email", LASTBACKUP = "lastBackup";
	EditText username, password;
	TextView lastBackup, info, title;
	Button createAccount, loginAccount, changePassword, logout, resetPassword;
	String dbNameReturned = "", errorMsg;
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private Context context;
	private Pattern pattern, emailPat;
	private Matcher matcher;
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z]).{6,20})";
	private static final String EMAIL_PATTERN = "([\\w-\\.]+)@((?:[\\w]+\\.)+)([a-zA-Z]{2,4})";
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_account);
		
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.settings);
	    bar.setDisplayHomeAsUpEnabled(true);
	    context = this;
		username = (EditText) findViewById(R.id.usernameEditTextUserAccount);
		password = (EditText) findViewById(R.id.passwordEditTextUserAccount);
		lastBackup = (TextView) findViewById(R.id.lastBackupEditTextUserAccount);
		info = (TextView) findViewById(R.id.infoUserAccountTextView);
		title = (TextView) findViewById(R.id.logoTitleUserAccountTextView);
		createAccount = (Button) findViewById(R.id.createAccountButton);
		loginAccount = (Button) findViewById(R.id.loginAccountButton);
		changePassword = (Button) findViewById(R.id.changePasswordAccountButton);
		logout = (Button) findViewById(R.id.logoutAccountButton);
		resetPassword = (Button) findViewById(R.id.resetPasswordAccountButton);
		refresh();
	}
	
	public boolean isConnected(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
		        return true;
		    }
		    Toast.makeText(this, "You must have a network connection to do this", Toast.LENGTH_LONG).show();
		    return false;
	}
	
	public void refresh(){
		AccountConnector accCon = new AccountConnector(this);
		String dbName = "";
		dbName = accCon.getDatabase();
		String lastBackupDate = accCon.getLastSync();
		
		Date date = new Date();
		try {
			date = df.parse(lastBackupDate);
		} catch (Exception e) {
			lastBackupDate = "";
		}
		
		if (!"".equals(dbName)){		//if Logged In
			username.setText(accCon.getUsername());
			username.setFocusable(false);
			username.setFocusableInTouchMode(false);
			password.setVisibility(View.INVISIBLE);
			info.setText("Here you can manage your account using the options below");
			title.setText("Your Backup Account");
			createAccount.setEnabled(false);
			createAccount.setVisibility(View.INVISIBLE);
			loginAccount.setEnabled(false);
			loginAccount.setVisibility(View.INVISIBLE);
			changePassword.setEnabled(true);
			changePassword.setVisibility(View.VISIBLE);
			resetPassword.setEnabled(false);
			resetPassword.setVisibility(View.INVISIBLE);
			logout.setEnabled(true);
			logout.setVisibility(View.VISIBLE);
			lastBackup.setText("");
			lastBackup.setVisibility(View.INVISIBLE);
		} else {			//if Logged Out
			SettingsConnector setCon = new SettingsConnector(this);
			setCon.open();
			Cursor cur = setCon.getAllSettings();
			cur.moveToFirst();
			int emailCol = cur.getColumnIndex(EMAIL);
			username.setText(cur.getString(emailCol));
			setCon.close();
			username.setFocusable(true);
			username.setFocusableInTouchMode(true);
			password.setText("");
			password.setVisibility(View.VISIBLE);
			info.setText(getResources().getString(R.string.info_account));
			title.setText("Your Backup Account");
			createAccount.setEnabled(true);
			createAccount.setVisibility(View.VISIBLE);
			loginAccount.setEnabled(true);
			loginAccount.setVisibility(View.VISIBLE);
			resetPassword.setEnabled(true);
			resetPassword.setVisibility(View.VISIBLE);
			changePassword.setEnabled(false);
			changePassword.setVisibility(View.INVISIBLE);
			logout.setEnabled(false);
			logout.setVisibility(View.INVISIBLE);
			lastBackup.setText("");
		}
	}
	
	public boolean validate(){
		errorMsg="";
		if (username.getText().length()==0 || password.getText().length()==0){
			lastBackup.setVisibility(View.VISIBLE);
			lastBackup.setText("Please enter Email and Password");
			return false;
		}
		String e = username.getText().toString();
		emailPat = Pattern.compile(EMAIL_PATTERN);
		matcher = emailPat.matcher(e);
		if (!matcher.matches()){
			lastBackup.setVisibility(View.VISIBLE);
			lastBackup.setText("Invalid email address");
			return false;
		}
		
		pattern = Pattern.compile(PASSWORD_PATTERN);
		String p = password.getText().toString();
		matcher = pattern.matcher(p);
		if (!matcher.matches()){
			lastBackup.setVisibility(View.VISIBLE);
			lastBackup.setText("Invalid Password\nMust be 6-20 characters containing both numbers and letters");
			return false;
		}
		return true;
	}
	
	public void onClick(View v){
		switch (v.getId()){
		case R.id.createAccountButton:
			if (isConnected()){
				createAccount.setEnabled(false);
				if (validate()){
					JSONObject request = new JSONObject();
					JSONArray commands = new JSONArray();
					for (String s : ServerCommands.commands){
						commands.put(s);
					}
					try{
						request.put("email", username.getText().toString());
						request.put("pass", password.getText().toString());
						request.put("date", df.format(new Date()));
						request.put("dbStruct", commands);
					} catch (JSONException e){
						e.printStackTrace();
					}
					CreateAccountTask task = new CreateAccountTask();
					task.execute(new String[] {"http://109.235.145.88/scripts/create.php", request.toString()});
				}
			}
			createAccount.setEnabled(true);
			break;
		case R.id.loginAccountButton:
			if (isConnected()){
				if (validate()){
					JSONObject request = new JSONObject();
					try{
						request.put("email", username.getText().toString());
						request.put("pass", password.getText().toString());
						request.put("date", df.format(new Date()));
					} catch (JSONException e){
						e.printStackTrace();
					}
					LoginAccountTask task = new LoginAccountTask();
					task.execute(new String[] {"http://109.235.145.88/scripts/login.php", request.toString()});
				}
			}
			break;
		case R.id.changePasswordAccountButton:
			if (isConnected()){
				Intent i = new Intent(this, ChangePasswordPopup.class);
				startActivity(i);
			}
			break;
		case R.id.logoutAccountButton:
			final Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Logout?");
			builder.setMessage("Are you sure you wish to logout of this account?");
			builder.setCancelable(true);
			builder.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							AccountConnector accCon = new AccountConnector(getApplicationContext());
							accCon.delete();
							refresh();
						}
					});
			builder.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
			break;
		
		case R.id.resetPasswordAccountButton:
			if (isConnected()){
				if (username.getText().length()==0){
					lastBackup.setVisibility(View.VISIBLE);
					lastBackup.setText("Please enter Email of account you wish to reset");
					break;
				}
				String e = username.getText().toString();
				if (!e.contains("@") || !e.contains(".") || e.length()<6){
					lastBackup.setVisibility(View.VISIBLE);
					lastBackup.setText("Invalid Email Address");
					break;
				}
				
				final Builder builder2 = new AlertDialog.Builder(this);
				builder2.setTitle("Reset Password?");
				builder2.setMessage("A reset link will be sent to this account:\n\n"+username.getText().toString());
				builder2.setCancelable(true);
				builder2.setPositiveButton("Continue",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								JSONObject json = new JSONObject();
								try {
									json.put("username", username.getText().toString());
								} catch (JSONException e){
									
								}						
								ResetPasswordTask task = new ResetPasswordTask();
								task.execute("http://109.235.145.88/scripts/sendReset.php", json.toString());
							}
						});
				builder2.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
							
							}
						});
				AlertDialog dialog2 = builder2.create();
				dialog2.show();
			}
			break;
		default:break;
		}
		
			
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    menu.removeItem(R.id.search_option);
	    menu.removeItem(R.id.settings_option);
	    menu.removeItem(R.id.menuSort);
	    return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		    case android.R.id.home:
		        finish();
		    	return true;
		    case R.id.help_option:	
		    	Intent ih = new Intent(this, Help.class);
		    	startActivity(ih);
		    	return true;
		    case R.id.home_option:
		    	Intent iHome = new Intent(this, MainActivity.class);
		    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    	startActivity(iHome);
		    	finish();
		    	return true;
		    default:
		    	return super.onOptionsItemSelected(item);
	    }
	}
	
	private class CreateAccountTask extends AsyncTask<String, Void, JSONObject>{
		@Override
		protected JSONObject doInBackground(String... params) {
			String result = "";
			JSONObject request;
			JSONObject resp = new JSONObject();
			try {
				request = new JSONObject(params[1]);
				HttpClient httpClient = new DefaultHttpClient();
		        HttpPost httpPost = new HttpPost(params[0]);
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("json", request.toString()));
		        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
		        HttpResponse response = httpClient.execute(httpPost);
		        HttpEntity entity = response.getEntity();
		        InputStream is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        result=sb.toString();
		        Log.i("Result String", result);
		        resp = new JSONObject(result);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return resp;
		}
		@Override
	    protected void onPostExecute(JSONObject result) {
			String code="0", dbName="";
			try {
				code = result.getString("code");
				dbName = result.getString("dbName");
			} catch (JSONException e){
				e.printStackTrace();
			}
			if (code.equals("1")){
				AccountConnector accCon = new AccountConnector(getApplicationContext());
				accCon.insertAccount(username.getText().toString(), password.getText().toString(), "", dbName, "1.0");
				
			    dbNameReturned = dbName;
				Toast.makeText(getApplicationContext(), "Account Created, You can now Sync!", Toast.LENGTH_SHORT).show();
				finish();
			} else {
				lastBackup.setVisibility(View.VISIBLE);
				lastBackup.setText("Email already in use\nTry pressing Login below, if this is your account");
			}
			createAccount.setEnabled(true);
	    }
	}
	
	private class LoginAccountTask extends AsyncTask<String, Void, JSONObject>{
		@Override
		protected JSONObject doInBackground(String... params) {
			String result = "";
			JSONObject request;
			JSONObject resp = new JSONObject();
			try {
				request = new JSONObject(params[1]);
				HttpClient httpClient = new DefaultHttpClient();
		        HttpPost httpPost = new HttpPost(params[0]);
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("json", request.toString()));
		        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
		        HttpResponse response = httpClient.execute(httpPost);
		        HttpEntity entity = response.getEntity();
		        InputStream is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        result=sb.toString();
		        Log.i("Login Result", result);
		        resp = new JSONObject(result);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return resp;
		}
		
		@Override
	    protected void onPostExecute(JSONObject result) {
			String code="0", msg="", createdDate="", dbName="", dbVersion="";
			try {
				code = result.getString("code");
				msg = result.getString("msg");
				if ("1".equals(code)){
					createdDate = result.getString("createdDate");
					dbName = result.getString("dbName");
					dbVersion = result.getString("dbVersion");
					AccountConnector accCon = new AccountConnector(getApplicationContext());
					accCon.delete();
					accCon.insertAccount(username.getText().toString(), password.getText().toString(), "", dbName, dbVersion);
					Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
					refresh();
					final Builder builder = new AlertDialog.Builder(context);
					builder.setTitle("Restore");
					builder.setMessage("It is highly recommended you restore from your online backup before continuing. This will overwrite the data currently on the phone with your last backup!");
					builder.setCancelable(true);
					builder.setPositiveButton("Restore",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									if (isConnected()){
										RestoreDataTask task = new RestoreDataTask();
						            	task.execute(null,null,null);
									}
								}		
							});
					builder.setNegativeButton("Skip",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									
								}
							});
					AlertDialog dialog = builder.create();
					dialog.show();
				} else {
					Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e){
				Toast.makeText(getApplicationContext(), "Login Failure", Toast.LENGTH_SHORT).show();
			}
		
	    }
	}
	
	private class ResetPasswordTask extends AsyncTask<String, Void, JSONObject>{
		@Override
		protected JSONObject doInBackground(String... params) {
			String result = "";
			JSONObject request;
			JSONObject resp = new JSONObject();
			try {
				request = new JSONObject(params[1]);
				HttpClient httpClient = new DefaultHttpClient();
		        HttpPost httpPost = new HttpPost(params[0]);
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("json", request.toString()));
		        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
		        HttpResponse response = httpClient.execute(httpPost);
		        HttpEntity entity = response.getEntity();
		        InputStream is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        result=sb.toString();
		        Log.i("returned", result);
		        resp = new JSONObject(result);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return resp;
		}
		
		@Override
	    protected void onPostExecute(JSONObject result) {
			String msg = "Error";
			try {
				msg = result.getString("msg");
			} catch (Exception e){
				
			}
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	    }
	}
	private class RestoreDataTask extends AsyncTask<String, Void, JSONObject>{

		@Override
		protected JSONObject doInBackground(String... params) {
			Context context = getBaseContext();
			JSONObject jObj = new JSONObject();
			AccountConnector accCon = new AccountConnector(context);
			String dbName = accCon.getDatabase();
			
			JSONObject job = new JSONObject();
			try {
				job.put("database", dbName);
				job.put("DBVersion", "1.0");
				job.put("username", username);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			String result= "";
			HttpClient httpClient = new DefaultHttpClient();
	        HttpPost httpPost = new HttpPost("http://109.235.145.88/scripts/restore.php");
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("json", job.toString()));
	        try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
		        httpClient.execute(httpPost);
		        HttpResponse response = httpClient.execute(httpPost);
		        HttpEntity entity = response.getEntity();
		        InputStream is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        result=sb.toString();
	            jObj = new JSONObject(result);
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				Log.i("store.php Result:",result);
	            Log.e("JSON Parser", "Error parsing data " + e.toString());
	        }  
			return jObj;
		}
		
		protected void onPostExecute(JSONObject result){
			int code=0;
			String msg="Restore Error";
			MasterConnector mCon = new MasterConnector(getApplicationContext());
			JSONObject tables = new JSONObject();
			try{
				code = Integer.parseInt(result.getString("code"));
				msg = result.getString("msg");
				tables = result.getJSONObject("commands");
				mCon.emptyTables();
				mCon.restoreDataFast(tables);
			} catch (JSONException e){
				Log.i("onPostExecute", "json parsing error");
			}
			//Log.i("Tables", tables.toString());
//			Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
		}
	}	
}