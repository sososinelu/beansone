package uk.co.beanssystems.settings;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.RatesConnector;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.jobs.Rate;
import uk.co.beanssystems.popups.AddRatePopup;
import uk.co.beanssystems.popups.AddTaskPopup;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class FinanceSettings extends SherlockActivity {
	private EditText vatNum, vatRate;
	private Spinner currency;
	private static final String VATNUM = "vatNum", VATRATE = "vatRate", CURRENCY = "currency";
	public static final String RATEID = "rateID", RATENAME = "rateName", RATEVALUE = "rateValue", 
			RATEVAT = "rateVAT", INCVAT = "incVAT", RATETIME = "rateTime";
	LayoutInflater inflater;
	LinearLayout linear;
	RelativeLayout inflatedView;
	View main;
	NumberFormat nf;
	private ArrayList<Rate> rates = new ArrayList<Rate>();
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		main = inflater.inflate(R.layout.activity_settings_finance, null);
		setContentView(main);	
		nf = NumberFormat.getCurrencyInstance(Locale.UK);
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.settings);
	    bar.setDisplayHomeAsUpEnabled(true);
	    
	    vatNum = (EditText)findViewById(R.id.vatNumberSettingsEditText);
	    vatRate = (EditText)findViewById(R.id.vatRateSettingsEditText);
	    loadFinance();
	    loadWorkRates();
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	    	updateFinance();
	        finish();
	    	return true;
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    menu.removeItem(R.id.search_option);
	    menu.removeItem(R.id.settings_option);
	    menu.removeItem(R.id.menuSort);
	    return true;
	}
	
	public void loadFinance(){
		SettingsConnector setCon = new SettingsConnector(this);
		setCon.open();
		Cursor cur = setCon.getFinance();
		cur.moveToFirst();
		int vatNumCol = cur.getColumnIndex(VATNUM);
		int vatRateCol = cur.getColumnIndex(VATRATE);
		try {
			vatNum.setText(cur.getString(vatNumCol));
			vatRate.setText(cur.getString(vatRateCol));
		} catch (Exception e){
				e.printStackTrace();
			}
		setCon.close();
	}
	
	public void updateFinance(){
		SettingsConnector setCon = new SettingsConnector(this);
		try {
			setCon.updateFinance(vatNum.getText().toString(), vatRate.getText().toString());
		} catch (Exception e){
				e.printStackTrace();
			}
		setCon.close();
	}
	
	public void loadWorkRates () {
		rates.clear();
		RatesConnector ratesCon = new RatesConnector(this);
		Cursor ratesCur;
		ratesCon.open();
		try{
			ratesCur = ratesCon.getAllRatess();
			ratesCur.moveToFirst();
			int rateIDCol = ratesCur.getColumnIndex(RATEID);
			int rateNameCol = ratesCur.getColumnIndex(RATENAME);
			int rateValueCol = ratesCur.getColumnIndex(RATEVALUE);
			int rateVATCol = ratesCur.getColumnIndex(RATEVAT);
			int incVATCol = ratesCur.getColumnIndex(INCVAT);
			int incTimeCol = ratesCur.getColumnIndex(RATETIME);			
			do{		
				Rate rate = new Rate(ratesCur.getInt(rateIDCol), ratesCur.getString(rateNameCol), ratesCur.getDouble(rateValueCol), ratesCur.getString(incTimeCol)); 
				rates.add(rate);
				inflatedView = (RelativeLayout) View.inflate(this, R.layout.activity_settings_finance_work_rate, null);
				((TextView) inflatedView.findViewById(R.id.workRateNameSettingsTextView)).setText(rate.rateName);
				((TextView) inflatedView.findViewById(R.id.workRateValueSettingsTextView)).setText(nf.format(rate.rateValue));
				inflatedView.setTag(rate.rateID);
		     	((LinearLayout) main.findViewById(R.id.parentLayoutViewWorkRate)).addView(inflatedView);
				
				System.out.println("RATE ADDED");
			}while(ratesCur.moveToNext());
		} catch (Exception e) { 
	    	System.out.println("++++++No Rates+++++");
	    }
		ratesCon.close();
		
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.addWorkRateSettingsButton:
			System.out.println("New Rate!!!");
        	Intent i = new Intent(FinanceSettings.this, AddRatePopup.class);
        	startActivityForResult(i, 0);
			break;
		case R.id.workRateFinanceRelativeLayout:
			System.out.println("Edit Rate!!!");
			for(int k=0; k < rates.size(); k++){
				Rate rate = rates.get(k);
				if(String.valueOf(rate.rateID).equals(view.getTag().toString())){
					Intent in = new Intent(FinanceSettings.this, AddRatePopup.class);
					in.putExtra("rateObj", rate);
					in.putExtra("edit", 1);
		        	startActivityForResult(in, 0);
		        }else{
		        	System.out.println("No Rate!");
		        }
			}
			break;		
		default:
			break;
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {		
		switch (requestCode){
		case 0:
			if (resultCode==RESULT_OK){
				linear = (LinearLayout)main.findViewById(R.id.parentLayoutViewWorkRate);
				linear.removeAllViews();
				loadWorkRates();
			}
			break;
		default:
			break;
		}
	}
}
