package uk.co.beanssystems.settings;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import uk.co.beanssystems.adapter.DetailInfo;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ExpSubTypeConnector;
import uk.co.beanssystems.database.ExpTypeConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.invoices.NewInvoice;
import uk.co.beanssystems.jobs.Expense;
import uk.co.beanssystems.jobs.Item;
import uk.co.beanssystems.jobs.Task;
import uk.co.beanssystems.popups.DeleteDialog;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class ExpensesSettings extends SherlockActivity {
	
	private LinkedHashMap<String, ParentInfo> myGroups = new LinkedHashMap<String, ParentInfo>();
	private ArrayList<ParentInfo> taskItemList = new ArrayList<ParentInfo>(); 
	private ExpandableExpensesAdapter listAdapter;
	private ExpandableListView myList;
	private static final String EXPTYPEID = "expTypeID";
	private static final String EXPTYPE = "expType";
	private static final String EXPSUBTYPEID = "expSubTypeID", EXPSUBTYPE = "expSubType", CODE = "code";
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_expenses);
		
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.settings);
	    bar.setDisplayHomeAsUpEnabled(true);
	    bar.setTitle("Expenses");
	}
	
	@Override
	public void onStart(){
		super.onStart();
		loadExpenses();
	}
	
	public void loadExpenses(){
	    taskItemList = new ArrayList<ParentInfo>();
		myGroups = new LinkedHashMap<String, ParentInfo>();
		myList = (ExpandableListView)findViewById(R.id.expensesExpandableExpandListView);
		ExpTypeConnector expCon = new ExpTypeConnector(this);
		expCon.open();
		
		ExpSubTypeConnector expSubCon = new ExpSubTypeConnector(this);
		expSubCon.open();
		try {
			Cursor parentCur = expCon.getAllExpTypes();
			parentCur.moveToFirst();
			int expTypeIdCol = parentCur.getColumnIndex(EXPTYPEID);
			int expTypeCol = parentCur.getColumnIndex(EXPTYPE);
			int expTypeID;
			String expType;
			//Get root and add their children
			do {
				expTypeID = parentCur.getInt(expTypeIdCol);
				expType = parentCur.getString(expTypeCol).trim();
				
				Cursor childCur = expSubCon.getAllExpSubTypesByRoot(expTypeID);
				childCur.moveToFirst();
				int expSubTypeIDCol = childCur.getColumnIndex(EXPSUBTYPEID);
				int expSubTypeCol = childCur.getColumnIndex(EXPSUBTYPE);
				int codeCol = childCur.getColumnIndex(CODE);
				do {
					addItem(expType, expTypeID, childCur.getString(expSubTypeCol).toString(), childCur.getInt(codeCol), childCur.getInt(expSubTypeIDCol));
				} while (childCur.moveToNext());
				
			} while (parentCur.moveToNext());
		} catch (Exception e){
			e.printStackTrace();
		}
		
		expSubCon.close();
		expCon.close();
		
		listAdapter = new ExpandableExpensesAdapter(this, taskItemList);
	    listAdapter.notifyDataSetChanged();
		myList.setAdapter(listAdapter);
		
	}
	
	public void onClick(View v){
		switch (v.getId()){
			case R.id.deleteCross:
				final ChildInfo ch = (ChildInfo) v.getTag();
				final Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Delete "+ch.getName()+"?");
				builder.setCancelable(true);
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						  ExpSubTypeConnector expSubCon = new ExpSubTypeConnector(ExpensesSettings.this);
						  expSubCon.deleteExpSubType(ch.getID());
						  loadExpenses();
						  myList.expandGroup(ch.getParentPosition());
					}
				});
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
				break;
			case R.id.editPencil:
				ChildInfo c = (ChildInfo) v.getTag();
				Intent i = new Intent(this, ExpenseEditPopup.class);
				i.putExtra("source", 0);
				i.putExtra("childID", c.getID());
				i.putExtra("name", c.getName());
				i.putExtra("group", c.getParentPosition());
				startActivityForResult(i, 0);
				break;
			case R.id.addExpenseSettingsImageView:
				ParentInfo p = (ParentInfo) v.getTag();
				Intent inAdd = new Intent(this, ExpenseEditPopup.class);
				inAdd.putExtra("source", 1);
				inAdd.putExtra("parentID", p.getID());
				inAdd.putExtra("group", p.getPosition());
				startActivityForResult(inAdd, 0);
				break;
			default:break;
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode){
		//Edit Child Expense
		case 0:
			if (resultCode==RESULT_OK){
				  ExpSubTypeConnector expSubCon = new ExpSubTypeConnector(this);
				  int source = data.getIntExtra("source", 0);
				  
				  if (source==0){
					  expSubCon.updateName(data.getIntExtra("childID",0), data.getStringExtra("nameReturned"));
				  } else {
					  expSubCon.insertExpSubType(data.getIntExtra("parentID", 0), data.getStringExtra("nameReturned"), "0");
				  }
				  loadExpenses();
				  myList.expandGroup(data.getIntExtra("group", 0));
			  }
			  break;
		  default:break;
		  }
	  }
	
	public int addItem(String group, int pID, String item, int fixed, int id){
		int groupPosition = 0;
		ParentInfo parentInfo = myGroups.get(group);
		if(parentInfo == null){
			parentInfo = new ParentInfo();
			parentInfo.setID(pID);
			parentInfo.setName(group);
			parentInfo.setPosition((myGroups.size()));
			myGroups.put(group, parentInfo);
			taskItemList.add(parentInfo);
		}
		ArrayList<ChildInfo> headerList = parentInfo.getHeaderList();
		ChildInfo childInfo = new ChildInfo();
		childInfo.setID(id);
		childInfo.setName(item);
		childInfo.setChildPosition((headerList.size()));
		childInfo.setParentPosition(parentInfo.getPosition());
		childInfo.setFixed(fixed);
		headerList.add(childInfo);
		parentInfo.setHeaderList(headerList);	 
		groupPosition = taskItemList.indexOf(parentInfo);
		return groupPosition;
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	        finish();
	    	return true;
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    menu.removeItem(R.id.search_option);
	    menu.removeItem(R.id.settings_option);
	    menu.removeItem(R.id.menuSort);
	    return true;
	}
}
