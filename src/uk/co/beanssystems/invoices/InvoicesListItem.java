package uk.co.beanssystems.invoices;

public class InvoicesListItem {
	public int id;
	public int icon;
	public int clientId;
    public String client;
    public int jobId;
    public String jobName;
    public String dateDue;
    public String outstandingSum;
    public String paidStatus;
    public String status;
    
	public InvoicesListItem() {
		super();
	}
	
	public InvoicesListItem(int id, int icon, int clientId, String client, int jobId, String jobName, String dateDue, String outstandingSum, String paidStatus, String status) {
		super();
		this.id = id;
		this.icon = icon;
		this.clientId = clientId;
		this.client = client;
		this.jobId = jobId;
		this.jobName = jobName;
		this.dateDue = dateDue;
		this.outstandingSum = outstandingSum;
		this.paidStatus = paidStatus;
		this.status = status;  
	}
}
