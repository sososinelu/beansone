package uk.co.beanssystems.invoices;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import uk.co.beanssystems.beansone.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class InvoicesAdapter extends ArrayAdapter<InvoicesListItem> {
	Context context;
	int layoutResourceId;
	ArrayList<InvoicesListItem> items = null;
	
	public InvoicesAdapter (Context context, int layoutResourceId, ArrayList<InvoicesListItem> items){
		super(context, layoutResourceId, items);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.items = items;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
        InvoicesListItemHolder holder = null;
        
        // Set currency format 
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.UK);
        if(row == null){
	        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	        row = inflater.inflate(layoutResourceId, parent, false);
	        holder = new InvoicesListItemHolder();
	        holder.icon = (ImageView)row.findViewById(R.id.invoice_list_row_image);
	        holder.client = (TextView)row.findViewById(R.id.invoice_list_row_client);
	        holder.jobName = (TextView)row.findViewById(R.id.invoice_list_row_jobName);
	        holder.dateDue = (TextView)row.findViewById(R.id.invoice_list_row_dateDue);
	        holder.outstandingSum = (TextView)row.findViewById(R.id.invoice_list_row_outstandingSum);
	        holder.paidStatus = (TextView) row.findViewById(R.id.invoice_list_row_paidStatus);
	        holder.status = (TextView) row.findViewById(R.id.invoice_list_row_status);
	        row.setTag(holder);
	    }else{
        	holder = (InvoicesListItemHolder)row.getTag();
	    }

        InvoicesListItem invoice = items.get(position);
        holder.icon.setImageResource(invoice.icon);
        holder.client.setText(invoice.client);
        holder.jobName.setText(invoice.jobName);
        holder.dateDue.setText(invoice.dateDue);
        holder.outstandingSum.setText(nf.format(Double.valueOf(invoice.outstandingSum)));
        holder.paidStatus.setText(invoice.paidStatus);
        holder.status.setText(invoice.status);
		if(invoice.status.equals("draft")){
			holder.status.setBackgroundResource(R.color.yellow2);
		}else if(invoice.status.equals("sent")){
			holder.status.setBackgroundResource(R.color.royalBlue);
		}else if(invoice.status.equals("overdue")){
			holder.status.setBackgroundResource(R.color.red1);
		}else if(invoice.status.equals("paid")){
			holder.status.setBackgroundResource(R.color.green3);
		}
        return row;       
	}
	
	static class InvoicesListItemHolder {
	       ImageView icon;
	       TextView client;
	       TextView jobName;
	       TextView dateDue;
	       TextView outstandingSum;
	       TextView paidStatus;
	       TextView status;
	   }
}
