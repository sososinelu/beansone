package uk.co.beanssystems.invoices;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.beanssystems.beansone.InvoicePDF;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.PDFMaker;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.database.InvoicesConnector;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.QitemsConnector;
import uk.co.beanssystems.database.QtasksConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.jobs.Expense;
import uk.co.beanssystems.jobs.Item;
import uk.co.beanssystems.jobs.JobsList;
import uk.co.beanssystems.jobs.Task;
import uk.co.beanssystems.popups.DeleteDialog;
import uk.co.beanssystems.popups.EnterPaymentPopup;
import uk.co.beanssystems.quotes.PrintDialog;
import uk.co.beanssystems.settings.Settings;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ViewInvoice extends SherlockActivity {
	private String emailSend;
	private Cursor tasksCur,  itemsCur, expsCur;
	private int invoiceID, clientID, jobID;
	private String jobName, dateDue, dateIssued, status, mobile, email, address, postcode, vatType;
	private double outstandingSum, totalTasks,totalItems, totalExps, fixedAmount,  subTotal, baseTotal, totalCost, totalVAT, totalDue;
	private ArrayList<Task> tasks = new ArrayList<Task>();
	private ArrayList<Item> items = new ArrayList<Item>();
	private ArrayList<Expense> expenses = new ArrayList<Expense>();
	private TextView totalSumTextView, outSumTextView, statusTextView, companyTextView, jobTextView,
		dateDueTextView, vatLabel, subtotalLabel, noIWETextView;
	private Button clientButton, sendButton;
	private boolean emailStarted=false, isFixed = false;
	private String clientName, file;
	double globalVAT = 20.0;
	Context context;
	View main, itemsView, expView, tasksView ;
	LayoutInflater inflater;
	NumberFormat nf;
	LinearLayout linear, sendLayout, vatLabelLayout, subTotalLabelLayout;;
	RelativeLayout quickLayout;	
	Drawable arrowDw, arrowUp;
	TranslateAnimation animationInSend, animationOutSend, animationInQuick, animationOutQuick;
	SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	InvoicesConnector invoicesCon = new InvoicesConnector(this);
	TasksConnector tCon = new TasksConnector(this);
	ItemsConnector iCon = new ItemsConnector(this);
	ExpensesConnector eCon = new ExpensesConnector(this);	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		main = inflater.inflate(R.layout.activity_view_invoice, null);
		setContentView(main);	
		nf = NumberFormat.getCurrencyInstance(Locale.UK);
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.invoice);
	    bar.setDisplayHomeAsUpEnabled(true);
	
		Intent ili = getIntent();
        invoiceID = ili.getIntExtra("passInvoiceID", -1);
        clientID = ili.getIntExtra("passClientID", -1);
        jobID = ili.getIntExtra("passJobID", -1);
        jobName = ili.getStringExtra("passJobName");
        dateDue = ili.getStringExtra("passDateDue");
        context = this;
        animationInSend = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
			    Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
	    animationOutSend = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f );
	     animationInQuick = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
			    Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
		animationOutQuick = new TranslateAnimation(
			    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -1.0f );
		quickLayout = (RelativeLayout) findViewById(R.id.quickContactPopupViewInvoice);
	    sendLayout = (LinearLayout) findViewById(R.id.sendPopupViewInvoice);	    
	    arrowDw = getApplicationContext().getResources().getDrawable(R.drawable.arrow_down);
	    arrowUp = getApplicationContext().getResources().getDrawable(R.drawable.arrow_up);
        totalSumTextView = (TextView) findViewById(R.id.totalDueViewInvoiceTextView);
        outSumTextView = (TextView) findViewById(R.id.outViewInvoiceTextView);
        statusTextView = (TextView) findViewById(R.id.statusViewInvoiceTextView); 
        clientButton = (Button) findViewById(R.id.clientViewInvoiceButton);
	    sendButton = (Button) findViewById(R.id.sendJobViewInvoiceButton);
        companyTextView = (TextView) findViewById(R.id.companyViewInvoiceTextView);
        jobTextView = (TextView) findViewById(R.id.jobViewInvoiceTextView);
        dateDueTextView = (TextView) findViewById(R.id.dueDateViewInvoiceTextView); 
        vatLabel = (TextView) findViewById(R.id.vatValueLabelViewInvoice);
	    vatLabelLayout = (LinearLayout) findViewById(R.id.vatLayoutViewInvoice);
	    subtotalLabel = (TextView) findViewById(R.id.subtotalValueLabelViewInvoice);
	    subTotalLabelLayout = (LinearLayout) findViewById(R.id.subtotalLayoutViewInvoice);
	    noIWETextView = (TextView) findViewById(R.id.noIWEViewInvoiceTextView);
        jobTextView.setText(jobName);
        loadInvoice();
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance(Locale.UK);
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
	
	public void onResume(){
		super.onPause();
		if (emailStarted){
    		if (!status.equals("paid")){
			InvoicesConnector iCon = new InvoicesConnector(this);
    		iCon.updateStatus(invoiceID, "sent");
    		iCon.close();
			status = "sent";
			statusTextView.setText("Sent");
			statusTextView.setBackgroundResource(R.color.royalBlue);	
    		}
			emailStarted = false;
		}else{
			loadInvoice();
		}
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		emailStarted=false;
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.clientViewInvoiceButton:		    	    
		    animationInQuick.setDuration(200);
		    animationOutQuick.setDuration(200);		    
		    if(quickLayout.getVisibility() == View.GONE){
		    	clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
		    	quickLayout.startAnimation(animationInQuick);
		    	quickLayout.setVisibility(View.VISIBLE);
	    	}else{
	    		clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
	    		quickLayout.startAnimation(animationOutQuick);
	    		quickLayout.setVisibility(View.GONE);
	    	} 
			break;			
		case R.id.sendJobViewInvoiceButton:	
			//Slide Up from 100% below itself		    
		    animationInSend.setDuration(200);
		    animationOutSend.setDuration(200);	    
		    if(sendLayout.getVisibility() == View.GONE){
		    	sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
		    	sendLayout.setVisibility(View.VISIBLE);
		    	sendLayout.startAnimation(animationInSend);
	    	}else{
	    		sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
	    		sendLayout.startAnimation(animationOutSend);
	    		sendLayout.setVisibility(View.GONE);
	    	} 
			break;		
		case R.id.paymentViewInvoiceButton:
			if(outstandingSum == 0.0){
				Toast.makeText(getApplicationContext(), "Invoice Paid!",
						Toast.LENGTH_LONG).show();
			}else{
				Intent inPayment = new Intent(this, EnterPaymentPopup.class);
				inPayment.putExtra("passOutstandingSum", outstandingSum);
				inPayment.putExtra("passInvoiceID", invoiceID);
				startActivityForResult(inPayment, 0);
			}
			break;		
		case R.id.mobileQCViewInvoiceTextView:
			try {
				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
				quickLayout.startAnimation(animationOutQuick);
				quickLayout.setVisibility(View.GONE);
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("tel", mobile, null)));
			} catch (Exception e){
			}
     		break;			
		case R.id.smsQCViewInvoiceTextView:
			try {
				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
				quickLayout.startAnimation(animationOutQuick);
				quickLayout.setVisibility(View.GONE);
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", mobile, null)));
			} catch (Exception e){
			}
					break;			
		case R.id.emailQCViewInvoiceTextView:
			try {
				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
				quickLayout.startAnimation(animationOutQuick);
				quickLayout.setVisibility(View.GONE);
				Intent intent = new Intent(Intent.ACTION_SEND);
	            intent.setType("plain/text");
	            intent.putExtra(Intent.EXTRA_EMAIL,new String[] {email});
//		            intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of the mail");
//		            intent.putExtra(Intent.EXTRA_TEXT, "body of the mail");
	            startActivity(Intent.createChooser(intent, "Choose email application."));
			} catch (Exception e){
			}
			break;		
		case R.id.mapQCViewInvoiceTextView:
			try {
				clientButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDw, null);
				quickLayout.startAnimation(animationOutQuick);
				quickLayout.setVisibility(View.GONE);
				String address1 = address + postcode; // Get address
				address1 = address1.replace(" ", "+");
				Intent geoIntent = new Intent (android.content.Intent.ACTION_VIEW, Uri.parse ("geo:0,0?q=" + address1)); // Prepare intent
				startActivity(geoIntent);	// Initiate lookup
			} catch (Exception e){
				Toast.makeText(getBaseContext(), "No Application Available to Display Maps!", Toast.LENGTH_SHORT).show();
				
			}
			break;		
		case R.id.emailLabelViewInvoiceTextView:
			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
			sendLayout.startAnimation(animationOutSend);
			sendLayout.setVisibility(View.GONE);
			ClientsConnector cliCon = new ClientsConnector(this);
			cliCon.open();
			Cursor cur = cliCon.getClient(clientID);
			cur.moveToFirst();
			int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
			String email = cur.getString(emailCol).replace(" ", "");
			cliCon.close();
			if (!email.equals("") && email.contains("@")){
				String client = clientName.length()<8 ? clientName : clientName.substring(0, 7);
				String job = jobName.length()<8 ? jobName : jobName.substring(0, 7);
				emailSend = email;
				String filename = invoiceID + "_inv_"+client+"_"+job+".pdf";
				InvoicePDF obj = new InvoicePDF(clientID, jobName, dateIssued, 
						dateDue, subTotal, totalVAT, totalDue);
				PDFMaker pdf = new PDFMaker(this, 1, filename, "Thank you for your business", obj);
	    		if (pdf.makeFile()){
	    			file = pdf.build();
	    			checkConStatus();
					sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
				} else {
					Toast.makeText(this, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();
				}
				} else {
				Toast.makeText(this, "You do not have an email address for this client. Please check client email and resend", Toast.LENGTH_LONG).show();
			}
			break;			
		case R.id.printLabelViewInvoiceTextView:
			Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
//			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
//			sendLayout.startAnimation(animationOutSend);
//			sendLayout.setVisibility(View.GONE);
//			makePDF();
//    		printPDF();
			break;		
		case R.id.pdfLabelViewInvoiceTextView:
			sendButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
			sendLayout.startAnimation(animationOutSend);
			sendLayout.setVisibility(View.GONE);
			String client = clientName.length()<8 ? clientName : clientName.substring(0, 7);
			String job = jobName.length()<8 ? jobName : jobName.substring(0, 7);
			String filename = invoiceID + "_inv_"+client+"_"+job+".pdf";
			InvoicePDF obj = new InvoicePDF(clientID, jobName, dateIssued, 
					dateDue, subTotal, totalVAT, totalDue);
			PDFMaker pdf = new PDFMaker(this, 1, filename, "Thank you for your business", obj);
    		if (pdf.makeFile()){
    			file = pdf.build();
				startPDF();
			} else {
				Toast.makeText(this, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();
			}
			break;		  
		default:
			break;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.menu_options_view, menu);
	    MenuItem menuDel = menu.findItem(R.id.delete_option);
		menuDel.setVisible(false);		
	    if(status.equals("draft")){
			menuDel.setVisible(true);
	    }
	    return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.edit_option:
			if ("paid".equals(status)){
				Toast.makeText(this, "You cannot edit a paid Invoice", Toast.LENGTH_SHORT).show();
				return false;
			} else {
			   Intent inInvoice = new Intent(this, NewInvoice.class);
			   inInvoice.putExtra("inNo", 3);
			   inInvoice.putExtra("invoiceID", invoiceID);
	     	   inInvoice.putExtra("jobID", jobID);
	     	   inInvoice.putExtra("clientID", clientID);
	     	   inInvoice.putExtra("jobName", jobName);
	     	   inInvoice.putExtra("clientName", clientName);
	     	   inInvoice.putExtra("status", status);
	     	   inInvoice.putExtra("dateDue", dateDue);
	     	   inInvoice.putExtra("dateIssued", dateIssued);
	     	   inInvoice.putExtra("tasks", tasks);
	     	   inInvoice.putExtra("items", items);
	     	   inInvoice.putExtra("expenses", expenses);
	     	   inInvoice.putExtra("fixedAmount", fixedAmount);
	     	   inInvoice.putExtra("vatType", vatType);
	     	   startActivityForResult(inInvoice,8);
				return true;
			}
		case R.id.delete_option:
			Intent inDel = new Intent(this, DeleteDialog.class);
			inDel.putExtra("title", "Invoice");
			startActivityForResult(inDel,1);
			return true;
	    case android.R.id.home:
	        finish();
	    	return true;	    	
	    case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  	        
	    case R.id.about_option:
	    	return true;	    	
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;	    	
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;	    	
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode){
		case 0:
			if (resultCode==RESULT_OK){
				double newOutstandingSum = data.getDoubleExtra("passOutstandingSum", -1);
				outSumTextView.setText(String.valueOf(nf.format(newOutstandingSum)));
				if(newOutstandingSum == 0.0){
					statusTextView.setBackgroundResource(R.color.green3);
					statusTextView.setText("paid");
					InvoicesConnector invCon = new InvoicesConnector(this);
					try{
						invCon.updateStatus(invoiceID, "paid");
						final Builder builder = new AlertDialog.Builder(this);
						builder.setTitle("Finish Job?");
						builder.setMessage("Would you like to finish this job now you have received payment?");
						builder.setCancelable(true);
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										jobDone();
										Intent i = new Intent(context, JobsList.class);
										startActivity(i);
										finish();
									}
								});
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										finish();
									}
								});
						AlertDialog dialog = builder.create();
						dialog.show();
					} catch(Exception e){
						System.out.println("++++++ERROR+++++ "+e.getLocalizedMessage().toString());
			   	  	}
				}
			}
			break;
		case 1:
			if(resultCode == RESULT_OK){
				invoicesCon.updateActive(invoiceID, "dead");
				for(int i=0; i < tasks.size(); i++){
					Task t = tasks.get(i);
					tCon.updateInvoiceId(t.taskID, 0);
				}
				for(int j=0; j < items.size(); j++){
					Item i = items.get(j);
					iCon.updateInvoiceId(i.itemID, 0);
				}
				for(int j=0; j < expenses.size(); j++){
					Expense e = expenses.get(j);
					eCon.updateInvoiceId(e.expID, 0);
				}
				finish();
			}else{
				Toast.makeText(this, "Delete Cancelled!", Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			break;
		}
	}
	
	public void jobDone(){
		Cursor cur = null;
		//UPDATE QUOTE-------------------------------------
		QuotesConnector qCon = new QuotesConnector(this);
		qCon.open();
		cur = qCon.getQuoteByJob(jobID);
		int quoteID=0;
		try{
			cur.moveToFirst();
			int quoteIDCol = cur.getColumnIndex(QuotesConnector.QUOTEID);
			quoteID = cur.getInt(quoteIDCol);
			qCon.updateOpenActive(quoteID, "false");
			qCon.close();
		} catch (Exception e){
			qCon.close();
		}//---------------------------------END UPDATE QUOTE
		
		cur = null;
		//UPDATE QITEMS-------------------------------------
		QitemsConnector qItCon = new QitemsConnector(this);
		qItCon.open();
		cur = qItCon.getItemsByQuote(quoteID);
		try{
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(QitemsConnector.QITEMID);
			do {
				int id = cur.getInt(idCol);
				qItCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			qItCon.close();
		} catch (Exception e){
			qItCon.close();
		}//-------------------------------------END UPDATE QITEMS
		
		cur = null;
		//UPDATE QTASKS-------------------------------------
		QtasksConnector qTkCon = new QtasksConnector(this);
		qTkCon.open();
		cur = qTkCon.getTasksByQuote(quoteID);
		try{
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(QtasksConnector.QTASKID);
			do {
				int id = cur.getInt(idCol);
				qTkCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			qTkCon.close();
		} catch (Exception e){
			qTkCon.close();
		}//-------------------------------------END UPDATE QTASKS
		
		cur = null;
		//UPDATE TASKS--------------------------------------
		tCon.open();
		cur = tCon.getTasksByJob(jobID);
		try {
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(TasksConnector.TASKID);
			do {
				int id = cur.getInt(idCol);
				tCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			tCon.close();
		} catch (Exception e){
			tCon.close();
		}//-----------------------------------END UPDATE TASKS
		
		cur = null;
		//UPDATE ITEMS--------------------------------------
		iCon.open();
		cur = iCon.getItemsByJob(jobID);
		try {
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(ItemsConnector.ITEMID);
			do {
				int id = cur.getInt(idCol);
				iCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			iCon.close();
		} catch (Exception e){
			iCon.close();
		}//-----------------------------------END UPDATE ITEMS
		
		cur = null;
		//UPDATE EXPENSES--------------------------------------
		eCon.open();
		cur = eCon.getExpsByJob(jobID);
		try {
			cur.moveToFirst();
			int idCol = cur.getColumnIndex(ExpensesConnector.EXPENSEID);
			do {
				int id = cur.getInt(idCol);
				eCon.updateOpenActive(id, "false");
			} while(cur.moveToNext());
			eCon.close();
		} catch (Exception e){
			eCon.close();
		}//-----------------------------------END UPDATE EXPENSES
		
		cur = null;
		//UPDATE JOB-----------------------------------------
		JobsConnector jCon = new JobsConnector(this);
		jCon.updateActive(jobID, "false");	//opens and closes itself
		
		InvoicesConnector invCon = new InvoicesConnector(this);
		invCon.updateActive(invoiceID, "false");
		//-------------------------------------END UPDATE JOB
	}
	
	public void loadInvoice () {
		linear = (LinearLayout)main.findViewById(R.id.parentLayoutViewInvoice);
		linear.removeAllViews();
		tasksView = inflater.inflate(R.layout.activity_view_invoice_holder, null);
		itemsView = inflater.inflate(R.layout.activity_view_invoice_holder, null);
		expView = inflater.inflate(R.layout.activity_view_invoice_holder, null);
		// Re initialise the arrays
		tasks = new ArrayList<Task>();
		items = new ArrayList<Item>();
		expenses = new ArrayList<Expense>();		
		getInvoice(invoiceID);
        getClient(clientID);
        getTasks(invoiceID);     
        getItems(invoiceID);
        getExp(invoiceID);
        if(tasks.size() == 0 && items.size() == 0 && expenses.size() == 0){
			linear.addView(noIWETextView);
		}
	}
	
	public void getInvoice(int id){
		if (id != 0){
			invoicesCon.open();
			try{
				Cursor invoiceCur = invoicesCon.getInvoice(id);
				invoiceCur.moveToFirst();
				
				int statusCol = invoiceCur.getColumnIndex(InvoicesConnector.STATUS);
				int dateIssuedCol = invoiceCur.getColumnIndex(InvoicesConnector.DATEISSUED);
				int dateDueCol = invoiceCur.getColumnIndex(InvoicesConnector.DATEDUE);
				int vatCol = invoiceCur.getColumnIndex(InvoicesConnector.VAT);
				int subtotalCol = invoiceCur.getColumnIndex(InvoicesConnector.SUBTOTAL);
				int totalDueCol = invoiceCur.getColumnIndex(InvoicesConnector.TOTALDUE);
				int basetotalCol = invoiceCur.getColumnIndex(InvoicesConnector.BASETOTAL);
				int vatTypeCol = invoiceCur.getColumnIndex(InvoicesConnector.VATTYPE);
				int outstandingCol = invoiceCur.getColumnIndex(InvoicesConnector.OUTSTANDING);
				int fixedCostCol = invoiceCur.getColumnIndex(InvoicesConnector.FIXEDCOST);
				
				totalDue = invoiceCur.getDouble(totalDueCol);
				dateIssued = invoiceCur.getString(dateIssuedCol);
				dateDue = invoiceCur.getString(dateDueCol);
				status = invoiceCur.getString(statusCol);
				outstandingSum = invoiceCur.getDouble(outstandingCol);
				fixedAmount = invoiceCur.getDouble(fixedCostCol);
				vatType = invoiceCur.getString(vatTypeCol);
				subTotal = invoiceCur.getDouble(subtotalCol);
				baseTotal = invoiceCur.getDouble(basetotalCol);
				totalVAT = invoiceCur.getDouble(vatCol);
				if (fixedAmount!=0){
					isFixed = true;
				}
				if(vatType.equals("none")){
					vatLabelLayout.setVisibility(View.GONE);
					subTotalLabelLayout.setVisibility(View.GONE);
				}else{
					vatLabelLayout.setVisibility(View.VISIBLE);
					subTotalLabelLayout.setVisibility(View.VISIBLE);
					vatLabel.setText(String.valueOf(nf.format(totalVAT)));
					subtotalLabel.setText(String.valueOf(nf.format(subTotal)));
				}
				totalSumTextView.setText(String.valueOf(nf.format(totalDue)));
				outSumTextView.setText(String.valueOf(nf.format(outstandingSum)));
				statusTextView.setText(status);
		        Calendar cal = Calendar.getInstance();
		        try {
		        	cal = convertStringToDate(dateDue);
		        } catch (Exception e){
		        	
		        }
				dateDueTextView.setText(outDate.format(cal.getTime()));				
				if(status.equals("draft")){
					statusTextView.setBackgroundResource(R.color.yellow2);
				}else if(status.equals("sent")){
					statusTextView.setBackgroundResource(R.color.royalBlue);
				}else if(status.equals("overdue")){
					statusTextView.setBackgroundResource(R.color.red1);
				}else if(status.equals("paid")){
					statusTextView.setBackgroundResource(R.color.green3);
				}
			} catch (Exception e) { 
	        	System.out.println("++++++No Invoices+++++ "+e.getLocalizedMessage().toString());
	        }
			
			invoicesCon.close();
		}
	}
	
	public void getClient(int id){		
		if (id != 0){
			ClientsConnector cliCon = new ClientsConnector(this);
			cliCon.open();
			try{
				Cursor cur = cliCon.getClient(id);
				cur.moveToFirst();
				int forenameCol = cur.getColumnIndex(ClientsConnector.FORENAME);
				int surnameCol = cur.getColumnIndex(ClientsConnector.SURNAME);
				int companyNameCol = cur.getColumnIndex(ClientsConnector.COMPANY);
				int mobileCol = cur.getColumnIndex(ClientsConnector.MOBILE);
				int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
				int addressCol = cur.getColumnIndex(ClientsConnector.ADDRESS);
				int postcodeCol = cur.getColumnIndex(ClientsConnector.POSTCODE);				
				clientName = cur.getString(forenameCol) + " " + cur.getString(surnameCol);
				String company = cur.getString(companyNameCol);
				mobile = cur.getString(mobileCol);
				email = cur.getString(emailCol);
				address = cur.getString(addressCol);
				postcode = cur.getString(postcodeCol);				
				clientButton.setText(clientName);
				companyTextView.setText(company);
			} catch (Exception e) { 
	        	System.out.println("++++++No Clients+++++ "+e.getLocalizedMessage().toString());
	        }
			cliCon.close();
		}
	}
	
	public void getTasks(int id) {
		totalTasks = 0;
		TasksConnector tasksCon = new TasksConnector(this);
		tasksCon.open();
		try{
			tasksCur = tasksCon.getTasksByJobInvoice(id);	
			if(tasksCur.getCount() != 0){
				((TextView) tasksView.findViewById(R.id.typeViewInvoiceHolderTextView)).setText("Work");
		        linear.addView(tasksView);
				tasksCur.moveToFirst();
				// Get the Task columns
				int taskIdCol = tasksCur.getColumnIndex(TasksConnector.TASKID);
				int taskNameCol = tasksCur.getColumnIndex(TasksConnector.TASKNAME);
				int actualDurationCol = tasksCur.getColumnIndex(TasksConnector.ACTUALDURATION);
				int chargeValueCol = tasksCur.getColumnIndex(TasksConnector.CHARGEVALUE);
				int jobIDCol = tasksCur.getColumnIndex(TasksConnector.JOBID);
				int invoiceIDCol = tasksCur.getColumnIndex(TasksConnector.INVOICEID);			
				do {
					Task tsk = new Task(tasksCur.getInt(taskIdCol), tasksCur.getInt(jobIDCol), tasksCur.getInt(invoiceIDCol), tasksCur.getString(taskNameCol),
							tasksCur.getDouble(actualDurationCol), tasksCur.getDouble(chargeValueCol)); 
					RelativeLayout inflatedView = (RelativeLayout) View.inflate(this, R.layout.activity_view_invoice_child, null);
					((TextView) inflatedView.findViewById(R.id.typeNameViewInvoiceTextView)).setText(tsk.taskName);
					((TextView) inflatedView.findViewById(R.id.quantityViewInvoiceTextView)).setText(" "+String.valueOf(tsk.duration)+" Hrs");
					((TextView) inflatedView.findViewById(R.id.unitViewInvoiceTextView)).setText("Rate = ");
					((TextView) inflatedView.findViewById(R.id.unitCostViewInvoiceTextView)).setText(String.valueOf(nf.format(tsk.value)));
			     	((LinearLayout) tasksView.findViewById(R.id.taskItemExpLayoutChild)).addView(inflatedView);
			     	if(tsk.taskName.equals("Callout")){
						totalTasks += tsk.value;
					}else{
						totalTasks += tsk.duration * tsk.value;
					}   
					tasks.add(tsk);
				}while(tasksCur.moveToNext());	
				((TextView) tasksView.findViewById(R.id.totalViewInvoiceTextView)).setText(String.valueOf(nf.format(totalTasks)));
			}
		} catch (Exception e) { 
        	System.out.println("++++++No Work+++++ "+e.getLocalizedMessage().toString());
        }
		tasksCon.close();
	}
	
	public void getItems(int id) {
		totalItems = 0;
		ItemsConnector itemsCon = new ItemsConnector(this);
		itemsCon.open();
		try{
			itemsCur = itemsCon.getItemsByJobInvoice(id);
			if(itemsCur.getCount() != 0){
				((TextView) itemsView.findViewById(R.id.typeViewInvoiceHolderTextView)).setText("Items");		        
		        linear.addView(itemsView);
				itemsCur.moveToFirst();
				// Get the Item columns
				int itemIdCol = itemsCur.getColumnIndex(ItemsConnector.ITEMID);
				int itemNameCol = itemsCur.getColumnIndex(ItemsConnector.ITEMNAME);
				int unitCostCol = itemsCur.getColumnIndex(ItemsConnector.UNITCOST);
				int quantityCol = itemsCur.getColumnIndex(ItemsConnector.QUANTITY);
				int jobIDCol = itemsCur.getColumnIndex(ItemsConnector.JOBID);
				int invoiceIDCol = itemsCur.getColumnIndex(ItemsConnector.INVOICEID);
				do {
			     	Item it = new Item(itemsCur.getInt(itemIdCol), itemsCur.getInt(jobIDCol), itemsCur.getInt(invoiceIDCol), itemsCur.getString(itemNameCol),
			     			itemsCur.getInt(quantityCol), itemsCur.getDouble(unitCostCol));
					RelativeLayout inflatedView = (RelativeLayout) View.inflate(this, R.layout.activity_view_invoice_child, null);
					((TextView) inflatedView.findViewById(R.id.typeNameViewInvoiceTextView)).setText(it.itemName);
					((TextView) inflatedView.findViewById(R.id.quantityViewInvoiceTextView)).setText(" X "+String.valueOf(it.quantity));
					((TextView) inflatedView.findViewById(R.id.unitViewInvoiceTextView)).setText("Unit = ");
					((TextView) inflatedView.findViewById(R.id.unitCostViewInvoiceTextView)).setText(String.valueOf(nf.format(it.cost)));
			     	((LinearLayout) itemsView.findViewById(R.id.taskItemExpLayoutChild)).addView(inflatedView);
			     	double totalCost = it.quantity * it.cost;
					totalItems += totalCost;
			     	items.add(it);
				}while (itemsCur.moveToNext());
				((TextView) itemsView.findViewById(R.id.totalViewInvoiceTextView)).setText(nf.format(totalItems));
			}
		} catch (Exception e) { 
        	e.printStackTrace();
        	System.out.println("++++++No Items+++++ "+e.getLocalizedMessage().toString());
        }
		itemsCon.close();
	}
	
	public void getExp(int id) {
		totalExps = 0;
		ExpensesConnector expsCon = new ExpensesConnector(this);
		expsCon.open();
		try{
			expsCur = expsCon.getExpsByJobInvoice(id);
			if(expsCur.getCount() != 0){
				((TextView) expView.findViewById(R.id.typeViewInvoiceHolderTextView)).setText("Expenses");
			    linear.addView(expView);
				expsCur.moveToFirst();
				// Get the Expenses columns
				int expIdCol = expsCur.getColumnIndex(ExpensesConnector.EXPENSEID);
				int expTypeCol = expsCur.getColumnIndex(ExpensesConnector.EXPTYPE);
				int expCostCol = expsCur.getColumnIndex(ExpensesConnector.COST);
				int jobIDCol = tasksCur.getColumnIndex(ExpensesConnector.JOBID);
				int invoiceIDCol = tasksCur.getColumnIndex(ExpensesConnector.INVOICEID);
				do {
			     	Expense ex = new Expense(expsCur.getInt(expIdCol), expsCur.getInt(jobIDCol), expsCur.getInt(invoiceIDCol), 
			     			expsCur.getString(expTypeCol), expsCur.getDouble(expCostCol));
					RelativeLayout inflatedView = (RelativeLayout) View.inflate(this, R.layout.activity_view_invoice_child, null);
					((TextView) inflatedView.findViewById(R.id.typeNameViewInvoiceTextView)).setText(ex.expType);
					((TextView) inflatedView.findViewById(R.id.quantityViewInvoiceTextView)).setText("");
					((TextView) inflatedView.findViewById(R.id.unitViewInvoiceTextView)).setText("Cost = ");
					((TextView) inflatedView.findViewById(R.id.unitCostViewInvoiceTextView)).setText(String.valueOf(nf.format(ex.expCost)));
			     	((LinearLayout) expView.findViewById(R.id.taskItemExpLayoutChild)).addView(inflatedView);
					expenses.add(ex);
					totalExps += ex.expCost;
				}while (expsCur.moveToNext());
				((TextView) expView.findViewById(R.id.totalViewInvoiceTextView)).setText(nf.format(totalExps));
			}
		} catch (Exception e) {
			e.printStackTrace();
        	System.out.println("++++++No Expenses+++++ "+e.getLocalizedMessage().toString());
        }
		expsCon.close();
	}
	
	public void startPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		
		if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } 
            catch (ActivityNotFoundException e) {
                Toast.makeText(this, "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
            }
		}
	}
	
	public void checkConStatus (){
		ConnectivityManager conMngr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		android.net.NetworkInfo wifi = conMngr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		android.net.NetworkInfo mobile = conMngr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if(wifi.isConnected() || mobile.isConnected()){
			emailPDF();
		}else{
			final Builder builder = new AlertDialog.Builder(this);			
			TextView title = new TextView(this);
			title.setText("No Internet Connection!");
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			//title.setTextColor(getResources().getColor(R.color.greenBG));
			title.setTextSize(20);
			builder.setCustomTitle(title);
			TextView msg = new TextView(this);
			msg.setText("No Internet connection is available. If you choose to send the Invoice, the email will stay in your Email Client Outbox till the device gets connected to the internet. Send email?");
			msg.setPadding(10, 10, 10, 10);
			msg.setGravity(Gravity.CENTER);
			msg.setTextSize(18);
			builder.setView(msg);
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					emailPDF();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}
	
	public void emailPDF(){
		emailStarted = true;
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		String[] mailto = {emailSend};
		i.putExtra(Intent.EXTRA_EMAIL, mailto);
		i.putExtra(Intent.EXTRA_SUBJECT, "Your Invoice");
		i.putExtra(Intent.EXTRA_TEXT, "Hello "+clientName+", your invoice has been attached to this email.");
		i.putExtra(Intent.EXTRA_STREAM,
			Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ java.io.File.separator + "BeansOne"+ java.io.File.separator + file)));
		i.setType("application/pdf");
	    startActivity(Intent.createChooser(i, "Send Email"));
	}
	
	public void printPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		Intent printIntent = new Intent(this, PrintDialog.class);
    	printIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
    	printIntent.putExtra("title", "Invoice for "+clientName);
    	startActivity(printIntent);
	}
}
