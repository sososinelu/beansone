package uk.co.beanssystems.invoices;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import uk.co.beanssystems.adapter.TaskItemExp;
import uk.co.beanssystems.beansone.InvoicePDF;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.PDFMaker;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.database.InvoicesConnector;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.jobs.Expense;
import uk.co.beanssystems.jobs.Item;
import uk.co.beanssystems.jobs.Task;
import uk.co.beanssystems.popups.AddExpensePopup;
import uk.co.beanssystems.popups.AddItemPopup;
import uk.co.beanssystems.popups.AddTaskPopup;
import uk.co.beanssystems.popups.ChooseVATPopup;
import uk.co.beanssystems.popups.SelectClientPopup;
import uk.co.beanssystems.popups.SelectJobPopup;
import uk.co.beanssystems.quotes.PrintDialog;
import uk.co.beanssystems.settings.Settings;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class NewInvoice extends SherlockActivity {
	EditText clientNameEditText, jobNameEditText, dateIssued, dateDue, notes;
	Button sendBtn, viewTaskButton, viewItemButton;
	ImageButton viewExpButton;
	TextView subtotal, taxes, total, workCount, itemsCount, expensesCount, fixedLabel;
	LinearLayout popup;
	Drawable arrowUp, arrowDown;
	TranslateAnimation animationIn, animationOut;
	private double totalTasks, totalItems, totalExps, finalSubtotal, finalTotal, finalVat, globalVAT = 20.0, fixedAmount = 0, baseTotal;
	private boolean isFixed = false, emailStarted=false;
	private String status, terms, file, emailSend, vatType = "none";
	private Cursor jobsCur, tasksCur, itemsCur, expsCur;
	ArrayList<Task> tasks = new ArrayList<Task>();
	ArrayList<Item> items = new ArrayList<Item>();
	ArrayList<Expense> expenses = new ArrayList<Expense>();
	private int invoiceId, jobId, clientId;
	NumberFormat nf;
	private static final int CLIENTMODE = 1, JOBMODE = 2, EDITMODE = 3, NEWMODE = 4;
	private int mode;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	Intent in;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_invoice);
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.invoice);
	    bar.setDisplayHomeAsUpEnabled(true);	 	
	    nf = NumberFormat.getCurrencyInstance(Locale.UK);
		clientNameEditText = (EditText) findViewById(R.id.clientNewInvoiceEditText);
		jobNameEditText = (EditText) findViewById(R.id.jobNewInvoiceEditText);
		dateIssued = (EditText) findViewById(R.id.dateIssuedNewInvoiceEditText);
		dateDue = (EditText) findViewById(R.id.dateDueNewInvoiceEditText);
		notes = (EditText) findViewById(R.id.notesNewInvoiceEditText);
		subtotal = (TextView) findViewById(R.id.subTotalValueNewInvoiceTextView);
		taxes = (TextView) findViewById(R.id.taxesValueNewInvoiceTextView);
		total = (TextView) findViewById(R.id.totalValueNewInvoiceTextView);
        popup = (LinearLayout)findViewById(R.id.sendPopupNewInvoice);
        sendBtn = (Button) findViewById(R.id.sendButtonNewInvoice);
        workCount = (TextView) findViewById(R.id.workCountNewInvoiceTextView);
        itemsCount = (TextView) findViewById(R.id.itemsCountNewInvoiceTextView);
        expensesCount = (TextView) findViewById(R.id.expensesCountNewInvoiceTextView);
        fixedLabel = (TextView) findViewById(R.id.fixedCostLabelTextView);
        
        Calendar today = Calendar.getInstance();
        dateIssued.setText(outDate.format(today.getTime()));
        today.add(Calendar.DAY_OF_MONTH, 14);
        dateDue.setText(outDate.format(today.getTime()));
        
        //Slide Up from 100% below itself
        animationIn = new TranslateAnimation(
        	Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
	        Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f );
	        animationIn.setDuration(200);
	        
	    //Slide Out to 100% below itself
        animationOut = new TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f);
        	animationOut.setDuration(200);
        
		arrowDown = getResources().getDrawable(R.drawable.arrow_down);
		arrowUp = getResources().getDrawable(R.drawable.arrow_up);
        
        if (getIntent().getExtras() != null){
        	in = getIntent();
        	mode = in.getIntExtra("inNo", 0);
        	switch (mode){
        	case NEWMODE:
        		//Nothing needs to be inserted or called
        		break;
        	case CLIENTMODE:
        		clientNameEditText.setText(in.getStringExtra("passClientName"));
				clientId = in.getIntExtra("clientID", 0);
        		break;
        	case JOBMODE:
        		jobId = in.getIntExtra("passJobID", 0);
				jobNameEditText.setText(in.getStringExtra("passJobName"));
				jobNameEditText.setEnabled(false);
				clientId = in.getIntExtra("passClientID", 0);
				clientNameEditText.setText(in.getStringExtra("passClientName"));
				JobsConnector jCon = new JobsConnector(this);
				fixedAmount = jCon.getFixedCost(jobId);
				if (fixedAmount!=0){
					isFixed = true;
					fixedLabel.setVisibility(View.VISIBLE);
					fixedLabel.setText(nf.format(fixedAmount));
				} else {
					isFixed = false;
					fixedLabel.setVisibility(View.GONE);
				}
				getTasks(jobId);
				getItems(jobId);
				getExpenses(jobId);
				setTotals();
        		break;
        	case EDITMODE:
        		baseTotal = in.getDoubleExtra("baseTotal", 0.0);
        		vatType = in.getStringExtra("vatType");
        		sendBtn.setVisibility(View.INVISIBLE);
        		invoiceId = in.getIntExtra("invoiceID", 0);
				jobId = in.getIntExtra("jobID",0);
				jobNameEditText.setText(in.getStringExtra("jobName"));
				clientId = in.getIntExtra("clientID", 0);
				clientNameEditText.setText(in.getStringExtra("clientName"));
				status = in.getStringExtra("status").toLowerCase();
				fixedAmount = in.getDoubleExtra("fixedAmount", 0);
				if (fixedAmount!=0){
					isFixed = true;
					fixedLabel.setVisibility(View.VISIBLE);
					fixedLabel.setText(nf.format(fixedAmount));
				} else {
					isFixed = false;
					fixedLabel.setVisibility(View.GONE);
				}
				Date due = new Date();
				try {
					due = inDate.parse(in.getStringExtra("dateDue"));
				} catch (Exception e){
				}
				Date issued = new Date();
				try {
					issued = inDate.parse(in.getStringExtra("dateIssued"));
				} catch (Exception e){
				}
				dateDue.setText(outDate.format(due));
				dateIssued.setText(outDate.format(issued));
				importInv();
				setTotals();
        		break;
        	default:
        		break;
        	}
        }
	}
	
	public void onResume(){
		super.onPause();
		if (emailStarted){
    		if (!status.equals("paid")){
				InvoicesConnector iCon = new InvoicesConnector(this);
				iCon.updateStatus(iCon.getNextId(), "sent");
	    		iCon.close();	
	    		finish();
    		}
			emailStarted = false;
		}
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		emailStarted=false;
	}
	
	public void importInv(){
		ArrayList<Task> tempTasks = (ArrayList<Task>) in.getSerializableExtra("tasks");
		totalTasks = 0;
		for (Task tsk : tempTasks){
			tasks.add(tsk);
			if(tsk.taskName.equals("Callout")){
				totalTasks += tsk.value;
			}else{
				totalTasks += tsk.duration * tsk.value;
			}
		}

		ArrayList<Item> tempItems = (ArrayList<Item>) in.getSerializableExtra("items");
		totalItems = 0;
		for (Item it : tempItems){
			items.add(it);
			System.out.println(it.itemName + " : "+ it.itemID + " : "+ it.jobID + " : "+ it.invoiceID);
			totalItems += it.cost * it.quantity;
		}

		ArrayList<Expense> tempExpenses = (ArrayList<Expense>) in.getSerializableExtra("expenses");
		totalExps = 0;
		for (Expense ex : tempExpenses){
			expenses.add(ex);
			totalExps += ex.expCost;
		}
	}
	
	public void getJobs(int id) {
		JobsConnector jobsCon = new JobsConnector(this);
		jobsCon.open();
		try{
			jobsCur = jobsCon.getActiveJob(id);
			jobsCur.moveToFirst();
			int jobIdCol = jobsCur.getColumnIndex(JobsConnector.JOBID);
			int jobNameCol = jobsCur.getColumnIndex(JobsConnector.JOBNAME);
			int fixedCostCol = jobsCur.getColumnIndex(JobsConnector.FIXEDCOST);
			int noOfJobs = 0;
			int jobIDLocal;
			double fix=0;
			String jobName;
			jobNameEditText.setText("");
			do {
				jobIDLocal = jobsCur.getInt(jobIdCol);
				jobName = jobsCur.getString(jobNameCol);
				fix = jobsCur.getDouble(fixedCostCol);
				noOfJobs++;				
			}while(jobsCur.moveToNext());	
			
			if(noOfJobs == 1){
				jobNameEditText.setText(jobName);
				jobId = jobIDLocal;
				fixedAmount = fix;
				if (fixedAmount!=0){
					isFixed = true;
					fixedLabel.setVisibility(View.VISIBLE);
					fixedLabel.setText(nf.format(fixedAmount));
				} else {
					isFixed = false;
					fixedLabel.setVisibility(View.GONE);
				}
				getTasks(jobId);
				getItems(jobId);
				getExpenses(jobId);			
				subtotal.setText(String.valueOf(totalTasks + totalItems + totalExps));
			}else if (noOfJobs > 1){
				Intent inJob = new Intent(this, SelectJobPopup.class);
				inJob.putExtra("passClientId", id);
				startActivityForResult(inJob, 1); 
			} else {
				Toast.makeText(this,"No jobs for client", Toast.LENGTH_LONG).show();
			}
		}catch(Exception e){
			System.out.println("++++++No Jobs in the database+++++ "+e.getLocalizedMessage().toString());
		}
		jobsCon.close();
	}
	
	public void getTasks(int id){
		totalTasks = 0;
		TasksConnector tasksCon = new TasksConnector(this);
		tasksCon.open();
		try{
			tasksCur = tasksCon.getTasksByJob(id);
			tasksCur.moveToFirst();
			int taskIdCol = tasksCur.getColumnIndex(TasksConnector.TASKID);
			int taskNameCol = tasksCur.getColumnIndex(TasksConnector.TASKNAME);
			int actualDurationCol = tasksCur.getColumnIndex(TasksConnector.ACTUALDURATION);
			int chargeValueCol = tasksCur.getColumnIndex(TasksConnector.CHARGEVALUE);
			int jobIDCol = tasksCur.getColumnIndex(TasksConnector.JOBID);
			int invoiceIDCol = tasksCur.getColumnIndex(TasksConnector.INVOICEID);
			do{		
				Task tsk = new Task(tasksCur.getInt(taskIdCol), tasksCur.getInt(jobIDCol), tasksCur.getInt(invoiceIDCol), tasksCur.getString(taskNameCol),
						tasksCur.getDouble(actualDurationCol), tasksCur.getDouble(chargeValueCol)); 
				tasks.add(tsk);
				if(tsk.taskName.equals("Callout")){
					totalTasks += tsk.value;
				}else{
					totalTasks += tsk.duration * tsk.value;
				}
			} while (tasksCur.moveToNext());
		} catch (Exception e) { 
	    	System.out.println("++++++No Works+++++");
	    }
		tasksCon.close();
	}
	
	public void getItems(int id){
		totalItems = 0;
		ItemsConnector itemsCon = new ItemsConnector(this);
		itemsCon.open();
		try{
			itemsCur = itemsCon.getItemsByJob(id);
			itemsCur.moveToFirst();
			int itemIdCol = itemsCur.getColumnIndex(ItemsConnector.ITEMID);
			int itemNameCol = itemsCur.getColumnIndex(ItemsConnector.ITEMNAME);
			int unitCostCol = itemsCur.getColumnIndex(ItemsConnector.UNITCOST);
			int quantityCol = itemsCur.getColumnIndex(ItemsConnector.QUANTITY);
			int jobIDCol = itemsCur.getColumnIndex(ItemsConnector.JOBID);
			int invoiceIDCol = itemsCur.getColumnIndex(ItemsConnector.INVOICEID);
			do {
				Item it = new Item(itemsCur.getInt(itemIdCol), itemsCur.getInt(jobIDCol), itemsCur.getInt(invoiceIDCol), itemsCur.getString(itemNameCol),
						itemsCur.getInt(quantityCol), itemsCur.getDouble(unitCostCol)); 
				items.add(it);
				totalItems += it.cost * it.quantity;
			}while (itemsCur.moveToNext());	
		} catch (Exception e) { 
        	System.out.println("++++++No Items+++++");
        }
		itemsCon.close();
	}
	
	public void getExpenses(int id){
		totalExps = 0;
		ExpensesConnector expsCon = new ExpensesConnector(this);
		expsCon.open();
		try{
			expsCur = expsCon.getExpsByJob(id);
			expsCur.moveToFirst();			
			int expIdCol = expsCur.getColumnIndex(ExpensesConnector.EXPENSEID);
			int jobIDCol = expsCur.getColumnIndex(ExpensesConnector.JOBID);
			int invoiceIDCol = expsCur.getColumnIndex(ExpensesConnector.INVOICEID);
			int expTypeCol = expsCur.getColumnIndex(ExpensesConnector.EXPTYPE);
			int expDateCol = expsCur.getColumnIndex(ExpensesConnector.DATE);
			int expCostCol = expsCur.getColumnIndex(ExpensesConnector.COST); 
			int expDescCol = expsCur.getColumnIndex(ExpensesConnector.DESCRIPTION); 
			int expPhotoCol = expsCur.getColumnIndex(ExpensesConnector.PHOTO); 			
			do {
				Expense exp = new Expense(expsCur.getInt(expIdCol), expsCur.getInt(jobIDCol), expsCur.getInt(invoiceIDCol), expsCur.getString(expTypeCol), 
						expsCur.getString(expDateCol), expsCur.getDouble(expCostCol), expsCur.getString(expDescCol), expsCur.getString(expPhotoCol)); 
				expenses.add(exp);
				totalExps += exp.expCost;
			}while (expsCur.moveToNext());	
		} catch (Exception e) { 
			System.out.println("+++No Expenses+++");
		}
		expsCon.close();
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode){
		//From ClientPopup
		case 0:
			if (resultCode==RESULT_OK){
				clientId = data.getIntExtra("clientID", 0);
				clientNameEditText.setText(data.getStringExtra("clientName"));
				getJobs(clientId);
				setTotals();
			} break;
		//From JobPopup
		case 1:
			if (resultCode==RESULT_OK){
				clientId = data.getIntExtra("passClientID", 0);
				clientNameEditText.setText(data.getStringExtra("passClientName"));
				jobId = data.getIntExtra("passJobId", 0);
				jobNameEditText.setText(data.getStringExtra("passJobName")); 
				JobsConnector jCon = new JobsConnector(this);
				fixedAmount = jCon.getFixedCost(jobId);
				if (fixedAmount!=0){
					isFixed = true;
					fixedLabel.setVisibility(View.VISIBLE);
					fixedLabel.setText(nf.format(fixedAmount));
				} else {
					isFixed = false;
					fixedLabel.setVisibility(View.GONE);
				}
				getTasks(jobId);
				getItems(jobId);
				getExpenses(jobId);
				setTotals();
			} break;
		//From AddTaskPopup
		case 2:
			if (resultCode==RESULT_OK){
				Task tsk = new Task(0, 0, 0, data.getStringExtra("taskName"), Double.valueOf(data.getStringExtra("duration")) , 
						Double.parseDouble(data.getStringExtra("value")));
				tasks.add(tsk);
				 if(tsk.taskName.equals("Callout")){
					 totalTasks += tsk.value;
				 }else{
				     totalTasks += tsk.duration * tsk.value;
				 }
				setTotals();
			} break;
		//From AddItemPopup
		case 3:
			if (resultCode==RESULT_OK){
				Item it = new Item(0, 0, 0, data.getStringExtra("itemName"), Integer.parseInt(data.getStringExtra("quantity")) , Double.parseDouble(data.getStringExtra("cost")));
				items.add(it);
				totalItems += it.cost * it.quantity;
				setTotals();
			} break;
		//From AddExpensePopup
		case 4:
			if (resultCode==RESULT_OK){
				Expense exp = new Expense(0, 0, 0, data.getStringExtra("expType"), data.getStringExtra("expDate"), data.getDoubleExtra("expCost", 0),
						data.getStringExtra("expDescription"), data.getStringExtra("photoPath"));
			  	expenses.add(exp);
			  	totalExps += exp.expCost;
				setTotals();
			} 
			break;
		// From TaskItemExp
		case 5:
			if (resultCode==RESULT_OK){
				System.out.println("On Activity Result - New Invoice");
				tasks = (ArrayList<Task>) data.getSerializableExtra("tasksArray");
				items = (ArrayList<Item>) data.getSerializableExtra("itemsArray");
				expenses = (ArrayList<Expense>) data.getSerializableExtra("expsArray");
				// Work
				if(tasks.size() == 0 ){
					totalTasks = 0;
				}else{
					totalTasks = 0;
					for(Task task : tasks){
						if (!task.dead){
					    	if(task.taskName.equals("Callout")){
								totalTasks += task.value;
					    	}else{
					    		totalTasks += task.duration * task.value;
					    	}
						}
				    }
				}
				// Items
				if(items.size() == 0){
					totalItems = 0;
				}else{
					totalItems = 0;
					for(Item item : items){
						if (!item.dead){
				    	totalItems += item.cost * item.quantity;
						}
					}
				}
				// Expenses
				if(expenses.size() == 0){
					totalExps = 0;
				}else{
					totalExps = 0;
					for(Expense exp : expenses){
						if (!exp.dead){
					  	totalExps += exp.expCost;
						}
				    }
				}
				setTotals();
			}
			break;
			// From ChooseVATPopup	
		case 6:
			if (resultCode==RESULT_OK){
				vatType = data.getStringExtra("vatType");
				finalSubtotal = data.getDoubleExtra("subtotal", 0.0);
				finalVat = data.getDoubleExtra("vat", 0.0);
				finalTotal = data.getDoubleExtra("total", 0.0);
				addInvoice();
				finish();
			}
			break;
		case 7:
			if (resultCode==RESULT_OK){
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
				popup.startAnimation(animationOut);
				popup.setVisibility(View.GONE);
				vatType = data.getStringExtra("vatType");
				finalSubtotal = data.getDoubleExtra("subtotal", 0.0);
				finalVat = data.getDoubleExtra("vat", 0.0);
				finalTotal = data.getDoubleExtra("total", 0.0);
				addInvoice();
				ClientsConnector cliCon = new ClientsConnector(this);
				cliCon.open();
				Cursor cur = cliCon.getClient(clientId);
				cur.moveToFirst();
				int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
				String email = cur.getString(emailCol).replace(" ", "");
				cliCon.close();
				if (!email.equals("") && email.contains("@")){
					String client = clientNameEditText.length()<8 ? clientNameEditText.getText().toString() : clientNameEditText.getText().toString().substring(0, 7);
					String job = jobNameEditText.length()<8 ? jobNameEditText.getText().toString() : jobNameEditText.getText().toString().substring(0, 7);
					emailSend = email;
					String filename = invoiceId + "_inv_"+client+"_"+job+".pdf";
					InvoicePDF obj = new InvoicePDF(clientId, jobNameEditText.getText().toString(), dateIssued.getText().toString(), 
							dateDue.getText().toString(), finalSubtotal, finalVat, finalTotal);
					PDFMaker pdf = new PDFMaker(this, 1, filename, "Thank you for your business", obj);
		    		if (pdf.makeFile()){
		    			file = pdf.build();
		    			checkConStatus();
						sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
		    		} else {
		    			Toast.makeText(this, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();
		    		}
		    		} else {
					Toast.makeText(this, "You do not have an email address for this client. Please check client email and resend", Toast.LENGTH_LONG).show();
				}
			}
			break;
		case 8:
			if (resultCode==RESULT_OK){
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
				popup.startAnimation(animationOut);
				popup.setVisibility(View.GONE);
				vatType = data.getStringExtra("vatType");
				finalSubtotal = data.getDoubleExtra("subtotal", 0.0);
				finalVat = data.getDoubleExtra("vat", 0.0);
				finalTotal = data.getDoubleExtra("total", 0.0);
				String client = clientNameEditText.length()<8 ? clientNameEditText.getText().toString() : clientNameEditText.getText().toString().substring(0, 7);
				String job = jobNameEditText.length()<8 ? jobNameEditText.getText().toString() : jobNameEditText.getText().toString().substring(0, 7);
				String filename = invoiceId + "_inv_"+client+"_"+job+".pdf";
				InvoicePDF obj = new InvoicePDF(clientId, jobNameEditText.getText().toString(), dateIssued.getText().toString(), 
						dateDue.getText().toString(), finalSubtotal, finalVat, finalTotal);
				PDFMaker pdf = new PDFMaker(this, 1, filename, "Thank you for your business", obj);
				if (pdf.makeFile()){
					file = pdf.build();
					startPDF();
				} else {
					Toast.makeText(this, "Not enough storage for PDF", Toast.LENGTH_SHORT).show();
				}
			}
			break;

			default: break;
		}
	}
	
	public void onClick(View view){
		System.out.println("onClick Called");
		switch (view.getId()){
		case R.id.clientNewInvoiceEditText:
			Intent inClient = new Intent(this, SelectClientPopup.class);
			startActivityForResult(inClient, 0);
			break;
		case R.id.jobNewInvoiceEditText:
			Intent inJob = new Intent(this, SelectJobPopup.class);
			inJob.putExtra("passClientId", clientId);
			startActivityForResult(inJob, 1);
			break;
		case R.id.dateIssuedNewInvoiceEditText:
			Calendar cal = Calendar.getInstance();
			DatePickerDialog datePickDiag = new DatePickerDialog(this, odsl, cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			datePickDiag.show();
			break;
		case R.id.dateDueNewInvoiceEditText:
			Calendar cal1 = Calendar.getInstance();
			DatePickerDialog datePickDiag1 = new DatePickerDialog(this, odsl1, cal1.get(Calendar.YEAR),
					cal1.get(Calendar.MONTH), cal1.get(Calendar.DAY_OF_MONTH));
			datePickDiag1.show();
			break;
		case R.id.addTaskNewInvoiceButton:
			passTasksItemsExps(0);
			break;  
		case R.id.addItemNewInvoiceButton:
			passTasksItemsExps(1);
			break;
		case R.id.addExpNewInvoiceButton:
			passTasksItemsExps(2);
			break;
		case R.id.viewTaskNewInvoiceButton:			
			System.out.println("NEW INVOICE - TASKS ?>>>> "+tasks.size());
			passTasksItemsExps(0);
			break;
		case R.id.itemsCountNewInvoiceTextView:
			System.out.println("NEW INVOICE - ITEMS ?>>>> "+items.size());
			passTasksItemsExps(1);
			break;
		case R.id.viewExpNewInvoiceButton:
			System.out.println("NEW INVOICE - EXPS ?>>>> "+expenses.size());
			passTasksItemsExps(2);
			break;
		case R.id.workCountNewInvoiceTextView:			
			System.out.println("NEW INVOICE - TASKS ?>>>> "+tasks.size());
			passTasksItemsExps(0);
			break;
		case R.id.viewItemNewInvoiceButton:
			System.out.println("NEW INVOICE - ITEMS ?>>>> "+items.size());
			passTasksItemsExps(1);
			break;
		case R.id.expensesCountNewInvoiceTextView:
			System.out.println("NEW INVOICE - EXPS ?>>>> "+expenses.size());
			passTasksItemsExps(2);
			break;
		case R.id.sendButtonNewInvoice:	        
			if (popup.getVisibility()==View.GONE){
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDown, null);
				popup.startAnimation(animationIn);
				popup.setVisibility(View.VISIBLE);
			} else {
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
				popup.startAnimation(animationOut);
				popup.setVisibility(View.GONE);
			}
			break;
		case R.id.emailLabelTextView:
			if( clientNameEditText.getText().length() == 0 ||  jobNameEditText.getText().length() == 0 || dateIssued.getText().length() == 0 || dateDue.getText().length() == 0 ){
				validation(clientNameEditText);
				validation(jobNameEditText);
				validation(dateIssued);
				validation(dateDue);
			}else if(tasks.size() == 0 && items.size() == 0 && expenses.size() ==0){
				Toast.makeText(getApplicationContext(), "You cannot have a blank invoice, return to the job and add new invoice", Toast.LENGTH_SHORT).show();
			}else{
				Intent i = new Intent(this, ChooseVATPopup.class);
				i.putExtra("source", 1);
				i.putExtra("total", baseTotal);
				i.putExtra("vatType", vatType);
				startActivityForResult(i, 7);
			}
			break;
		case R.id.printLabelTextView:
			Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
//			if( clientNameEditText.getText().length() == 0 ||  jobNameEditText.getText().length() == 0 || dateIssued.getText().length() == 0 || dateDue.getText().length() == 0 ){
//				validation(clientNameEditText);
//				validation(jobNameEditText);
//				validation(dateIssued);
//				validation(dateDue);
//			}else{
//				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
//				popup.startAnimation(animationOut);
//				popup.setVisibility(View.GONE);
//				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
//				popup.startAnimation(animationOut);
//				popup.setVisibility(View.GONE);
//				makePDF();
//	    		printPDF();
//			}
			break;
		case R.id.pdfLabelTextView:
			if( clientNameEditText.getText().length() == 0 ||  jobNameEditText.getText().length() == 0 || dateIssued.getText().length() == 0 || dateDue.getText().length() == 0 ){
				validation(clientNameEditText);
				validation(jobNameEditText);
				validation(dateIssued);
				validation(dateDue);
			} else if (tasks.size() == 0 && items.size() == 0 && expenses.size() ==0){
				Toast.makeText(getApplicationContext(), "You cannot have a blank invoice, return to the job and add new invoice", Toast.LENGTH_SHORT).show();
			}else{
				Intent i = new Intent(this, ChooseVATPopup.class);
				i.putExtra("source", 1);
				i.putExtra("total", baseTotal);
				i.putExtra("vatType", vatType);
				startActivityForResult(i, 8);
			}
			break;
		default:
			break;
		}
	}
	
	public void passTasksItemsExps (int currentItem){
		Intent in = new Intent(this, TaskItemExp.class);
		in.putExtra("tasksArray", tasks);
		in.putExtra("itemsArray", items);
		in.putExtra("expsArray", expenses);		
		in.putExtra("currentItem", currentItem);
		in.putExtra("loadFrags", 3);
		in.putExtra("source", 2);
		startActivityForResult(in, 5);
	}
	
	public void addInvoice(){
		Log.i("<<<<< MODE", String.valueOf(mode));
		InvoicesConnector inCon = new InvoicesConnector(this);
		TasksConnector tCon = new TasksConnector(this);
		ItemsConnector iCon = new ItemsConnector(this);
		ExpensesConnector eCon = new ExpensesConnector(this);
		int id=0;
		if (in!=null){
			id = in.getIntExtra("inNo", 0);
		}		
		if (!isFixed){
			fixedAmount = 0;
		} else {
			fixedAmount = finalSubtotal;
		}
		Date issued = new Date();
		Date due = new Date();
		try{
			issued = outDate.parse(dateIssued.getText().toString());
			due = outDate.parse(dateDue.getText().toString());
		} catch (ParseException e){
			
		}

		if (mode==3){
			//EditMode
			try {
				status = "draft";
				inCon.updateInvoice(invoiceId, clientId, jobId, 0, inDate.format(issued), inDate.format(due), finalSubtotal,
						finalVat, finalTotal, baseTotal, vatType, finalTotal, fixedAmount, status, terms, notes.getText().toString());
				for (Task t : tasks){
						tCon.updateTask(t.taskID, t.jobID, t.invoiceID, t.taskName, t.duration, "", t.value);
				}
				for (Item i : items){
						iCon.updateItem(i.itemID, i.jobID, 0, i.invoiceID, i.itemName, i.cost, "", i.quantity, 0, 0);
				}
				for (Expense e : expenses){
						eCon.updateExpense(e.expID, e.jobID, clientId,  e.invoiceID, 0, 1, e.expType, e.expDate, e.expCost, 0.0, 0, 
								e.expDescription, e.photoPath);
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		} else {
			//New Mode
			try{
				status = "draft";
				inCon.insertInvoice(clientId, jobId, 0, inDate.format(issued), inDate.format(due), finalSubtotal, finalVat, finalTotal, 
						baseTotal, vatType, finalTotal, fixedAmount, status, terms, notes.getText().toString());
				
				invoiceId = inCon.getNextId();				
				for (Task t : tasks){
//					if (t.jobID==0){
//						tCon.insertTask(jobId, invoiceId, t.taskName, t.duration, "", t.value);
//					} else {
						tCon.updateIds(t.taskID, t.jobID, invoiceId);
					//}
				}				
				for (Item i : items){
//					if (i.jobID==0){
//						iCon.insertItem(jobId, 0, invoiceId, i.itemName, i.cost,  "", i.quantity, 0, 0);
//					} else {
						iCon.updateIds(i.itemID, i.jobID, invoiceId);				
					//}
				}				
				for (Expense e : expenses){
//					if (e.jobID==0){
//						eCon.insertExpense(jobId, clientId, invoiceId, 0, 1, e.expType, e.expDate, e.expCost, 0.0, 0, e.expDescription, e.photoPath);
//					} else {
						eCon.updateIds(e.expID, jobId, clientId, invoiceId);
					//}
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public void setTotals (){
		if (isFixed){
			total.setText("Total: "+String.valueOf(nf.format(fixedAmount)));
			finalTotal = fixedAmount;
		} else {
			finalTotal = totalTasks + totalItems + totalExps;	 
			total.setText("Total: "+String.valueOf(nf.format(finalTotal)));
		}			
		baseTotal = finalTotal;		
		if(vatType.equals("inc")){
			subtotal.setVisibility(View.VISIBLE);
			taxes.setVisibility(View.VISIBLE);
			subtotal.setText("Sub: "+String.valueOf(nf.format(finalTotal - (globalVAT / (globalVAT + 100.0) * finalTotal))));
	    	taxes.setText("VAT: "+String.valueOf(nf.format((globalVAT / (globalVAT + 100.0) * finalTotal))));
	    	total.setText("  Total: "+String.valueOf(nf.format(finalTotal)));
		}else if(vatType.equals("add")){
			subtotal.setVisibility(View.VISIBLE);
			taxes.setVisibility(View.VISIBLE);
			subtotal.setText("Sub: "+String.valueOf(nf.format(finalTotal)));
	    	taxes.setText("VAT: "+String.valueOf(nf.format((finalTotal * (globalVAT + 100.0) / 100.0) - finalTotal)));
	    	total.setText("  Total: "+String.valueOf(nf.format(finalTotal * (globalVAT + 100.0) / 100.0)));
		}else{
			subtotal.setVisibility(View.GONE);
			taxes.setVisibility(View.GONE);
			total.setText("  Total: "+String.valueOf(nf.format(finalTotal)));
		}
		
		double hours=0.0;
		for (Task t : tasks){
			if (!t.dead){
				hours += t.duration;
			}
		}
		workCount.setText("Work: "+String.valueOf(hours)+" Hrs");
		int quantity=0;
		for (Item i : items){
			if (!i.dead){
			quantity += i.quantity;
			}
		}
		itemsCount.setText("Items: "+String.valueOf(quantity));
		int eCount = 0;
		for (Expense e : expenses){
			if (!e.dead){
				eCount++;
			}
		}
		expensesCount.setText("Expenses: "+String.valueOf(eCount));
	}	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_options, menu);
		if (mode==EDITMODE){
			MenuItem it = menu.findItem(R.id.add_option);
			it.setTitle("Save");
		}
		return true;
	}
	
	public boolean blankInvoice(){
		for (Task t : tasks){
			if (!t.dead){
				return false;
			}
		}
		for (Item i : items){
			if (!i.dead){
				return false;
			}
		}
		for (Expense e: expenses){
			if (!e.dead){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_option:
			item.setEnabled(false);
			if( clientNameEditText.getText().length() == 0 ||  jobNameEditText.getText().length() == 0 || dateIssued.getText().length() == 0 || dateDue.getText().length() == 0 ){
				item.setEnabled(true);
				validation(clientNameEditText);
				validation(jobNameEditText);
				validation(dateIssued);
				validation(dateDue);
			}else if(blankInvoice()){
				item.setEnabled(true);
				Toast.makeText(getApplicationContext(), "Set a fixed cost or add Work, Items & Expenses!", Toast.LENGTH_SHORT).show();
			}else{
				//baseTotal = finalTotal;
				Intent i = new Intent(this, ChooseVATPopup.class);
				i.putExtra("source", 1);
				i.putExtra("total", baseTotal);
				i.putExtra("vatType", vatType);
				startActivityForResult(i, 6);
				item.setEnabled(true);
			}
    		return true;
		case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  
	    case R.id.about_option:
	    	return true;
	    case R.id.help_option:
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;	
		case android.R.id.home:
			final Builder builder = new AlertDialog.Builder(this);			
			TextView title = new TextView(this);
			title.setText("Invoice details not saved!");
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			//title.setTextColor(getResources().getColor(R.color.greenBG));
			title.setTextSize(20);
			builder.setCustomTitle(title);
			TextView msg = new TextView(this);
			msg.setText("Are you sure you want to go back?");
			msg.setPadding(10, 10, 10, 10);
			msg.setGravity(Gravity.CENTER);
			msg.setTextSize(18);
			builder.setView(msg);
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
			        finish();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			AlertDialog dialog = builder.create();
			if (tasks.size() !=0 || items.size() !=0 || expenses.size() !=0|| jobNameEditText.getText().length()!=0
					|| notes.getText().length()!=0){
				dialog.show();
			}else{
				finish();
			}
	    	return true;
		case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void validation (final EditText text){
		if( text.getText().length() == 0 ){
			   text.setError( "Input is required!" );
		}else{
			text.setError(null);
		}
		
		text.addTextChangedListener(new TextWatcher() {	
			public void afterTextChanged(Editable s) {
				if( text.getText().length() == 0 ){
					text.setError( "Input is required!" );
				}else{
					text.setError(null);
				}
			}
		 
			public void beforeTextChanged(CharSequence s, int start, 
		     int count, int after) {	  
			}
		 
			public void onTextChanged(CharSequence s, int start, 
		     int before, int count) {  
			}
		});
	}
	
	final OnDateSetListener odsl = new OnDateSetListener() {
		public void onDateSet(DatePicker arg0, int year, int month, int dayOfMonth) {
			month++;
			String dayStr = String.valueOf(dayOfMonth);
			String monthStr = String.valueOf(month);
			if (dayStr.length()==1){
				dayStr = "0"+ dayStr;
			}
			if (monthStr.length()==1){
				monthStr = "0"+ monthStr;
			}
			dateIssued.setText(dayStr + "/" + monthStr + "/" + year);
		}
	};
		
	final OnDateSetListener odsl1 = new OnDateSetListener() {
		public void onDateSet(DatePicker arg0, int year, int month, int dayOfMonth) {
			month++;
			String dayStr = String.valueOf(dayOfMonth);
			String monthStr = String.valueOf(month);
			if (dayStr.length()==1){
				dayStr = "0"+ dayStr;
			}
			if (monthStr.length()==1){
				monthStr = "0"+ monthStr;
			}
			dateDue.setText(dayStr + "/" + monthStr + "/" + year);
		}
	};
		
	public void startPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		
		if (file.exists()) {
	        Uri path = Uri.fromFile(file);
	        Intent intent = new Intent(Intent.ACTION_VIEW);
	        intent.setDataAndType(path, "application/pdf");
	        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	
	        try {
	            startActivity(intent);
	        } 
	        catch (ActivityNotFoundException e) {
	            Toast.makeText(this, "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
	        }
		}
	}
	
	public void checkConStatus (){
		ConnectivityManager conMngr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		android.net.NetworkInfo wifi = conMngr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		android.net.NetworkInfo mobile = conMngr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if(wifi.isConnected() || mobile.isConnected()){
			emailPDF();
		}else{
			final Builder builder = new AlertDialog.Builder(this);			
			TextView title = new TextView(this);
			title.setText("No Internet Connection!");
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			//title.setTextColor(getResources().getColor(R.color.greenBG));
			title.setTextSize(20);
			builder.setCustomTitle(title);
			TextView msg = new TextView(this);
			msg.setText("No Internet connection is available. If you choose to send the Invoice, the email will stay in your Email Client Outbox till the device gets connected to the internet. Send email?");
			msg.setPadding(10, 10, 10, 10);
			msg.setGravity(Gravity.CENTER);
			msg.setTextSize(18);
			builder.setView(msg);
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					emailPDF();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}
		
	public void emailPDF(){
		emailStarted = true;
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		String[] mailto = {emailSend};
		i.putExtra(Intent.EXTRA_EMAIL, mailto);
		i.putExtra(Intent.EXTRA_SUBJECT, "Your Invoice");
		i.putExtra(Intent.EXTRA_TEXT, "Hello "+clientNameEditText.getText().toString()+", your invoice has been attached to this email.");
		i.putExtra(Intent.EXTRA_STREAM,
			Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ java.io.File.separator + "BeansOne"+ java.io.File.separator + file)));
		i.setType("application/pdf");
		startActivity(Intent.createChooser(i, "Send Email"));
	}
	
	public void printPDF(){
		Uri doc = Uri.parse(Environment.getExternalStorageDirectory() + java.io.File.separator + "BeansOne" +java.io.File.separator + file);
		File file = new File(doc.getPath());
		Intent printIntent = new Intent(this, PrintDialog.class);
    	printIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
    	printIntent.putExtra("title", "Invoice for "+ clientNameEditText.getText().toString());
    	startActivity(printIntent);
	}
}