package uk.co.beanssystems.invoices;

import java.util.ArrayList;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.InvoicesConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.settings.Settings;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class InvoicesList extends SherlockActivity {
	private ListView invoicesListView;
	private EditText searchEditText;
	private ImageView clearSearch;
	ArrayList<InvoicesListItem> invoicesArrayList = null, invoicesFixed = null;
	private ArrayList<String> invoicesSearchArray;
	private Cursor invoiceCur, clientCur, jobCur;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoices_list);
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.invoice);
	    bar.setDisplayHomeAsUpEnabled(true);
    }
	
	@Override
	protected void onStart() {
		super.onStart();
		setInvoicesList ();	
	}
	
	public void setInvoicesList () {
		InvoicesConnector invoicesCon = new InvoicesConnector(this);
        ClientsConnector clientsCon = new ClientsConnector(this);
        JobsConnector jobsCon = new JobsConnector(this);
        invoicesArrayList = new ArrayList<InvoicesListItem>();
        invoicesFixed = new ArrayList<InvoicesListItem>();
        invoicesSearchArray = new ArrayList<String>();
        searchEditText = (EditText)findViewById(R.id.invListSearch);
		clearSearch = (ImageView)findViewById(R.id.clearSearchInv);
        invoicesListView = (ListView)findViewById(R.id.invoices_list);
        
        invoicesCon.open();
        clientsCon.open();
        jobsCon.open();
        invoiceCur = invoicesCon.getAllInvoices();
        if(invoiceCur.getCount() > 0){
        	for (int i=0; i<4; i++){
        		invoiceCur.moveToFirst();
        		int idCol = invoiceCur.getColumnIndex(InvoicesConnector.INVOICEID);
    	        int clientIdCol = invoiceCur.getColumnIndex(InvoicesConnector.CLIENTID);
    	        int jobIdCol = invoiceCur.getColumnIndex(InvoicesConnector.JOBID);
    	        int dateDueCol = invoiceCur.getColumnIndex(InvoicesConnector.DATEDUE);
    	        int outstandingCol = invoiceCur.getColumnIndex(InvoicesConnector.OUTSTANDING);
    	        int totalDueCol = invoiceCur.getColumnIndex(InvoicesConnector.TOTALDUE);
    			int statusCol = invoiceCur.getColumnIndex(InvoicesConnector.STATUS);
    				
        		do{
        			int id = invoiceCur.getInt(idCol);
        			int clientId = invoiceCur.getInt(clientIdCol);
        			int jobId = invoiceCur.getInt(jobIdCol);
                    String dateDue = invoiceCur.getString(dateDueCol);
                    double totalDue = invoiceCur.getDouble(totalDueCol);
                	double outstanding = invoiceCur.getDouble(outstandingCol);
                    String status = invoiceCur.getString(statusCol);
                    String paidStatus;               
                    if (outstanding == 0.0){
                    	paidStatus = "Paid";
                    	outstanding = totalDue;
                    	Log.e("Outstanding", String.valueOf(outstanding));
                    	System.out.println("Outstanding"+ outstanding );
                    }else{
                    	paidStatus = "Outstanding";
                    }
                    
        			clientCur = clientsCon.getClient(clientId);
        			clientCur.moveToFirst();
        			int clientForenameCol = clientCur.getColumnIndex(ClientsConnector.FORENAME);
        			int clientSurnameCol = clientCur.getColumnIndex(ClientsConnector.SURNAME);
        			int clientCompanyCol = clientCur.getColumnIndex(ClientsConnector.COMPANY);
        			String clientName = clientCur.getString(clientForenameCol) + " " + clientCur.getString(clientSurnameCol);
        			String company = clientCur.getString(clientCompanyCol);
        			int icon;
    	        	if (company.compareTo("Private")==0){
    	        		icon = R.drawable.user;
    	        	} else {
    	        		icon = R.drawable.work_user;
    	        	}
        	        
        			jobCur = jobsCon.getJob(jobId);
        			jobCur.moveToFirst();
        			int jobNameCol = jobCur.getColumnIndex(JobsConnector.JOBNAME);
        			String jobName = jobCur.getString(jobNameCol);
        			String string = clientName +" "+jobName +" "+ dateDue;
        			
                  	if (status.equals("overdue") && i==0){
                  		invoicesSearchArray.add(string.toLowerCase());
                    	InvoicesListItem invoice = new InvoicesListItem(id, icon, clientId, clientName, jobId, jobName, dateDue, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                    	invoicesFixed.add(invoice);
                    	System.out.println("OVERDUE ADDED");
                  	} else if (status.equals("draft")   && i==1){
                  		invoicesSearchArray.add(string.toLowerCase());
                  		InvoicesListItem invoice = new InvoicesListItem(id, icon, clientId, clientName, jobId, jobName, dateDue, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                    	invoicesFixed.add(invoice);
                    	System.out.println("DRAFT ADDED");
                  	} else if (status.equals("sent") && i==2){
                  		invoicesSearchArray.add(string.toLowerCase());
                  		InvoicesListItem invoice = new InvoicesListItem(id, icon, clientId, clientName, jobId, jobName, dateDue, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                    	invoicesFixed.add(invoice);
                    	System.out.println("SENT ADDED");
                  	} else if (status.equals("paid") && i==3){
                  		invoicesSearchArray.add(string.toLowerCase());
                  		InvoicesListItem invoice = new InvoicesListItem(id, icon, clientId, clientName, jobId, jobName, dateDue, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                    	invoicesFixed.add(invoice);
                    	System.out.println("PAID ADDED");
                  	}else{
                  		System.out.println("STATUS +++++++++++++++++"+status);
                  	}      			                  	    
                } while(invoiceCur.moveToNext());
        	}
        }
        	
    	invoicesListView.setEmptyView(findViewById(R.id.noInvsInvoicesListLayout));
    	InvoicesAdapter invoicesAdapter = new InvoicesAdapter(this, R.layout.activity_invoices_list_row, invoicesArrayList);
    	invoicesListView.setAdapter(invoicesAdapter);
    	invoicesListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				InvoicesListItem ili = (InvoicesListItem) invoicesListView.getItemAtPosition(position);
				Intent i = new Intent(getApplicationContext(), ViewInvoice.class);
				i.putExtra("passInvoiceID", ili.id);
				i.putExtra("passClientID", ili.clientId);
				i.putExtra("passJobID", ili.jobId);
				i.putExtra("passJobName", ili.jobName);
				i.putExtra("passDateDue", ili.dateDue);			
				i.putExtra("passPaidStatus", ili.paidStatus);
				startActivity(i);
			}
    	});
        
        invoicesCon.close();
        clientsCon.close();
        jobsCon.close();
        
        searchEditText.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {	
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				invoicesArrayList.clear();
				
				String search = searchEditText.getText().toString().toLowerCase();
				
				if(searchEditText.getText().toString().length() == 0){
					clearSearch.setVisibility(View.GONE);
				}else{
					clearSearch.setVisibility(View.VISIBLE);
				}
				
				boolean exist = false;		

				int searchListLength = invoicesSearchArray.size();
				for (int i = 0; i < searchListLength; i++) {
					if (invoicesSearchArray.get(i).contains(search)) {	
						add (i);
						exist = true;
					}
				} 

				if (exist == false) {
					Toast.makeText(getApplicationContext(),
							search + " Not Found" + "",
							Toast.LENGTH_SHORT).show();
				}
				
				InvoicesAdapter invoicesAdapterT = new InvoicesAdapter(InvoicesList.this, R.layout.activity_invoices_list_row, invoicesArrayList);
	        	invoicesListView.setAdapter(invoicesAdapterT);
			}
		});
	}
	
	public void add (int i){
		InvoicesListItem j = (InvoicesListItem) invoicesFixed.get(i); 
		invoicesArrayList.add(j);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    MenuItem sort1 = menu.findItem(R.id.menuSort1);
	    MenuItem sort2 = menu.findItem(R.id.menuSort2);
	    MenuItem sort3 = menu.findItem(R.id.menuSort3);
	    MenuItem sort4 = menu.findItem(R.id.menuSort4);
	    sort1.setTitle("Overdue");
	    sort2.setTitle("Draft");
	    sort3.setTitle("Sent");
	    sort4.setTitle("Paid");
	    return true;
	}
	  
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case android.R.id.home:
	    	Intent intent = new Intent(this, MainActivity.class);            
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	        startActivity(intent);
	        finish();
	    	return true;
	    	
	    case R.id.search_option:
	    	RelativeLayout item1 = (RelativeLayout)findViewById(R.id.invListLayout);
	    	if(item1.getVisibility() == View.GONE){
	    		item1.setVisibility(View.VISIBLE);
	    	}else{
	    		item1 .setVisibility(View.GONE);
	    		searchEditText.setText("");
	    	}  	
	    	return true;
	    	
	    case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  
	        
	    case R.id.about_option:
	    	return true;
	    	
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    case R.id.menuSortAll:
	    	sortList("all");
	    	return true;
	    case R.id.menuSort1:
	    	sortList("overdue");
	    	return true;
	    case R.id.menuSort2:
	    	sortList("draft");
	    	return true;
	    case R.id.menuSort3:
	    	sortList("sent");
	    	return true;
	    case R.id.menuSort4:
	    	sortList("paid");
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	public void sortList (String sort){
		invoicesArrayList.clear();
    	for (int i = 0; i < invoicesFixed.size(); i++) {
    		if(sort.equals("all")){
    			add (i);
    		}else{
    			InvoicesListItem j = (InvoicesListItem) invoicesFixed.get(i); 
    			if (j.status.equals(sort)) {	
    				add (i);
    			}
    		}
		} 
    	InvoicesAdapter invoicesAdapterS = new InvoicesAdapter(InvoicesList.this, R.layout.activity_invoices_list_row, invoicesArrayList);
    	invoicesListView.setAdapter(invoicesAdapterS);
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.clearSearchInv:			
				searchEditText.setText("");
			break;
		case R.id.newInvoicesListButton:			
			Intent i = new Intent(this, NewInvoice.class);
	    	startActivity(i);
          	break;
		  
		default:
			break;
		}
	}
}