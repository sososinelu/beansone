package uk.co.beanssystems.clients;

import java.text.NumberFormat;
import java.util.Locale;

import com.actionbarsherlock.app.SherlockFragment;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.InvoicesConnector;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ViewClientInfo extends SherlockFragment {
	private TextView name, company, mobile, phone, email, address, postcode, notes, outstandingAmount;
	private ImageView clientImage;
	private LinearLayout mobileLayout, phoneLayout, emailLayout, addressLayout, notesLayout;
	private Context context;
	private static String company1="";
	private Cursor cur, invCur;
	private int clientID;
	
	public ViewClientInfo(){
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View fragView = inflater.inflate(R.layout.activity_view_client_info, container, false);
		return fragView;
	}
	@Override
	public void onActivityCreated(Bundle instance){
		super.onActivityCreated(instance);
		context = getActivity().getApplicationContext();
		clientID = getArguments().getInt("id");
		outstandingAmount = (TextView)getActivity().findViewById(R.id.outstandingAmountViewClientTextView);
		clientImage =(ImageView)getActivity().findViewById(R.id.clientNewClientImageView); 
		name = (TextView)getActivity().findViewById(R.id.clientNameViewClientTextView);
		company = (TextView)getActivity().findViewById(R.id.companyValueViewClientTextView);
		mobile = (TextView)getActivity().findViewById(R.id.mobileValueViewClientTextView);
		phone = (TextView)getActivity().findViewById(R.id.phoneValueViewClientTextView);
		email = (TextView)getActivity().findViewById(R.id.emailValueViewClientTextView);
		address = (TextView)getActivity().findViewById(R.id.addressValueViewClientTextView);
		postcode = (TextView)getActivity().findViewById(R.id.postcodeValueViewClientTextView);
		notes = (TextView)getActivity().findViewById(R.id.notesValueViewClientTextView);	
		mobileLayout = (LinearLayout)getActivity().findViewById(R.id.mobileLayout);
		phoneLayout = (LinearLayout)getActivity().findViewById(R.id.phoneLayout);
		emailLayout = (LinearLayout)getActivity().findViewById(R.id.emailLayout);
		addressLayout = (LinearLayout)getActivity().findViewById(R.id.addressLayout);
		notesLayout = (LinearLayout)getActivity().findViewById(R.id.notesLayout);
		
		mobile.setOnClickListener(clickListener);
		phone.setOnClickListener(clickListener);
		email.setOnClickListener(clickListener);
		address.setOnClickListener(clickListener);
		postcode.setOnClickListener(clickListener);
	}

	@Override
	public void onStart(){
		super.onStart();
		ClientsConnector cliCon = new ClientsConnector(context);
		cliCon.open();
		cur = cliCon.getClient(clientID);
		cur.moveToFirst();
		int titleCol = cur.getColumnIndex(ClientsConnector.TITLE);
		int forenameCol = cur.getColumnIndex(ClientsConnector.FORENAME);
		int surnameCol = cur.getColumnIndex(ClientsConnector.SURNAME);
		int companyCol = cur.getColumnIndex(ClientsConnector.COMPANY);
		int mobileCol = cur.getColumnIndex(ClientsConnector.MOBILE);
		int phoneCol = cur.getColumnIndex(ClientsConnector.PHONE);
		int emailCol = cur.getColumnIndex(ClientsConnector.EMAIL);
		int addressCol = cur.getColumnIndex(ClientsConnector.ADDRESS);
		int postcodeCol = cur.getColumnIndex(ClientsConnector.POSTCODE);
		int notesCol = cur.getColumnIndex(ClientsConnector.NOTES);
		
		checkField(cur.getString(mobileCol), mobile, mobileLayout);
		checkField(cur.getString(phoneCol), phone, phoneLayout);
		checkField(cur.getString(emailCol), email, emailLayout);
		checkField(cur.getString(addressCol), address, addressLayout);
		checkField(cur.getString(postcodeCol), postcode, addressLayout);
		checkField(cur.getString(notesCol), notes, notesLayout);
		
		name.setText(cur.getString(titleCol)+ " " + cur.getString(forenameCol) + " " + cur.getString(surnameCol));
		company1 = cur.getString(companyCol);
		company.setText(company1);
		
		cliCon.close();
		int icon;
    	if (company1.compareTo("Private")==0){
    		icon = R.drawable.user;
    	} else {
    		icon = R.drawable.work_user;
    	}
    	clientImage.setImageResource(icon); 
    	
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.UK);
		InvoicesConnector invCon = new InvoicesConnector(context);
		invCon.open();
		double total = 0;
		outstandingAmount.setText(nf.format(total));
		try{
			invCur = invCon.getInvoicesByClient(clientID);
			invCur.moveToFirst();
			int outstandingCol = invCur.getColumnIndex(InvoicesConnector.OUTSTANDING);
			do {
				double outstanding = invCur.getDouble(outstandingCol);
				total += outstanding;
			}while(invCur.moveToNext());
	        outstandingAmount.setText(nf.format(total));
		} catch (Exception e){
			System.out.println("No Invoices for Client");
		}
        invCon.close();
	}
	
	public void checkField (String string, TextView textView, LinearLayout linearLayout) {
		System.out.println(string);
		if("".equals(string)){
			linearLayout.setVisibility(View.GONE);
		} else {
			linearLayout.setVisibility(View.VISIBLE);
			if(textView.getId() == notes.getId()){
				textView.setText(string);
			}else{
				textView.setText(string);
				textView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
			}
		}
	}
	
	OnClickListener clickListener = new OnClickListener() {
		public void onClick(View view){
			switch (view.getId()){
			case R.id.mobileValueViewClientTextView:
				try {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("tel", mobile.getText().toString(), null)));
				} catch (Exception e){
				}
				break;
			case R.id.phoneValueViewClientTextView:
				try {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("tel", phone.getText().toString(), null)));
				} catch (Exception e){
				}
				break;
			case R.id.emailValueViewClientTextView:
				try {
					Intent intent = new Intent(Intent.ACTION_SEND);
		            intent.setType("plain/text");
		            intent.putExtra(Intent.EXTRA_EMAIL,new String[] { email.getText().toString() });

		            startActivity(Intent.createChooser(intent, "Title of the chooser dialog"));
				} catch (Exception e){
				}
				break;
			case R.id.addressValueViewClientTextView:
				setAdressMap();
				break;
			case R.id.postcodeValueViewClientTextView:
				setAdressMap();
				break;
			default:
				break;
			}
		}
	};
	
	public void setAdressMap () {
		try {
			String address1 = address.getText().toString() + " " + postcode.getText().toString(); // Get address
			address1 = address1.replace(" ", "+");
			Intent geoIntent = new Intent (android.content.Intent.ACTION_VIEW, Uri.parse ("geo:0,0?q=" + address1)); // Prepare intent
			startActivity(geoIntent);	// Initiate lookup
		} catch (Exception e){
			Toast.makeText(getSherlockActivity(), "No Map Application Installed!", Toast.LENGTH_SHORT).show();
		}
	}
}
