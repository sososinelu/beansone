package uk.co.beanssystems.clients;

import java.util.ArrayList;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.quotes.QuotesAdapter;
import uk.co.beanssystems.quotes.QuotesList;
import uk.co.beanssystems.quotes.QuotesListItem;
import uk.co.beanssystems.settings.Settings;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
//import android.widget.Toast;
import android.widget.ViewFlipper;

public class ClientList extends SherlockActivity {
	private ListView lvClient;
	private EditText searchEditText;
	private ImageView clearSearch;
	ArrayList<ClientsListItem> clients = null, clientsFixed = null;
	private Cursor cur;
	private static final String CLIENTID = "clientID", FORENAME = "forename", SURNAME = "surname", COMPANY = "company";
	private ArrayList<String> clientsSearchArray;
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		setContentView(R.layout.activity_clients_list);
		
		ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.clients);
	    bar.setDisplayHomeAsUpEnabled(true);
		lvClient = (ListView)findViewById(R.id.clients_list);
		searchEditText = (EditText)findViewById(R.id.cliListSearch);
		clearSearch = (ImageView)findViewById(R.id.clearSearchClients);
        
	}
	
	public void onStart(){
		super.onStart();
		refresh();
	}
	
	public void refresh(){
		clients = new ArrayList<ClientsListItem>();
		clientsFixed = new ArrayList<ClientsListItem>();
		clientsSearchArray = new ArrayList<String>();

        ClientsConnector cliCon = new ClientsConnector(this);
        cliCon.open();
	    cur = cliCon.getClientOrderSurname();
	    if(cur.getCount() > 0){
	        cur.moveToFirst();
	        do{
	        	int idCol = cur.getColumnIndex(CLIENTID);	
	        	int forenameCol = cur.getColumnIndex(FORENAME);
	        	int surnameCol = cur.getColumnIndex(SURNAME);
	        	int companyCol = cur.getColumnIndex(COMPANY);
	        	int id = cur.getInt(idCol);
	        	String forename = cur.getString(forenameCol);
	        	String surname = cur.getString(surnameCol);
	        	String company = cur.getString(companyCol);
	        	
	        	String string = forename+" "+surname;
	        	clientsSearchArray.add(string.toLowerCase());
	        	
	        	int icon;
	        	if (company.compareTo("Private")==0){
	        		icon = R.drawable.user;
	        	} else {
	        		icon = R.drawable.work_user;
	        	}
	        	
	        	ClientsListItem item = new ClientsListItem(id, icon, forename+" "+surname , company);
	        	clients.add(item);
	        	clientsFixed.add(item);
	        	
	        }while(cur.moveToNext());
	    }
	    
	    lvClient.setEmptyView(findViewById(R.id.noClientsListLayout));
        ClientsAdapter adapterToday = new ClientsAdapter(this, R.layout.activity_clients_list_row, clients);
        lvClient.setAdapter(adapterToday);
        lvClient.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ClientsListItem c = (ClientsListItem) lvClient.getItemAtPosition(position);
				Intent i = new Intent(getApplicationContext(), ViewClient.class);
				i.putExtra("cName", c.clientName);
				i.putExtra("cCompany", c.companyName);
				i.putExtra("clientID", c.id);
				startActivity(i);;			
			}
        });
	        

        cliCon.close();
        
        searchEditText.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// Abstract method
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				clients.clear();

				String search = searchEditText.getText().toString().toLowerCase();
				
				if(searchEditText.getText().toString().length() == 0){
					clearSearch.setVisibility(View.GONE);
				}else{
					clearSearch.setVisibility(View.VISIBLE);
				}
				
				//boolean exist = false;		

				int searchListLength = clientsSearchArray.size();
				for (int i = 0; i < searchListLength; i++) {
					if (clientsSearchArray.get(i).contains(search)) {	
						add (i);
						//exist = true;
					}
				} 
//				if (exist == false) {
//				}
				
				ClientsAdapter adapterT = new ClientsAdapter(ClientList.this, R.layout.activity_clients_list_row, clients);
		        lvClient.setAdapter(adapterT);
			}
		});
	}
	
	public void add (int i){
		ClientsListItem c = (ClientsListItem) clientsFixed.get(i);
		clients.add(c);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.list_menu, menu);
	    MenuItem sort1 = menu.findItem(R.id.menuSort1);
	    MenuItem sort2 = menu.findItem(R.id.menuSort2);
	    MenuItem sort3 = menu.findItem(R.id.menuSort3);
	    MenuItem sort4 = menu.findItem(R.id.menuSort4);
	    sort1.setTitle("Private");
	    sort2.setTitle("Business");
	    sort3.setVisible(false);
	    sort4.setVisible(false);
	    return true;
	}
	  
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) { 
	    case android.R.id.home:
	    	Intent intent = new Intent(this, MainActivity.class);            
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	        startActivity(intent);
	        finish();
	    	return true;	    
	    case R.id.search_option:
	    	RelativeLayout item1 = (RelativeLayout)findViewById(R.id.cliListLayout);
	    	if(item1.getVisibility() == View.GONE){
	    		item1.setVisibility(View.VISIBLE);
	    	}else{
	    		item1 .setVisibility(View.GONE);
	    		searchEditText.setText("");
	    	}  	
	    	return true;
	    case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  	        
	    case R.id.about_option:
	    	return true;	    	
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;	
	    case R.id.menuSortAll:
	    	sortList("all");
	    	return true;
	    case R.id.menuSort1:
	    	sortList("Private");
	    	return true;
	    case R.id.menuSort2:
	    	sortList("Business");
	    	return true;
	    
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	public void sortList (String sort){
		clients.clear();
    	for (int i = 0; i < clientsFixed.size(); i++) {
    		ClientsListItem c = (ClientsListItem) clientsFixed.get(i);
    		System.out.println("<<< Company Name >>> "+c.companyName);
    		if(sort.equals("all")){
    			System.out.println("<<< All >>> "+sort);
    			add(i);
    		}else if(sort.equals("Private")){
    			System.out.println("<<< Private >>> "+sort);
    			if(c.companyName.equals("Private")){
    				add(i);
    			}
    		}else if (sort.equals("Business")){
    			if(c.companyName.equals("Private")){
    				
    			}else{
    				add(i);
    			}
    			System.out.println("<<< Business >>> "+sort);
    		}
		} 
    	ClientsAdapter adapterS = new ClientsAdapter(ClientList.this, R.layout.activity_clients_list_row, clients);
        lvClient.setAdapter(adapterS);
	}
	
	public void onClick(View view){
		switch (view.getId()){
		case R.id.clearSearchClients:			
				searchEditText.setText("");
			break;
		case R.id.newClientsListButton:			
			Intent in = new Intent(this, NewClient.class);
	    	startActivity(in);
          	break;
		default:
			break;
		}
	}
}
