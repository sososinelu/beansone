package uk.co.beanssystems.clients;

public class ClientsListItem {
	public int id;
	public int icon;
	public String clientName;
	public String companyName;
	
	public ClientsListItem(){
		super();
	}
	
		public ClientsListItem(int id, int icon, String clientName, String companyName){
			super();
			this.id = id;
			this.icon = icon;
			this.clientName = clientName;
			this.companyName = companyName;
		}
}
