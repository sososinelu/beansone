package uk.co.beanssystems.clients;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.actionbarsherlock.app.SherlockFragment;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.jobs.JobsAdapter;
import uk.co.beanssystems.jobs.JobsListItem;
import uk.co.beanssystems.jobs.NewJob;
import uk.co.beanssystems.jobs.ViewJob;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import android.widget.AdapterView.OnItemClickListener;

public class ViewClientJobs extends SherlockFragment {
	private ListView lvToday;
	ArrayList<JobsListItem> jobsToday = null;
	Button newJob;
	private Cursor cur;
	private JobsAdapter adapterToday;
	private Context context;
	private int clientID;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        clientID = getArguments().getInt("id");
        System.out.println(clientID);
        loadJobs();
    }
	
	public void loadJobs(){
		JobsConnector jobs = new JobsConnector(context);
		ArrayList<JobsListItem> jobsList = null;
		jobsToday = new ArrayList<JobsListItem>();
        for (int i=0;i<4;i++){
        	switch (i){
	        	case 0:
	        		jobsList = jobs.getJobsCurrent(clientID);
	        		break;
	        	case 1:
	        		jobsList = jobs.getJobsToday(clientID);
	        		break;
	        	case 2:
	        		jobsList = jobs.getJobsWeek(clientID);
	        		break;
	        	case 3:
	        		jobsList = jobs.getJobsLater(clientID);
	        		break;
        		default:break;
        	}

			for (JobsListItem job : jobsList){
	        	jobsToday.add(job);
	        }
        }

	}
	OnClickListener clickListener = new OnClickListener() {
	    public void onClick(final View v) {
	        switch(v.getId()) {
	           case R.id.newJobsListButton:
					Intent i = new Intent(context, NewJob.class);
					i.putExtra("vcjclientID", clientID);
					startActivity(i);
	              break;
	           default:
		          System.out.println("Unknown");
	        	   break;
	        }
	    }
	};
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View fragView = inflater.inflate(R.layout.activity_view_client_jobs, container, false);
		return fragView;
	}
	
	@Override
	public void onActivityCreated(Bundle instance){
		super.onActivityCreated(instance);
        lvToday = (ListView)getActivity().findViewById(R.id.view_client_jobs_list);
        newJob = (Button)getActivity().findViewById(R.id.newJobsListButton);
        newJob.setOnClickListener(clickListener);
	}
	
	@Override
	public void onStart(){
		super.onStart();
		loadJobs();
		lvToday.setEmptyView(getActivity().findViewById(R.id.noJobsViewClientJobLayout));
		adapterToday = new JobsAdapter(context, R.layout.activity_jobs_list_row, jobsToday);
		adapterToday.notifyDataSetChanged();
        lvToday.setAdapter(adapterToday);
        lvToday.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				JobsListItem j = (JobsListItem) lvToday.getItemAtPosition(position);
				Intent i = new Intent(context, ViewJob.class);
				i.putExtra("jobID", j.id);
				i.putExtra("jobName", j.jobName);
				startActivity(i);
			}
        });
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance(Locale.UK);
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
}