package uk.co.beanssystems.clients;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.settings.Settings;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class NewClient extends SherlockActivity {
	private static final int REQUEST_CODE_PICK_CONTACTS = 1;
	private Uri uriContact;
	private String contactID;   // Imported contact unique ID
	private String address = "", postcode = "", className = "", tempName = ""; 
	Spinner cTitle;
	EditText cForename, cSurname, cMobile, cPhone, cEmail, cCompany, cAddress, cPostcode, cNotes;
	private ComponentName sourceClass;
	ClientsConnector cliCon = null;
	int clientID;
	private static final String EMAIL_PATTERN = "([\\w-\\.]+)@((?:[\\w]+\\.)+)([a-zA-Z]{2,4})";

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_new_client);
	    
        ActionBar bar = getSupportActionBar();
	    bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_HOME);
	    bar.setDisplayUseLogoEnabled(true);
	    bar.setLogo(R.drawable.clients);
	    bar.setDisplayHomeAsUpEnabled(true);
	    
		cTitle = (Spinner)findViewById(R.id.titleNewClientSpinner);
		cForename = (EditText)findViewById(R.id.forenameNewClientEditText);
		cSurname = (EditText)findViewById(R.id.surnameNewClientEditText);
		cMobile = (EditText)findViewById(R.id.mobileNewClientEditText);
		cPhone = (EditText)findViewById(R.id.phoneNewClientEditText);
		cEmail = (EditText)findViewById(R.id.emailNewClientEditText);
		cCompany = (EditText)findViewById(R.id.companyNameNewClientEditText);
		cAddress = (EditText)findViewById(R.id.addressNewClientEditText);
		cPostcode = (EditText)findViewById(R.id.postcodeNewClientEditText);
		cPostcode.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(8)});
		cNotes = (EditText)findViewById(R.id.notesNewClientEditText);

		try {
			sourceClass = getCallingActivity();
			className = sourceClass.getShortClassName().replace("uk.co.beanssystems.clients.", "");
			if (className.compareTo("ViewClient")==0){
				Intent i = getIntent();
				clientID = i.getIntExtra("clientID", 0);				
				ArrayAdapter myAdap = (ArrayAdapter) cTitle.getAdapter(); // Cast to an ArrayAdapter
	            int spinnerPosition = myAdap.getPosition(i.getStringExtra("title"));
	            //set the default according to value
	            cTitle.setSelection(spinnerPosition);	            
				cForename.setText(i.getStringExtra("forename"));
				cSurname.setText(i.getStringExtra("surname"));
				tempName = cForename.getText().toString() + " " + cSurname.getText().toString();
				cMobile.setText(i.getStringExtra("mobile"));
				cPhone.setText(i.getStringExtra("phone"));
				cEmail.setText(i.getStringExtra("email"));
				cCompany.setText(i.getStringExtra("company"));
				cAddress.setText(i.getStringExtra("address"));
				cPostcode.setText(i.getStringExtra("postcode"));
				cNotes.setText(i.getStringExtra("notes"));
			}
		} catch (Exception e){
			sourceClass = null;
		}
		
		//insertClients();
	}
	
	// Insert clients directly to database
	public void insertClients () {
		ClientsConnector sd = new ClientsConnector(this);
		sd.insertClient("Mr", "Stephen", "Jones", "Private", "38, Oakwood Avenue, Cardiff", "CF14 7AD", "07812345678", "02920123456", "s.jones@iemail.com", "");
		sd.insertClient("Mrs", "Alison", "Richards", "Private", "16, Evergreen St, Newport", "CF10 6BS", "07785467982", "01564123456", "allison56@myemail.com", "");
		sd.insertClient("Mr", "Simon", "Evans", "MobileTech Ltd", "Unit 3, Redmond Ind. Estate, Cardiff", "CF14 4TG", "07745879767", "02920888445", "simon.evans@mobiletech.co.uk", "");
		sd.insertClient("Mr", "Alun", "Mcoy", "SoundRise", "45 Harriet St, Cardiff", "CF45 4BW", "07578956412", "02920256354", "alun@soundrise.co.uk", "");
	}
	
	public void validation (final EditText text){
		if(text.getText().length()==0){
			text.setError("Please Complete Field");
		} else {
			text.setError(null,null);
		}
		text.addTextChangedListener(new TextWatcher() {	 
		   public void afterTextChanged(Editable s) {
			   if(text.getText().length()==0){
				   text.setError("Please Complete Field");
			   }else{
				   text.setError(null,null);
			   }
		   }
		 
		   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		 
		   public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
	
	public void phoneNoValidation (final EditText text){
		if(text.getText().length()==0){
			text.setError("Need a Mobile or Phone no");
		} else {
			text.setError(null,null);
		}
		text.addTextChangedListener(new TextWatcher() {	 
		   public void afterTextChanged(Editable s) {
			   if(text.getText().length()!=0){
				   if(text.getId() == R.id.mobileNewClientEditText){
					   cPhone.setError(null,null);
				   }else{
					   cMobile.setError(null,null);
				   }				   
   			   }else{
   				   text.setError("Need a Mobile or Phone no");
   			   }
		   }
		   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}		 
		   public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
	
	  @Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.menu_options, menu);
		    if (className.compareTo("ViewClient")==0){
				MenuItem it = menu.findItem(R.id.add_option);
				it.setTitle("Save");
			}
	    return true;
	  }
	  	  
	  @Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	      switch (item.getItemId()) {
	      case R.id.add_option:
	    	  item.setEnabled(false);
	    	  if (cEmail.getText().length()!=0){
	    		   	Pattern pattern = Pattern.compile(EMAIL_PATTERN);
	    			String e = cEmail.getText().toString();
	    			Matcher matcher = pattern.matcher(e);
	    			if (!matcher.matches()){
	    				cEmail.setError("Invalid Email Address");
	    			}
	    	  }   	  	    	  	    	  	    	  
	    	  
	    	  if (cForename.getText().length()==0 || cSurname.getText().length()==0 
	    			  || cMobile.getText().length()==0 && cPhone.getText().length()==0){
	    		  item.setEnabled(true);
	    		  validation(cForename);
	    		  validation(cSurname);
	    		  phoneNoValidation(cMobile);
	    		  phoneNoValidation(cPhone);
	    		  return false;
	      	  }else{
		    	  addClient();
		    	  if (className.compareTo("ViewClient")==0) {
		    		  Intent i = new Intent(this, ViewClient.class);
		    		  setResult(RESULT_OK,i);
		    		  finish();
		    	  } else if (sourceClass==null && className.compareTo("ViewClient")!=0){
		    		  Intent i = new Intent(this, ClientList.class);
		    		  startActivity(i);
		    		  finish();
		    	  } else {
		    		  String clientName = cForename.getText().toString() + " " + cSurname.getText().toString();
		    		  Intent i = new Intent(this, sourceClass.getClass());
		    		  i.putExtra("clientID", cliCon.getNextId());
		    		  i.putExtra("clientName", clientName);
		    		  setResult(RESULT_OK,i);
		    		  finish();
		    	  }
		          return true;
	    	  }
		case android.R.id.home:
			final Builder builder = new AlertDialog.Builder(this);			
			TextView title = new TextView(this);
			title.setText("Client details not saved!");
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			//title.setTextColor(getResources().getColor(R.color.greenBG));
			title.setTextSize(20);
			builder.setCustomTitle(title);
			TextView msg = new TextView(this);
			msg.setText("Are you sure you want to go back?");
			msg.setPadding(10, 10, 10, 10);
			msg.setGravity(Gravity.CENTER);
			msg.setTextSize(18);
			builder.setView(msg);
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
			        finish();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			AlertDialog dialog = builder.create();
			if (cForename.getText().length()!=0 || cSurname.getText().length()!=0 || cMobile.getText().length()!=0 || cPhone.getText().length()!=0 ||
	    			cEmail.getText().length()!=0 || cCompany.getText().length()!=0 || cAddress.getText().length()!=0 || cPostcode.getText().length()!=0 ||
	    			cNotes.getText().length()!=0){
				dialog.show();
			}else{
				finish();
			}
	    	return true;	    	
		case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  	        
	    case R.id.about_option:
	    	return true;	    	
	    case R.id.help_option:	
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;	    	
	      default:
	          return super.onOptionsItemSelected(item);
	      }
	  }
	  
	  @Override
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		  super.onActivityResult(requestCode, resultCode, data);
		  if (requestCode == REQUEST_CODE_PICK_CONTACTS && resultCode == RESULT_OK) {
			  uriContact = data.getData();	            
			  readContacts();
		  }
	  } 
	  
	  public void readContacts(){
		  // Reset fields
		  cTitle.setSelection(0);
		  cForename.setText("");
		  cSurname.setText("");
		  cMobile.setText("");
		  cPhone.setText("");
		  cEmail.setText("");
		  cCompany.setText("");
		  cAddress.setText("");
		  cPostcode.setText("");
		  cNotes.setText("");
		  address = ""; 
		  postcode = "";
		  
		  ContentResolver cr = getContentResolver();
	      // getting contacts ID
	      Cursor cursorID = getContentResolver().query(uriContact, new String[]{ContactsContract.Contacts._ID}, null, null, null);
	      if (cursorID.moveToFirst()) {
	    	  contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
	      }
	      cursorID.close();
	      
	      // Get Name	      
	      String whereName = ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " = ?";
	      String[] whereNameParams = new String[] { ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, contactID };
	      Cursor nameCur = cr.query(ContactsContract.Data.CONTENT_URI, null, whereName, whereNameParams, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
	      while (nameCur.moveToNext()) {
	    	  String fname = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
        	  String lname = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
	          String display = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
	          System.out.println("Forename: " + fname);
    		  System.out.println("Surename: " + lname);
    		  System.out.println("Display: " + display);
        	  cForename.setText(fname);
    		  cSurname.setText(lname);
	      }
	      nameCur.close();
	      
          // Get the phone number
          Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?", new String[]{contactID}, null);
          while (pCur.moveToNext()) {
        	  int phoneType = pCur.getInt(pCur.getColumnIndex(Phone.TYPE));
        	  if (phoneType == Phone.TYPE_MOBILE || phoneType == Phone.TYPE_MAIN){
        	  	  String mobile = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
        	      System.out.println("Mobile: " + mobile);
                  cMobile.setText(mobile);
        	  }
        	  if (phoneType == Phone.TYPE_WORK || phoneType == Phone.TYPE_HOME){
        	  	  String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
        	      System.out.println("Phone: " + phone);
        	      cPhone.setText(phone);
          	  }         
          }
          pCur.close();

          // Get email and type
          Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{contactID}, null);
          final List<String> where = new ArrayList<String>();
          String email = "";
          while (emailCur.moveToNext()) {
          	  email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
              String emailType = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
              where.add(email);
          }
          emailCur.close();
          String[] simpleArray = new String[ where.size() ];
          where.toArray( simpleArray );
          if(where.size() > 1){
        	  AlertDialog.Builder builder = new AlertDialog.Builder(this);
              builder.setTitle("Select email address");
              builder.setItems(simpleArray, new DialogInterface.OnClickListener() {
            	  public void onClick(DialogInterface dialog, int item) {
            		  System.out.println("Email: " + where.get(item).toString());
                 	  cEmail.setText(where.get(item).toString());
                  }
              });
              AlertDialog alert = builder.create();
              alert.show();
          }else{
        	  System.out.println("Email: " + email);
         	  cEmail.setText(email);
          }

          // Get note.......
          String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
          String[] noteWhereParams = new String[]{contactID,ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE};
          Cursor noteCur = cr.query(ContactsContract.Data.CONTENT_URI, null, noteWhere, noteWhereParams, null);
          if (noteCur.moveToFirst()) {
        	  String note = noteCur.getString(noteCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
              System.out.println("Note " + note);
              cNotes.setText(note);
          }
          noteCur.close();
           
          //Get Postal Address....
          String addrWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
          String[] addrWhereParams = new String[]{contactID, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE};
          Cursor addrCur = cr.query(ContactsContract.Data.CONTENT_URI,  null, addrWhere, addrWhereParams, null);          
          while(addrCur.moveToNext()){
        	  String street = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
        	  String pcode = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
        	  String city = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
        	  // Set the address depending on the format stored on the mobile device
        	  if(city == null && pcode == null){
        		  address = street;
        	  }else if(city == null){
        		  address = street+", "+pcode;
        	  }else{
        		  address = street+", "+city+", "+pcode;
        	  }        
        	  System.out.println("Address is : " +city+", "+street+", "+pcode);
          }
          addrCur.close();
          // Pattern for identifying the post code in the address
          String pc1="([A-PR-UWYZ](([0-9](([0-9]|[A-HJKSTUW])?)?)|([A-HK-Y][0-9]([0-9]|[ABEHMNPRVWXY])?)) [0-9][ABD-HJLNP-UW-Z]{2})|GIR 0AA ";
          Pattern pattern = Pattern.compile(pc1);
          // Find the postcode & store it in a String
          Matcher matcher = pattern.matcher(address.toUpperCase());
          if (matcher.find()) {
        	  postcode = matcher.group();
          } else { Log.d("NO","NO PCODE"); }
          // Remove the postcode from the address
          address = address.replaceAll( "(?i)\\b" + postcode + "\\b" , "" ); 
          // Set fields
          cAddress.setText(address);
          cPostcode.setText(postcode);
          
          // Get Company & Title
          String compWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
          String[] compWhereParams = new String[]{contactID, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE};
          Cursor compCur = cr.query(ContactsContract.Data.CONTENT_URI, null, compWhere, compWhereParams, null);
          if (compCur.moveToFirst()) {
        	  String compName = compCur.getString(compCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DATA));
              String title = compCur.getString(compCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
              System.out.println("Title " + title);
              System.out.println("Company " + compName);
              cCompany.setText(compName);
              ArrayAdapter myAdap = (ArrayAdapter) cTitle.getAdapter(); // Cast to an ArrayAdapter
              int spinnerPosition = myAdap.getPosition(title);
              //set the default according to value
              cTitle.setSelection(spinnerPosition);
          }
          compCur.close();
      }
	  
	  public void onClick(View view){
			switch (view.getId()){
			case R.id.imageButtonImportContact:			
		        startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), REQUEST_CODE_PICK_CONTACTS);
				break;
			default:
				break;
			}
		}
	  
	  public void updateJobs(){
		  JobsConnector jCon = new JobsConnector(this);
		  ArrayList<Integer> ids = jCon.getJobIDsByClient(clientID);
		  for (int i : ids){
			  jCon.updateClient(i, clientID, cForename.getText().toString() + " " + cSurname.getText().toString());
		  }
		  //jCon already closed
	  }
	  
	  public void addClient(){		  		  
		  cliCon = new ClientsConnector(this);
		  String company;
		  
		  if (cCompany.getText().length()>0){
			  company = cCompany.getText().toString();  
		  } else {
			  company = "Private";
		  }
		  try{
			  if (className.compareTo("ViewClient")==0){
				  cliCon.updateClient(clientID, cTitle.getSelectedItem().toString(), cForename.getText().toString(), cSurname.getText().toString(), company,
						  cAddress.getText().toString(), cPostcode.getText().toString(), cMobile.getText().toString(), cPhone.getText().toString(),
						  cEmail.getText().toString(), cNotes.getText().toString());
				  if (!tempName.equals(cForename.getText().toString() + " " + cSurname.getText().toString())){
					  updateJobs();	//update client name in jobs
				  }
			  } else {
				  cliCon.insertClient(cTitle.getSelectedItem().toString(), cForename.getText().toString(), cSurname.getText().toString(), company,
						  cAddress.getText().toString(), cPostcode.getText().toString(), cMobile.getText().toString(), cPhone.getText().toString(),
						  cEmail.getText().toString(), cNotes.getText().toString());
			  }
		  } catch (Exception e){
			  e.printStackTrace();
		  }
		  cliCon.close();
	  }
}
