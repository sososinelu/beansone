package uk.co.beanssystems.clients;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.quotes.NewQuote;
import uk.co.beanssystems.quotes.QuotesAdapter;
import uk.co.beanssystems.quotes.QuotesListItem;
import uk.co.beanssystems.quotes.ViewQuote;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.SherlockFragment;

public class ViewClientQuotes extends SherlockFragment {
	private ListView quotesListView;
	ArrayList<QuotesListItem> quotes = null;
	QuotesAdapter quotesAdapter;
	Context context;
	String clientName;
	Button newQuote;
	int clientID;
	private Cursor quoteCur, clientCur;
	private static SimpleDateFormat inDate = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat outDate = new SimpleDateFormat("dd/MM/yyyy");
	
	public ViewClientQuotes() {
	}
	
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		context = getActivity();
		clientID = getArguments().getInt("id");
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View fragView = inflater.inflate(R.layout.activity_view_client_quotes, container, false);
       return fragView;
    }
	
	@Override
	public void onActivityCreated(Bundle instance){
		super.onActivityCreated(instance);
		quotesListView = (ListView)getActivity().findViewById(R.id.view_client_quotes_list);
		newQuote = (Button)getActivity().findViewById(R.id.newQuotesListButton);
		newQuote.setOnClickListener(clickListener);
	}
	
	OnClickListener clickListener = new OnClickListener() {
	    public void onClick(final View v) {
	        switch(v.getId()) {
	           case R.id.newQuotesListButton:
					Intent i = new Intent(context, NewQuote.class);
					i.putExtra("clientID", clientID);
					i.putExtra("clientName", clientName);
					i.putExtra("source", "ViewClient");
					startActivity(i);
	              break;
	           default:
		          System.out.println("Unknown");
	        	   break;
	        }
	    }
	};
	@Override
	public void onStart(){
		super.onStart();
		loadQuotes();
		quotesListView.setEmptyView(getActivity().findViewById(R.id.noQuotesViewClientLayout));
		quotesAdapter = new QuotesAdapter(context, R.layout.activity_quotes_list_row, quotes);
		quotesAdapter.notifyDataSetChanged();
		quotesListView.setAdapter(quotesAdapter);
		quotesListView.setOnItemClickListener(new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			QuotesListItem ili = (QuotesListItem) quotesListView.getItemAtPosition(position);
				Intent i = new Intent(context, ViewQuote.class);
				i.putExtra("passQuoteID", ili.id);
				i.putExtra("passStatus", ili.status);
				startActivity(i);			
			}
	    });
	}
	
	public Calendar convertStringToDate(String strDate) throws ParseException{
	    Calendar cal = Calendar.getInstance();
	    Date date = inDate.parse(strDate);
	    cal.setTime(date);
	    cal.roll(Calendar.MONTH, -1);
	    cal.set(Calendar.MINUTE, 0);
	    return cal;
	}
	
	public void loadQuotes(){
        quotes = new ArrayList<QuotesListItem>();
		QuotesConnector quotesCon = new QuotesConnector(context);
        ClientsConnector clientsCon = new ClientsConnector(context);
        quotesCon.open();
        clientsCon.open();
        try{
            quoteCur = quotesCon.getQuotesByClient(clientID);
        	for (int i=0; i<2; i++){
        		quoteCur.moveToFirst();
        		int idCol = quoteCur.getColumnIndex(QuotesConnector.QUOTEID);
    	        int clientIdCol = quoteCur.getColumnIndex(QuotesConnector.CLIENTID);
    	        int jobNameCol = quoteCur.getColumnIndex(QuotesConnector.JOBNAME);
    	        int dateIssuedCol = quoteCur.getColumnIndex(QuotesConnector.DATEISSUED);
    	        int totalCostCol = quoteCur.getColumnIndex(QuotesConnector.TOTALCOST);
    			int statusCol = quoteCur.getColumnIndex(QuotesConnector.STATUS);
    			int fixedCostCol = quoteCur.getColumnIndex(QuotesConnector.FIXEDCOST);
        		do{
        			int id = quoteCur.getInt(idCol);
        			int clientId = quoteCur.getInt(clientIdCol);
        			String jobName = quoteCur.getString(jobNameCol);
                    String dateIssued = quoteCur.getString(dateIssuedCol);
                    double totalCost = quoteCur.getDouble(totalCostCol);
                    String status = quoteCur.getString(statusCol);
                    double fixedCost = quoteCur.getDouble(fixedCostCol);
                    if (fixedCost!=0){
                    	totalCost = fixedCost;
                    }
                    Calendar cal = Calendar.getInstance();
                    try {
                    	cal = convertStringToDate(dateIssued);
                    } catch (ParseException e){
                    	
                    }
                    
        			clientCur = clientsCon.getClient(clientId);
        			clientCur.moveToFirst();
        			int clientForenameCol = clientCur.getColumnIndex(ClientsConnector.FORENAME);
        			int clientSurnameCol = clientCur.getColumnIndex(ClientsConnector.SURNAME);
        			int clientCompanyCol = clientCur.getColumnIndex(ClientsConnector.COMPANY);
           			clientName = clientCur.getString(clientForenameCol) + " " + clientCur.getString(clientSurnameCol);
        			String company = clientCur.getString(clientCompanyCol);
    	        	
        			int icon;
    	        	if ("Private".equals(company)){
    	        		icon = R.drawable.user;
    	        	} else {
    	        		icon = R.drawable.work_user;
    	        	}
        	        
                  	if (status.equals("draft") && i==0){
                    	QuotesListItem quote = new QuotesListItem(id, icon, clientId, clientName, jobName, outDate.format(cal.getTime()), String.valueOf(totalCost), status);
                    	quotes.add(quote);
                  	} else if (status.equals("sent") && i==1){
                  		QuotesListItem quote = new QuotesListItem(id, icon, clientId, clientName, jobName, outDate.format(cal.getTime()), String.valueOf(totalCost), status);
                    	quotes.add(quote);
                  	}
                  	
                } while(quoteCur.moveToNext());
        	}
       } catch (Exception e) {
        	System.out.println("No quotes in database");
       }
       quotesCon.close();
       clientsCon.close();
	}
}
