package uk.co.beanssystems.clients;

import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockFragment;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.InvoicesConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.invoices.InvoicesAdapter;
import uk.co.beanssystems.invoices.InvoicesListItem;
import uk.co.beanssystems.invoices.NewInvoice;
import uk.co.beanssystems.invoices.ViewInvoice;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ViewClientInvoices extends SherlockFragment {
	private ListView invoicesListView;
	ArrayList<InvoicesListItem> invoicesArrayList = null;
	InvoicesAdapter invoicesAdapter;
	private Cursor invoiceCur, clientCur, jobCur;
	private int clientID;
	private String clientName;
	private Context context;
	Button newInvoice, newInvoiceFaded;
	@Override
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		context = getActivity();
		clientID = getArguments().getInt("id");
		//loadInvoices();
	}
	
	OnClickListener clickListener = new OnClickListener() {
	    public void onClick(final View v) {
	        switch(v.getId()) {
	           case R.id.newInvoicesListButton:
					Intent i = new Intent(context, NewInvoice.class);
					i.putExtra("clientID", clientID);
					i.putExtra("passClientName", clientName);
					i.putExtra("inNo", 1);
					startActivity(i);
	              break;
	           default:
		          System.out.println("Unknown");
	        	   break;
	        }
	    }
	};
	public void loadInvoices(){
		invoicesArrayList = new ArrayList<InvoicesListItem>();      
        ClientsConnector cliCon = new ClientsConnector(context);
        cliCon.open();
        clientCur = cliCon.getSingleClientName(clientID);        
        clientCur.moveToFirst();
        int forenameCol = clientCur.getColumnIndex(ClientsConnector.FORENAME);
        int surnameCol = clientCur.getColumnIndex(ClientsConnector.SURNAME);
        clientName = clientCur.getString(forenameCol) + " " + clientCur.getString(surnameCol);
        cliCon.close();
        
        InvoicesConnector invoicesCon = new InvoicesConnector(context);
        JobsConnector jobsCon = new JobsConnector(context);
        invoicesCon.open();
        jobsCon.open();
        invoiceCur = invoicesCon.getInvoicesByClient(clientID);
        try{
        	for (int i=0; i<3; i++){
        		invoiceCur.moveToFirst();
        		int idCol = invoiceCur.getColumnIndex(InvoicesConnector.INVOICEID);
        		int jobIdCol = invoiceCur.getColumnIndex(InvoicesConnector.JOBID);
    	        int dateIssuedCol = invoiceCur.getColumnIndex(InvoicesConnector.DATEISSUED);
    	        int outstandingCol = invoiceCur.getColumnIndex(InvoicesConnector.OUTSTANDING);
    	        int totalDueCol = invoiceCur.getColumnIndex(InvoicesConnector.TOTALDUE);
    			int statusCol = invoiceCur.getColumnIndex(InvoicesConnector.STATUS);
    				
        		do{
        			int id = invoiceCur.getInt(idCol);
        			int jobID = invoiceCur.getInt(jobIdCol);
                    String dateIssued = invoiceCur.getString(dateIssuedCol);
                	double outstanding = invoiceCur.getDouble(outstandingCol);
                	double totalDue = invoiceCur.getDouble(totalDueCol);
                    String status = invoiceCur.getString(statusCol);
                    String paidStatus;
                    if (outstanding == 0){
                    	paidStatus = "Paid";
                    	outstanding = totalDue;
                    }else{
                    	paidStatus = "Outstanding";
                    }
                    jobCur = jobsCon.getJob(jobID);
                    jobCur.moveToFirst();
                    int jobNameCol = jobCur.getColumnIndex(JobsConnector.JOBNAME);
                    String jobName = jobCur.getString(jobNameCol);
                  	if (status.compareTo("overdue")==0 && i==0){
                    	InvoicesListItem invoice = new InvoicesListItem(id, R.drawable.user, clientID, clientName, jobID, jobName, dateIssued, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                  	} else if (status.compareTo("draft")==0 && i==1){
                  		InvoicesListItem invoice = new InvoicesListItem(id, R.drawable.user, clientID, clientName, jobID, jobName, dateIssued, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                  	} else if (status.compareTo("sent")==0 && i==2){
                  		InvoicesListItem invoice = new InvoicesListItem(id, R.drawable.user, clientID, clientName, jobID, jobName, dateIssued, String.valueOf(outstanding), paidStatus, status);
                    	invoicesArrayList.add(invoice);
                  	}
                } while(invoiceCur.moveToNext());
        	}

        } catch (Exception e) {
        	System.out.println("No Invoices Available");
       }
     Cursor cur;
     cur = jobsCon.getJobsByClient(clientID);
     try {
    	 if (cur.getCount()>0){
    		 newInvoice.setEnabled(true);
    		 newInvoice.setVisibility(View.VISIBLE);
    		 newInvoiceFaded.setEnabled(false);
    		 newInvoiceFaded.setVisibility(View.GONE);
    	 } else {
    		 newInvoice.setEnabled(false);
    		 newInvoice.setVisibility(View.GONE);
    		 newInvoiceFaded.setEnabled(true);
    		 newInvoiceFaded.setVisibility(View.VISIBLE);
    	 }
     } catch (Exception e){
    	 newInvoice.setEnabled(false);
		 newInvoice.setVisibility(View.GONE);
		 newInvoiceFaded.setEnabled(true);
		 newInvoiceFaded.setVisibility(View.VISIBLE);
     }
     jobsCon.close();
	 invoicesCon.close();
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View fragView = inflater.inflate(R.layout.activity_view_client_invoices, container, false);

       return fragView;
    }
	
	@Override
	public void onActivityCreated(Bundle instance){
		super.onActivityCreated(instance);
		invoicesListView = (ListView)getActivity().findViewById(R.id.view_client_invoices_list);
		newInvoice = (Button)getActivity().findViewById(R.id.newInvoicesListButton);
		newInvoiceFaded = (Button)getActivity().findViewById(R.id.newInvoicesListButton2);
		newInvoice.setOnClickListener(clickListener);
	}
	
	@Override
	public void onStart(){
		super.onStart();
		loadInvoices();
		invoicesListView.setEmptyView(getActivity().findViewById(R.id.noInvsViewClientJobLayout));
		invoicesAdapter = new InvoicesAdapter(context, R.layout.activity_invoices_list_row, invoicesArrayList);
		invoicesAdapter.notifyDataSetChanged();
    	invoicesListView.setAdapter(invoicesAdapter);
    	invoicesListView.setOnItemClickListener(new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			InvoicesListItem ili = (InvoicesListItem) invoicesListView.getItemAtPosition(position);
				Intent i = new Intent(context, ViewInvoice.class);
				i.putExtra("passInvoiceID", ili.id);
				i.putExtra("passClientID", ili.clientId);
				i.putExtra("passJobID", ili.jobId);
				i.putExtra("passJobName", ili.jobName);
				i.putExtra("passDateDue", ili.dateDue);			
				i.putExtra("passPaidStatus", ili.paidStatus);
				startActivity(i);			
			}
	    });
	}
	
}
