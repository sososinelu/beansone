package uk.co.beanssystems.clients;

import java.util.ArrayList;

import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.clients.ClientsListItem;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ClientsAdapter extends ArrayAdapter<ClientsListItem> {
	Context context;
	int layoutResourceId;
	ArrayList<ClientsListItem> items = null;
	
	public ClientsAdapter (Context context, int layoutResourceId, ArrayList<ClientsListItem> items){
		super(context, layoutResourceId, items);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.items = items;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ClientsListItemHolder holder = null;
		
		if(row == null){
	        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	        row = inflater.inflate(layoutResourceId, parent, false);
	        holder = new ClientsListItemHolder();
	        holder.icon = (ImageView)row.findViewById(R.id.list_row_client_image);
	        holder.clientName = (TextView)row.findViewById(R.id.clientNameClientListTextView);
	        holder.companyName = (TextView)row.findViewById(R.id.companyNameClientListTextView);
	        row.setTag(holder);
	    }
        else{
        	holder = (ClientsListItemHolder)row.getTag();
	    }

        ClientsListItem client = items.get(position);
        holder.clientName.setText(client.clientName);
        holder.companyName.setText(client.companyName);
        holder.icon.setImageResource(client.icon);
		return row;

	}
	static class ClientsListItemHolder
	   {
	       ImageView icon;
	       TextView clientName;
	       TextView companyName;
	   }
}
