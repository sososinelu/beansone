package uk.co.beanssystems.clients;

import java.util.ArrayList;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import uk.co.beanssystems.beansone.MainActivity;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.beansone.TabManager;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.QitemsConnector;
import uk.co.beanssystems.database.QtasksConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.popups.DeleteDialog;
import uk.co.beanssystems.settings.Settings;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

public class ViewClient extends SherlockFragmentActivity {
	private static String company="", name;
	private int clientID;
	Cursor cliCur;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.activity_view_client);
                
        Intent i = getIntent();
        name =  i.getStringExtra("cName");
        company = i.getStringExtra("cCompany");
        clientID = i.getIntExtra("clientID", 0);
        
        final ActionBar bar = getSupportActionBar();
        bar.setTitle(name);
        bar.setIcon(R.drawable.clients);
	    bar.setDisplayHomeAsUpEnabled(true);
	    bar.setDisplayShowTitleEnabled(true);
	    bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	    
	    int icon;
    	if (company.compareTo("Private")==0){
    		icon = R.drawable.user_mini;
    	} else {
    		icon = R.drawable.work_user_mini;
    	}
    	
	    Bundle args = new Bundle();
	    args.putInt("id", clientID);
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(icon)).setTabListener(new TabManager<ViewClientInfo>(this, "info", ViewClientInfo.class, args)));
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.tab_quotes_mini)).setTabListener(new TabManager<ViewClientQuotes>(this, "quotes", ViewClientQuotes.class, args)));
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.tab_jobs_mini)).setTabListener(new TabManager<ViewClientJobs>(this, "jobs", ViewClientJobs.class, args)));
        bar.addTab(bar.newTab().setIcon(getResources().getDrawable(R.drawable.tab_invoice_mini)).setTabListener(new TabManager<ViewClientInvoices>(this, "invoices", ViewClientInvoices.class, args)));
	}

	public void hideEdit(){
		
	}  
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	switch (requestCode){
    	case 0:
    		//Client Edited
    		break;
    	case 1:
    		if (resultCode==RESULT_OK){
    			JobsConnector jCon = new JobsConnector(this);
    			ClientsConnector cCon = new ClientsConnector(this);
    			if (jCon.clientHasJobs(clientID)){
    				cCon.updateActive(clientID, "false");
    				updateQuotes();
    			} else {
    				cCon.updateActive(clientID, "dead");
    				updateQuotes();
    			}
    			finish();
    		}
    		break;
    	default:break;
    	}
    }
    
    public void updateQuotes(){
    	ArrayList<Integer> qID = new ArrayList<Integer>();
    	QuotesConnector qCon = new QuotesConnector(this);
    	QtasksConnector tCon = new QtasksConnector(this);
    	QitemsConnector iCon = new QitemsConnector(this);
    	qID = qCon.getClientQuotes(clientID);
    	for (int id : qID){
    		qCon.updateActive(id, "dead");
    		tCon.updateAllActive(id, "dead");
    		iCon.updateAllActive(id, "dead");
    	}
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	   MenuInflater inflater = getSupportMenuInflater();
	   inflater.inflate(R.menu.menu_options_view, menu);
	   return true;
	}
    
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case android.R.id.home:
	    	Intent intent = new Intent(this, ClientList.class);            
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	        startActivity(intent);
	        finish();
	    	return true;
		case R.id.edit_option:
			ClientsConnector cliCon = new ClientsConnector(this);
			cliCon.open();
			cliCur = cliCon.getClient(clientID);
			cliCur.moveToFirst();
			int titleCol = cliCur.getColumnIndex(ClientsConnector.TITLE);
			int forenameCol = cliCur.getColumnIndex(ClientsConnector.FORENAME);
			int surnameCol = cliCur.getColumnIndex(ClientsConnector.SURNAME);
			int companyCol = cliCur.getColumnIndex(ClientsConnector.COMPANY);
			int mobileCol = cliCur.getColumnIndex(ClientsConnector.MOBILE);
			int phoneCol = cliCur.getColumnIndex(ClientsConnector.PHONE);
			int emailCol = cliCur.getColumnIndex(ClientsConnector.EMAIL);
			int addressCol = cliCur.getColumnIndex(ClientsConnector.ADDRESS);
			int postcodeCol = cliCur.getColumnIndex(ClientsConnector.POSTCODE);
			int notesCol = cliCur.getColumnIndex(ClientsConnector.NOTES);
			Intent i = new Intent(this, NewClient.class);
			i.putExtra("clientID", clientID);
			i.putExtra("title", cliCur.getString(titleCol));
			i.putExtra("forename", cliCur.getString(forenameCol));
			i.putExtra("surname", cliCur.getString(surnameCol));
			i.putExtra("company", cliCur.getString(companyCol));
			i.putExtra("mobile", cliCur.getString(mobileCol));
			i.putExtra("phone", cliCur.getString(phoneCol));
			i.putExtra("email", cliCur.getString(emailCol));
			i.putExtra("address", cliCur.getString(addressCol));
			i.putExtra("postcode", cliCur.getString(postcodeCol));
			i.putExtra("notes", cliCur.getString(notesCol));
			cliCon.close();
			startActivityForResult(i, 0);
			return true;
		case R.id.delete_option:
			Intent inDel = new Intent(this, DeleteDialog.class);
			inDel.putExtra("title", "Client");
			startActivityForResult(inDel, 1);
			return true;
		case R.id.settings_option:
	    	Intent in = new Intent(this, Settings.class);
	    	startActivity(in);
	        return true;  
	    case R.id.about_option:
	    	return true;
	    case R.id.help_option:	
	    	return true;
	    case R.id.home_option:
	    	Intent iHome = new Intent(this, MainActivity.class);
	    	iHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(iHome);
	    	finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
}