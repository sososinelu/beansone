package uk.co.beanssystems.beansone;

import java.io.Serializable;

public class QuotePDF implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9035235096466905696L;
	public int clientID;
	public String jobName, dateIssued;
	public double sub, vat, total;

	public QuotePDF(){
		
	}
	
	public QuotePDF(int clientID, String jobName, String dateIssued, double sub, double vat, double total){
		this.clientID = clientID;
		this.jobName = jobName;
		this.dateIssued = dateIssued;
		this.sub = sub;
		this.vat = vat;
		this.total = total;
	}
}
