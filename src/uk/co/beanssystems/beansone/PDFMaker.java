package uk.co.beanssystems.beansone;

import harmony.java.awt.Color;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.InvoicesConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.QuotesConnector;
import uk.co.beanssystems.database.SettingsConnector;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class PDFMaker {
	public static final int QUOTE = 0;
	private String myAddress, myCompany, myPostcode, myNumber, myEmail;
	private String jobName, clientName, address, postcode, dateIssued, dateDue;
	private int clientID;
	private double subVal, vatVal, totalVal;
	public static final int INVOICE = 1;
	private boolean fileExists;
	private Context context;
	private int type;
	private String filename, footer="";
	private Document doc;
	private InvoicePDF inv;
	private QuotePDF quo;
	Rectangle noBorder = new Rectangle(0f, 0f);
	SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
	NumberFormat nf;
	
	public PDFMaker(Context context, int type, String filename, String footer, Object pdf){
		cleanup();
		this.context = context;
		this.fileExists = false;
		this.filename = filename;
		this.footer = footer;
		this.type = type;
		if (type==QUOTE){
			quo = (QuotePDF) pdf;
			clientID = quo.clientID;
			jobName = quo.jobName;
			dateIssued = quo.dateIssued;
			subVal = quo.sub;
			vatVal = quo.vat;
			totalVal = quo.total;
		} else {
			inv = (InvoicePDF) pdf;
			clientID = inv.clientID;
			jobName = inv.jobName;
			dateIssued = inv.dateIssued;
			dateDue = inv.dateDue;
			subVal = inv.sub;
			vatVal = inv.vat;
			totalVal = inv.total;
		}
		noBorder.setBorderWidthLeft(0f);
		noBorder.setBorderWidthBottom(0f);
		noBorder.setBorderWidthRight(0f);
		noBorder.setBorderWidthTop(0f);
		nf = NumberFormat.getCurrencyInstance(Locale.UK);
	}
	
	public void cleanup(){
		File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "BeansOne");
		if (mediaStorageDir.exists()){
			File[] files;
			files = mediaStorageDir.listFiles();
			if (files.length>5){
				for (File f : files){
					f.delete();
				}
			}
		}
	}
	
	public boolean makeFile(){
		File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "BeansOne");
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            return false;
	        }
	    }
	    doc = new Document();
		try {
			PdfWriter.getInstance(doc, new FileOutputStream(Environment.getExternalStorageDirectory() + java.io.File.separator +
					"BeansOne" +java.io.File.separator + filename));
			fileExists = true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return fileExists;
	}
		
	public String build(){
		if (fileExists){
			doc.open();
			SettingsConnector setCon = new SettingsConnector(context);
			setCon.open();
			Cursor setCur = setCon.getUserDetails();
			setCur.moveToFirst();
			myAddress = setCur.getString(setCur.getColumnIndex(SettingsConnector.ADDRESS));
			myPostcode = setCur.getString(setCur.getColumnIndex(SettingsConnector.POSTCODE));
			myCompany = setCur.getString(setCur.getColumnIndex(SettingsConnector.USERCOMPANY));
			myNumber = setCur.getString(setCur.getColumnIndex(SettingsConnector.CONTACTNUM));
			myEmail = setCur.getString(setCur.getColumnIndex(SettingsConnector.EMAIL));
			setCon.close();
			//Header Table
			float[] widthsHeader = { 0.65f, 0.35f };
			PdfPTable tblHeader = new PdfPTable(widthsHeader);	
			PdfPCell cell = new PdfPCell(new Paragraph(myCompany+"\n"+(myAddress==null ? " " : myAddress)+"\n"+(myPostcode==null ? " " : myPostcode)));
			cell.cloneNonPositionParameters(noBorder);
			tblHeader.addCell(cell);
			cell = new PdfPCell(new Paragraph(type==QUOTE ? "\nQUOTE\n" : "\nINVOICE\n"));
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			tblHeader.addCell(cell);
			tblHeader.setSpacingAfter(30);
			//End of Header Table
			
			//Info Table
			ClientsConnector cliCon = new ClientsConnector(context);
			cliCon.open();
			Cursor cCur = cliCon.getClient(clientID);
			cCur.moveToFirst();
			address = cCur.getString(cCur.getColumnIndex(ClientsConnector.ADDRESS));
			postcode = cCur.getString(cCur.getColumnIndex(ClientsConnector.POSTCODE));
			clientName = cCur.getString(cCur.getColumnIndex(ClientsConnector.FORENAME))+" "+cCur.getString(cCur.getColumnIndex(ClientsConnector.SURNAME));
			cliCon.close();
			float[] widthsInfo = {0.5f,0.5f};
			PdfPTable tblInfo = new PdfPTable(widthsInfo);
			cell = new PdfPCell(new Paragraph("To:"));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			cell.setBackgroundColor(Color.LIGHT_GRAY);
			tblInfo.addCell(cell);
			cell = new PdfPCell(new Paragraph("Info:"));
			cell.setBackgroundColor(Color.LIGHT_GRAY);
			tblInfo.addCell(cell);
			cell = new PdfPCell(new Paragraph(clientName+"\n"+address+"\n"+postcode));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			cell.setPadding(10);
			tblInfo.addCell(cell);
			String dates = "Date Issued: "+dateIssued;
			if (type==INVOICE){
				dates += "\n\nDate Due: "+dateDue;
			}
			cell = new PdfPCell(new Paragraph(dates));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			cell.setPadding(10);
			tblInfo.addCell(cell);
			tblInfo.setSpacingAfter(30);
			//End of Info
			
			//Costs Table
			float[] widthsCosts = {0.65f,0.35f};
			PdfPTable tblCosts = new PdfPTable(widthsCosts);
			cell = new PdfPCell(new Paragraph("Description"));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			cell.setBackgroundColor(Color.LIGHT_GRAY);
			tblCosts.addCell(cell);
			cell = new PdfPCell(new Paragraph("Total (�)"));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			cell.setBackgroundColor(Color.LIGHT_GRAY);
			tblCosts.addCell(cell);
			cell = new PdfPCell(new Paragraph("\n\n"+jobName+"\n\n"));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			tblCosts.addCell(cell);
			cell = new PdfPCell(new Paragraph("\n\n"+nf.format(totalVal)+"\n\n"));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			tblCosts.addCell(cell);
			//End of Costs
			
			//Totals Table
			float[] widthsTotals = {0.3f, 0.35f, 0.35f};
			PdfPTable tblTotals = new PdfPTable(widthsTotals);
			cell = new PdfPCell(new Paragraph(""));
			cell.cloneNonPositionParameters(noBorder);
			tblTotals.addCell(cell);
			cell = new PdfPCell(new Paragraph("Sub Total:\n\nVAT:\n\nTotal:"));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			tblTotals.addCell(cell);
			cell = new PdfPCell(new Paragraph(nf.format(subVal)+"\n\n"+nf.format(vatVal)+"\n\n"+nf.format(totalVal)));
			cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
			tblTotals.addCell(cell);
			tblTotals.setSpacingAfter(50);
			//End of Totals
			
			//Footer Table
			float[] widthsFooter = {1f};
			PdfPTable tblFooter = new PdfPTable(widthsFooter);
			cell = new PdfPCell(new Paragraph(footer+"\n\nIf you have any queries please contact us on "+myNumber+" or by email: "+myEmail));
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.cloneNonPositionParameters(noBorder);
			tblFooter.addCell(cell);
			//End of Footer
			try {
				doc.add(tblHeader);
				doc.add(tblInfo);
				doc.add(tblCosts);
				doc.add(tblTotals);
				doc.add(tblFooter);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		 	doc.close();
		 	Toast.makeText(context, "PDF Made", Toast.LENGTH_SHORT).show();
			return filename;
		} else {
			return "";
		}
	}
}