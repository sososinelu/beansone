package uk.co.beanssystems.beansone;

import java.io.Serializable;

public class InvoicePDF implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8649175116003980520L;
	public int clientID;
	public String jobName, dateIssued, dateDue;
	public double sub, vat, total;
	
	public InvoicePDF(){
		
	}
	
	public InvoicePDF(int clientID, String jobName, String dateIssued, String dateDue, double sub, double vat, double total){
		this.clientID = clientID;
		this.jobName = jobName;
		this.dateIssued = dateIssued;
		this.dateDue = dateDue;
		this.sub = sub;
		this.vat = vat;
		this.total = total;
	}
}
