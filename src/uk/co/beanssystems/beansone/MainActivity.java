package uk.co.beanssystems.beansone;

import java.util.ArrayList;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.slidingmenu.lib.SlidingMenu;
import uk.co.beanssystems.clients.ClientList;
import uk.co.beanssystems.clients.NewClient;
import uk.co.beanssystems.database.ClientsConnector;
import uk.co.beanssystems.database.ExpSubTypeConnector;
import uk.co.beanssystems.database.ExpTypeConnector;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.database.JobsConnector;
import uk.co.beanssystems.database.SettingsConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.expenses.ExpenseList;
import uk.co.beanssystems.expenses.NewExpense;
import uk.co.beanssystems.help.Help;
import uk.co.beanssystems.jobs.JobsList;
import uk.co.beanssystems.jobs.NewJob;
import uk.co.beanssystems.settings.MyDetails;
import uk.co.beanssystems.settings.Settings;
import uk.co.beanssystems.settings.SyncSettings;
import uk.co.beanssystems.setup.ExportDatabaseFileTask;
import uk.co.beanssystems.setup.Setup;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends SherlockActivity {
	TextView jobCount, company, clients, jobs, expenses, help, settings;
	ImageView microphoneImageView;
	SlidingMenu menu;
	ArrayList<Double> totals = null;
	Cursor setCur = null, jobCur = null, expTypeCur;
	Context context;
	
	@Override
	  public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main_sliding_menu_root);  
        context = this;
        
	    menu = (SlidingMenu)findViewById(R.id.slidingmenulayout);
	    //Side menu labels
	    clients = (TextView)findViewById(R.id.clientsLabel);
	    jobs = (TextView)findViewById(R.id.jobsLabel);
	    expenses = (TextView)findViewById(R.id.expensesLabel);
	    help = (TextView)findViewById(R.id.helpLabel);
	    settings = (TextView)findViewById(R.id.settingsLabel);
	    //Listeners for side menu labels
	    clients.setOnClickListener(clickListener);
	    jobs.setOnClickListener(clickListener);
	    expenses.setOnClickListener(clickListener);
	    help.setOnClickListener(clickListener);
	    settings.setOnClickListener(clickListener);	    
	    jobCount = (TextView)findViewById(R.id.jobsCountValueMainScreenTextView);
	    company = (TextView)findViewById(R.id.userCompanyNameMainScreenTextView);
	   
	    SettingsConnector setCon = new SettingsConnector(this);
	    setCon.open();
	    try {
		    setCur = setCon.getSetting(1);
		    setCur.moveToFirst();
		    int companyCol = setCur.getColumnIndex(SettingsConnector.USERCOMPANY);
		    String companyName = setCur.getString(companyCol);
		    
		    company.setText(companyName);
	    } catch (Exception e){
	    	Intent in = new Intent(this, Setup.class);
	    	startActivity(in);
	    	setCon.close();
	    	finish();
	    }
	    setCon.close();
		insertExpense();
		//insertClients();
	}
	  
	public void exportDatabase (){
	ExportDatabaseFileTask expData = new ExportDatabaseFileTask(this);
	expData.execute();
	}
	
	public void insertClients(){
		//INSERT CLIENTS
		ClientsConnector clientCon = new ClientsConnector(this);
		for (int i=0;i<150;i++){
			clientCon.insertClient("Mr", "Jack", "Sparow", "Sparow INC", "45 Harriet St, Cathays, Cardiff", "CF45 4BW", "07574567123", "08001234123", "jack@sparow.co.uk", "Johny's Son");
		}	
		
		//INSERT JOBS
		JobsConnector jobCon = new JobsConnector(this);
		for (int i=0;i<200;i++){		
			jobCon.insertJob(1 , "Jack Sparow", "Aircon"+i, 10.0, 500.0, "Fixed", 1000.0, "2013-03-07", "2013-03-07", "", "Aircon Job");
	  	}	
		
		//INSERT TASKS	    
		TasksConnector tCon = new TasksConnector(this);
  	  	for (int i=0;i<400;i++){
  	  		tCon.insertTask(1, 0, "Installation"+i , 10, "", 500);
  	  	}	 
  	  
  	  	//INSERT ITEMS
  	  	ItemsConnector iCon = new ItemsConnector(this);
  	  	for (int i=0;i<500;i++){
  	  		iCon.insertItem(1, 0, 0, "Aircon Unit"+i, 1200, "true", 1, 1200, 200);
  	  	}
	}
	
	public void insertExpense(){
		ExpTypeConnector expTypeCon = new ExpTypeConnector(this);
		expTypeCon.open();
	    try {
	    	expTypeCur = expTypeCon.getExpType(1);
	    	expTypeCur.moveToFirst();
	    	int expTypeCol = expTypeCur.getColumnIndex(ExpTypeConnector.EXPTYPE);
	    	String exp = expTypeCur.getString(expTypeCol);
	    	System.out.println(">>>>>> Exp Type - True <<<<<<<");
	    	System.out.println(">>>>>> Exp Type - "+exp);    	
	    } catch (Exception e){	
	    	System.out.println(">>>>>> Exp Type - False <<<<<<<");
	    	ExpTypeConnector expCon = new ExpTypeConnector(this);
	    	expCon.insertExpType("Advertising & Media", "1");
	    	expCon.insertExpType("Vehicle", "1");
	    	expCon.insertExpType("Food & Entertainment", "1");
	    	expCon.insertExpType("Hardware", "1");
	    	expCon.insertExpType("Office", "1");
	    	expCon.insertExpType("Personal", "1");
	    	expCon.insertExpType("Professional Fees", "1");
	    	expCon.insertExpType("Rent or Lease", "1");
	    	expCon.insertExpType("Travel", "1");
	    	expCon.insertExpType("Utilities ", "1");
	    	expCon.insertExpType("Stock ", "1");
			
			ExpSubTypeConnector expSubCon = new ExpSubTypeConnector(this);
			expSubCon.insertExpSubType(1, "Advertising", "1");
			expSubCon.insertExpSubType(1, "Newspaper", "1");
			expSubCon.insertExpSubType(1, "Website", "1");
			expSubCon.insertExpSubType(1, "Networking", "1");
			expSubCon.insertExpSubType(1, "PR", "1");
			
			expSubCon.insertExpSubType(2, "Fines", "1");
			expSubCon.insertExpSubType(2, "Fuel", "1");
			expSubCon.insertExpSubType(2, "Mileage", "1");
			expSubCon.insertExpSubType(2, "Repairs", "1");
			expSubCon.insertExpSubType(2, "Vehicle Insurance", "1");
			expSubCon.insertExpSubType(2, "Vehicle Tax", "1");
			expSubCon.insertExpSubType(2, "Signage", "1");
			
			expSubCon.insertExpSubType(3, "Entertainment", "1");
			expSubCon.insertExpSubType(3, "Restaurants / Dining", "1");

			expSubCon.insertExpSubType(4, "Mobile Phones", "1");
			expSubCon.insertExpSubType(4, "Computers", "1");
			expSubCon.insertExpSubType(4, "Equipment / Tools", "1");
			
			expSubCon.insertExpSubType(5, "Office Supplies", "1");
			expSubCon.insertExpSubType(5, "Packging", "1");
			expSubCon.insertExpSubType(5, "Printing", "1");
			expSubCon.insertExpSubType(5, "Shipping & Couriers", "1");
			expSubCon.insertExpSubType(5, "Software", "1");
			expSubCon.insertExpSubType(5, "Stationery", "1");

			expSubCon.insertExpSubType(6, "Personal ", "1");
			expSubCon.insertExpSubType(6, "Clothing & Footwear ", "1");

			expSubCon.insertExpSubType(7, "Accounting", "1");
			expSubCon.insertExpSubType(7, "Legal Services", "1");
			
			expSubCon.insertExpSubType(8, "Equipment", "1");
			expSubCon.insertExpSubType(8, "Machinery", "1");
			expSubCon.insertExpSubType(8, "Office Space", "1");
			expSubCon.insertExpSubType(8, "Vehicles", "1");
			
			expSubCon.insertExpSubType(9, "Air Travel", "1");
			expSubCon.insertExpSubType(9, "Accomodation", "1");
			expSubCon.insertExpSubType(9, "Parking", "1");
			expSubCon.insertExpSubType(9, "Public Transport", "1");
			expSubCon.insertExpSubType(9, "Toll Fees", "1");
			
			expSubCon.insertExpSubType(10, "Gas & Electrical", "1");
			expSubCon.insertExpSubType(10, "Internet", "1");
			expSubCon.insertExpSubType(10, "Phone", "1");
			expSubCon.insertExpSubType(10, "Water", "1");
			expSubCon.insertExpSubType(10, "Council Tax", "1");
			expSubCon.insertExpSubType(10, "Tax", "1");
			
			expSubCon.insertExpSubType(11, "Stock", "1");
	    	expTypeCon.close();
	    	System.out.println(">>>>>> Finish inserting <<<<<<<");
	    }	
	    expTypeCon.close();
	}

	public boolean isWifiOn() {
	     WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
	     return wifi.isWifiEnabled();
	}
	
	public void onStart(){
		super.onStart();
		totals = new ArrayList<Double>();
        JobsConnector jobCon = new JobsConnector(this);
        //opens() itself in Connector
        int activeJobs = jobCon.countActiveJobs();
        jobCount.setText(String.valueOf(activeJobs));
        //closes() itself in Connector
	}
	
	OnClickListener clickListener = new OnClickListener() {
		public void onClick(final View v) {
			switch(v.getId()) {
			case R.id.clientsLabel:
				Intent inClient = new Intent(context, ClientList.class);
				startActivity(inClient);
				break;
			case R.id.jobsLabel:
				Intent inJob = new Intent(context, JobsList.class);
				startActivity(inJob);
				break;
			case R.id.expensesLabel:
				Intent inExpenses = new Intent(context, ExpenseList.class);
				startActivity(inExpenses);
				break;
			case R.id.helpLabel:
				Intent inHelp = new Intent(context, Help.class);
				startActivity(inHelp);
				break;
			case R.id.settingsLabel:
				Intent inSettings = new Intent(context, Settings.class);
		    	startActivity(inSettings);
				break;
			default:
				break;
			}
		}
	};
	
	public void onClick(View v){
		switch(v.getId()){
		case R.id.beansLogoImageView:
			menu.toggle();
			break;
		case R.id.clientsImageView:
			Intent inClient = new Intent(this, ClientList.class);
			startActivity(inClient);
			break;
		case R.id.jobsImageView:
			Intent inJob = new Intent(this, JobsList.class);
			startActivity(inJob);
			break;
		case R.id.expensesImageView:
			Intent inExpenses = new Intent(this, ExpenseList.class);
			startActivity(inExpenses);
			break;
		case R.id.helpImageView:
			Intent inHelp = new Intent(this, Help.class);
			startActivity(inHelp);
			break;
		case R.id.settingsImageView:
			Intent inSettings = new Intent(this, Settings.class);
	    	startActivity(inSettings);
			break;
		case R.id.jobCountWrapperMainScreen:
			Intent inJob2 = new Intent(this, JobsList.class);
			startActivity(inJob2);
			break;
		case R.id.userCompanyNameMainScreenTextView:
			Intent inUser = new Intent(this, MyDetails.class);
			startActivity(inUser);
			break;	
		case R.id.favouriteImage1:
			Intent inNewClient = new Intent(this, NewClient.class);
	    	startActivity(inNewClient);
			break;
		case R.id.favouriteImage2:
			Intent inBusExp = new Intent(this, NewExpense.class);
	    	inBusExp.putExtra("expenseCat", 2);
	    	startActivityForResult(inBusExp, 0);
			break;
		case R.id.favouriteImage3:
			Intent inNewJob = new Intent(this, NewJob.class);
	    	startActivity(inNewJob);
			break;
		case R.id.favouriteImage4:
			Intent inBackup = new Intent(this, SyncSettings.class);
	    	startActivity(inBackup);
			break;
		default:break;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.activity_main, menu);
	   // menu.removeItem(R.id.export_database);
	    menu.removeItem(R.id.about_option);
	    return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
	    case R.id.settings_option:
	    	Intent i = new Intent(this, Settings.class);
	    	startActivity(i);
	        return true;
	    case R.id.about_option:
	    	return true;
	    case R.id.help_option:
	    	Intent ih = new Intent(this, Help.class);
	    	startActivity(ih);
	    	return true;
		case R.id.export_database:
	    	// Create AlertDialog
			final Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Export Database");
			builder.setMessage("This feature is not part of the app; is here just to help us, (Sorin & Stephen :D - Hello!) to quickly view the database on your phone. Are you sure you want to export the database?");
			builder.setCancelable(true);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					exportDatabase();
					// Intent to SQLite Debugger App
//					String dbName = "BeansSystems";
//					String path = openOrCreateDatabase(dbName, MODE_WORLD_READABLE, null).getPath();
//					Intent intent = new Intent (Intent.ACTION_EDIT);
//					intent.setData(Uri.parse("sqlite:" + path));
//					startActivity(intent);
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
	    	
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
}