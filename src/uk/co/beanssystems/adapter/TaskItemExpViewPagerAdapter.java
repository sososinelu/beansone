package uk.co.beanssystems.adapter;

import java.util.ArrayList;
import uk.co.beanssystems.jobs.Expense;
import uk.co.beanssystems.jobs.Item;
import uk.co.beanssystems.jobs.Task;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TaskItemExpViewPagerAdapter extends FragmentPagerAdapter {
	private ArrayList<Task> tasks;
	private ArrayList<Item> items; 
	private ArrayList<Expense> exps;
	private int count = 0, source = 0;
	
	public TaskItemExpViewPagerAdapter(FragmentManager fm, ArrayList<Task> tasks, ArrayList<Item> items, int count, int source) {
		super(fm);
		this.tasks = tasks;
		this.items = items;
		this.count = count;
		this.source = source;
	}
	
	public TaskItemExpViewPagerAdapter(FragmentManager fm, ArrayList<Task> tasks, ArrayList<Item> items, 
			ArrayList<Expense> exps, int count, int source) {
		super(fm);
		this.tasks = tasks;
		this.items = items;
		this.exps = exps;
		this.count = count;
		this.source = source;
	}	
	
	@Override
	public Fragment getItem(int position) {      
	    switch (position) {
        case 0:
        	System.out.println("VIEW PAGER ADAPTER - TASKS ?>>>> "+tasks.size());
        	return new ViewTaskFrag(tasks, source);
        case 1:
        	System.out.println("VIEW PAGER ADAPTER - ITEMS ?>>>> "+items.size());
        	return new ViewItemFrag(items, source);
        	
        case 2:
        	System.out.println("VIEW PAGER ADAPTER - EXPS ?>>>> "+exps.size());
        	return new ViewExpFrag(exps, source);
        }
	    return null;
	}
	
    public int getCount() {
        return count;
    }
}
