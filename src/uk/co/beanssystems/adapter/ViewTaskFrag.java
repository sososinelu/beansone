package uk.co.beanssystems.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.QtasksConnector;
import uk.co.beanssystems.database.TasksConnector;
import uk.co.beanssystems.jobs.Task;
import uk.co.beanssystems.popups.AddTaskPopup;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.SherlockFragment;

@SuppressLint("ValidFragment")
public class ViewTaskFrag  extends SherlockFragment {
	private ListView tasksList;
	ArrayList<DetailInfo> detailInfoArray = null;
	static ArrayList<Task> tasks = new ArrayList<Task>();
	public static ArrayList<Task> delTasks = new ArrayList<Task>();
	Context context;
	TaskItemExpListAdapter taskAdapter;
	int source = 0;

	public ViewTaskFrag(ArrayList<Task> tasks, int source) {
		super();
		ViewTaskFrag.tasks = tasks;
		this.source = source;
	}	
	
	public ViewTaskFrag() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		context = getActivity().getApplicationContext();
	}
	    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {	
		View view = inflater.inflate(R.layout.activity_view_tie_task, container, false);
		System.out.println("VIEW TASK FRAG ?>>>> "+tasks.size());
	    return view;
	}
	
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		tasksList = (ListView)getActivity().findViewById(R.id.tasksListView);
		detailInfoArray = new ArrayList<DetailInfo>();		
	    for(int i=0; i<tasks.size();i++){
	    	Task task = tasks.get(i);
	    	System.out.println("Name ?>>>> "+task.taskName);
	    	System.out.println("Duration ?>>>> "+task.duration);
	    	double totalTasks = 0;
	    	if(task.taskName.equals("Callout")){
				totalTasks += task.value;
			}else{
				totalTasks += task.duration * task.value;
			}
	    	if (!task.dead){
		    	DetailInfo detailInfo = new DetailInfo(task.taskName , String.valueOf(task.duration), 
		    			String.valueOf(task.value),  String.valueOf(totalTasks), "task");
		    	detailInfoArray.add(detailInfo);
	    	}
	    }

		tasksList.setEmptyView(getActivity().findViewById(R.id.noTasksListLayout));	
    	taskAdapter = new TaskItemExpListAdapter(context, R.layout.child_row, detailInfoArray);
    	tasksList.setAdapter(taskAdapter);
    	
    	tasksList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Task task = tasks.get(position);
		    	Intent inTask = new Intent(context, AddTaskPopup.class);
		    	inTask.putExtra("taskName", task.taskName);
		    	inTask.putExtra("duration", task.duration);
		    	inTask.putExtra("value", task.value);
		    	inTask.putExtra("childPos", position);
				startActivityForResult(inTask, 0);
			}
    	});
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		int childPos;
		switch (requestCode){
		case 0:
			if (resultCode == -1){
				childPos = data.getIntExtra("childPos", -1);
				double total = 0;
				String taskName = data.getStringExtra("taskName");
				String duration = data.getStringExtra("duration");
				String value = data.getStringExtra("value");
				
				Task task = tasks.get(childPos);
				task.setTaskName(taskName);
				task.setTaskDuration(Double.valueOf(duration));
				task.setTaskValue(Double.valueOf(value));
				if(taskName.equals("Callout")){
					total = Double.parseDouble(value);
				}else{
					total = Double.parseDouble(duration) * Double.parseDouble(value);
				} 			
				DetailInfo detailInfo = detailInfoArray.get(childPos);
				detailInfo.updateDetailInfo(taskName, duration, value, String.valueOf(total), "task");
				taskAdapter.notifyDataSetChanged();
				System.out.println("++++++++++++++++++TASK EDITED++++++++++++++++++++++");

			}else if (resultCode == -2){
				childPos = data.getIntExtra("childPos", -1);
				delTask(childPos);
			}
			
			break;

		default:
			break;
		}
	}
	
	public void delTask(final int childPos){
    	// Create AlertDialog
		final Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Delete selected Task");
		//builder.setMessage("Update the new job with the Quote details ?");
		builder.setCancelable(true);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				try{	    	
			    	Task task = tasks.get(childPos);
			    	if(source == 1){ // NewQuote
			    		tasks.remove(childPos);
			    		delTasks.add(task);
//			    		QtasksConnector qtCon = new QtasksConnector(context);
//			    		qtCon.deleteQtask(task.taskID);
					}else if (source == 2){ // NewInvoice
						tasks.get(childPos).invoiceID = 0;
						tasks.get(childPos).dead = true;
					}else if(source == 3){ // ViewJobInfo
						tasks.remove(childPos);
						TasksConnector taskCon = new TasksConnector(context);
						taskCon.deleteTask(task.taskID);
					}else{
						
					}
					// Remove selected task from the array list
					detailInfoArray.remove(childPos);
					taskAdapter.notifyDataSetChanged();
					System.out.println("Task removed from array ");	
				} catch (Exception e) { 
					System.out.println("++++++ERROR+++++ "+e.getLocalizedMessage().toString());
				}
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}