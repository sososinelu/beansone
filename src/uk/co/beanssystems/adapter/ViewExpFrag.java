package uk.co.beanssystems.adapter;

import java.util.ArrayList;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ExpensesConnector;
import uk.co.beanssystems.jobs.Expense;
import uk.co.beanssystems.popups.AddExpensePopup;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import com.actionbarsherlock.app.SherlockFragment;

@SuppressLint("ValidFragment")
public class ViewExpFrag  extends SherlockFragment {
	private ListView expsList;
	ArrayList<DetailInfo> detailInfoArray = null;
	static ArrayList<Expense> exps = new ArrayList<Expense>();
	Context context;
	TaskItemExpListAdapter expAdapter;
	private int source = 0;
	
	public ViewExpFrag(ArrayList<Expense> exps, int source) {
		super();
		ViewExpFrag.exps = exps;
		this.source = source;
	}
	
	public ViewExpFrag() {
		// TODO Auto-generated constructor stub
	}
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		context = getActivity().getApplicationContext();
	}
	    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {	
		View view = inflater.inflate(R.layout.activity_view_tie_exp, container, false);
	    return view;
	}
	
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		expsList = (ListView)getActivity().findViewById(R.id.expsListView);
		detailInfoArray = new ArrayList<DetailInfo>();
	    for(int i=0; i<exps.size();i++){
	    	Expense exp = exps.get(i);
	    	System.out.println("Type ?>>>> "+exp.expType);
	    	System.out.println("Cost ?>>>> "+exp.expCost);
	    	if (!exp.dead){
		    	DetailInfo detailInfo = new DetailInfo(exp.expType , String.valueOf(exp.expDate), 
		    			String.valueOf(exp.expCost), String.valueOf(exp.expDescription), String.valueOf(exp.photoPath), "exp");
		    	detailInfoArray.add(detailInfo);
	    	}
	    }

		expsList.setEmptyView(getActivity().findViewById(R.id.noExpsListLayout));	
    	expAdapter = new TaskItemExpListAdapter(context, R.layout.child_row, detailInfoArray);
    	expsList.setAdapter(expAdapter);
    	
    	expsList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Expense exp = exps.get(position);
		    	Intent inExp = new Intent(context, AddExpensePopup.class);
		    	inExp.putExtra("expType", exp.expType);
		    	inExp.putExtra("expDate", exp.expDate);
		    	inExp.putExtra("expCost", String.valueOf(exp.expCost));
		    	inExp.putExtra("expDescription", exp.expDescription);
		    	inExp.putExtra("photoPath", exp.photoPath);
		    	System.out.println("Photo Path >>>>>>>>> "+exp.photoPath);
		    	inExp.putExtra("childPos", position);
				startActivityForResult(inExp, 0);
			}
    	});
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		int childPos;	
		switch (requestCode){
		case 0:
			if (resultCode ==  -1){
				childPos = data.getIntExtra("childPos", -1);
				String expType = data.getStringExtra("expType");
				String expDate = data.getStringExtra("expDate");
				double expCost = data.getDoubleExtra("expCost", -1);
				String expDescription = data.getStringExtra("expDescription");
				String photoPath = data.getStringExtra("photoPath");
				
				Expense exp = exps.get(childPos);
				exp.setExpType(expType);
				exp.setExpDate(expDate);
				exp.setExpCost(expCost);
				exp.setExpDescription(expDescription);
				exp.setExpPhotoPath(photoPath);
				
				DetailInfo detailInfo = detailInfoArray.get(childPos);
				detailInfo.updateDetailInfoExp(expType , expDate, String.valueOf(expCost), expDescription, photoPath, "exp");
				expAdapter.notifyDataSetChanged();
				System.out.println("++++++++++++++++++EXPENSE EDITED++++++++++++++++++++++");

			}else if (resultCode == -2){
				childPos = data.getIntExtra("childPos", -1);
				delExpense(childPos);
			}			
			break;

		default:
			break;
		}
	}
	
	public void delExpense(final int childPos){
    	// Create AlertDialog
		final Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Delete selected Expense");
		//builder.setMessage("Update the new job with the Quote details ?");
		builder.setCancelable(true);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				try{
					Expense exp = exps.get(childPos);					
					if(source == 1){ // NewQuote
						exps.remove(childPos);
					}else if (source == 2){ // NewInvoice
						exps.get(childPos).invoiceID = 0;
						exps.get(childPos).dead = true;
					}else if(source == 3){ // ViewJobInfo
						exps.remove(childPos);
						ExpensesConnector expCon = new ExpensesConnector(context);
						expCon.deleteExpense(exp.expID);
					}else{
						
					}		
					// Remove selected exp from the array list
					detailInfoArray.remove(childPos);				
					expAdapter.notifyDataSetChanged();
			    	System.out.println("Expense removed from array ");
			    	System.out.println("VieEx" + "pFrag - Delete: "+exps.size());
				} catch (Exception e) { 
					System.out.println("++++++ERROR+++++ "+e.getLocalizedMessage().toString());
				}
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}