package uk.co.beanssystems.adapter;

import java.util.ArrayList;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.database.ItemsConnector;
import uk.co.beanssystems.jobs.Item;
import uk.co.beanssystems.popups.AddItemPopup;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.SherlockFragment;

@SuppressLint("ValidFragment")
public class ViewItemFrag  extends SherlockFragment {
	private ListView itemsList;
	ArrayList<DetailInfo> detailInfoArray = null;
	static ArrayList<Item> items = new ArrayList<Item>();
	public static ArrayList<Item> delItems = new ArrayList<Item>();
	Context context;
	TaskItemExpListAdapter itemAdapter;
	int source = 0;
	
	public ViewItemFrag(ArrayList<Item> items, int source) {
		super();
		ViewItemFrag.items = items;
		this.source = source;
	}
	
	public ViewItemFrag() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		context = getActivity().getApplicationContext();
	}
	    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {	
		View view = inflater.inflate(R.layout.activity_view_tie_item, container, false);
		System.out.println("VIEW ITEM FRAG ?>>>> "+items.size());
	    return view;
	}
	
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		itemsList = (ListView)getActivity().findViewById(R.id.itemsListView);
		detailInfoArray = new ArrayList<DetailInfo>();
	    for(int i=0; i<items.size();i++){
	    	Item item = items.get(i);
	    	System.out.println("Name ?>>>> "+item.itemName);
	    	System.out.println("Quantity ?>>>> "+item.quantity);
	    	if (!item.dead){
		    	DetailInfo detailInfo = new DetailInfo(item.itemName , String.valueOf(item.quantity), 
		    			String.valueOf(item.cost), String.valueOf(item.quantity  * item.cost), "item");
		    	detailInfoArray.add(detailInfo);
	    	}
	    }

		itemsList.setEmptyView(getActivity().findViewById(R.id.noItemsListLayout));	
    	itemAdapter = new TaskItemExpListAdapter(context, R.layout.child_row, detailInfoArray);
    	itemsList.setAdapter(itemAdapter);
    	
    	itemsList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Item item = items.get(position);
		    	Intent inItem = new Intent(context, AddItemPopup.class);
		    	inItem.putExtra("itemName", item.itemName);
		    	inItem.putExtra("quantity", item.quantity);
		    	inItem.putExtra("cost", item.cost);
		    	inItem.putExtra("childPos", position);
				startActivityForResult(inItem, 0);
			}
    	});
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		int childPos;	
		switch (requestCode){
		case 0:
			if (resultCode ==  -1){
				childPos = data.getIntExtra("childPos", -1);
				double total = 0;
				String itemName = data.getStringExtra("itemName");
				String quantity = data.getStringExtra("quantity");
				String cost = data.getStringExtra("cost");
				
				Item item = items.get(childPos);
				item.setItemName(itemName);
				item.setItemQuantity(Integer.valueOf(quantity));
				item.setItemCost(Double.valueOf(cost));
				total = Integer.parseInt(quantity) * Double.parseDouble(cost);
				
				DetailInfo detailInfo = detailInfoArray.get(childPos);
				detailInfo.updateDetailInfo(itemName, quantity, cost, String.valueOf(total), "item");
				itemAdapter.notifyDataSetChanged();
				System.out.println("++++++++++++++++++QTASK EDITED++++++++++++++++++++++");
			}else if (resultCode == -2){
				childPos = data.getIntExtra("childPos", -1);
				delItem(childPos);
			}			
			break;
		default:
			break;
		}
	}
	
	public void delItem(final int childPos){
    	// Create AlertDialog
		final Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Delete selected Item");
		//builder.setMessage("Update the new job with the Quote details ?");
		builder.setCancelable(true);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				try{
			    	Item item = items.get(childPos);
			    	if(source == 1){ // NewQuote
			    		items.remove(childPos);
			    		delItems.add(item);
					}else if (source == 2){ // NewInvoice
						items.get(childPos).invoiceID = 0;
						items.get(childPos).dead = true;
					}else if(source == 3){ // ViewJobInfo
						items.remove(childPos);
						ItemsConnector itemCon = new ItemsConnector(context);
						itemCon.deleteItem(item.itemID);
					}else{
						
					}				
					// Remove selected item from the array list
					detailInfoArray.remove(childPos);
					itemAdapter.notifyDataSetChanged();
			    	System.out.println("Item removed from array ");
			    	System.out.println("VieItemFrag - Delete: "+items.size());
				} catch (Exception e) { 
					System.out.println("++++++ERROR+++++ "+e.getLocalizedMessage().toString());
				}
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}