package uk.co.beanssystems.adapter;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import uk.co.beanssystems.beansone.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TaskItemExpListAdapter extends ArrayAdapter<DetailInfo>  {
	Context context;
	int layoutResourceId;
	ArrayList<DetailInfo> details = null;
	NumberFormat nf;
	
	public TaskItemExpListAdapter (Context context, int layoutResourceId, ArrayList<DetailInfo> details){
		super(context, layoutResourceId, details);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.details = details;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
        DetailsListItemHolder holder = null;
        if(row == null){
	        row = View.inflate(context, layoutResourceId, null);	        
	        holder = new DetailsListItemHolder();
	        holder.nameTextView = (TextView)row.findViewById(R.id.childItem);
	        holder.totalTextView = (TextView)row.findViewById(R.id.childItem1);
	        holder.unitTextView = (TextView)row.findViewById(R.id.childItem2);
	        
	        row.setTag(holder);
	    }
        else{
        	holder = (DetailsListItemHolder)row.getTag();
	    }

        DetailInfo detailInfo = details.get(position);
        if(detailInfo.type.equals("item")){
        	holder.nameTextView.setText(detailInfo.name);
        	holder.unitTextView.setText("Quantity: "+detailInfo.param1);
        }else if(detailInfo.type.equals("task")){
        	holder.nameTextView.setText(detailInfo.name);
        	holder.unitTextView.setText(detailInfo.param1+" Hours");
        }else if(detailInfo.type.equals("exp")) {
        	holder.nameTextView.setText(detailInfo.name);
        	holder.unitTextView.setText("Desc: "+detailInfo.description);
        }
        double total = Double.valueOf(detailInfo.total);
        nf = NumberFormat.getCurrencyInstance(Locale.UK);
        holder.totalTextView.setText(String.valueOf(nf.format(total)));
        return row;
	}
	
	static class DetailsListItemHolder {
	       TextView nameTextView;
	       TextView totalTextView;
	       TextView unitTextView;
	   }
}
