package uk.co.beanssystems.adapter;

public class DetailInfo {
	public String total;
	public String param1;
	public String param2;
	public String name;
	public String detail;
	public String type;
	// For Expense
	public String expDate;
	public String description;
	public String photoPath;
	 
	public DetailInfo() {
		super();
	}
		
	public DetailInfo(String name, String param1, String param2, String total, String type) {
		super();
		this.name = name;
		this.param1 = param1;
		this.param2 = param2;
		this.total = total;
		this.type = type;
	}
	
	public DetailInfo(String name, String expDate, String total, String description, String photoPath, String type) {
		super();
		this.name = name;
		this.expDate = expDate;
		this.total = total;
		this.description = description;
		this.photoPath = photoPath;
		this.type = type;
	}
	
	public void updateDetailInfo(String name, String param1, String param2, String total, String type) {
		this.name = name;
		this.param1 = param1;
		this.param2 = param2;
		this.total = total;
		this.type = type;
	}
	
	public void updateDetailInfoExp(String name, String expDate, String total, String description, String photoPath, String type) {
		this.name = name;
		this.expDate = expDate;
		this.total = total;
		this.description = description;
		this.photoPath = photoPath;
		this.type = type;
		
	}
	 
	 public String getName() {
		 return name;
	 }
	 
	 public void setName(String name) {
		 this.name = name;
	 }
	 
	 public String getParam1() {
		 return param1;
	 }
	 
	 public void setParam1(String param1) {
		 this.param1 = param1;
	 }
	 
	 public String getParam2() {
		 return param2;
	 }
	 
	 public void setParam2(String param2) {
		 this.param2 = param2;
	 }
	 
	 public String getTotal() {
		 return total;
	 }
	 
	 public void setTotal(String total) {
		 this.total = total;
	 }

	 public String getDetail() {
		  return detail;
	 }
		 
	 public void setDetail(String detail) {
		 this.detail = detail;
	 }
}
