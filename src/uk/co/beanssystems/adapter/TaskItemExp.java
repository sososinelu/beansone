package uk.co.beanssystems.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import uk.co.beanssystems.beansone.R;
import uk.co.beanssystems.jobs.Expense;
import uk.co.beanssystems.jobs.Item;
import uk.co.beanssystems.jobs.Task;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class TaskItemExp extends SherlockFragmentActivity {
	TaskItemExpViewPagerAdapter vAdapter;
	ArrayList<Task> tasks;
	ArrayList<Task> delTasks;
	ArrayList<Item> items;
	ArrayList<Item> delItems;
	ArrayList<Expense> exps;
	
	int loadFrags = 0, source = 0;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_tie_view_pager);
		
		Intent i = getIntent();
		tasks = (ArrayList<Task>) i.getSerializableExtra("tasksArray");
		items = (ArrayList<Item>) i.getSerializableExtra("itemsArray");	
		int currentItem = i.getIntExtra("currentItem", 0);
		loadFrags = i.getIntExtra("loadFrags", 0);
		source = i.getIntExtra("source", 0);
		System.out.println("TaskItemExp - Tasks ?>>>> "+tasks.size());
		System.out.println("TaskItemExp - Items ?>>>> "+items.size());
		if(loadFrags == 2){
			vAdapter = new TaskItemExpViewPagerAdapter(getSupportFragmentManager(), tasks, items, 2, source);
		}else if(loadFrags == 3){
			exps = (ArrayList<Expense>) i.getSerializableExtra("expsArray");
			System.out.println("TaskItemExp - Exps ?>>>> "+exps.size());
			vAdapter = new TaskItemExpViewPagerAdapter(getSupportFragmentManager(), tasks, items, exps, 3, source);
		}
	    ViewPager myPager = (ViewPager) findViewById(R.id.viewPagerTie);
	    myPager.setAdapter(vAdapter);
	    myPager.setOffscreenPageLimit(3);
	    myPager.setCurrentItem(currentItem); 	
	    vAdapter.notifyDataSetChanged();
	}
	 
	public void onClick(View v){
		switch(v.getId()){
		case R.id.closeViewTaskButton: 
			callArrays();			
			finish();
			break;
		case R.id.closeViewItemButton:   	
			callArrays();
			finish();
			break;
		case R.id.closeViewExpButton:   	
			callArrays();
			finish();
			break;
		default:break;
		}
	}
	
	public void callArrays(){
		tasks = ViewTaskFrag.tasks;
		delTasks = ViewTaskFrag.delTasks; 
		items = ViewItemFrag.items;
		delItems = ViewItemFrag.delItems;
		Intent i = new Intent ();
		i.putExtra("tasksArray", tasks);
		i.putExtra("delTasksArray", delTasks);
		i.putExtra("itemsArray", items);
		i.putExtra("delItemsArray", delItems);
		if(loadFrags == 3){
			exps = ViewExpFrag.exps;
			i.putExtra("expsArray", exps);
		}
		setResult(RESULT_OK, i);
	}
	
}